// 导出页面为PDF格式
import html2Canvas from "html2canvas";
import JsPDF from "jspdf";
export default {
  async install(Vue, options) {
    Vue.prototype.getPdf = async function (title) {
      let nodeList = document.querySelectorAll("#pdfDom");
      // title文件名称
      nodeList = document.querySelectorAll("#pdfDom");
      let PDF = new JsPDF("", "pt", "a4");
      for (let i = 0; i <= nodeList.length; i++) {
        let canvas = await html2Canvas(nodeList[i], {
          allowTaint: true, //解决跨域
          useCORS: true, //是否尝试使用CORS从服务器加载图像  解决跨域
          allowTaint: true, // 解决跨域
          dpi: 600, //解决生产图片模糊
          scale: 6, //清晰度--放大倍数
        });
        let contentWidth = canvas.width; //内容宽度
        let contentHeight = canvas.height; //内容高度
        let imgWidth = 595.28;
        let imgHeight = (592.28 / contentWidth) * contentHeight;
        let pageData = canvas.toDataURL("image/jpeg", 1.0); //生成图片 base64
        if (!imgWidth || !imgHeight || imgWidth == 0 || imgHeight == 0) {
          // 数据异常，无内容的结束本次循环，跳到下次循环
          continue;
        } else {
          // 页面添加图片
          PDF.addImage(pageData, "JPEG", 0, 0, imgWidth, imgHeight);
        }
        //是否最后一页
        if (i == nodeList.length - 1) {
          PDF.save(title + ".pdf"); // 是最后一页，输出pdf
          return;
        } else {
          PDF.addPage(); // 不是最后一页，添加下一个页面
        }
      }
    };
  },
};
