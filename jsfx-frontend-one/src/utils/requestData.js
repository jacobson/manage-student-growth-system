import axios from 'axios'
import store from '@/store'
import router from '@/router'

// 创建一个自定的axios方法(比原axios多了个基地址)
// axios函数请求的url地址前面会被拼接基地址, 然后axios请求baseURL+url后台完整地址
export const baseURL ='http://1.15.237.156:8080'
const myAxios = axios.create({
  baseURL
})

export default myAxios