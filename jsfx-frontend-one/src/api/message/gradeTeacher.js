import request from '@/utils/request'

//回显教师
export function getTeacher({userId}){
    return request({
        url: '/business/teacherManage/echoTeacherManageInfo',
        method: 'get',
        params: userId
    })
}

//选择已填写与未填写评语人集
export function getCommentsStatus(status){
    return request({
        url: '/business/comments/FillOutStatus?status=' + status,
        method: 'get',
        // params:{status}
    })
}

//单个学生的评语信息
export function getOneComments(studentId){
    return request({
        url: '/business/comments/chooseStudent',
        method: 'get',
        params:{studentId}
    })
}

//修改单个学生的评语信息
export function postOneStudentComments(data){
    return request({
        url: '/business/comments/confirm/remark',
        method: 'post',
        data:data
    })
}

//获取班级信息
export function getClassManage(gradeId){
    return request({
        url: '/classManage',
        method: 'get',
        params:{gradeId}
    })
}