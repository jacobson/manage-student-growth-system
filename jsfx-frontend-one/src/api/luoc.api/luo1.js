import request from '@/utils/request'
import store from '@/store'
//获取班级成员的的gradId
export function getGradeId({}) {//如果携带参数在这里传参数
    return request({
      url: '/business/gradeTeacher/getGrade',
      method: 'get',
      data() {
        return {
            
        }
      },
    })
    
  }
  
  //班级管理页面获取学生信息
   export function getstudentImation({gradeId}) {//如果携带参数在这里传参数
      return request({
        url:'business/students/list/'+gradeId,
        method: 'get',
      })
      
    }