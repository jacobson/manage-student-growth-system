import request from '@/utils/request'

// 获取成长报告页面信息
export const getRport = ({ stuId }) => {
  return request({
    url: '/business/growth/' + stuId,
    method: 'GET',
    data() {

    }
  })
}

// 获取班级成员的gradId
export function getGradeId({ }) {
  return request({
    url: '/business/gradeTeacher/getGrade',
    method: 'get',
    data() {
      return {
      }
    },
  })
}

// 班级管理页面获取学生信息
export function getstudentImation({ gradeId }) {
  return request({
    url: 'business/students/list/' + gradeId,
    method: 'get',
  })
}

// 基本信息页面获取学生信息
export function getclassId(stuId) {
  return request({
    url: '/business/students/' + stuId,
    method: 'get',
  })
}

// 获取用户信息
export function myInfo() {
  return request({
    url: '/business/gradeTeacher/myInfo',
    method: 'get',
  })
}
