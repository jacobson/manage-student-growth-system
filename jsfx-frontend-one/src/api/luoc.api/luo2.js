import request from '@/utils/request'
import store from '@/store'

//获取学生成长报告已定稿页面
export const makereport1 = ({growthId,studentId}) => {
    return request({
      url:'/business/growthItem/getItems',
      method:'GET',
      params:{
        growthId,
        studentId
      }
    })
  }