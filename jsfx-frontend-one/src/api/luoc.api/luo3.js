import request from '@/utils/request'
import store from '@/store'

//获取学生成长报告制作中页面
export const makereport = ({growthId,studentId}) => {
    return request({
      url:'/business/growthItem/getItems',
      method:'GET',
      params:{
        growthId,
        studentId
      }
    })
  }