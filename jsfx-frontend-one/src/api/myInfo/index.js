import request from '@/utils/request'
import store from '@/store'
// 获取个人页面信息
export const getMyManage = ({}) => {
  return request({
    url: '/business/gradeTeacher/myInfo',
    method: 'get',
  })
}

//获取个人信息页面
export const getMyManage1 = ({}) => {
  return request({
    url: '/business/gradeTeacher/myInfo',
    method: 'get',
  })
}

//获取成长报告页面信息
export const getRport = ({stuId}) => {
  return request({
    url: '/business/growth/'+stuId,
    method: 'GET',
    data(){
      
    }
  })
}

//获取有几个孩子
export function childrenmessageAPI(){
  return request({
    url:'/business/busParentStudent/getMyChild',
    method:'GET'
  })
}

//获取孩子信息
export const getChild = ({}) => {
  return request({
    url: '/business/busParentStudent/getMyChild',
    method: 'GET',
    data(){
      
    }
  })
}

//获取第一个孩子的信息
export const getChild1 = ({}) => {
  return request({
    url: '/business/busParentStudent/getMyChild',
    method: 'GET',
    data(){
    }
  })
}

//获取第二个孩子的信息
export const getChild2 = ({}) => {
  return request({
    url: '/business/busParentStudent/getMyChild',
    method: 'GET',
    data(){
    }
  })
}

//获取学生成长报告制作中页面
export const makereport = ({growthId,studentId}) => {
  return request({
    url:'/business/growthItem/getItems',
    method:'GET',
    params:{
      growthId,
      studentId
    }
  })
}

//获取学生成长报告已定稿页面
export const makereport1 = ({growthId,studentId}) => {
  return request({
    url:'/business/growthItem/getItems',
    method:'GET',
    params:{
      growthId,
      studentId
    }
  })
}

//获取修改学生头像
export const huanavatar = ({studnetId}) => {
  return request({
    url:'/business/students/avatar/'+studnetId,
    method:'POST',
  })
}