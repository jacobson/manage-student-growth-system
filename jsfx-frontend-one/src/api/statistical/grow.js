import request from '@/utils/request'

// 获取所带班级及在班级中的岗位
export const getGrade = () => {
  return request({
    url: '/business/gradeTeacher/getGrade',
    method: 'get',
  })
}

 // 获取班级信息
export function getstudentManage(gradeId) {
  return request({
    url: '/business/growth/pace',
    method: 'get',
    params: { gradeId }
  })
}

// 各模块完成的人的名字
export function verifyModule(gradeId) {
  return request({
    url: '/business/growthItem/verifyModule',
    method: 'get',
    params: { gradeId }
  })
}

// 各模块未完成的人的名字
export function notVerifyModule(gradeId) {
  return request({
    url: '/business/growthItem/notVerifyModule',
    method: 'get',
    params: { gradeId }
  })
}