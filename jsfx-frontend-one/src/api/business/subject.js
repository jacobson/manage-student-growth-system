import request from '@/utils/request'

// 查询学科列表
export function listSubject(query) {
  return request({
    url: '/business/subject/list',
    method: 'get',
    params: query
  })
}

// 查询学科详细
export function getSubject(subjectId) {
  return request({
    url: '/business/subject/' + subjectId,
    method: 'get'
  })
}

// 新增学科
export function addSubject(data) {
  return request({
    url: '/business/subject',
    method: 'post',
    data: data
  })
}

// 修改学科
export function updateSubject(data) {
  return request({
    url: '/business/subject',
    method: 'put',
    data: data
  })
}

// 删除学科
export function delSubject(subjectId) {
  return request({
    url: '/business/subject/' + subjectId,
    method: 'delete'
  })
}