import request from '@/utils/request'

// 查询学生管理列表
export function listStudents(query) {
  return request({
    url: '/business/students/list',
    method: 'get',
    params: query
  })
}

// 查询学生管理详细
export function getStudents(stuId) {
  return request({
    url: '/business/students/' + stuId,
    method: 'get'
  })
}

// 新增学生管理
export function addStudents(data) {
  return request({
    url: '/business/students',
    method: 'post',
    data: data
  })
}

// 修改学生管理
export function updateStudents(data) {
  return request({
    url: '/business/students',
    method: 'put',
    data: data
  })
}

// 删除学生管理
export function delStudents(stuId) {
  return request({
    url: '/business/students/' + stuId,
    method: 'delete'
  })
}
