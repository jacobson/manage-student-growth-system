import request from '@/utils/request'

//我的孩子获取孩子信息
export function childrenmessageAPI(){
    return request({
      url:'/business/busParentStudent/getMyChild',
      method:'GET'
    })
  }
//获取学生成长报告列表
  export function studentlistAPI({stuId}){
    return request({
      url:"/business/growth/"+ stuId,
      method:'GET',
    })
  }
  //获取学生成长报告
  export function studentbsogsoAPI({growthId,studentId}){
    return request({
      url:'business/growthItem/getItems',
      method:'GET',
      params:{
        growthId,
        studentId
      }
    })

  }
  //查询可选的任课教师
  export function inviteteacherAPI({studentId}){
    return request({
      url:'/business/comments/getTeacherOptions/' + studentId ,
      method:"GET",
      params:{
        studentId
      }
    })
  }
  //从图库中选择图片填写成长报告
  export function timeclassphotoAPI({studentId}){
    return request({
      url:'/business/gradegallery/getGradeGallery',
      method:'GET',
      data:{
        studentId
      }
    })
  }
//获取图库中的图片
  export function photoimageAPI({galleryId}){
    return request({
      url:'/business/gallery/'+galleryId,
      method:'GET',
    })
  }
//从图库中选择图片填写成长报告
  export function optionimgreport({studentId,growthKey,remark,path}){
    return request({
      url:'/business/growthItem/writeGrowthByGallery?studentId='+studentId+'&growthKey='+growthKey+'&remark='+remark+'&path='+path,
      method:'POST',
    })

  }
//查看班级图库及历史图库
export function getGallery(query) {
  return request({
      url: '/business/gradegallery',
      method: 'get',
      params: query
  })
}
//图库重命名
export function renameGallery(data) {
  return request({
      url: '/business/gradegallery',
      method: 'put',
      data: data
  })
}
//删除图库并清空图库图片
export function deleteGallery(galleryId) {
  return request({
      url: '/business/gradegallery/' + galleryId,
      method: 'delete'
  })
}
//删除指定图片
export function deleteImage(imgName){
  return request({
      url: '/business/gallery/deleteImage',
      method: 'delete',
      params: {imgName}
  })
}
//获取学生图库图片
export function allphotosAPI({studentId}){
  return request({
    url:'/business/gallery/getPhotos',
    method:'GET',
    params:{
      studentId
    }
  })
}
//邀请任课老师
export function inviteAPI({studentId,userId}){
  return request({
    url:'/business/comments/invite/',
    method:'POST',
    data:{
      studentId,
      userId
    }
  })

}