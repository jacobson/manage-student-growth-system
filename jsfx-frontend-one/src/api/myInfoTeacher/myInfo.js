import request from '@/utils/request'

// 获取用户信息
export function myInfo() {
  return request({
    url: '/business/gradeTeacher/myInfo',
    method: 'get',
  })
}

