import request from '@/utils/request'

// 获取担任班主任的班级
export function LeadGrade() {
  return request({
      url: '/business/gradeTeacher/getLeadGrade',
      method: 'get',
  })
}

// 获取用户信息
export function myInfo() {
  return request({
    url: '/business/gradeTeacher/myInfo',
    method: 'get',
  })
}

// 获取单科所有教师
export function SubjectTeacher(query) {
  return request({
    url: '/business/teacherManage/getSubjectTeacher',
    method: 'get',
    params: query 
  })
}

// 修改班级的任课教师
export function changeGradeTeacher(data) {
  return request({
    url: '/business/teacherManage/changeGradeTeacher',
    method: 'put',
    data:data
  })
}

// 获取班级教师配置
// /business/gradeTeacher/getGradeTeacher/{gradeId}
export function GradeTeacher(gradeId) {
  return request({
    url: '/business/gradeTeacher/getGradeTeacher/' + gradeId,
    method: 'get',
    // params:{gradeId}
  })
}


