import request from '@/utils/request'

// 获取用户印章
export function listStudents() {
  return request({
    url: '/business/seal/seals',
    method: 'get',
  })
}

// 上传用户印章
export function getStudents(imgFile) {
  return request({
    url: '/business/seal/upload' + imgFile,
    method: 'post'
  })
}
