import request from '@/utils/request'

// 获取担任班主任的班级
export function LeadGrade() {
    return request({
        url: '/business/gradeTeacher/getLeadGrade',
        method: 'get',
    })
}

// 查看班级图库及历史图库
export function getgradegallery(query) {
    return request({
        url: '/business/gradegallery',
        method: 'get',
        params: query
    })
}

// 根据galleryId 获取图库中图片
export function getGallery(galleryId) {
    return request({
        url: '/business/gallery/' + galleryId,
        method: 'get',
    })
}

// 图库重命名
export function renameGallery(data) {
    return request({
        url: '/business/gradegallery',
        method: 'put',
        data: data
    })
}

// 新增班级图库
export function addGallery(data) {
    return request({
        url: '/business/gradegallery',
        method: 'post',
        data: data
    })
}

// 删除图库并清空图库图片 1
export function deleteGallery(galleryId) {
    return request({
        url: '/business/gradegallery/' + galleryId,
        method: 'DELETE'
    })
}

// 获取学生图库里的所有图片
export function getGalleryByid(galleryId) {
    return request({
        url: '/business/gallery/' + galleryId,
        method: 'get'
    })
}

// 删除指定图片 1
export function deleteImage(imgName) {
    return request({
        url: '/business/gallery/deleteImage',
        method: 'delete',
        params: { imgName }
    })
}
