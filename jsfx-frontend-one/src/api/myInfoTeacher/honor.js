import request from '@/utils/request'

// 获取担任班主任的班级
export function LeadGrade() {
  return request({
    url: '/business/gradeTeacher/getLeadGrade',
    method: 'get',
  })
}

// 全部荣誉的人员名单
export function getPerson(query) {
  return request({
    url: '/business/honor/honorNameList?gradeId=' + query,
    method: 'get',
    // params: query
  })
}

// 调整单个荣誉接口
export function SingleHonor(gradeId, honorModule) {
  return request({
    url: '/business/honor/adjust',
    method: 'get',
    params: {
      gradeId, honorModule
    }
  })
}

// 修改荣誉人
export function updateHonorPerson({ candidate, notCandidate, module }) {
  return request({
    url: '/business/honor/confirm/adjust',
    method: 'post',
    data: {
      candidate, notCandidate, module
    }
  })
}

