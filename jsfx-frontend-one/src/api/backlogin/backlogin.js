import request from '@/utils/request'

//退出登录
export function backlogin(){
    return request({
        url:'/logout',
        method:'POST'
    })
}
