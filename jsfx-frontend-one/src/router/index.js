import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import commonMenu from '@/layout/commonMenu.vue'
/**
 * Note: 路由配置项
 *
 * hidden: true                     // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true                 // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                  // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                  // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                  // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect             // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'               // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * query: '{"id": 1, "name": "ry"}' // 访问路由的默认传递参数
 * roles: ['admin', 'common']       // 访问路由的角色权限
 * permissions: ['a:a:a', 'b:b:b']  // 访问路由的菜单权限
 * meta : {
    noCache: true                   // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'                  // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'                // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false               // 如果设置为false，则不会在breadcrumb面包屑中显示
    activeMenu: '/system/user'      // 当路由设置了该属性，则会高亮相对应的侧边栏。
  }
 */

// 公共路由
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login'),
    hidden: true
  },
  {
    path: '/register',
    component: () => import('@/views/register'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/index'),
        name: 'Index',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: '/',
    component: commonMenu,
    redirect: 'report1',
    children: [
      {
        path: '/report1',
        name: 'report1',
        component: () => import('@/views/grow/report/report'),
        meta: {
          title: '成长报告',
          activeMenu: '/report1'
        },
      },
      {
        path: '/report1/make',
        name: 'make',
        component: () => import('@/views/grow/make/make'),
        hidden: true,
        meta: {
          title: '成长报告详情',
          activeMenu: '/report1'
        }
      },
      {
        path: '/report1/define',
        name: 'define',
        component: () => import('@/views/grow/define/define'),
        hidden: true
      },
      {
        path: '/doGrowth',
        name: 'doGrowth',
        component: () => import('@/views/makes/first'),
        hidden: true,
        meta: {
          title: '制作报告',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/myself',
        name: 'myself',
        component: () => import('@/views/makes/myself'),
        hidden: true,
        meta: {
          title: '这就是我',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/myfirend',
        name: 'myfirend',
        component: () => import('@/views/makes/myfirend'),
        hidden: true,
        meta: {
          title: '我和我的伙伴',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/myword',
        name: 'myword',
        component: () => import('@/views/makes/myword'),
        hidden: true,
        meta: {
          title: '我写的字',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/myhomework',
        name: 'myhomework',
        component: () => import('@/views/makes/myhomework'),
        hidden: true,
        meta: {
          title: '我的作业',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/myscore',
        name: 'myscore',
        component: () => import('@/views/makes/myscore'),
        hidden: true,
        meta: {
          title: '我的试卷',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/mytheme',
        name: 'mytheme',
        component: () => import('@/views/makes/mytheme'),
        hidden: true,
        meta: {
          title: '我的作文',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/myactive',
        name: 'myactive',
        component: () => import('@/views/makes/myactive'),
        hidden: true,
        meta: {
          title: '我参加的活动',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/report',
        name: 'report',
        component: () => import('@/views/makes/report'),
        hidden: true,
        meta: {
          title: '学业素养报告单',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/teacherword',
        name: 'teacherword',
        component: () => import('@/views/makes/teacherword'),
        hidden: true,
        meta: {
          title: '老师寄语',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/friendsword',
        name: 'friendsword',
        component: () => import('@/views/makes/friendsword'),
        hidden: true,
        meta: {
          title: '小伙伴的话',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/myhonour',
        name: 'myhonour',
        component: () => import('@/views/makes/myhonour'),
        hidden: true,
        meta: {
          title: '我的荣誉',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/semestersum',
        name: 'semestersum',
        component: () => import('@/views/makes/semestersum'),
        hidden: true,
        meta: {
          title: '我的学期小结',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/mygrow',
        name: 'mygrow',
        component: () => import('@/views/makes/mygrow'),
        hidden: true,
        meta: {
          title: '我的十能成长目录',
          activeMenu: '/report1'
        }
      },
      {
        path: '/doGrowth/personality',
        name: 'personality',
        component: () => import('@/views/makes/personality'),
        hidden: true,
        meta: {
          title: '个性',
          activeMenu: '/report1'
        }
      },
      {
        path: '/gallery',
        name: 'gallery',
        component: () => import('@/views/photo/photos'),
        hidden: true,
        meta: {
          title: '图片库',
          activeMenu: '/gallery'
        }
      },
      {
        path: '/myInfo',
        name: 'myInfo',
        component: () => import('@/views/myInfo/myInfo'),
        hidden: true,
        meta: {
          title: '个人信息',
          activeMenu: '/myInfo'
        }
      },
      {
        path: '/statistical',
        name: 'statistical',
        component: () => import('@/views/statistical/statistical'),
        hidden: true,
        meta: { title: '统计', activeMenu: '/statistical' }
      },
      {
        path: '/details',
        name: 'details',
        component: () => import('@/views/statistical/details'),
        hidden: true,
        meta: { title: '统计名单', activeMenu: '/statistical' }
      },
      {               
        path: '/grow',
        name: "grow",
        component: () => import('@/views/statistical/grow'),
        hidden: true,
        meta: { title: '个人中心', activeMenu: '/statistical' }
      },
      {
        path: '/goclass',
        name: 'goclass',
        component: () => import('@/views/classluo/class/class'),
        meta: { title: '班级', activeMenu: '/goclass' },
      },
      {
        path: '/remarks',
        name: 'remarks',
        component: () => import('@/views/message/remarks'),
        hidden: true,
        meta: {
          title: '寄语',
          activeMenu: '/remarks'
        }
      },
      {
        path: '/myInfoTeacher',
        name: 'myInfoTeacher',
        component: () => import('@/views/myInfoTeacher/myInfo'),
        hidden: true,
        meta: {
          title: '我的',
          activeMenu: '/myInfoTeacher'
        }
      },
      {
        path: '/teachClasses',
        name: 'teachClasses',
        component: () => import('@/views/classroomTeacher/teachClasses/myInfo'),
        hidden: true,
        meta: {
          activeMenu: '/teachClasses'
        }
      },
      {
        path: '/makeluo',
        name: 'makeluo',
        component: () => import('@/views/classluo/make/make'),
        hidden: true,
        meta: {
          activeMenu: '/goclass'
        }
      },
      {
        path: '/defineluo',
        name: 'defineluo',
        component: () => import('@/views/classluo/define/define'),
        hidden: true,
        meta: {
          activeMenu: '/goclass'
        }
      },
      {
        path: '/writeRemarks',
        name: 'writeRemarks',
        component: () => import('@/views/message/writeRemarks'),
        hidden: true,
        meta: {
          title: '填写寄语',
          activeMenu: '/remarks'
        }
      },
      {
        path: '/submitRemarks',
        name: 'submitRemarks',
        component: () => import('@/views/message/submitRemarks'),
        hidden: true,
        meta: {
          title: '修改寄语',
          activeMenu: '/remarks'
        }
      },
      {
        path: '/reportluo',
        name: 'reportluo',
        component: () => import('@/views/classluo/report/report'),
        meta: { title: '成长报告', activeMenu: '/goclass' }
      },
      {
        path: '/goclassrenke',
        name: 'goclassrenke',
        component: () => import('@/views/classroomTeacher/classrenke/class/class'),
        hidden: true,
        meta: { title: '任课老师班级', activeMenu: '/goclassrenke' }
      },
    ]
  },

  {
    path: '/user',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'profile',
        component: () => import('@/views/system/user/profile/index'),
        name: 'Profile',
        meta: { title: '个人中心', icon: 'user' }
      }
    ]
  },
  {
    path: '/first',
    component: () => import('../views/makes/first'),
    hidden: true
  },
  {
    path: '/remarksC',
    name: 'remarks',
    component: () => import('../views/classroomTeacher/message/remarks'),
    hidden: true
  },
  {
    path: '/writeRemarksC',
    name: 'writeRemarksC',
    component: () => import('../views/classroomTeacher/message/writeRemarks'),
    hidden: true
  },
  {
    path: '/submitRemarksC',
    name: 'submitRemarksC',
    component: () => import('../views/classroomTeacher/message/submitRemarks'),
    hidden: true
  },
  {                 // 班级
    path: '/reportrenke',
    name: 'reportrenke',
    component: () => import('../views/classroomTeacher/classrenke/report/report'),
    hidden: true
  },
  {
    path: '/definerenke',
    name: 'definerenke',
    component: () => import('../views/classroomTeacher/classrenke/define/define'),
    hidden: true
  },
  {
    path: '/makerenke',
    name: 'makerenke',
    component: () => import('../views/classroomTeacher/classrenke/make/make'),
    hidden: true
  },
]

// 动态路由，基于用户权限动态去加载
export const dynamicRoutes = [
  {
    path: '/system/user-auth',
    component: Layout,
    hidden: true,
    permissions: ['system:user:edit'],
    children: [
      {
        path: 'role/:userId(\\d+)',
        component: () => import('@/views/system/user/authRole'),
        name: 'AuthRole',
        meta: { title: '分配角色', activeMenu: '/system/user' }
      }
    ]
  },
  {
    path: '/system/role-auth',
    component: Layout,
    hidden: true,
    permissions: ['system:role:edit'],
    children: [
      {
        path: 'user/:roleId(\\d+)',
        component: () => import('@/views/system/role/authUser'),
        name: 'AuthUser',
        meta: { title: '分配用户', activeMenu: '/system/role' }
      }
    ]
  },
  {
    path: '/system/dict-data',
    component: Layout,
    hidden: true,
    permissions: ['system:dict:list'],
    children: [
      {
        path: 'index/:dictId(\\d+)',
        component: () => import('@/views/system/dict/data'),
        name: 'Data',
        meta: { title: '字典数据', activeMenu: '/system/dict' }
      }
    ]
  },
  {
    path: '/monitor/job-log',
    component: Layout,
    hidden: true,
    permissions: ['monitor:job:list'],
    children: [
      {
        path: 'index',
        component: () => import('@/views/monitor/job/log'),
        name: 'JobLog',
        meta: { title: '调度日志', activeMenu: '/monitor/job' }
      }
    ]
  },
  {
    path: '/tool/gen-edit',
    component: Layout,
    hidden: true,
    permissions: ['tool:gen:edit'],
    children: [
      {
        path: 'index/:tableId(\\d+)',
        component: () => import('@/views/tool/gen/editTable'),
        name: 'GenEdit',
        meta: { title: '修改生成配置', activeMenu: '/tool/gen' }
      }
    ]
  }
]

// 防止连续点击多次路由报错
let routerPush = Router.prototype.push;
Router.prototype.push = function push (location) {
  return routerPush.call(this, location).catch(err => err)
}

export default new Router({
  mode: 'history', // 去掉url中的#
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})
