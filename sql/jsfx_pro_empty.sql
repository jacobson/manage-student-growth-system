/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : jsfx_pro

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 06/12/2022 15:27:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bus_common_seal
-- ----------------------------
DROP TABLE IF EXISTS `bus_common_seal`;
CREATE TABLE `bus_common_seal`  (
  `seal_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `seal_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `seal_illustrate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `seal_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`seal_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 88 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_common_seal
-- ----------------------------
INSERT INTO `bus_common_seal` VALUES ('成长报告专用章', '/profile/img/seal/2022/12/05/b71595156ee0b2ac51b11c83a9d2516_20221205095030A038.png', '用于成长报告', 4);
INSERT INTO `bus_common_seal` VALUES ('卓越少年专用章', '/profile/img/seal/2022/12/05/f992f530a51c47e75033dd98f996d74_20221205100859A061.png', '用于荣誉', 5);
INSERT INTO `bus_common_seal` VALUES ('励志少年专用章', '/profile/img/seal/2022/12/05/563a311b2d08cf6f1b0c6fffff78ebd_20221205110046A093.png', '用于荣誉', 77);
INSERT INTO `bus_common_seal` VALUES ('模范学生专用章', '/profile/img/seal/2022/12/05/ad50ff07311674531d7bc70c2e39286_20221205100732A057.png', '用于荣誉', 84);
INSERT INTO `bus_common_seal` VALUES ('梦想领袖专用章', '/profile/img/seal/2022/12/05/4d4925ca5af03b09d1dd1936ce003ad_20221205100748A058.png', '用于荣誉', 85);
INSERT INTO `bus_common_seal` VALUES ('文明学生专用章', '/profile/img/seal/2022/12/05/466eb886cb1785a44337c3ae89533f3_20221205100825A059.png', '用于荣誉', 86);

-- ----------------------------
-- Table structure for bus_gallery_image
-- ----------------------------
DROP TABLE IF EXISTS `bus_gallery_image`;
CREATE TABLE `bus_gallery_image`  (
  `gallery_id` bigint(20) NOT NULL COMMENT '图库id',
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片url'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '图库表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_gallery_image
-- ----------------------------

-- ----------------------------
-- Table structure for bus_grade
-- ----------------------------
DROP TABLE IF EXISTS `bus_grade`;
CREATE TABLE `bus_grade`  (
  `grade_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '班级id',
  `semester_id` bigint(20) NULL DEFAULT NULL COMMENT '学期id',
  `year_level_id` bigint(20) NULL DEFAULT NULL COMMENT '年级id',
  `campus_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '校区名称',
  `grade_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '班级名称',
  `grade_number` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '班级人数',
  `grade_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '班级状态',
  `grade_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '班级备注',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '班主任id',
  PRIMARY KEY (`grade_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 255509 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '班级表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_grade
-- ----------------------------

-- ----------------------------
-- Table structure for bus_grade_gallery
-- ----------------------------
DROP TABLE IF EXISTS `bus_grade_gallery`;
CREATE TABLE `bus_grade_gallery`  (
  `grade_id` bigint(20) NOT NULL COMMENT '班级id',
  `gallery_id` bigint(20) NOT NULL COMMENT '图库id',
  `gallery_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图库名',
  `semester_id` bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '班级图库表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_grade_gallery
-- ----------------------------

-- ----------------------------
-- Table structure for bus_grade_student
-- ----------------------------
DROP TABLE IF EXISTS `bus_grade_student`;
CREATE TABLE `bus_grade_student`  (
  `grade_id` bigint(20) NOT NULL COMMENT '班级id',
  `student_id` bigint(20) NOT NULL COMMENT '学生id'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '班级学生表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_grade_student
-- ----------------------------

-- ----------------------------
-- Table structure for bus_grade_student_history
-- ----------------------------
DROP TABLE IF EXISTS `bus_grade_student_history`;
CREATE TABLE `bus_grade_student_history`  (
  `grade_id` bigint(20) NOT NULL COMMENT '班级id',
  `student_id` bigint(20) NOT NULL COMMENT '学生id',
  `semester_id` bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '班级学生表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_grade_student_history
-- ----------------------------

-- ----------------------------
-- Table structure for bus_grade_teacher
-- ----------------------------
DROP TABLE IF EXISTS `bus_grade_teacher`;
CREATE TABLE `bus_grade_teacher`  (
  `grade_id` bigint(20) NOT NULL COMMENT '班级id',
  `post_id` bigint(20) NOT NULL COMMENT '岗位id',
  `user_id` bigint(20) NOT NULL COMMENT '教师id',
  `subject_id` bigint(20) NULL DEFAULT NULL COMMENT '任课科目id',
  PRIMARY KEY (`grade_id`, `post_id`) USING BTREE,
  INDEX `teacher_teach`(`user_id`) USING BTREE,
  CONSTRAINT `teacher_teach` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '班级教师表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_grade_teacher
-- ----------------------------

-- ----------------------------
-- Table structure for bus_grade_teacher_history
-- ----------------------------
DROP TABLE IF EXISTS `bus_grade_teacher_history`;
CREATE TABLE `bus_grade_teacher_history`  (
  `grade_id` bigint(20) NOT NULL COMMENT '班级id',
  `user_id` bigint(20) NOT NULL COMMENT '教师id',
  `semester_id` bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '班主任与班级历史表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_grade_teacher_history
-- ----------------------------

-- ----------------------------
-- Table structure for bus_music
-- ----------------------------
DROP TABLE IF EXISTS `bus_music`;
CREATE TABLE `bus_music`  (
  `music_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '音乐id',
  `music_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音乐名称',
  `music_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音乐地址',
  PRIMARY KEY (`music_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '推荐音乐表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_music
-- ----------------------------

-- ----------------------------
-- Table structure for bus_semester
-- ----------------------------
DROP TABLE IF EXISTS `bus_semester`;
CREATE TABLE `bus_semester`  (
  `semester_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '学年学期id',
  `semester_year` bigint(20) NULL DEFAULT NULL COMMENT '学年',
  `semester_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学期名称',
  `semester_now` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为当前学期',
  `create_time` datetime(6) NULL DEFAULT NULL COMMENT '创建的时间',
  `has_report` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否已开启成长报告',
  PRIMARY KEY (`semester_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学年学期表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_semester
-- ----------------------------

-- ----------------------------
-- Table structure for bus_student_growth
-- ----------------------------
DROP TABLE IF EXISTS `bus_student_growth`;
CREATE TABLE `bus_student_growth`  (
  `growth_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '成长报告id',
  `student_id` bigint(20) NULL DEFAULT NULL COMMENT '学生id',
  `semester_id` bigint(20) NULL DEFAULT NULL COMMENT '学年学期id',
  `report_start_time` datetime NULL DEFAULT NULL COMMENT '报告开始时间',
  `report_start_end` datetime NULL DEFAULT NULL COMMENT '报告结束时间',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（制作中、已定稿、未开始）',
  `growth_result` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '成长报告最终结果',
  PRIMARY KEY (`growth_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2533 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生成长报告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_student_growth
-- ----------------------------

-- ----------------------------
-- Table structure for bus_student_growth_item
-- ----------------------------
DROP TABLE IF EXISTS `bus_student_growth_item`;
CREATE TABLE `bus_student_growth_item`  (
  `growth_item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '成长报告项id',
  `growth_id` bigint(20) NULL DEFAULT NULL COMMENT '成长报告id',
  `growth_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '成长报告环节项',
  `growth_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '成长报告环节项值',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态（制作中、已定稿、未开始）',
  PRIMARY KEY (`growth_item_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38609 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生成长报告项表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_student_growth_item
-- ----------------------------

-- ----------------------------
-- Table structure for bus_student_honor
-- ----------------------------
DROP TABLE IF EXISTS `bus_student_honor`;
CREATE TABLE `bus_student_honor`  (
  `student_id` bigint(20) NOT NULL COMMENT '学生id',
  `honor1` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否获取卓越少年荣誉',
  `honor2` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否获取励志少年',
  `honor3` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否获取模范学生',
  `honor4` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否获取梦想领袖',
  `honor5` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否获取文明学生',
  PRIMARY KEY (`student_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_student_honor
-- ----------------------------

-- ----------------------------
-- Table structure for bus_student_parents
-- ----------------------------
DROP TABLE IF EXISTS `bus_student_parents`;
CREATE TABLE `bus_student_parents`  (
  `student_id` bigint(20) NOT NULL COMMENT '学生id',
  `user_id` bigint(20) NOT NULL COMMENT '家长id',
  `relation` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学生与家长关系'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生家长表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_student_parents
-- ----------------------------

-- ----------------------------
-- Table structure for bus_students
-- ----------------------------
DROP TABLE IF EXISTS `bus_students`;
CREATE TABLE `bus_students`  (
  `stu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '学生id',
  `stu_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学生姓名',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `id_card` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '身份证',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`stu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20220221 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_students
-- ----------------------------

-- ----------------------------
-- Table structure for bus_subject
-- ----------------------------
DROP TABLE IF EXISTS `bus_subject`;
CREATE TABLE `bus_subject`  (
  `subject_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '学科id',
  `subject_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学科名称',
  `subject_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`subject_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学科表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_subject
-- ----------------------------
INSERT INTO `bus_subject` VALUES (49, '数学', '');
INSERT INTO `bus_subject` VALUES (50, '语文', '');
INSERT INTO `bus_subject` VALUES (51, '英语', '');
INSERT INTO `bus_subject` VALUES (52, '科学', '');
INSERT INTO `bus_subject` VALUES (53, '体育', '');
INSERT INTO `bus_subject` VALUES (54, '美术', '');
INSERT INTO `bus_subject` VALUES (55, '音乐', '');
INSERT INTO `bus_subject` VALUES (56, '信息与技术', NULL);
INSERT INTO `bus_subject` VALUES (69, '道德与法治', NULL);

-- ----------------------------
-- Table structure for bus_user_comments
-- ----------------------------
DROP TABLE IF EXISTS `bus_user_comments`;
CREATE TABLE `bus_user_comments`  (
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `student_id` bigint(20) NULL DEFAULT NULL COMMENT '学生id',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '评语状态',
  `remark` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否受邀',
  `comments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '评语内容'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户评语表（教师、班主任）' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_user_comments
-- ----------------------------

-- ----------------------------
-- Table structure for bus_user_gallery
-- ----------------------------
DROP TABLE IF EXISTS `bus_user_gallery`;
CREATE TABLE `bus_user_gallery`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `gallery_id` bigint(20) NOT NULL COMMENT '图库id',
  `gallery_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图库名',
  PRIMARY KEY (`user_id`, `gallery_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户图库表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_user_gallery
-- ----------------------------

-- ----------------------------
-- Table structure for bus_user_seal
-- ----------------------------
DROP TABLE IF EXISTS `bus_user_seal`;
CREATE TABLE `bus_user_seal`  (
  `seal_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '印章id',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `seal_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '印章地址',
  PRIMARY KEY (`seal_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户印章表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_user_seal
-- ----------------------------

-- ----------------------------
-- Table structure for bus_year_level
-- ----------------------------
DROP TABLE IF EXISTS `bus_year_level`;
CREATE TABLE `bus_year_level`  (
  `year_level_id` bigint(20) NOT NULL COMMENT '年级id',
  `year_level_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '年级名称',
  PRIMARY KEY (`year_level_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '年级表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_year_level
-- ----------------------------
INSERT INTO `bus_year_level` VALUES (1, '一年级第一学期');
INSERT INTO `bus_year_level` VALUES (2, '一年级第二学期');
INSERT INTO `bus_year_level` VALUES (3, '二年级第一学期');
INSERT INTO `bus_year_level` VALUES (4, '二年级第二学期');
INSERT INTO `bus_year_level` VALUES (5, '三年级第一学期');
INSERT INTO `bus_year_level` VALUES (6, '三年级第二学期');
INSERT INTO `bus_year_level` VALUES (7, '四年级第一学期');
INSERT INTO `bus_year_level` VALUES (8, '四年级第二学期');
INSERT INTO `bus_year_level` VALUES (9, '五年级第一学期');
INSERT INTO `bus_year_level` VALUES (10, '五年级第二学期');
INSERT INTO `bus_year_level` VALUES (11, '六年级第一学期');
INSERT INTO `bus_year_level` VALUES (12, '六年级第二学期');

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'bus_subject', '学科表', NULL, NULL, 'BusSubject', 'crud', 'com.ruoyi.business', 'business', 'subject', '学科', 'ruoyi', '0', '/', '{}', 'admin', '2022-10-18 13:44:11', '', '2022-10-18 13:45:11', NULL);
INSERT INTO `gen_table` VALUES (3, 'sys_job_log', '定时任务调度日志表', NULL, NULL, 'SysJobLog', 'crud', 'com.ruoyi.system', 'system', 'log', '定时任务调度日志', 'ruoyi', '0', '/', NULL, 'admin', '2022-10-18 16:00:18', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (6, 'bus_students', '学生表', NULL, NULL, 'BusStudents', 'crud', 'com.ruoyi.business', 'business', 'students', '学生管理', 'ruoyi', '0', '/', '{\"parentMenuId\":\"2014\"}', 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49', NULL);
INSERT INTO `gen_table` VALUES (7, 'bus_grade_teacher', '班级教师表', NULL, NULL, 'BusGradeTeacher', 'crud', 'com.ruoyi.business', 'BusGradeTeacher', 'gradeTeacher', '班级教师', 'ruoyi', '0', '/', '{}', 'admin', '2022-10-22 18:00:44', '', '2022-10-22 18:03:09', NULL);
INSERT INTO `gen_table` VALUES (8, 'bus_grade', '班级表', NULL, NULL, 'BusGrade', 'crud', 'com.ruoyi.business', 'business', 'grade', '班级', 'ruoyi', '0', '/', '{}', 'admin', '2022-10-22 18:17:04', '', '2022-10-22 18:17:34', NULL);
INSERT INTO `gen_table` VALUES (9, 'bus_student_growth', '学生成长报告表', NULL, NULL, 'BusStudentGrowth', 'crud', 'com.ruoyi.business', 'business', 'growth', '学生成长报告', 'ruoyi', '0', 'E:\\java_code\\jsfx\\ruoyi-admin', '{}', 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24', NULL);
INSERT INTO `gen_table` VALUES (10, 'bus_student_growth_item', '学生成长报告项表', NULL, NULL, 'BusStudentGrowthItem', 'crud', 'com.ruoyi.business', 'business', 'growthItem', '学生成长报告项', 'ruoyi', '0', 'C:\\Users\\wangm\\Desktop\\自动生成代码', '{}', 'admin', '2022-10-22 19:22:37', '', '2022-10-23 09:44:59', NULL);
INSERT INTO `gen_table` VALUES (11, 'bus_music', '推荐音乐表', NULL, NULL, 'BusMusic', 'crud', 'com.ruoyi.business', 'business', 'music', '推荐音乐', 'ruoyi', '0', '/', '{}', 'admin', '2022-10-23 10:17:58', '', '2022-10-23 10:18:23', NULL);
INSERT INTO `gen_table` VALUES (12, 'bus_grade_gallery', '班级图库表', NULL, NULL, 'BusGradeGallery', 'crud', 'com.ruoyi.business', 'business', 'gradeGallery', '班级图库', 'ruoyi', '0', '/', '{}', 'admin', '2022-10-25 10:06:22', '', '2022-10-25 10:06:51', NULL);
INSERT INTO `gen_table` VALUES (13, 'bus_user_seal', '用户印章表', NULL, NULL, 'BusUserSeal', 'crud', 'com.ruoyi.system', 'system', 'seal', '用户印章', 'ruoyi', '0', '/', NULL, 'admin', '2022-12-02 09:04:23', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (14, 'classmanagetest', '', NULL, NULL, 'Classmanagetest', 'crud', 'com.ruoyi.system', 'system', 'classmanagetest', NULL, 'ruoyi', '0', '/', NULL, 'admin', '2022-12-02 16:32:27', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (15, 'studentscoretest', '', NULL, NULL, 'Studentscoretest', 'crud', 'com.ruoyi.system', 'system', 'studentscoretest', NULL, 'ruoyi', '0', '/', NULL, 'admin', '2022-12-03 17:14:46', '', NULL, NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 83 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'subject_id', '学科id', 'bigint(20)', 'Long', 'subjectId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-18 13:44:11', '', '2022-10-18 13:45:11');
INSERT INTO `gen_table_column` VALUES (2, '1', 'subject_name', '学科名称', 'varchar(100)', 'String', 'subjectName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-10-18 13:44:11', '', '2022-10-18 13:45:11');
INSERT INTO `gen_table_column` VALUES (5, '3', 'job_log_id', '任务日志ID', 'bigint(20)', 'Long', 'jobLogId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-18 16:00:18', '', NULL);
INSERT INTO `gen_table_column` VALUES (6, '3', 'job_name', '任务名称', 'varchar(64)', 'String', 'jobName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-10-18 16:00:18', '', NULL);
INSERT INTO `gen_table_column` VALUES (7, '3', 'job_group', '任务组名', 'varchar(64)', 'String', 'jobGroup', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-18 16:00:18', '', NULL);
INSERT INTO `gen_table_column` VALUES (8, '3', 'invoke_target', '调用目标字符串', 'varchar(500)', 'String', 'invokeTarget', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'textarea', '', 4, 'admin', '2022-10-18 16:00:18', '', NULL);
INSERT INTO `gen_table_column` VALUES (9, '3', 'job_message', '日志信息', 'varchar(500)', 'String', 'jobMessage', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 5, 'admin', '2022-10-18 16:00:18', '', NULL);
INSERT INTO `gen_table_column` VALUES (10, '3', 'status', '执行状态（0正常 1失败）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 6, 'admin', '2022-10-18 16:00:18', '', NULL);
INSERT INTO `gen_table_column` VALUES (11, '3', 'exception_info', '异常信息', 'varchar(2000)', 'String', 'exceptionInfo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 7, 'admin', '2022-10-18 16:00:18', '', NULL);
INSERT INTO `gen_table_column` VALUES (12, '3', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-10-18 16:00:18', '', NULL);
INSERT INTO `gen_table_column` VALUES (23, '6', 'stu_id', '学生id', 'bigint(20)', 'Long', 'stuId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49');
INSERT INTO `gen_table_column` VALUES (24, '6', 'stu_no', '学号', 'varchar(100)', 'String', 'stuNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49');
INSERT INTO `gen_table_column` VALUES (25, '6', 'stu_name', '学生姓名', 'varchar(100)', 'String', 'stuName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49');
INSERT INTO `gen_table_column` VALUES (26, '6', 'avatar', '头像地址', 'varchar(100)', 'String', 'avatar', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49');
INSERT INTO `gen_table_column` VALUES (27, '6', 'sex', '用户性别（0男 1女 2未知）', 'char(1)', 'String', 'sex', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 5, 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49');
INSERT INTO `gen_table_column` VALUES (28, '6', 'id_card', '身份证', 'varchar(100)', 'String', 'idCard', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49');
INSERT INTO `gen_table_column` VALUES (29, '6', 'status', '帐号状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 7, 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49');
INSERT INTO `gen_table_column` VALUES (30, '6', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49');
INSERT INTO `gen_table_column` VALUES (31, '6', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49');
INSERT INTO `gen_table_column` VALUES (32, '6', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49');
INSERT INTO `gen_table_column` VALUES (33, '6', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49');
INSERT INTO `gen_table_column` VALUES (34, '6', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', '1', '1', '1', '1', NULL, 'EQ', 'textarea', '', 12, 'admin', '2022-10-21 16:10:07', '', '2022-10-21 16:21:49');
INSERT INTO `gen_table_column` VALUES (35, '7', 'grade_id', '班级id', 'bigint(20)', 'Long', 'gradeId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 1, 'admin', '2022-10-22 18:00:44', '', '2022-10-22 18:03:09');
INSERT INTO `gen_table_column` VALUES (36, '7', 'post_id', '岗位id', 'bigint(20)', 'Long', 'postId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-10-22 18:00:44', '', '2022-10-22 18:03:09');
INSERT INTO `gen_table_column` VALUES (37, '7', 'user_id', '教师id', 'bigint(20)', 'Long', 'userId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-22 18:00:44', '', '2022-10-22 18:03:09');
INSERT INTO `gen_table_column` VALUES (38, '7', 'subject_id', '任课科目id', 'bigint(20)', 'Long', 'subjectId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-10-22 18:00:44', '', '2022-10-22 18:03:09');
INSERT INTO `gen_table_column` VALUES (39, '8', 'grade_id', '班级id', 'bigint(20)', 'Long', 'gradeId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-22 18:17:04', '', '2022-10-22 18:17:34');
INSERT INTO `gen_table_column` VALUES (40, '8', 'semester_id', '学期id', 'bigint(20)', 'Long', 'semesterId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-10-22 18:17:04', '', '2022-10-22 18:17:34');
INSERT INTO `gen_table_column` VALUES (41, '8', 'year_level_id', '年级id', 'bigint(20)', 'Long', 'yearLevelId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-22 18:17:04', '', '2022-10-22 18:17:34');
INSERT INTO `gen_table_column` VALUES (42, '8', 'grade_no', '班级编号', 'varchar(100)', 'String', 'gradeNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-10-22 18:17:04', '', '2022-10-22 18:17:34');
INSERT INTO `gen_table_column` VALUES (43, '8', 'campus_name', '校区名称', 'varchar(100)', 'String', 'campusName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2022-10-22 18:17:04', '', '2022-10-22 18:17:34');
INSERT INTO `gen_table_column` VALUES (44, '8', 'grade_name', '班级名称', 'varchar(255)', 'String', 'gradeName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 6, 'admin', '2022-10-22 18:17:04', '', '2022-10-22 18:17:34');
INSERT INTO `gen_table_column` VALUES (45, '8', 'grade_number', '班级人数', 'int(11)', 'Long', 'gradeNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-10-22 18:17:04', '', '2022-10-22 18:17:34');
INSERT INTO `gen_table_column` VALUES (46, '8', 'grade_status', '班级状态', 'char(1)', 'String', 'gradeStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 8, 'admin', '2022-10-22 18:17:04', '', '2022-10-22 18:17:34');
INSERT INTO `gen_table_column` VALUES (47, '8', 'grade_remark', '班级备注', 'varchar(255)', 'String', 'gradeRemark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-10-22 18:17:04', '', '2022-10-22 18:17:34');
INSERT INTO `gen_table_column` VALUES (48, '9', 'growth_id', '成长报告id', 'bigint(20)', 'Long', 'growthId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24');
INSERT INTO `gen_table_column` VALUES (49, '9', 'student_id', '学生id', 'bigint(20)', 'Long', 'studentId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24');
INSERT INTO `gen_table_column` VALUES (50, '9', 'semester_id', '学年学期id', 'bigint(20)', 'Long', 'semesterId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24');
INSERT INTO `gen_table_column` VALUES (51, '9', 'growth_seal_url', '盖章logo', 'varchar(255)', 'String', 'growthSealUrl', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24');
INSERT INTO `gen_table_column` VALUES (52, '9', 'growth_seal_time', '盖章时间', 'datetime', 'Date', 'growthSealTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 5, 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24');
INSERT INTO `gen_table_column` VALUES (53, '9', 'report_start_time', '报告开始时间', 'datetime', 'Date', 'reportStartTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 6, 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24');
INSERT INTO `gen_table_column` VALUES (54, '9', 'report_start_end', '报告结束时间', 'datetime', 'Date', 'reportStartEnd', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 7, 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24');
INSERT INTO `gen_table_column` VALUES (55, '9', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24');
INSERT INTO `gen_table_column` VALUES (56, '9', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24');
INSERT INTO `gen_table_column` VALUES (57, '9', 'status', '状态（制作中、已定稿、未开始）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 10, 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24');
INSERT INTO `gen_table_column` VALUES (58, '9', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24');
INSERT INTO `gen_table_column` VALUES (59, '9', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 12, 'admin', '2022-10-22 19:22:23', '', '2022-10-22 19:33:24');
INSERT INTO `gen_table_column` VALUES (60, '10', 'growth_item_id', '成长报告项id', 'bigint(20)', 'Long', 'growthItemId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-22 19:22:37', '', '2022-10-23 09:44:59');
INSERT INTO `gen_table_column` VALUES (61, '10', 'growth_id', '成长报告id', 'bigint(20)', 'Long', 'growthId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-10-22 19:22:37', '', '2022-10-23 09:44:59');
INSERT INTO `gen_table_column` VALUES (62, '10', 'growth_key', '成长报告环节项', 'varchar(100)', 'String', 'growthKey', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-22 19:22:37', '', '2022-10-23 09:44:59');
INSERT INTO `gen_table_column` VALUES (63, '10', 'growth_value', '成长报告环节项值', 'text', 'String', 'growthValue', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 4, 'admin', '2022-10-22 19:22:37', '', '2022-10-23 09:44:59');
INSERT INTO `gen_table_column` VALUES (64, '10', 'status', '状态（制作中、已定稿、未开始）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 5, 'admin', '2022-10-22 19:22:37', '', '2022-10-23 09:44:59');
INSERT INTO `gen_table_column` VALUES (65, '11', 'music_id', '音乐id', 'bigint(20)', 'Long', 'musicId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-23 10:17:58', '', '2022-10-23 10:18:23');
INSERT INTO `gen_table_column` VALUES (66, '11', 'music_name', '音乐名称', 'varchar(100)', 'String', 'musicName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-10-23 10:17:58', '', '2022-10-23 10:18:23');
INSERT INTO `gen_table_column` VALUES (67, '11', 'music_url', '音乐地址', 'varchar(100)', 'String', 'musicUrl', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-23 10:17:58', '', '2022-10-23 10:18:23');
INSERT INTO `gen_table_column` VALUES (68, '12', 'grade_id', '班级id', 'bigint(20)', 'Long', 'gradeId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 1, 'admin', '2022-10-25 10:06:23', '', '2022-10-25 10:06:51');
INSERT INTO `gen_table_column` VALUES (69, '12', 'gallery_id', '图库id', 'bigint(20)', 'Long', 'galleryId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-10-25 10:06:23', '', '2022-10-25 10:06:51');
INSERT INTO `gen_table_column` VALUES (70, '13', 'seal_id', '印章id', 'bigint(20)', 'Long', 'sealId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-12-02 09:04:23', '', NULL);
INSERT INTO `gen_table_column` VALUES (71, '13', 'user_id', '用户id', 'bigint(20)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-12-02 09:04:23', '', NULL);
INSERT INTO `gen_table_column` VALUES (72, '13', 'seal_url', '印章地址', 'varchar(255)', 'String', 'sealUrl', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-12-02 09:04:23', '', NULL);
INSERT INTO `gen_table_column` VALUES (73, '14', 'grade_id', '班级编号', 'varchar(255)', 'String', 'gradeId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 1, 'admin', '2022-12-02 16:32:27', '', NULL);
INSERT INTO `gen_table_column` VALUES (74, '14', 'head_teacher_name', '班主任', 'varchar(255)', 'String', 'headTeacherName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-12-02 16:32:27', '', NULL);
INSERT INTO `gen_table_column` VALUES (75, '14', 'count_students', '班级人数', 'int(255)', 'Long', 'countStudents', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-12-02 16:32:27', '', NULL);
INSERT INTO `gen_table_column` VALUES (76, '15', 'student_id', '学号', 'bigint(20)', 'Long', 'studentId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 1, 'admin', '2022-12-03 17:14:46', '', NULL);
INSERT INTO `gen_table_column` VALUES (77, '15', 'student_name', '姓名', 'varchar(255)', 'String', 'studentName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-12-03 17:14:46', '', NULL);
INSERT INTO `gen_table_column` VALUES (78, '15', 'gradeId', '班级', 'varchar(255)', 'String', 'gradeid', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-12-03 17:14:46', '', NULL);
INSERT INTO `gen_table_column` VALUES (79, '15', 'height', '身高(cm)\r\n', 'varchar(255)', 'String', 'height', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-12-03 17:14:46', '', NULL);
INSERT INTO `gen_table_column` VALUES (80, '15', 'weight', '体重(kg)', 'varchar(255)', 'String', 'weight', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-12-03 17:14:46', '', NULL);
INSERT INTO `gen_table_column` VALUES (81, '15', 'left_eye', '视力(左)', 'varchar(255)', 'String', 'leftEye', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-12-03 17:14:46', '', NULL);
INSERT INTO `gen_table_column` VALUES (82, '15', 'right_eye', '视力(右)', 'varchar(255)', 'String', 'rightEye', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-12-03 17:14:46', '', NULL);

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '日历信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '已触发的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '任务详细信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '暂停的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '调度器状态表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '触发器详细信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-10-18 11:10:06', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-10-18 11:10:06', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-10-18 11:10:06', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaEnabled', 'true', 'N', 'admin', '2022-10-18 11:10:06', 'admin', '2022-10-21 20:17:26', '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2022-10-18 11:10:06', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 202 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, 0, '', '学校', 0, NULL, NULL, NULL, '0', '0', '', NULL, '', NULL);
INSERT INTO `sys_dept` VALUES (200, 1, ',1', '教师', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-10-21 20:45:29', '', NULL);
INSERT INTO `sys_dept` VALUES (201, 0, '', '家长', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-10-21 20:45:41', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2022-10-18 11:10:06', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-10-18 11:10:06', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-10-18 11:10:06', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-10-18 11:10:06', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6306 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2081 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2022-10-18 11:10:04', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, '', 1, 0, 'M', '1', '1', '', 'monitor', 'admin', '2022-10-18 11:10:04', 'admin', '2022-10-21 11:40:58', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2022-10-18 11:10:04', '', NULL, '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 0, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-10-18 11:10:04', 'admin', '2022-10-21 16:06:03', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-10-18 11:10:04', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-10-18 11:10:04', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2022-10-18 11:10:04', 'admin', '2022-10-21 22:24:58', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2022-10-18 11:10:04', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-10-18 11:10:04', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-10-18 11:10:04', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-10-18 11:10:04', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2022-10-18 11:10:04', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-10-18 11:10:04', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-10-18 11:10:04', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2022-10-18 11:10:04', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-10-18 11:10:04', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2022-10-18 11:10:04', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '缓存列表', 2, 6, 'cacheList', 'monitor/cache/list', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis-list', 'admin', '2022-10-18 11:10:04', '', NULL, '缓存列表菜单');
INSERT INTO `sys_menu` VALUES (115, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-10-18 11:10:04', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (116, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-10-18 11:10:04', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (117, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-10-18 11:10:04', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2022-10-18 11:10:04', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2022-10-18 11:10:04', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '日志导出', 500, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '账户解锁', 501, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:unlock', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 6, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 116, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 116, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 116, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 116, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 116, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 116, 6, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-10-18 11:10:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2012, '班级管理', 0, 4, 'classmanagetest/index', 'business/classmanagetest/index', NULL, 1, 0, 'C', '0', '0', '', 'peoples', 'admin', '2022-10-21 14:23:03', 'admin', '2022-12-04 14:18:16', '');
INSERT INTO `sys_menu` VALUES (2013, '教师管理', 0, 5, 'teacher', 'business/teacher/index', NULL, 1, 0, 'C', '0', '0', '', 'peoples', 'admin', '2022-10-21 14:24:29', 'admin', '2022-12-03 16:07:10', '');
INSERT INTO `sys_menu` VALUES (2014, '学生管理', 0, 6, 'student', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'peoples', 'admin', '2022-10-21 14:25:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2015, '学生成绩管理', 0, 7, 'achievementmanage', 'business/studentscoretest/index', NULL, 1, 0, 'C', '0', '0', '', 'excel', 'admin', '2022-10-21 14:26:02', 'admin', '2022-12-06 08:21:47', '');
INSERT INTO `sys_menu` VALUES (2016, '基础设置管理', 0, 7, 'basics', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'list', 'admin', '2022-10-21 14:28:55', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2017, '通用印章管理', 2016, 0, 'basicestting', 'business/seal/seal', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-10-21 14:29:38', 'admin', '2022-12-05 09:35:32', '');
INSERT INTO `sys_menu` VALUES (2018, '学科管理', 2016, 0, 'subject', 'business/subject/index', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-10-21 14:30:04', 'admin', '2022-12-02 09:05:39', '');
INSERT INTO `sys_menu` VALUES (2019, '学年学期管理', 2016, 0, 'semester', 'business/semester/index', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-10-21 14:31:14', 'admin', '2022-12-03 16:06:02', '');
INSERT INTO `sys_menu` VALUES (2021, '个人中心', 0, 8, 'my', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'people', 'admin', '2022-10-21 14:32:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2022, '我的印章', 2021, 0, 'myseal', NULL, NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-10-21 14:33:18', 'admin', '2022-10-21 15:55:46', '');
INSERT INTO `sys_menu` VALUES (2023, '修改基本信息', 2021, 0, 'changeInfo', NULL, NULL, 1, 0, 'M', '0', '0', NULL, '#', 'admin', '2022-10-21 14:33:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2024, '修改密码', 2021, 0, 'password', NULL, NULL, 1, 0, 'M', '0', '0', NULL, '#', 'admin', '2022-10-21 14:34:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2050, '学生管理', 2014, 1, 'stumange', 'business/stumange/index', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-10-22 16:10:21', 'admin', '2022-12-06 14:28:52', '学生管理菜单');
INSERT INTO `sys_menu` VALUES (2051, '学生管理查询', 2050, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'business:students:query', '#', 'admin', '2022-10-22 16:10:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2052, '学生管理新增', 2050, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'business:students:add', '#', 'admin', '2022-10-22 16:10:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2053, '学生管理修改', 2050, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'business:students:edit', '#', 'admin', '2022-10-22 16:10:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2054, '学生管理删除', 2050, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'business:students:remove', '#', 'admin', '2022-10-22 16:10:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2055, '学生管理导出', 2050, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'business:students:export', '#', 'admin', '2022-10-22 16:10:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2056, '成长报告', 0, 2, 'report1', 'grow/report/report', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-10-26 10:47:34', 'admin', '2022-11-27 09:51:48', '');
INSERT INTO `sys_menu` VALUES (2057, '制作报告', 0, 2, 'doGrowth', 'makes/first', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-10-26 10:51:23', 'admin', '2022-11-14 14:48:44', '');
INSERT INTO `sys_menu` VALUES (2058, '图片库', 0, 2, 'gallery', 'photo/photos', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-10-26 10:52:10', 'admin', '2022-11-10 18:33:04', '');
INSERT INTO `sys_menu` VALUES (2060, '班级', 0, 2, 'class', 'classluo/class/class', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-10-26 10:57:07', 'admin', '2022-11-22 16:13:36', '');
INSERT INTO `sys_menu` VALUES (2061, '统计', 0, 2, 'statistical', 'statistical/statistical', NULL, 1, 1, 'C', '0', '0', '', '#', 'admin', '2022-10-26 10:58:08', 'admin', '2022-11-14 14:48:15', '');
INSERT INTO `sys_menu` VALUES (2062, '寄语', 0, 2, 'message', 'message/remarks', NULL, 1, 1, 'C', '0', '0', '', 'build', 'admin', '2022-10-26 10:58:47', 'admin', '2022-11-14 14:58:12', '');
INSERT INTO `sys_menu` VALUES (2063, '我的', 0, 2, 'myInfo', 'myInfo/myInfo', NULL, 1, 1, 'C', '0', '0', '', '#', 'admin', '2022-10-27 09:15:32', 'admin', '2022-11-25 15:44:20', '');
INSERT INTO `sys_menu` VALUES (2071, '成长报告-详细-制作中', 2056, 0, 'make1', 'business/make1/make1', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-11-06 16:00:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2072, '成长报告-详细-已定稿1', 2056, 1, 'define1', 'business/define1/define1', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-11-06 16:03:25', 'admin', '2022-11-07 19:22:23', '');
INSERT INTO `sys_menu` VALUES (2074, '成长报告模式一', 0, 1, 'growth', 'grow/report/report', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-11-08 08:47:13', 'admin', '2022-11-14 19:29:07', '');
INSERT INTO `sys_menu` VALUES (2077, '推荐音乐管理', 2016, 3, 'music', 'business/music/index', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-11-10 16:26:08', 'admin', '2022-11-22 23:35:27', '');
INSERT INTO `sys_menu` VALUES (2078, '班主任我的', 0, 2, 'myInfoTeacher', 'myInfoTeacher/myInfo', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-11-11 14:00:00', 'admin', '2022-11-25 08:48:05', '');
INSERT INTO `sys_menu` VALUES (2079, '成长报告制作中（班主任）', 0, 1, 'growth2', 'grow/make/make', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-11-14 12:45:59', 'admin', '2022-11-14 14:52:54', '');
INSERT INTO `sys_menu` VALUES (2080, '成长报告已完稿(班主任端)', 0, 1, 'growth4', 'grow/define/define', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-11-14 12:56:13', 'admin', '2022-11-14 14:53:28', '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2022-10-18 11:10:06', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2022-10-18 11:10:07', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 290 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (101, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"myInfoTeacher/myInfo\",\"createTime\":\"2022-11-11 14:00:00\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2078,\"menuName\":\"班主任我的\",\"menuType\":\"C\",\"orderNum\":2,\"params\":{},\"parentId\":0,\"path\":\"myInfoTeacher\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-11-25 08:48:05');
INSERT INTO `sys_oper_log` VALUES (102, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/25/blob_20221125151737A001.jpeg\",\"code\":200}', 0, NULL, '2022-11-25 15:17:37');
INSERT INTO `sys_oper_log` VALUES (103, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.updatePwd()', 'PUT', 1, '13405557267', NULL, '/system/user/profile/updatePwd', '127.0.0.1', '内网IP', '\"123456\" \"123456z\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-11-25 15:18:13');
INSERT INTO `sys_oper_log` VALUES (104, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13474133603', NULL, '/system/user/profile/avatar', '211.140.143.38', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/25/p9Wenhj3gb7Ka82d2a0195e455c7c2ca3bc5034cc8d5_20221125152647A008.jpg\",\"code\":200}', 0, NULL, '2022-11-25 15:26:47');
INSERT INTO `sys_oper_log` VALUES (105, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"myInfo/myInfo\",\"createTime\":\"2022-10-27 09:15:32\",\"icon\":\"#\",\"isCache\":\"1\",\"isFrame\":\"1\",\"menuId\":2063,\"menuName\":\"我的\",\"menuType\":\"C\",\"orderNum\":2,\"params\":{},\"parentId\":0,\"path\":\"myInfo\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-11-25 15:43:26');
INSERT INTO `sys_oper_log` VALUES (106, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"myInfo/myInfo\",\"createTime\":\"2022-10-27 09:15:32\",\"icon\":\"#\",\"isCache\":\"1\",\"isFrame\":\"1\",\"menuId\":2063,\"menuName\":\"我的\",\"menuType\":\"C\",\"orderNum\":2,\"params\":{},\"parentId\":0,\"path\":\"myInfo\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-11-25 15:44:06');
INSERT INTO `sys_oper_log` VALUES (107, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"myInfo/myInfo\",\"createTime\":\"2022-10-27 09:15:32\",\"icon\":\"#\",\"isCache\":\"1\",\"isFrame\":\"1\",\"menuId\":2063,\"menuName\":\"我的\",\"menuType\":\"C\",\"orderNum\":2,\"params\":{},\"parentId\":0,\"path\":\"myInfo\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-11-25 15:44:20');
INSERT INTO `sys_oper_log` VALUES (108, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13565499464', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/25/blob_20221125154657A020.jpeg\",\"code\":200}', 0, NULL, '2022-11-25 15:46:57');
INSERT INTO `sys_oper_log` VALUES (109, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13421459974', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/25/blob_20221125160641A003.jpeg\",\"code\":200}', 0, NULL, '2022-11-25 16:06:41');
INSERT INTO `sys_oper_log` VALUES (110, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13421459974', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/25/blob_20221125160901A004.jpeg\",\"code\":200}', 0, NULL, '2022-11-25 16:09:01');
INSERT INTO `sys_oper_log` VALUES (111, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13529527881', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/25/blob_20221125162701A008.jpeg\",\"code\":200}', 0, NULL, '2022-11-25 16:27:01');
INSERT INTO `sys_oper_log` VALUES (112, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '211.140.143.35', 'XX XX', '', NULL, 1, '文件[0bfRhGO13d9baabde6c69a372330a5855a97d6b3aa5e.webp]后缀[webp]不正确，请上传[bmp, gif, jpg, jpeg, png]格式', '2022-11-26 23:03:50');
INSERT INTO `sys_oper_log` VALUES (113, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/27/blob_20221127092947A019.jpeg\",\"code\":200}', 0, NULL, '2022-11-27 09:29:47');
INSERT INTO `sys_oper_log` VALUES (114, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/27/blob_20221127093230A020.jpeg\",\"code\":200}', 0, NULL, '2022-11-27 09:32:30');
INSERT INTO `sys_oper_log` VALUES (115, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13489643150', NULL, '/system/user/profile/avatar', '122.227.41.218', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/27/VX35QCNliMKAea6fea53e29a61c8196448e6301d9f4c_20221127093532A021.jpg\",\"code\":200}', 0, NULL, '2022-11-27 09:35:32');
INSERT INTO `sys_oper_log` VALUES (116, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/27/blob_20221127094714A022.jpeg\",\"code\":200}', 0, NULL, '2022-11-27 09:47:14');
INSERT INTO `sys_oper_log` VALUES (117, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/27/blob_20221127094831A023.jpeg\",\"code\":200}', 0, NULL, '2022-11-27 09:48:31');
INSERT INTO `sys_oper_log` VALUES (118, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"grow/report/report\",\"createTime\":\"2022-10-26 10:47:34\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2056,\"menuName\":\"成长报告\",\"menuType\":\"C\",\"orderNum\":2,\"params\":{},\"parentId\":0,\"path\":\"report1\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-11-27 09:51:48');
INSERT INTO `sys_oper_log` VALUES (119, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2022-10-21 14:14:22\",\"dataScope\":\"5\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[2056,2057,2058,2063],\"params\":{},\"roleId\":102,\"roleKey\":\"parent\",\"roleName\":\"家长\",\"roleSort\":\"4\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-11-27 09:54:27');
INSERT INTO `sys_oper_log` VALUES (120, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2022-10-21 14:14:22\",\"dataScope\":\"5\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[2056,2057,2058,2063,2021,2022,2023,2024],\"params\":{},\"roleId\":102,\"roleKey\":\"parent\",\"roleName\":\"家长\",\"roleSort\":\"4\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-11-27 09:59:24');
INSERT INTO `sys_oper_log` VALUES (121, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13489643150', NULL, '/system/user/profile/avatar', '122.227.41.218', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/27/Ege3cfmWKjYS732d58447e6df2762f85c7a646603bef_20221127101150A025.jpg\",\"code\":200}', 0, NULL, '2022-11-27 10:11:50');
INSERT INTO `sys_oper_log` VALUES (122, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13507015169', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/27/blob_20221127105721A026.jpeg\",\"code\":200}', 0, NULL, '2022-11-27 10:57:21');
INSERT INTO `sys_oper_log` VALUES (123, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13571342733', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/27/blob_20221127105853A027.jpeg\",\"code\":200}', 0, NULL, '2022-11-27 10:58:53');
INSERT INTO `sys_oper_log` VALUES (124, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13509649413', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/27/blob_20221127114526A031.jpeg\",\"code\":200}', 0, NULL, '2022-11-27 11:45:26');
INSERT INTO `sys_oper_log` VALUES (125, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13384543950', NULL, '/system/user/profile/avatar', '211.140.143.38', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/29/blob_20221129143040A155.jpeg\",\"code\":200}', 0, NULL, '2022-11-29 14:30:40');
INSERT INTO `sys_oper_log` VALUES (126, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13384543950', NULL, '/system/user/profile/avatar', '211.140.143.38', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/11/29/blob_20221129144006A156.jpeg\",\"code\":200}', 0, NULL, '2022-11-29 14:40:06');
INSERT INTO `sys_oper_log` VALUES (127, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"teachermange/index\",\"createTime\":\"2022-10-21 14:24:29\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2013,\"menuName\":\"教师管理\",\"menuType\":\"C\",\"orderNum\":5,\"params\":{},\"parentId\":0,\"path\":\"teacher\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-01 21:02:36');
INSERT INTO `sys_oper_log` VALUES (128, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13544869690', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/01/blob_20221201211622A024.jpeg\",\"code\":200}', 0, NULL, '2022-12-01 21:16:22');
INSERT INTO `sys_oper_log` VALUES (129, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13544869690', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/01/blob_20221201233658A003.jpeg\",\"code\":200}', 0, NULL, '2022-12-01 23:36:58');
INSERT INTO `sys_oper_log` VALUES (130, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 08:07:20');
INSERT INTO `sys_oper_log` VALUES (131, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"bus_user_seal\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:04:23');
INSERT INTO `sys_oper_log` VALUES (132, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-12-02 09:04:28');
INSERT INTO `sys_oper_log` VALUES (133, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/subject/index\",\"createTime\":\"2022-10-21 14:30:04\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2018,\"menuName\":\"学科管理\",\"menuType\":\"C\",\"orderNum\":0,\"params\":{},\"parentId\":2016,\"path\":\"subject\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:05:39');
INSERT INTO `sys_oper_log` VALUES (134, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"test/index\",\"createTime\":\"2022-10-21 14:24:29\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2013,\"menuName\":\"教师管理\",\"menuType\":\"C\",\"orderNum\":5,\"params\":{},\"parentId\":0,\"path\":\"teacher\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:11:03');
INSERT INTO `sys_oper_log` VALUES (135, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/semester/index\",\"createTime\":\"2022-10-21 14:31:14\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2019,\"menuName\":\"学年学期管理\",\"menuType\":\"C\",\"orderNum\":0,\"params\":{},\"parentId\":2016,\"path\":\"semester\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:14:37');
INSERT INTO `sys_oper_log` VALUES (136, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"test/index\",\"createTime\":\"2022-10-21 14:24:29\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2013,\"menuName\":\"教师管理\",\"menuType\":\"C\",\"orderNum\":5,\"params\":{},\"parentId\":0,\"path\":\"teacher\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:15:50');
INSERT INTO `sys_oper_log` VALUES (137, '学科', 1, 'com.ruoyi.business.controller.BusSubjectController.add()', 'POST', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectName\":\"1111111\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:17:18');
INSERT INTO `sys_oper_log` VALUES (138, '学科', 3, 'com.ruoyi.business.controller.BusSubjectController.remove()', 'DELETE', 1, 'admin', NULL, '/business/subject/59', '127.0.0.1', '内网IP', '{subjectIds=59}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:17:24');
INSERT INTO `sys_oper_log` VALUES (139, '学科', 1, 'com.ruoyi.business.controller.BusSubjectController.add()', 'POST', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectName\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:17:41');
INSERT INTO `sys_oper_log` VALUES (140, '学科', 1, 'com.ruoyi.business.controller.BusSubjectController.add()', 'POST', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectName\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:20:02');
INSERT INTO `sys_oper_log` VALUES (141, '学科', 2, 'com.ruoyi.business.controller.BusSubjectController.edit()', 'PUT', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectId\":61,\"subjectName\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:20:18');
INSERT INTO `sys_oper_log` VALUES (142, '学科', 2, 'com.ruoyi.business.controller.BusSubjectController.edit()', 'PUT', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectId\":61,\"subjectName\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:20:30');
INSERT INTO `sys_oper_log` VALUES (143, '学科', 3, 'com.ruoyi.business.controller.BusSubjectController.remove()', 'DELETE', 1, 'admin', NULL, '/business/subject/61', '127.0.0.1', '内网IP', '{subjectIds=61}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:20:39');
INSERT INTO `sys_oper_log` VALUES (144, '学科', 1, 'com.ruoyi.business.controller.BusSubjectController.add()', 'POST', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectName\":\"11\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:20:44');
INSERT INTO `sys_oper_log` VALUES (145, '学科', 1, 'com.ruoyi.business.controller.BusSubjectController.add()', 'POST', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectName\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:21:36');
INSERT INTO `sys_oper_log` VALUES (146, '学科', 1, 'com.ruoyi.business.controller.BusSubjectController.add()', 'POST', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"1\",\"subjectName\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:24:53');
INSERT INTO `sys_oper_log` VALUES (147, '学科', 3, 'com.ruoyi.business.controller.BusSubjectController.remove()', 'DELETE', 1, 'admin', NULL, '/business/subject/64', '127.0.0.1', '内网IP', '{subjectIds=64}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:25:02');
INSERT INTO `sys_oper_log` VALUES (148, '学科', 3, 'com.ruoyi.business.controller.BusSubjectController.remove()', 'DELETE', 1, 'admin', NULL, '/business/subject/63', '127.0.0.1', '内网IP', '{subjectIds=63}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:25:05');
INSERT INTO `sys_oper_log` VALUES (149, '学科', 3, 'com.ruoyi.business.controller.BusSubjectController.remove()', 'DELETE', 1, 'admin', NULL, '/business/subject/62', '127.0.0.1', '内网IP', '{subjectIds=62}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:25:07');
INSERT INTO `sys_oper_log` VALUES (150, '学科', 2, 'com.ruoyi.business.controller.BusSubjectController.edit()', 'PUT', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"sss\",\"subjectId\":52,\"subjectName\":\"科学\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:25:17');
INSERT INTO `sys_oper_log` VALUES (151, '学科', 2, 'com.ruoyi.business.controller.BusSubjectController.edit()', 'PUT', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"\",\"subjectId\":52,\"subjectName\":\"科学\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:25:22');
INSERT INTO `sys_oper_log` VALUES (152, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/02/blob_20221202094924A035.jpeg\",\"code\":200}', 0, NULL, '2022-12-02 09:49:24');
INSERT INTO `sys_oper_log` VALUES (153, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/02/blob_20221202095004A036.jpeg\",\"code\":200}', 0, NULL, '2022-12-02 09:50:04');
INSERT INTO `sys_oper_log` VALUES (154, '学科', 1, 'com.ruoyi.business.controller.BusSubjectController.add()', 'POST', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"12121212\",\"subjectName\":\"12121212\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:51:56');
INSERT INTO `sys_oper_log` VALUES (155, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/02/blob_20221202095211A037.jpeg\",\"code\":200}', 0, NULL, '2022-12-02 09:52:11');
INSERT INTO `sys_oper_log` VALUES (156, '学科', 2, 'com.ruoyi.business.controller.BusSubjectController.edit()', 'PUT', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"没得上\",\"subjectId\":65,\"subjectName\":\"计算机\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:52:27');
INSERT INTO `sys_oper_log` VALUES (157, '学科', 3, 'com.ruoyi.business.controller.BusSubjectController.remove()', 'DELETE', 1, 'admin', NULL, '/business/subject/65', '127.0.0.1', '内网IP', '{subjectIds=65}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:52:30');
INSERT INTO `sys_oper_log` VALUES (158, '学科', 1, 'com.ruoyi.business.controller.BusSubjectController.add()', 'POST', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectName\":\"信息\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 09:52:37');
INSERT INTO `sys_oper_log` VALUES (159, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13548193559', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/02/blob_20221202101848A008.jpeg\",\"code\":200}', 0, NULL, '2022-12-02 10:18:48');
INSERT INTO `sys_oper_log` VALUES (160, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, 'admin', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/02/blob_20221202103654A001.jpeg\",\"code\":200}', 0, NULL, '2022-12-02 10:36:54');
INSERT INTO `sys_oper_log` VALUES (161, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/02/blob_20221202162047A029.jpeg\",\"code\":200}', 0, NULL, '2022-12-02 16:20:47');
INSERT INTO `sys_oper_log` VALUES (162, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"classmanagetest\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 16:32:27');
INSERT INTO `sys_oper_log` VALUES (163, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-12-02 16:32:38');
INSERT INTO `sys_oper_log` VALUES (164, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"classmanagetest/index\",\"createTime\":\"2022-10-21 14:23:03\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2012,\"menuName\":\"班级管理\",\"menuType\":\"C\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"cardClass\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 16:36:18');
INSERT INTO `sys_oper_log` VALUES (165, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/classmanagetest/index\",\"createTime\":\"2022-10-21 14:23:03\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2012,\"menuName\":\"班级管理\",\"menuType\":\"C\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"cardClass\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 16:37:43');
INSERT INTO `sys_oper_log` VALUES (166, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 19:29:01');
INSERT INTO `sys_oper_log` VALUES (167, '学科', 1, 'com.ruoyi.business.controller.BusSubjectController.add()', 'POST', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"111\",\"subjectName\":\"111\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 19:38:01');
INSERT INTO `sys_oper_log` VALUES (168, '学科', 1, 'com.ruoyi.business.controller.BusSubjectController.add()', 'POST', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"111\",\"subjectName\":\"111\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 19:38:02');
INSERT INTO `sys_oper_log` VALUES (169, '学科', 3, 'com.ruoyi.business.controller.BusSubjectController.remove()', 'DELETE', 1, 'admin', NULL, '/business/subject/68', '127.0.0.1', '内网IP', '{subjectIds=68}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 19:38:06');
INSERT INTO `sys_oper_log` VALUES (170, '学科', 3, 'com.ruoyi.business.controller.BusSubjectController.remove()', 'DELETE', 1, 'admin', NULL, '/business/subject/67', '127.0.0.1', '内网IP', '{subjectIds=67}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 19:38:09');
INSERT INTO `sys_oper_log` VALUES (171, '学科', 2, 'com.ruoyi.business.controller.BusSubjectController.edit()', 'PUT', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"111\",\"subjectId\":49,\"subjectName\":\"数学\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 19:38:35');
INSERT INTO `sys_oper_log` VALUES (172, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-12-02 20:41:07');
INSERT INTO `sys_oper_log` VALUES (173, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/classmanagetest/index\",\"createTime\":\"2022-10-21 14:23:03\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2012,\"menuName\":\"班级管理\",\"menuType\":\"C\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"classmanagetest/index\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 20:52:13');
INSERT INTO `sys_oper_log` VALUES (174, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"D:\\\\ruoyi-ui\\\\src\\\\viewsystem/classmanagetest/ndex\",\"createTime\":\"2022-10-21 14:23:03\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2012,\"menuName\":\"班级管理\",\"menuType\":\"C\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"classmanagetest/index\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 20:55:40');
INSERT INTO `sys_oper_log` VALUES (175, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/classmanagetest/index\",\"createTime\":\"2022-10-21 14:23:03\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2012,\"menuName\":\"班级管理\",\"menuType\":\"C\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"classmanagetest/index\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 20:57:12');
INSERT INTO `sys_oper_log` VALUES (176, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"classmanagetest/index\",\"createTime\":\"2022-10-21 14:23:03\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2012,\"menuName\":\"班级管理\",\"menuType\":\"C\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"classmanagetest/index\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 21:21:44');
INSERT INTO `sys_oper_log` VALUES (177, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/classmanagetest/index\",\"createTime\":\"2022-10-21 14:23:03\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2012,\"menuName\":\"班级管理\",\"menuType\":\"C\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"classmanagetest/index\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 22:38:33');
INSERT INTO `sys_oper_log` VALUES (178, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/classmanagetest/index\",\"createTime\":\"2022-10-21 14:23:03\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2012,\"menuName\":\"班级管理\",\"menuType\":\"C\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"classmanagetest/index\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 22:38:54');
INSERT INTO `sys_oper_log` VALUES (179, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/classmanagetest/index\",\"createTime\":\"2022-10-21 14:23:03\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2012,\"menuName\":\"班级管理\",\"menuType\":\"C\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"classmanagetest/index\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 22:48:35');
INSERT INTO `sys_oper_log` VALUES (180, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-02 23:16:43');
INSERT INTO `sys_oper_log` VALUES (181, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/blob_20221203140342A001.jpeg\",\"code\":200}', 0, NULL, '2022-12-03 14:03:42');
INSERT INTO `sys_oper_log` VALUES (182, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/blob_20221203142243A002.jpeg\",\"code\":200}', 0, NULL, '2022-12-03 14:22:43');
INSERT INTO `sys_oper_log` VALUES (183, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-03 14:55:45');
INSERT INTO `sys_oper_log` VALUES (184, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-03 15:40:30');
INSERT INTO `sys_oper_log` VALUES (185, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/semester/index\",\"createTime\":\"2022-10-21 14:31:14\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2019,\"menuName\":\"学年学期管理\",\"menuType\":\"C\",\"orderNum\":0,\"params\":{},\"parentId\":2016,\"path\":\"semester\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-03 16:06:02');
INSERT INTO `sys_oper_log` VALUES (186, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-03 16:06:37');
INSERT INTO `sys_oper_log` VALUES (187, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/teacher/index\",\"createTime\":\"2022-10-21 14:24:29\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2013,\"menuName\":\"教师管理\",\"menuType\":\"C\",\"orderNum\":5,\"params\":{},\"parentId\":0,\"path\":\"teacher\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-03 16:07:10');
INSERT INTO `sys_oper_log` VALUES (188, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"studentscoretest\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-03 17:14:46');
INSERT INTO `sys_oper_log` VALUES (189, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-12-03 17:14:52');
INSERT INTO `sys_oper_log` VALUES (190, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/studentscoretest/index\",\"createTime\":\"2022-10-21 14:26:02\",\"icon\":\"excel\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2015,\"menuName\":\"学生成绩管理\",\"menuType\":\"C\",\"orderNum\":7,\"params\":{},\"parentId\":0,\"path\":\"achievementmanage\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-03 17:17:03');
INSERT INTO `sys_oper_log` VALUES (191, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '211.140.143.36', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/G5UEvuifVrxPf790ac8e97ec65211740b43d1f3f1840_20221203192815A006.png\",\"code\":200}', 0, NULL, '2022-12-03 19:28:15');
INSERT INTO `sys_oper_log` VALUES (192, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '211.140.143.36', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/lzfqIiIdfIR939484db9c22533a1e83e11b1f0d578d0_20221203192907A007.png\",\"code\":200}', 0, NULL, '2022-12-03 19:29:07');
INSERT INTO `sys_oper_log` VALUES (193, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13356007154', NULL, '/system/user/profile/avatar', '211.140.143.33', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/blob_20221203193410A008.jpeg\",\"code\":200}', 0, NULL, '2022-12-03 19:34:10');
INSERT INTO `sys_oper_log` VALUES (194, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/blob_20221203194357A013.jpeg\",\"code\":200}', 0, NULL, '2022-12-03 19:43:57');
INSERT INTO `sys_oper_log` VALUES (195, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13356773395', NULL, '/system/user/profile/avatar', '211.140.143.38', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/blob_20221203201023A034.jpeg\",\"code\":200}', 0, NULL, '2022-12-03 20:10:23');
INSERT INTO `sys_oper_log` VALUES (196, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/classmanagetest/index\",\"createTime\":\"2022-10-21 14:23:03\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2012,\"menuName\":\"班级管理\",\"menuType\":\"C\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"classmanagetest/index\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-03 20:10:34');
INSERT INTO `sys_oper_log` VALUES (197, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '211.140.143.33', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/blob_20221203202024A041.jpeg\",\"code\":200}', 0, NULL, '2022-12-03 20:20:24');
INSERT INTO `sys_oper_log` VALUES (198, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '211.140.143.33', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/blob_20221203202103A042.jpeg\",\"code\":200}', 0, NULL, '2022-12-03 20:21:03');
INSERT INTO `sys_oper_log` VALUES (199, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/blob_20221203205855A049.jpeg\",\"code\":200}', 0, NULL, '2022-12-03 20:58:55');
INSERT INTO `sys_oper_log` VALUES (200, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '211.140.143.33', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/blob_20221203213145A059.jpeg\",\"code\":200}', 0, NULL, '2022-12-03 21:31:45');
INSERT INTO `sys_oper_log` VALUES (201, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '211.140.143.36', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/UQifPiqPO30H8571a618a22137e8d04d35d4847234bf_20221203220022A066.png\",\"code\":200}', 0, NULL, '2022-12-03 22:00:22');
INSERT INTO `sys_oper_log` VALUES (202, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '211.140.143.36', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/nMXHA2REzsv68571a618a22137e8d04d35d4847234bf_20221203220025A067.png\",\"code\":200}', 0, NULL, '2022-12-03 22:00:25');
INSERT INTO `sys_oper_log` VALUES (203, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '211.140.143.37', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/03/tmp_7b3e0a173161872d747c4421e819f318f09748b3e2eefbb6_20221203223549A006.jpg\",\"code\":200}', 0, NULL, '2022-12-03 22:35:49');
INSERT INTO `sys_oper_log` VALUES (204, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.updatePwd()', 'PUT', 1, '13489643150', NULL, '/system/user/profile/updatePwd', '211.140.143.38', 'XX XX', '\"123456\" \"123456\"', '{\"msg\":\"新密码不能与旧密码相同\",\"code\":500}', 0, NULL, '2022-12-04 02:11:16');
INSERT INTO `sys_oper_log` VALUES (205, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204092855A037.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 09:28:55');
INSERT INTO `sys_oper_log` VALUES (206, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204093233A038.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 09:32:33');
INSERT INTO `sys_oper_log` VALUES (207, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204093431A039.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 09:34:32');
INSERT INTO `sys_oper_log` VALUES (208, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204093721A042.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 09:37:21');
INSERT INTO `sys_oper_log` VALUES (209, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204093740A043.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 09:37:40');
INSERT INTO `sys_oper_log` VALUES (210, '学科', 2, 'com.ruoyi.business.controller.BusSubjectController.edit()', 'PUT', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"\",\"subjectId\":49,\"subjectName\":\"数学\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 09:37:57');
INSERT INTO `sys_oper_log` VALUES (211, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.updatePwd()', 'PUT', 1, '13464871395', NULL, '/system/user/profile/updatePwd', '211.140.143.38', 'XX XX', '\"123456\" \"1234567\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 09:40:52');
INSERT INTO `sys_oper_log` VALUES (212, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.updatePwd()', 'PUT', 1, '13464871395', NULL, '/system/user/profile/updatePwd', '211.140.143.38', 'XX XX', '\"123456\" \"1234567\"', '{\"msg\":\"修改密码失败，旧密码错误\",\"code\":500}', 0, NULL, '2022-12-04 09:40:55');
INSERT INTO `sys_oper_log` VALUES (213, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.updatePwd()', 'PUT', 1, '13464871395', NULL, '/system/user/profile/updatePwd', '211.140.143.38', 'XX XX', '\"1234567\" \"123456\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 09:41:52');
INSERT INTO `sys_oper_log` VALUES (214, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204094441A044.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 09:44:41');
INSERT INTO `sys_oper_log` VALUES (215, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204094454A045.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 09:44:54');
INSERT INTO `sys_oper_log` VALUES (216, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204094503A046.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 09:45:03');
INSERT INTO `sys_oper_log` VALUES (217, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204094917A047.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 09:49:17');
INSERT INTO `sys_oper_log` VALUES (218, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204094931A048.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 09:49:31');
INSERT INTO `sys_oper_log` VALUES (219, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204094943A049.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 09:49:43');
INSERT INTO `sys_oper_log` VALUES (220, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204095006A050.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 09:50:06');
INSERT INTO `sys_oper_log` VALUES (221, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 09:51:02');
INSERT INTO `sys_oper_log` VALUES (222, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204100107A054.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:01:07');
INSERT INTO `sys_oper_log` VALUES (223, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204100328A055.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:03:28');
INSERT INTO `sys_oper_log` VALUES (224, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204100355A057.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:03:55');
INSERT INTO `sys_oper_log` VALUES (225, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204100409A058.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:04:09');
INSERT INTO `sys_oper_log` VALUES (226, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204100423A059.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:04:23');
INSERT INTO `sys_oper_log` VALUES (227, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204100439A060.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:04:39');
INSERT INTO `sys_oper_log` VALUES (228, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204100502A061.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:05:02');
INSERT INTO `sys_oper_log` VALUES (229, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204100541A062.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:05:41');
INSERT INTO `sys_oper_log` VALUES (230, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 10:07:51');
INSERT INTO `sys_oper_log` VALUES (231, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204100856A064.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:08:56');
INSERT INTO `sys_oper_log` VALUES (232, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204101140A066.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:11:40');
INSERT INTO `sys_oper_log` VALUES (233, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204101151A067.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:11:51');
INSERT INTO `sys_oper_log` VALUES (234, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204101159A069.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:11:59');
INSERT INTO `sys_oper_log` VALUES (235, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204101302A072.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:13:02');
INSERT INTO `sys_oper_log` VALUES (236, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204101318A074.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:13:18');
INSERT INTO `sys_oper_log` VALUES (237, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13578862471', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204101410A076.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:14:10');
INSERT INTO `sys_oper_log` VALUES (238, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204101450A077.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:14:50');
INSERT INTO `sys_oper_log` VALUES (239, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204101635A078.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:16:35');
INSERT INTO `sys_oper_log` VALUES (240, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204101645A079.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 10:16:45');
INSERT INTO `sys_oper_log` VALUES (241, '学科', 2, 'com.ruoyi.business.controller.BusSubjectController.edit()', 'PUT', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"hjhjhjkl\",\"subjectId\":49,\"subjectName\":\"数学\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 10:38:10');
INSERT INTO `sys_oper_log` VALUES (242, '学科', 2, 'com.ruoyi.business.controller.BusSubjectController.edit()', 'PUT', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"\",\"subjectId\":49,\"subjectName\":\"数学\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 10:38:16');
INSERT INTO `sys_oper_log` VALUES (243, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204110511A020.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 11:05:11');
INSERT INTO `sys_oper_log` VALUES (244, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204110529A021.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 11:05:29');
INSERT INTO `sys_oper_log` VALUES (245, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204110539A022.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 11:05:39');
INSERT INTO `sys_oper_log` VALUES (246, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204110557A023.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 11:05:57');
INSERT INTO `sys_oper_log` VALUES (247, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204110719A024.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 11:07:19');
INSERT INTO `sys_oper_log` VALUES (248, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204110736A025.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 11:07:36');
INSERT INTO `sys_oper_log` VALUES (249, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/seal/seal\",\"createTime\":\"2022-10-21 14:29:38\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2017,\"menuName\":\"通用印章管理\",\"menuType\":\"C\",\"orderNum\":0,\"params\":{},\"parentId\":2016,\"path\":\"seal\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 11:07:56');
INSERT INTO `sys_oper_log` VALUES (250, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13405557267', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204110758A026.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 11:07:58');
INSERT INTO `sys_oper_log` VALUES (251, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 11:37:06');
INSERT INTO `sys_oper_log` VALUES (252, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '10.46.0.72', '内网IP', '{\"children\":[],\"component\":\"business\\\\basicestting\\\\seal1\",\"createTime\":\"2022-10-21 14:29:38\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2017,\"menuName\":\"通用印章管理\",\"menuType\":\"C\",\"orderNum\":0,\"params\":{},\"parentId\":2016,\"path\":\"seal\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 14:01:18');
INSERT INTO `sys_oper_log` VALUES (253, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '10.46.0.72', '内网IP', '{\"children\":[],\"component\":\"business\\\\basicestting\\\\seal1\",\"createTime\":\"2022-10-21 14:29:38\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2017,\"menuName\":\"通用印章管理\",\"menuType\":\"C\",\"orderNum\":0,\"params\":{},\"parentId\":2016,\"path\":\"basicestting\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 14:03:48');
INSERT INTO `sys_oper_log` VALUES (254, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '10.46.0.72', '内网IP', '{\"children\":[],\"component\":\"src\\\\views\\\\business\\\\basicestting\\\\seal1.vue\",\"createTime\":\"2022-10-21 14:29:38\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2017,\"menuName\":\"通用印章管理\",\"menuType\":\"C\",\"orderNum\":0,\"params\":{},\"parentId\":2016,\"path\":\"basicestting\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 14:06:03');
INSERT INTO `sys_oper_log` VALUES (255, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '10.46.0.72', '内网IP', '{\"children\":[],\"component\":\"business\\\\basicestting\\\\seal1\",\"createTime\":\"2022-10-21 14:29:38\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2017,\"menuName\":\"通用印章管理\",\"menuType\":\"C\",\"orderNum\":0,\"params\":{},\"parentId\":2016,\"path\":\"basicestting\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 14:07:46');
INSERT INTO `sys_oper_log` VALUES (256, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/basicestting/seal1\",\"createTime\":\"2022-10-21 14:29:38\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2017,\"menuName\":\"通用印章管理\",\"menuType\":\"C\",\"orderNum\":0,\"params\":{},\"parentId\":2016,\"path\":\"basicestting\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 14:16:25');
INSERT INTO `sys_oper_log` VALUES (257, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/classmanagetest/index\",\"createTime\":\"2022-10-21 14:23:03\",\"icon\":\"peoples\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2012,\"menuName\":\"班级管理\",\"menuType\":\"C\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"classmanagetest/index\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 14:18:16');
INSERT INTO `sys_oper_log` VALUES (258, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13412342976', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/04/blob_20221204151522A034.jpeg\",\"code\":200}', 0, NULL, '2022-12-04 15:15:23');
INSERT INTO `sys_oper_log` VALUES (259, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 22:48:53');
INSERT INTO `sys_oper_log` VALUES (260, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-12-04 23:16:45');
INSERT INTO `sys_oper_log` VALUES (261, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-04 23:24:06');
INSERT INTO `sys_oper_log` VALUES (262, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/seal/seal\",\"createTime\":\"2022-10-21 14:29:38\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2017,\"menuName\":\"通用印章管理\",\"menuType\":\"C\",\"orderNum\":0,\"params\":{},\"parentId\":2016,\"path\":\"basicestting\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 09:35:32');
INSERT INTO `sys_oper_log` VALUES (263, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 10:49:00');
INSERT INTO `sys_oper_log` VALUES (264, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/05/blob_20221205132911A117.jpeg\",\"code\":200}', 0, NULL, '2022-12-05 13:29:11');
INSERT INTO `sys_oper_log` VALUES (265, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 14:09:33');
INSERT INTO `sys_oper_log` VALUES (266, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/05/blob_20221205144248A189.jpeg\",\"code\":200}', 0, NULL, '2022-12-05 14:42:48');
INSERT INTO `sys_oper_log` VALUES (267, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/05/blob_20221205144320A193.jpeg\",\"code\":200}', 0, NULL, '2022-12-05 14:43:20');
INSERT INTO `sys_oper_log` VALUES (268, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/05/blob_20221205145008A197.jpeg\",\"code\":200}', 0, NULL, '2022-12-05 14:50:08');
INSERT INTO `sys_oper_log` VALUES (269, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"students\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 16:38:23');
INSERT INTO `sys_oper_log` VALUES (270, '学科', 2, 'com.ruoyi.business.controller.BusSubjectController.edit()', 'PUT', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"\",\"subjectId\":49,\"subjectName\":\"数学1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 18:40:02');
INSERT INTO `sys_oper_log` VALUES (271, '学科', 2, 'com.ruoyi.business.controller.BusSubjectController.edit()', 'PUT', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectDescription\":\"\",\"subjectId\":49,\"subjectName\":\"数学\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 18:40:09');
INSERT INTO `sys_oper_log` VALUES (272, '学科', 1, 'com.ruoyi.business.controller.BusSubjectController.add()', 'POST', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectName\":\"道德与法治\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 18:45:04');
INSERT INTO `sys_oper_log` VALUES (273, '学科', 1, 'com.ruoyi.business.controller.BusSubjectController.add()', 'POST', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectName\":\"111\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 18:45:16');
INSERT INTO `sys_oper_log` VALUES (274, '学科', 3, 'com.ruoyi.business.controller.BusSubjectController.remove()', 'DELETE', 1, 'admin', NULL, '/business/subject/70', '127.0.0.1', '内网IP', '{subjectIds=70}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 18:45:19');
INSERT INTO `sys_oper_log` VALUES (275, '学科', 2, 'com.ruoyi.business.controller.BusSubjectController.edit()', 'PUT', 1, 'admin', NULL, '/business/subject', '127.0.0.1', '内网IP', '{\"subjectId\":56,\"subjectName\":\"信息与技术\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 18:50:11');
INSERT INTO `sys_oper_log` VALUES (276, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '211.140.143.35', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/05/blob_20221205185604A005.jpeg\",\"code\":200}', 0, NULL, '2022-12-05 18:56:04');
INSERT INTO `sys_oper_log` VALUES (277, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, '13514695070', NULL, '/system/user/profile/avatar', '101.66.185.32', 'XX XX', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/img/avatar/2022/12/05/blob_20221205192124A031.jpeg\",\"code\":200}', 0, NULL, '2022-12-05 19:21:25');
INSERT INTO `sys_oper_log` VALUES (278, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"system/stumange/index\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 23:39:52');
INSERT INTO `sys_oper_log` VALUES (279, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"system/stumange/index\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 23:40:27');
INSERT INTO `sys_oper_log` VALUES (280, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"stumange\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 23:40:54');
INSERT INTO `sys_oper_log` VALUES (281, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"stumange\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-05 23:55:32');
INSERT INTO `sys_oper_log` VALUES (282, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/studentscoretest/index1\",\"createTime\":\"2022-10-21 14:26:02\",\"icon\":\"excel\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2015,\"menuName\":\"学生成绩管理\",\"menuType\":\"C\",\"orderNum\":7,\"params\":{},\"parentId\":0,\"path\":\"achievementmanage\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 02:03:44');
INSERT INTO `sys_oper_log` VALUES (283, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"stumange\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 08:16:08');
INSERT INTO `sys_oper_log` VALUES (284, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/studentscoretest/index\",\"createTime\":\"2022-10-21 14:26:02\",\"icon\":\"excel\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2015,\"menuName\":\"学生成绩管理\",\"menuType\":\"C\",\"orderNum\":7,\"params\":{},\"parentId\":0,\"path\":\"achievementmanage\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 08:21:47');
INSERT INTO `sys_oper_log` VALUES (285, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"stumange\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 10:40:33');
INSERT INTO `sys_oper_log` VALUES (286, '学科', 5, 'com.ruoyi.business.controller.BusSubjectController.export()', 'POST', 1, 'admin', NULL, '/business/subject/export', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-12-06 10:41:23');
INSERT INTO `sys_oper_log` VALUES (287, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"stumange\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 14:21:06');
INSERT INTO `sys_oper_log` VALUES (288, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"bussiness/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"stumange\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 14:28:41');
INSERT INTO `sys_oper_log` VALUES (289, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"business/stumange/index\",\"createTime\":\"2022-10-22 16:10:21\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"学生管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2014,\"path\":\"stumange\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 14:28:52');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, '01', '班主任', 0, '0', 'admin', '2022-10-22 08:56:29', '', NULL, NULL);
INSERT INTO `sys_post` VALUES (50, NULL, '数学老师', 50, '0', '', NULL, '', NULL, NULL);
INSERT INTO `sys_post` VALUES (51, NULL, '语文老师', 51, '0', '', NULL, '', NULL, NULL);
INSERT INTO `sys_post` VALUES (52, NULL, '英语老师', 52, '0', '', NULL, '', NULL, NULL);
INSERT INTO `sys_post` VALUES (53, NULL, '科学老师', 53, '0', '', NULL, '', NULL, NULL);
INSERT INTO `sys_post` VALUES (54, NULL, '体育老师', 54, '0', '', NULL, '', NULL, NULL);
INSERT INTO `sys_post` VALUES (55, NULL, '美术老师', 55, '0', '', NULL, '', NULL, NULL);
INSERT INTO `sys_post` VALUES (56, NULL, '音乐老师', 56, '0', '', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '开发者', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2022-10-18 11:10:04', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '管理员', 'common', 2, '4', 1, 1, '0', '0', 'admin', '2022-10-18 11:10:04', 'admin', '2022-11-24 22:49:05', '普通角色');
INSERT INTO `sys_role` VALUES (100, '班主任', 'bzr', 3, '2', 1, 0, '0', '0', 'admin', '2022-10-21 13:25:02', 'admin', '2022-11-24 22:49:34', NULL);
INSERT INTO `sys_role` VALUES (101, '任课教师', 'teacher', 3, '5', 1, 1, '0', '0', 'admin', '2022-10-21 14:13:38', 'admin', '2022-11-24 22:49:40', NULL);
INSERT INTO `sys_role` VALUES (102, '家长', 'parent', 4, '5', 1, 1, '0', '0', 'admin', '2022-10-21 14:14:22', 'admin', '2022-11-27 09:59:24', NULL);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (100, 201);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 2012);
INSERT INTO `sys_role_menu` VALUES (2, 2013);
INSERT INTO `sys_role_menu` VALUES (2, 2014);
INSERT INTO `sys_role_menu` VALUES (2, 2015);
INSERT INTO `sys_role_menu` VALUES (2, 2016);
INSERT INTO `sys_role_menu` VALUES (2, 2017);
INSERT INTO `sys_role_menu` VALUES (2, 2018);
INSERT INTO `sys_role_menu` VALUES (2, 2019);
INSERT INTO `sys_role_menu` VALUES (2, 2021);
INSERT INTO `sys_role_menu` VALUES (2, 2022);
INSERT INTO `sys_role_menu` VALUES (2, 2023);
INSERT INTO `sys_role_menu` VALUES (2, 2024);
INSERT INTO `sys_role_menu` VALUES (2, 2050);
INSERT INTO `sys_role_menu` VALUES (2, 2051);
INSERT INTO `sys_role_menu` VALUES (2, 2052);
INSERT INTO `sys_role_menu` VALUES (2, 2053);
INSERT INTO `sys_role_menu` VALUES (2, 2054);
INSERT INTO `sys_role_menu` VALUES (2, 2055);
INSERT INTO `sys_role_menu` VALUES (2, 2077);
INSERT INTO `sys_role_menu` VALUES (100, 1);
INSERT INTO `sys_role_menu` VALUES (100, 100);
INSERT INTO `sys_role_menu` VALUES (100, 1000);
INSERT INTO `sys_role_menu` VALUES (100, 1001);
INSERT INTO `sys_role_menu` VALUES (100, 1002);
INSERT INTO `sys_role_menu` VALUES (100, 1003);
INSERT INTO `sys_role_menu` VALUES (100, 1004);
INSERT INTO `sys_role_menu` VALUES (100, 1005);
INSERT INTO `sys_role_menu` VALUES (100, 1006);
INSERT INTO `sys_role_menu` VALUES (100, 2012);
INSERT INTO `sys_role_menu` VALUES (100, 2015);
INSERT INTO `sys_role_menu` VALUES (100, 2021);
INSERT INTO `sys_role_menu` VALUES (100, 2022);
INSERT INTO `sys_role_menu` VALUES (100, 2023);
INSERT INTO `sys_role_menu` VALUES (100, 2024);
INSERT INTO `sys_role_menu` VALUES (100, 2060);
INSERT INTO `sys_role_menu` VALUES (100, 2061);
INSERT INTO `sys_role_menu` VALUES (100, 2062);
INSERT INTO `sys_role_menu` VALUES (100, 2063);
INSERT INTO `sys_role_menu` VALUES (100, 2078);
INSERT INTO `sys_role_menu` VALUES (101, 2021);
INSERT INTO `sys_role_menu` VALUES (101, 2022);
INSERT INTO `sys_role_menu` VALUES (101, 2023);
INSERT INTO `sys_role_menu` VALUES (101, 2024);
INSERT INTO `sys_role_menu` VALUES (101, 2060);
INSERT INTO `sys_role_menu` VALUES (101, 2062);
INSERT INTO `sys_role_menu` VALUES (101, 2078);
INSERT INTO `sys_role_menu` VALUES (102, 2021);
INSERT INTO `sys_role_menu` VALUES (102, 2022);
INSERT INTO `sys_role_menu` VALUES (102, 2023);
INSERT INTO `sys_role_menu` VALUES (102, 2024);
INSERT INTO `sys_role_menu` VALUES (102, 2056);
INSERT INTO `sys_role_menu` VALUES (102, 2057);
INSERT INTO `sys_role_menu` VALUES (102, 2058);
INSERT INTO `sys_role_menu` VALUES (102, 2063);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `open_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信openid',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `raw_password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'base64加密的密码',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1599241033600422 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, NULL, '', 'admin', '开发者', '00', '', '15888888888', '1', '/profile/img/avatar/2022/12/02/blob_20221202103654A001.jpeg', '$2a$10$PbCH426Z1s.Jw6SKbvsTtO1P9xU//hkckVw8CUnvE.Nm3D0wAlo7m', '0', '0', '127.0.0.1', '2022-11-20 10:11:07', 'admin', '2022-10-18 11:10:03', '', '2022-11-20 10:11:08', '开发者', NULL);
INSERT INTO `sys_user` VALUES (2, 1, '', 'admin1', '管理员', '00', '', '15666666666', '1', '/profile/img/gallery/2022/11/18/Eur0rlqopdbD04dfc68d50dc645c2c573d8bde40c12a_20221106161044A005_20221118091049A080.JPG', '$2a$10$wId5KfWU/OY692UO.Ck3audyfRdBnIRzUysU1mImg2nn3l1bxR2jO', '0', '0', '127.0.0.1', '2022-10-27 19:47:04', 'admin', '2022-10-18 11:10:03', 'admin', '2022-11-23 08:11:16', '管理员', 'MTIzNDU2');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
