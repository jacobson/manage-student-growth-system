package com.ruoyi.system.common;

public class RedisKeyPrefix {
    public static String ROLE_PREFIX = "login:type:userName:";
    public static String ROLE_FLAG_PREFIX = "role:flag:userName:";
    public static String CURRENT_SEMESTER_ID = "current:semester:id";
    public static String CURRENT_REPORT_FINISH_TIME = "report:finishTime:semesterId:";
    public static String CURRENT_REPORT_FINISH_FLAG = "report:finish";
    public static String GROWTH_ITEM_WRITE_NOW = "growth:write:now:growthId";
}
