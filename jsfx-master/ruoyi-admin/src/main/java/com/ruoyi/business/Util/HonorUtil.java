package com.ruoyi.business.Util;

import com.ruoyi.business.domain.BusStudentHonor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HonorUtil {

    /**
     * 对应得荣誉，以及对应得获得者名单
     */
    private static final Map<String, List<String>> mapHonor=new HashMap<>();


    //all代表着当前班里的这个学生
    public static Map<String, List<String>> getMapHonor() {
        String[] item=new String[]{"honor1","honor2","honor3","honor4","honor5","candidate"};
        for (String x:item) {
            mapHonor.put(x, new ArrayList<>());
        }
        System.out.println(mapHonor);
        return mapHonor;
    }
    //模块判断
    public static BusStudentHonor confirmBusStudentHonor(BusStudentHonor busStudentHonor,String module,boolean flag){
        if (module.equals("honor1")){
            if (flag){
                busStudentHonor.setHonor1("1");
            }else {
                busStudentHonor.setHonor1("0");
            }
        }
        if (module.equals("honor2")){
            if (flag){
                busStudentHonor.setHonor2("1");
            }else {
                busStudentHonor.setHonor2("0");
            }
        }
        if (module.equals("honor3")){
            if (flag){
                busStudentHonor.setHonor3("1");
            }else {
                busStudentHonor.setHonor3("0");
            }
        }
        if (module.equals("honor4")){
            if (flag){
                busStudentHonor.setHonor4("1");
            }else {
                busStudentHonor.setHonor4("0");
            }
        }
        if (module.equals("honor5")){
            if (flag){
                busStudentHonor.setHonor5("1");
            }else {
                busStudentHonor.setHonor5("0");
            }
        }
        return busStudentHonor;
    }
}
