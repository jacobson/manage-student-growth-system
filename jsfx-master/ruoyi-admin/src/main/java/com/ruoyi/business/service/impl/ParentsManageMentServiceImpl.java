package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.BusStudentParents;
import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.business.domain.ParentsManageMent;
import com.ruoyi.business.mapper.ParentsManageMentMapper;
import com.ruoyi.business.service.IBusStudentParentsService;
import com.ruoyi.business.service.IBusStudentsService;
import com.ruoyi.business.service.IParentsManageMentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ParentsManageMentServiceImpl extends ServiceImpl<ParentsManageMentMapper, ParentsManageMent> implements IParentsManageMentService {
    //user表
    @Autowired
    private IParentsManageMentService parentsManageMentService;

    //学生表
    @Autowired
    private IBusStudentsService busStudentsService;

    //学生家长表
    @Autowired
    private IBusStudentParentsService busStudentParentsService;


    @Override
    public List<ParentsManageMent> allParentsManages() {

        LambdaQueryWrapper<ParentsManageMent> plqw = new LambdaQueryWrapper<>();
        plqw.eq(ParentsManageMent::getDeptId,201);
        List<ParentsManageMent> list = parentsManageMentService.list(plqw);
        //遍历每个家长
        List<ParentsManageMent> collect = list.stream().peek(item -> {
            //封装孩子的身份证号和名字
            Map<String,String> map = new HashMap<>();

            //取出user中的家长的id，到学生家长表中查询学生的id和家长的身份
            Long userId = item.getUserId();
            System.out.println(userId);

            LambdaQueryWrapper<BusStudentParents> splqw = new LambdaQueryWrapper<>();
            splqw.eq(BusStudentParents::getUserId,userId);
            List<BusStudentParents> list1 = busStudentParentsService.list(splqw);

            //遍历这个家长有几个孩子，还有获取家长的身份
            for (BusStudentParents busStudentParents : list1) {
                //获取学生id
                Long studentId = busStudentParents.getStudentId();
                //获取家长的身份，会重复
                String relation = busStudentParents.getRelation();
                //将家长的身份给identity来设置，一直覆盖一个家长只有一个省份
                item.setIdentity(relation);

                //根据这个家长的孩子的id，到学生表中找姓名和身份证号
                LambdaQueryWrapper<BusStudents> slqw = new LambdaQueryWrapper<>();
                slqw.eq(BusStudents::getStuId,studentId);
                List<BusStudents> list2 = busStudentsService.list(slqw);
                //遍历一个家长的所以的孩子

                for (BusStudents busStudents : list2) {
                    //学生的身份证号
                    String idCard = busStudents.getIdCard();
                    //学生的名字
                    String stuName = busStudents.getStuName();

                    map.put(stuName,idCard);
                    item.setParentAndChild(map);
                }

            }

        }).collect(Collectors.toList());
        return collect;
    }



    //条件查询
    @Override
    public List<ParentsManageMent> conditionParentsManages(String name, String phonenumber) {
        LambdaQueryWrapper<ParentsManageMent> plqw = new LambdaQueryWrapper<>();
        plqw.eq(ParentsManageMent::getDeptId,201);
        plqw.like(!name.isEmpty(),ParentsManageMent::getNickName,name);
        plqw.like(!phonenumber.isEmpty(),ParentsManageMent::getPhonenumber,phonenumber);

        List<ParentsManageMent> list = parentsManageMentService.list(plqw);
        //遍历每个家长
        List<ParentsManageMent> collect = list.stream().peek(item -> {
            //封装孩子的身份证号和名字
            Map<String,String> map = new HashMap<>();

            //取出user中的家长的id，到学生家长表中查询学生的id和家长的身份
            Long userId = item.getUserId();
            System.out.println(userId);

            LambdaQueryWrapper<BusStudentParents> splqw = new LambdaQueryWrapper<>();
            splqw.eq(BusStudentParents::getUserId,userId);
            List<BusStudentParents> list1 = busStudentParentsService.list(splqw);

            //遍历这个家长有几个孩子，还有获取家长的身份
            for (BusStudentParents busStudentParents : list1) {
                //获取学生id
                Long studentId = busStudentParents.getStudentId();
                //获取家长的身份，会重复
                String relation = busStudentParents.getRelation();
                //将家长的身份给identity来设置，一直覆盖一个家长只有一个省份
                item.setIdentity(relation);

                //根据这个家长的孩子的id，到学生表中找姓名和身份证号
                LambdaQueryWrapper<BusStudents> slqw = new LambdaQueryWrapper<>();
                slqw.eq(BusStudents::getStuId,studentId);
                List<BusStudents> list2 = busStudentsService.list(slqw);
                //遍历一个家长的所以的孩子

                for (BusStudents busStudents : list2) {
                    //学生的身份证号
                    String idCard = busStudents.getIdCard();
                    //学生的名字
                    String stuName = busStudents.getStuName();

                    map.put(stuName,idCard);
                    item.setParentAndChild(map);
                }

            }
        }).collect(Collectors.toList());
        return collect;
    }



    //删除家长
    @Override
    public boolean deleteParentsManageInfo(Long UserId) {
        //删除用户表中的信息
        LambdaQueryWrapper<ParentsManageMent> pmlqw = new LambdaQueryWrapper<>();
        pmlqw.eq(ParentsManageMent::getUserId,UserId)
                .eq(ParentsManageMent::getDeptId,201);
        boolean remove1 = parentsManageMentService.remove(pmlqw);

        //删除学生家长表中的信息
        LambdaQueryWrapper<BusStudentParents> splqw = new LambdaQueryWrapper<>();
        splqw.eq(BusStudentParents::getUserId,UserId);
        boolean remove2 = busStudentParentsService.remove(splqw);

        boolean b = remove1 || remove2;
        return b;
    }




    //新增家长
    @Override
    public boolean addParentsManageInfo(ParentsManageMent parentsManageMent) {
        //添加user表中相应的家长信息
        boolean save = this.save(parentsManageMent);

        boolean save1= false;

        //身份
        String identity = parentsManageMent.getIdentity();
        //用户id
        Long userId = parentsManageMent.getUserId();
        //学生的姓名和身份证号
        Map<String, String> parentAndChild = parentsManageMent.getParentAndChild();
        //遍历map的健，来挨个挨个读取他的值
        Set<String> strings = parentAndChild.keySet();
        for (String string : strings) {
            //取到了学生的身份证号
            String s = parentAndChild.get(string);

            //用身份证号去学生表中查询学生的id
            LambdaQueryWrapper<BusStudents> slqw = new LambdaQueryWrapper<>();
            slqw.eq(BusStudents::getIdCard,s);
            BusStudents one = busStudentsService.getOne(slqw);
            Long stuId = one.getStuId();

            //向学生家长表中添加数据
            BusStudentParents busStudentParents = new BusStudentParents();
            busStudentParents.setStudentId(stuId);
            busStudentParents.setRelation(identity);
            busStudentParents.setUserId(userId);

            save1 = busStudentParentsService.save(busStudentParents);
        }

        return save && save1;
    }


    //编辑条件回显
    @Override
    public List<ParentsManageMent> echoParentsManageInfo(Long UserId) {
        LambdaQueryWrapper<ParentsManageMent> plqw = new LambdaQueryWrapper<>();
        plqw.eq(ParentsManageMent::getDeptId,201);
        plqw.eq(ParentsManageMent::getUserId,UserId);

        List<ParentsManageMent> list = parentsManageMentService.list(plqw);
        //遍历每个家长
        List<ParentsManageMent> collect = list.stream().peek(item -> {
            //封装孩子的身份证号和名字
            Map<String,String> map = new HashMap<>();

            //取出user中的家长的id，到学生家长表中查询学生的id和家长的身份
            Long userId = item.getUserId();
            System.out.println(userId);

            LambdaQueryWrapper<BusStudentParents> splqw = new LambdaQueryWrapper<>();
            splqw.eq(BusStudentParents::getUserId,userId);
            List<BusStudentParents> list1 = busStudentParentsService.list(splqw);

            //遍历这个家长有几个孩子，还有获取家长的身份
            for (BusStudentParents busStudentParents : list1) {
                //获取学生id
                Long studentId = busStudentParents.getStudentId();
                //获取家长的身份，会重复
                String relation = busStudentParents.getRelation();
                //将家长的身份给identity来设置，一直覆盖一个家长只有一个省份
                item.setIdentity(relation);

                //根据这个家长的孩子的id，到学生表中找姓名和身份证号
                LambdaQueryWrapper<BusStudents> slqw = new LambdaQueryWrapper<>();
                slqw.eq(BusStudents::getStuId,studentId);
                List<BusStudents> list2 = busStudentsService.list(slqw);
                //遍历一个家长的所以的孩子

                for (BusStudents busStudents : list2) {
                    //学生的身份证号
                    String idCard = busStudents.getIdCard();
                    //学生的名字
                    String stuName = busStudents.getStuName();

                    map.put(stuName,idCard);
                    item.setParentAndChild(map);
                }
            }
        }).collect(Collectors.toList());
        return collect;
    }


}
