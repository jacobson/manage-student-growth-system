package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.business.mapper.BusStudentsMapper;
import com.ruoyi.business.service.IBusParentStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusParentStudentServiceImpl implements IBusParentStudentService {
    @Autowired
    private BusStudentsMapper busStudentsMapper;
    @Override
    public List<BusStudents> getChild(Long userId) {
        List<BusStudents> child = busStudentsMapper.getChild(userId);
        child.forEach((item)->{
            item.setGradeId(busStudentsMapper.getGradeId(item.getStuId()));
        });
        return child;
    }

    @Override
    public List<Long> getParentUserIds(Long studentId) {
        return busStudentsMapper.getParentUserIds(studentId);
    }
}
