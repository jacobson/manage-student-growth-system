package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusUser;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginBody;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户表 数据层
 * 
 * @author ruoyi
 */
@Mapper
public interface BusUserMapper extends BaseMapper<BusUser>
{
    /**
     * 根据条件分页查询用户列表
     * 
     * @param busUser 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserList(BusUser busUser);

    /**
     * 根据条件分页查询已配用户角色列表
     * 
     * @param busUser 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectAllocatedList(BusUser busUser);

    /**
     * 根据条件分页查询未分配用户角色列表
     * 
     * @param busUser 用户信息
     * @return 用户信息集合信息
     */
    public List<BusUser> selectUnallocatedList(BusUser busUser);

    /**
     * 通过用户名查询用户
     * 
     * @param userName 用户名
     * @return 用户对象信息
     */
    public BusUser selectUserByUserName(String userName);

    /**
     * 通过用户ID查询用户
     * 
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public BusUser selectUserById(Long userId);

    /**
     * 新增用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    public boolean insertUser(BusUser user);

    /**
     * 修改用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    public int updateUser(BusUser user);

    /**
     * 修改用户头像
     * 
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    public int updateUserAvatar(@Param("userName") String userName, @Param("avatar") String avatar);

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    public int resetUserPwd(@Param("userName") String userName, @Param("password") String password);

    /**
     * 通过用户ID删除用户
     * 
     * @param userId 用户ID
     * @return 结果
     */
    public int deleteUserById(Long userId);

    /**
     * 批量删除用户信息
     * 
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    public int deleteUserByIds(Long[] userIds);

    /**
     * 校验用户名称是否唯一
     * 
     * @param userName 用户名称
     * @return 结果
     */
    public BusUser checkUserNameUnique(String userName);

    /**
     * 校验手机号码是否唯一
     *
     * @param phonenumber 手机号码
     * @return 结果
     */
    public BusUser checkPhoneUnique(String phonenumber);

    /**
     * 校验email是否唯一
     *
     * @param email 用户邮箱
     * @return 结果
     */
    public BusUser checkEmailUnique(String email);

    /**
     * 通过openId获取用户的账号密码
     */
    LoginBody getLoginBodyByOpenId(String openId);
    /**
     * 为用户绑定openId
     */
    Integer updateOpenIdByUserName(@Param("openId") String openId, @Param("userName")String userName);
    /**
     * 使用openId登录过一次后插入base64加密的密码
     */
    void updateRawPasswordByUserName(@Param("rawPassword") String rawPassword, @Param("userName")String userName);
    Long getUserId(String time);
}
