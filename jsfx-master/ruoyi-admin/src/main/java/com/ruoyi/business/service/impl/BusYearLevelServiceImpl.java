package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.BusYearLevel;
import com.ruoyi.business.mapper.BusYearLevelMapper;
import com.ruoyi.business.service.IBusYearLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 年级Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-12
 */
@Service
public class BusYearLevelServiceImpl extends ServiceImpl<BusYearLevelMapper,BusYearLevel> implements IBusYearLevelService
{
    @Autowired
    private BusYearLevelMapper busYearLevelMapper;

    /**
     * 查询年级
     * 
     * @param yearLevelId 年级主键
     * @return 年级
     */
    @Override
    public BusYearLevel selectBusYearLevelByYearLevelId(Long yearLevelId)
    {
        return busYearLevelMapper.selectBusYearLevelByYearLevelId(yearLevelId);
    }

    /**
     * 查询年级列表
     * 
     * @param busYearLevel 年级
     * @return 年级
     */
    @Override
    public List<BusYearLevel> selectBusYearLevelList(BusYearLevel busYearLevel)
    {
        return busYearLevelMapper.selectBusYearLevelList(busYearLevel);
    }

    /**
     * 新增年级
     * 
     * @param busYearLevel 年级
     * @return 结果
     */
    @Override
    public int insertBusYearLevel(BusYearLevel busYearLevel)
    {
        return busYearLevelMapper.insertBusYearLevel(busYearLevel);
    }

    /**
     * 修改年级
     * 
     * @param busYearLevel 年级
     * @return 结果
     */
    @Override
    public int updateBusYearLevel(BusYearLevel busYearLevel)
    {
        return busYearLevelMapper.updateBusYearLevel(busYearLevel);
    }

    /**
     * 批量删除年级
     * 
     * @param yearLevelIds 需要删除的年级主键
     * @return 结果
     */
    @Override
    public int deleteBusYearLevelByYearLevelIds(Long[] yearLevelIds)
    {
        return busYearLevelMapper.deleteBusYearLevelByYearLevelIds(yearLevelIds);
    }

    /**
     * 删除年级信息
     * 
     * @param yearLevelId 年级主键
     * @return 结果
     */
    @Override
    public int deleteBusYearLevelByYearLevelId(Long yearLevelId)
    {
        return busYearLevelMapper.deleteBusYearLevelByYearLevelId(yearLevelId);
    }
}
