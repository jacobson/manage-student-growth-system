package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusSemester;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * 学年学期Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@Mapper
public interface BusSemesterMapper extends BaseMapper<BusSemester>
{
    //获取当前学年学期id
    Long getCurrentSemesterIdByMapper();
    @Update("update bus_semester set has_report = 1 where semester_now = 1")
    void update();
    @Select("select semester_id from bus_semester where semester_now = 1 and has_report = 1")
    Long exitReport();
}
