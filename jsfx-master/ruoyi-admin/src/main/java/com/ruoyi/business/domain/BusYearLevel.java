package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * 年级对象 bus_year_level
 * 
 * @author ruoyi
 * @date 2022-11-12
 */
@Data
public class BusYearLevel
{
    private static final long serialVersionUID = 1L;

    /** 年级id */
    private Long yearLevelId;

    /** 年级名称 */
    @Excel(name = "年级名称")
    private String yearLevelName;


}
