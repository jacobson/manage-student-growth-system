package com.ruoyi.business.Dto;

import lombok.Data;

@Data
public class UserGalleryDto {
    private Long userId;
    private String galleryName;
    private Long galleryId;
}
