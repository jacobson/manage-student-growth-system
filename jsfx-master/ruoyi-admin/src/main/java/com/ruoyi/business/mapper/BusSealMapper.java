package com.ruoyi.business.mapper;

public interface BusSealMapper {
    String getSealByUserId(Long userId);
}
