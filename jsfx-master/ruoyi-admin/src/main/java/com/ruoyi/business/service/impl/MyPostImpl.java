package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.MyPost;
import com.ruoyi.business.mapper.MyPostMapper;
import com.ruoyi.business.service.IMyPostService;
import org.springframework.stereotype.Service;

@Service
public class MyPostImpl extends ServiceImpl<MyPostMapper, MyPost> implements IMyPostService {
}
