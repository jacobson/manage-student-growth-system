package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.ParentsManageMent;

public interface ParentsManageMentMapper extends BaseMapper<ParentsManageMent> {
}
