package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.BusSubject;
import com.ruoyi.business.domain.MyPost;
import com.ruoyi.business.mapper.BusSubjectMapper;
import com.ruoyi.business.service.IBusSubjectService;
import com.ruoyi.business.service.IMyPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 学科Service业务层处理
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@Service
public class BusSubjectServiceImpl extends ServiceImpl<BusSubjectMapper,BusSubject> implements IBusSubjectService
{
    @Autowired
    private BusSubjectMapper busSubjectMapper;

    @Autowired
    private IMyPostService iMyPostService;

    //返还全部学科
    @Override
    public  List<BusSubject> getAllSubject() {
        return this.list();
    }
    //拿到所有学科的中文名称
    @Override
    public List<String> getAllSubjectName() {
        List<BusSubject> busSubjects = this.getAllSubject();
        List<String> subjectNameList = new ArrayList<>();
        for (BusSubject busSubject : busSubjects) {
            subjectNameList.add(busSubject.getSubjectName());
        }
        return subjectNameList;
    }

    @Override
    public boolean addSubject(BusSubject busSubject) {
       if (check(busSubject)){
           //先新增科目
           boolean save = this.save(busSubject);
           LambdaQueryWrapper<BusSubject> busSubjectLambdaQueryWrapper = new LambdaQueryWrapper<>();
           busSubjectLambdaQueryWrapper.eq(BusSubject::getSubjectName,busSubject.getSubjectName());
           BusSubject busSubjectInfo = this.getOne(busSubjectLambdaQueryWrapper);
           //拿到这个新增的科目ID
           Long subjectId = busSubjectInfo.getSubjectId();
           //再新增对应的Post信息
           //PostId = subject+1
           MyPost sysPost = new MyPost();
           sysPost.setPostSort(subjectId+1+"");
           sysPost.setPostId(subjectId+1);
           sysPost.setPostName(busSubject.getSubjectName()+"老师");
           boolean save2 = iMyPostService.save(sysPost);
           return save && save2;
       }else {
           //信息错误不允许新增，已有课程
           return false;
       }
    }
    //判断新增时到底有没有存在信息
    public boolean check(BusSubject busSubject){
        LambdaQueryWrapper<BusSubject> busSubjectLambdaQueryWrapper = new LambdaQueryWrapper<>();
        busSubjectLambdaQueryWrapper.eq(BusSubject::getSubjectName,busSubject.getSubjectName());
        BusSubject busSubjectInfo = this.getOne(busSubjectLambdaQueryWrapper);
        //可以增加
        return busSubjectInfo == null;
    }

    /**
     * 查询学科
     *
     * @param subjectId 学科主键
     * @return 学科
     */
    @Override
    public BusSubject selectBusSubjectBySubjectId(Long subjectId)
    {
        return busSubjectMapper.selectBusSubjectBySubjectId(subjectId);
    }

    /**
     * 查询学科列表
     *
     * @param busSubject 学科
     * @return 学科
     */
    @Override
    public List<BusSubject> selectBusSubjectList(BusSubject busSubject)
    {
        return busSubjectMapper.selectBusSubjectList(busSubject);
    }

    /**
     * 新增学科
     *
     * @param busSubject 学科
     * @return 结果
     */
    @Override
    public int insertBusSubject(BusSubject busSubject)
    {
        //新增一个学科，就新增一个POST信息

        return busSubjectMapper.insertBusSubject(busSubject);
    }

    /**
     * 修改学科
     *
     * @param busSubject 学科
     * @return 结果
     */
    @Override
    public int updateBusSubject(BusSubject busSubject)
    {
        return busSubjectMapper.updateBusSubject(busSubject);
    }

    /**
     * 批量删除学科
     *
     * @param subjectIds 需要删除的学科主键
     * @return 结果
     */
    @Override
    public int deleteBusSubjectBySubjectIds(Long[] subjectIds)
    {
        return busSubjectMapper.deleteBusSubjectBySubjectIds(subjectIds);
    }

    /**
     * 删除学科信息
     *
     * @param subjectId 学科主键
     * @return 结果
     */
    @Override
    public int deleteBusSubjectBySubjectId(Long subjectId)
    {   //同时删除这个学科的Post
        LambdaQueryWrapper<MyPost> sysPostLambdaQueryWrapper = new LambdaQueryWrapper<>();
        sysPostLambdaQueryWrapper.eq(MyPost::getPostId,subjectId+1);
        iMyPostService.remove(sysPostLambdaQueryWrapper);
        return busSubjectMapper.deleteBusSubjectBySubjectId(subjectId)  ;
    }

    @Override
    public Boolean exitSubject(String subjectName) {
        Long r = busSubjectMapper.exitSubject(subjectName);
        return r!=null;
    }
}
