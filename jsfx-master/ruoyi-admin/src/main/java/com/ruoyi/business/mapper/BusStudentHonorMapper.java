package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusStudentHonor;
import com.ruoyi.business.domain.HonorBody;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 学生荣誉Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@Mapper
public interface BusStudentHonorMapper extends BaseMapper<BusStudentHonor>
{
    /**
     * 查询学生荣誉
     * 
     * @param growthId 学生荣誉主键
     * @return 学生荣誉
     */
    public BusStudentHonor selectBusStudentHonorByGrowthId(Long growthId);

    /**
     * 查询学生荣誉列表
     * 
     * @param busStudentHonor 学生荣誉
     * @return 学生荣誉集合
     */
    public List<BusStudentHonor> selectBusStudentHonorList(BusStudentHonor busStudentHonor);

    /**
     * 新增学生荣誉
     * 
     * @param busStudentHonor 学生荣誉
     * @return 结果
     */
    public int insertBusStudentHonor(BusStudentHonor busStudentHonor);

    /**
     * 修改学生荣誉
     * 
     * @param busStudentHonor 学生荣誉
     * @return 结果
     */
    public int updateBusStudentHonor(BusStudentHonor busStudentHonor);

    /**
     * 删除学生荣誉
     * 
     * @param growthId 学生荣誉主键
     * @return 结果
     */
    public int deleteBusStudentHonorByGrowthId(Long growthId);

    /**
     * 批量删除学生荣誉
     * 
     * @param growthIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusStudentHonorByGrowthIds(Long[] growthIds);
    @Delete("delete from bus_student_honor")
    void clear();

    int confirmList(@Param("honorBody") String honorBody,@Param("studentId")Long studentId,@Param("semesterId")Long semesterId);

    List<HonorBody> getStudentHonor(List<Long> studentIds);
}
