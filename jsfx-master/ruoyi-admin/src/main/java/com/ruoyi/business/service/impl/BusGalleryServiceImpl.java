package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.Dto.UserGalleryDto;
import com.ruoyi.business.Util.IdUtil;
import com.ruoyi.business.domain.BusGallery;
import com.ruoyi.business.mapper.BusGalleryMapper;
import com.ruoyi.business.mapper.BusGradeGalleryMapper;
import com.ruoyi.business.service.IBusGalleryService;
import com.ruoyi.business.service.IBusParentStudentService;
import com.ruoyi.business.service.IBusSemesterService;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 图库Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@Service
public class BusGalleryServiceImpl extends ServiceImpl<BusGalleryMapper, BusGallery>  implements IBusGalleryService
{
    @Autowired
    private BusGalleryMapper busGalleryMapper;

    @Autowired
    private BusGradeGalleryMapper busGradeGalleryMapper;

    @Autowired
    private IBusSemesterService semesterService;

    @Autowired
    private IBusParentStudentService busParentStudentService;
    /**
     * 根据图库Id获取图片
     */
    @Override
    public List<String> getImagesByGalleryId(Long galleryId){
        List<String> imageNames = busGalleryMapper.getImagesByGalleryId(galleryId);
        return new ArrayList<>(imageNames);
    }

    /**
     * 新增图库
     * 
     * @param userGalleryDto 图库
     * @return 结果
     */
    @Override
    @Transactional
    public int insertBusUserGallery(UserGalleryDto userGalleryDto)
    {
        Long id = IdUtil.getId();
        userGalleryDto.setGalleryId(id);
        return busGalleryMapper.insertUserGallery(userGalleryDto);
    }


    @Override
    public void upload(Long galleryId, MultipartFile file) throws IOException, InvalidExtensionException {
        String name = FileUploadUtils.upload(RuoYiConfig.getGalleryPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
        busGalleryMapper.recordImages(galleryId,name);
    }
    @Override
    public void delete(String imgName){

        busGalleryMapper.deleteByName(imgName);
    }

    /**
     * 获取用户图库id
     * @return 图库id
     */
    @Override
    public Long getUserGallery(Long userId){
        return busGalleryMapper.getUserGalleryByUserId(userId);
    }

    /**
     * 获取学生家长及当前班级的图库
     * @param studentId
     * @return
     */
    @Override
    public Map<String,List<String>> getPhotosByStuId(Long studentId) {
        Long currentSemesterId = semesterService.getCurrentSemesterId();
        //根据studentId获取家长的userId
        List<Long> parentUserIds = busParentStudentService.getParentUserIds(studentId);
        List<Long> userGalleryIds = new ArrayList<>();
        parentUserIds.forEach((id)->{
            userGalleryIds.add(busGalleryMapper.getUserGalleryByUserId(id));
        });
        List<Long> galleryIds = busGradeGalleryMapper.getGradeGalleryByStuId(studentId, currentSemesterId);

        List<String> gradePhotos = new ArrayList<>();
        List<String> userPhotos = new ArrayList<>();
        galleryIds.forEach((id)->{
            //根据图库id获取所有图片存入photos列表中
            gradePhotos.addAll(getImagesByGalleryId(id));
        });
        userGalleryIds.forEach((id)->{
            userPhotos.addAll(getImagesByGalleryId(id));
        });
        HashMap<String, List<String>> resMap = new HashMap<>();
        resMap.put("userGallery",userPhotos);
        resMap.put("gradeGallery",gradePhotos);
        return resMap;
    }
}
