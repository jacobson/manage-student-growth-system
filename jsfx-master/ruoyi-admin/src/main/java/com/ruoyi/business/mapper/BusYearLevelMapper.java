package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusYearLevel;

import java.util.List;

/**
 * 年级Mapper接口
 * 
 * @author ruoyi
 * @date 2022-11-12
 */
public interface BusYearLevelMapper extends BaseMapper<BusYearLevel>
{
    /**
     * 查询年级
     * 
     * @param yearLevelId 年级主键
     * @return 年级
     */
    public BusYearLevel selectBusYearLevelByYearLevelId(Long yearLevelId);

    /**
     * 查询年级列表
     * 
     * @param busYearLevel 年级
     * @return 年级集合
     */
    public List<BusYearLevel> selectBusYearLevelList(BusYearLevel busYearLevel);

    /**
     * 新增年级
     * 
     * @param busYearLevel 年级
     * @return 结果
     */
    public int insertBusYearLevel(BusYearLevel busYearLevel);

    /**
     * 修改年级
     * 
     * @param busYearLevel 年级
     * @return 结果
     */
    public int updateBusYearLevel(BusYearLevel busYearLevel);

    /**
     * 删除年级
     * 
     * @param yearLevelId 年级主键
     * @return 结果
     */
    public int deleteBusYearLevelByYearLevelId(Long yearLevelId);

    /**
     * 批量删除年级
     * 
     * @param yearLevelIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusYearLevelByYearLevelIds(Long[] yearLevelIds);
}
