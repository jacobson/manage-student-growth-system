package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.BusCommonSeal;
import com.ruoyi.business.domain.BusUserSeal;
import com.ruoyi.business.mapper.BusUserSealMapper;
import com.ruoyi.business.service.IBusUserSealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户印章Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-31
 */
@Service
public class BusUserSealServiceImpl extends ServiceImpl<BusUserSealMapper,BusUserSeal> implements IBusUserSealService
{
    @Autowired
    private BusUserSealMapper busUserSealMapper;


    /**
     * 新增用户印章
     * @param userId
     * @param sealUrl
     * @return
     */
    @Override
    public int insertBusUserSeal(Long userId, String sealUrl) {
        return busUserSealMapper.insertBusUserSeal(userId, sealUrl);
    }

    @Override
    public String getSealByUserId(Long userId) {
        return busUserSealMapper.getSealByUserId(userId);
    }
    @Override
    public List<BusCommonSeal> getCommonSeals() {
        return busUserSealMapper.getCommonSeals();
    }
}
