package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.BusCommonSeal;
import com.ruoyi.business.domain.BusUserSeal;

import java.util.List;

/**
 * 用户印章Service接口
 * 
 * @author ruoyi
 * @date 2022-10-31
 */
public interface IBusUserSealService extends IService<BusUserSeal>
{

    int insertBusUserSeal(Long userId, String url);
    String getSealByUserId(Long userId);
    List<BusCommonSeal> getCommonSeals();
}
