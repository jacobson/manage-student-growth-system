package com.ruoyi.business.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.business.domain.SemesterManageMent;
import com.ruoyi.business.service.ISemesterService;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/business/semesters")
public class SemesterManageMentController {

    @Autowired
    private ISemesterService semesterService;

    /**
     * 后台学期学年管理查询列表
     * @return
     */
    @GetMapping("/semesterslist")
    public AjaxResult semesterslist(int pageSize,int pageNum) {
        List<SemesterManageMent> semesterManageMents = semesterService.BusSemesterList();

        // 将当前页的数据放进busGradeTeacherDtos
        List<SemesterManageMent> SemesterManageMents =new ArrayList<>();
        //结果集
        int size = semesterManageMents.size();
        if (size>0){
            //解析分页参数
            if (pageSize > size) {
                pageSize = size;
            }
            // 求出最大页数，防止currentPage越界
            int maxPage = size % pageSize == 0 ? size / pageSize : size / pageSize + 1;
            if (pageNum > maxPage) {
                pageNum = maxPage;
            }
            // 当前页第一条数据的下标
            int curIdx = pageNum > 1 ? (pageNum - 1) * pageSize : 0;
            // 将当前页的数据放进busGradeTeacherDtos

            for (int i = 0; i < pageSize && curIdx + i < size; i++) {
                SemesterManageMents.add(semesterManageMents.get(curIdx + i));
            }
        }


        return AjaxResult.success(SemesterManageMents);
    }

    /**
     * 新增数据
     * @return
     */
    @PostMapping("/addsemesters")
    public AjaxResult addsemesters(@RequestBody SemesterManageMent semesterManageMent) {

        boolean b = semesterService.addBusSemester(semesterManageMent);
        return AjaxResult.success(b);
    }

    /**
     * 后台学期学年管理数据回显
     * @return
     */
    @GetMapping("/echosemester")
    public AjaxResult EchoSemester(Long semesterId) {
        List<SemesterManageMent> semesterManageMents = semesterService.echoBusSemesterManageInfo(semesterId);
        return AjaxResult.success(semesterManageMents);
    }

    /**
     * 修改数据
     * @return
     */
    @PostMapping("/updatesemester")
    public AjaxResult Updatesemesters(@RequestBody SemesterManageMent semesterManageMent) {
        boolean remove=false;
        boolean save=false;
        Long semesterId = semesterManageMent.getSemesterId();
        LambdaQueryWrapper<SemesterManageMent> lqw1 = new LambdaQueryWrapper<>();
            lqw1.eq(SemesterManageMent::getSemesterId,semesterId);
            List<SemesterManageMent> list = semesterService.list(lqw1);
            for (SemesterManageMent manageMent : list) {
                    Long semesterId1 = semesterManageMent.getSemesterId();
                    Long semesterYear = semesterManageMent.getSemesterYear();
                    String semesterName = semesterManageMent.getSemesterName();
                    String semesterNow = manageMent.getSemesterNow();
                    Date createTime = manageMent.getCreateTime();
                    SemesterManageMent fz1 = new SemesterManageMent();
                    fz1.setSemesterId(semesterId1);
                    fz1.setSemesterYear(semesterYear);
                    fz1.setSemesterName(semesterName);
                    fz1.setSemesterNow(semesterNow);
                    fz1.setCreateTime(createTime);

                remove = semesterService.remove(lqw1);
                save = semesterService.save(fz1);
            }

        return AjaxResult.success(remove && save);
    }



    /**
     * Excel文件导入
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("/import")
    public AjaxResult importData(MultipartFile file) throws Exception {
        boolean flag = false;

        ExcelUtil<SemesterManageMent> excelUtil = new ExcelUtil<>(SemesterManageMent.class);

        List<SemesterManageMent> semesterManageMents = excelUtil.importExcel(file.getInputStream());
        for (SemesterManageMent semesterManageMent : semesterManageMents) {
            flag = semesterService.addBusSemester(semesterManageMent);
        }
        return AjaxResult.success(flag);
    }

    /**
     * 导出文件
     * @param response
     * @param semesterManageMent
     */
    @GetMapping("/export")
    public void exportData(HttpServletResponse response, SemesterManageMent semesterManageMent){
        List<SemesterManageMent> semesterManageMents = semesterService.BusSemesterList();
        ExcelUtil<SemesterManageMent> excelUtil = new ExcelUtil<SemesterManageMent>(SemesterManageMent.class);
        excelUtil.exportExcel(response,semesterManageMents,"学年学期数据");
    }

}
