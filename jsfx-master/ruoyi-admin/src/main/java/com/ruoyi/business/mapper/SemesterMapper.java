package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.SemesterManageMent;
import org.apache.ibatis.annotations.Update;

public interface SemesterMapper extends BaseMapper<SemesterManageMent> {
    @Update("update bus_semester set semester_id = 0")
    void resetSemester();
    void createSemester(SemesterManageMent semesterManageMent);
}
