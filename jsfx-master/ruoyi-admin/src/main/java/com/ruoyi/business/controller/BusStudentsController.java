package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.business.service.IBusStudentsService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 学生管理Controller
 * 
 * @author ruoyi
 * @date 2022-10-21
 */
@RestController
@RequestMapping("/business/students")
public class BusStudentsController extends BaseController
{
    @Autowired
    private IBusStudentsService busStudentsService;

    /**
     * 查询指定班级学生列表
     */
    @GetMapping("/list/{gradeId}")
    public AjaxResult list(@PathVariable Long gradeId)
    {
        List<BusStudents> list = busStudentsService.
                selectBusStudentsByGradeId(gradeId);
        return AjaxResult.success(list);
    }

    /**
     * 导出学生管理列表
     */
    @PreAuthorize("@ss.hasPermi('business:students:export')")
    @Log(title = "学生管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusStudents busStudents)
    {
        List<BusStudents> list = busStudentsService.selectBusStudentsList(busStudents);
        ExcelUtil<BusStudents> util = new ExcelUtil<BusStudents>(BusStudents.class);
        util.exportExcel(response, list, "学生管理数据");
    }

    /**
     * 获取学生管理详细信息
     */
//    @PreAuthorize("@ss.hasPermi('business:students:query')")
    @GetMapping(value = "/{stuId}")
    public AjaxResult getInfo(@PathVariable("stuId") Long stuId)
    {
        return AjaxResult.success(busStudentsService.
                selectBusStudentsByStuId(stuId));
    }

    /**
     * 新增学生管理
     */
    @PreAuthorize("@ss.hasPermi('business:students:add')")
    @Log(title = "学生管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusStudents busStudents)
    {
        return toAjax(busStudentsService.insertBusStudents(busStudents));
    }

    /**
     * 修改学生管理
     */
    @PreAuthorize("@ss.hasPermi('business:students:edit')")
    @Log(title = "学生管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusStudents busStudents)
    {
        return toAjax(busStudentsService.updateBusStudents(busStudents));
    }

    /**
     * 删除学生管理
     */
    @PreAuthorize("@ss.hasPermi('business:students:remove')")
    @Log(title = "学生管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{stuIds}")
    public AjaxResult remove(@PathVariable Long[] stuIds)
    {
        return toAjax(busStudentsService.deleteBusStudentsByStuIds(stuIds));
    }

    /**
     * 修改学生头像
     */
    @PostMapping("/avatar/{studentId}")
    public AjaxResult editAvatar(@PathVariable Long studentId, MultipartFile file) throws IOException, InvalidExtensionException {
        busStudentsService.editAvatar(studentId,file);
        return AjaxResult.success("修改成功");
    }
}
