package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.Dto.BusStudentParentsDto;
import com.ruoyi.business.domain.BusGradeStudent;
import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.business.mapper.BusGradeStudentMapper;
import com.ruoyi.business.service.IBusGradeStudentService;
import com.ruoyi.business.service.IBusStudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 班级学生Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-24
 */
@Service
public class BusGradeStudentServiceImpl extends ServiceImpl<BusGradeStudentMapper,BusGradeStudent> implements IBusGradeStudentService
{
    @Autowired
    private BusGradeStudentMapper busGradeStudentMapper;

    @Autowired
    private IBusStudentsService iBusStudentsService;

    @Autowired
    private IBusGradeStudentService iBusGradeStudentService;

    //返回指定班级的人数
    @Override
    public int getCountStudents(Long gradeId) {
        LambdaQueryWrapper<BusGradeStudent> busGradeStudentWrapper = new LambdaQueryWrapper<>();
        busGradeStudentWrapper.eq(BusGradeStudent::getGradeId,gradeId);
        //返回这个班级的人数
        return this.count(busGradeStudentWrapper);
    }
    //拿到这个学生对应的班级ID
    @Override
    public Long getGradeIdByStudentId(Long StudentId) {
        LambdaQueryWrapper<BusGradeStudent> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(BusGradeStudent::getStudentId,StudentId);
        BusGradeStudent busGradeStudent = getOne(lambdaQueryWrapper);
        Long gradeId = busGradeStudent.getGradeId();
        return gradeId;
    }


    @Override
    public List<BusStudents> getStudentInfoByGradeId(Long gradeId) {
        //拿到这个班级的所有学生Id
        LambdaQueryWrapper<BusGradeStudent> busGradeStudentLambdaQueryWrapper = new LambdaQueryWrapper<>();
        busGradeStudentLambdaQueryWrapper.eq(BusGradeStudent::getGradeId,gradeId);
        //拿到指定班级ID的所有对象
        List<BusGradeStudent> busGradeStudents = iBusGradeStudentService.list(busGradeStudentLambdaQueryWrapper);
        if (!busGradeStudents.isEmpty()){
            //通过流的方式收集学生对象
            return busGradeStudents.stream().map(item -> {
                Long studentId = item.getStudentId();
                LambdaQueryWrapper<BusStudents> busStudentsLambdaQueryWrapper = new LambdaQueryWrapper<>();
                busStudentsLambdaQueryWrapper.eq(BusStudents::getStuId, studentId);
                //拿到这个学生的信息
                BusStudents students = iBusStudentsService.getOne(busStudentsLambdaQueryWrapper);
                return students;
            }).collect(Collectors.toList());
        }else {
            return null;
        }
    }

    /**
     * 查询班级学生
     * 
     * @param gradeId 班级学生主键
     * @return 班级学生
     */
    @Override
    public BusGradeStudent selectBusGradeStudentByGradeId(Long gradeId)
    {
        return busGradeStudentMapper.selectBusGradeStudentByGradeId(gradeId);
    }

    /**
     * 把学生添加到班级
     * @param busStudentParentsDto 传入的数据
     * @return
     */
    @Override
    public boolean addBusGradeStudent(BusStudentParentsDto busStudentParentsDto) {
        Long gradeId = busStudentParentsDto.getGradeId();
        String idCard = busStudentParentsDto.getIdCard();
        LambdaQueryWrapper<BusStudents> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BusStudents::getIdCard,idCard);
        BusStudents busStudentsServiceOne = iBusStudentsService.getOne(queryWrapper);
        Long stuId = busStudentsServiceOne.getStuId();
        BusGradeStudent busGradeStudent = new BusGradeStudent();
        busGradeStudent.setGradeId(gradeId);
        busGradeStudent.setStudentId(stuId);
        return iBusGradeStudentService.save(busGradeStudent);
    }

    @Override
    public BusGradeStudent selectBusGradeStudentByStudentId(Long studentId) {
        LambdaQueryWrapper<BusGradeStudent> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BusGradeStudent::getStudentId,studentId);
        BusGradeStudent one = iBusGradeStudentService.getOne(queryWrapper);
        return one;
    }

    /**
     * 查询班级学生列表
     * 
     * @param busGradeStudent 班级学生
     * @return 班级学生
     */
    @Override
    public List<BusGradeStudent> selectBusGradeStudentList(BusGradeStudent busGradeStudent)
    {
        return busGradeStudentMapper.selectBusGradeStudentList(busGradeStudent);
    }

    /**
     * 修改学生班级信息
     * @param busStudents 班级学生
     * @return
     */
    @Override
    public int updateBusGradeStudentZ(BusStudents busStudents) {
        //修改学生班级表中的信息
        BusGradeStudent busGradeStudent = new BusGradeStudent();
        busGradeStudent.setStudentId(busStudents.getStuId());
        busGradeStudent.setGradeId(busStudents.getGradeId());
        iBusGradeStudentService.deleteBusGradeStudentByStudentId(busGradeStudent.getStudentId());
        iBusGradeStudentService.save(busGradeStudent);
        return 1;
    }

    /**
     * 新增班级学生
     * 
     * @param busGradeStudent 班级学生
     * @return 结果
     */
    @Override
    public int insertBusGradeStudent(BusGradeStudent busGradeStudent)
    {
        return busGradeStudentMapper.insertBusGradeStudent(busGradeStudent);
    }

    /**
     * 修改班级学生
     * 
     * @param busGradeStudent 班级学生
     * @return 结果
     */
    @Override
    public int updateBusGradeStudent(BusGradeStudent busGradeStudent)
    {
        return busGradeStudentMapper.updateBusGradeStudent(busGradeStudent);
    }

    /**
     * 批量删除班级学生
     * 
     * @param gradeIds 需要删除的班级学生主键
     * @return 结果
     */
    @Override
    public int deleteBusGradeStudentByGradeIds(Long[] gradeIds)
    {
        return busGradeStudentMapper.deleteBusGradeStudentByGradeIds(gradeIds);
    }

    /**
     * 删除班级学生信息
     * 
     * @param gradeId 班级学生主键
     * @return 结果
     */
    @Override
    public int deleteBusGradeStudentByGradeId(Long gradeId)
    {
        return busGradeStudentMapper.deleteBusGradeStudentByGradeId(gradeId);
    }


    /**
     * 删除班级学生信息
     *
     * @param studentId 班级学生主键
     * @return 结果
     */
    @Override
    public int deleteBusGradeStudentByStudentId(Long studentId) {
        return busGradeStudentMapper.deleteBusGradeStudentByStudentId(studentId);
    }

    @Override
    public boolean insertBusGradeStudentByMyself(BusStudentParentsDto busStudentParentsDto) {
        Long gradeId = busStudentParentsDto.getGradeId();
        Long stuId = busStudentParentsDto.getStuId();
        BusGradeStudent busGradeStudent =new BusGradeStudent();
        busGradeStudent.setGradeId(gradeId);
        busGradeStudent.setStudentId(stuId);
        return  iBusGradeStudentService.save(busGradeStudent);
    }


}
