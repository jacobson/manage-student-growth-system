package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.Dto.GradeTeacherHistory;
import com.ruoyi.business.domain.BusGradeTeacher;
import com.ruoyi.business.domain.SubjectTeacher;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 班级教师Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-22
 */
@Mapper
public interface BusGradeTeacherMapper extends BaseMapper<BusGradeTeacher>
{
    /**
     * 查询班级教师
     * 
     * @param gradeId 班级教师主键
     * @return 班级教师
     */
    public BusGradeTeacher selectBusGradeTeacherByGradeId(Long gradeId);

    /**
     * 查询班级教师列表
     * 
     * @param busGradeTeacher 班级教师
     * @return 班级教师集合
     */
    public List<BusGradeTeacher> selectBusGradeTeacherList(BusGradeTeacher busGradeTeacher);

    /**
     * 新增班级教师
     * 
     * @param busGradeTeacher 班级教师
     * @return 结果
     */
    public int insertBusGradeTeacher(BusGradeTeacher busGradeTeacher);

    /**
     * 修改班级教师
     * 
     * @param busGradeTeacher 班级教师
     * @return 结果
     */
    public int updateBusGradeTeacher(BusGradeTeacher busGradeTeacher) throws DataAccessException;

    /**
     * 删除班级教师
     * 
     * @param gradeId 班级教师主键
     * @return 结果
     */
    public int deleteBusGradeTeacherByGradeId(Long gradeId);

    /**
     * 批量删除班级教师
     * 
     * @param gradeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusGradeTeacherByGradeIds(Long[] gradeIds);

    List<Map<String,Long>> getGradeByUserId(Long userId);

    Set<GradeTeacherHistory> getHistoryGradeByUserId(Long userId);

    Set<GradeTeacherHistory> getCurrentGradeByUserId(Long userId);

    List<SubjectTeacher> getGradeTeacher(Long gradeId);

    List<Long> getLeadGrade(Long userId);
}
