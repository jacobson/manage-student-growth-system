package com.ruoyi.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 学生成长报告项对象 bus_student_growth_item
 * 
 * @author ruoyi
 * @date 2022-10-23
 */
@Data
public class BusStudentGrowthItem implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 成长报告项id */
    private Long growthItemId;

    /** 成长报告id */
    @Excel(name = "成长报告id")
    private Long growthId;

    /** 成长报告环节项 */
    @Excel(name = "成长报告环节项")
    private String growthKey;

    /** 成长报告环节项值 */
    @Excel(name = "成长报告环节项值")
    private String growthValue;

    /** 状态（制作中、已定稿、未开始） */
    private String status;
    @TableField(exist = false)
    private Long studentId;
    /** 该环节的第几张图片*/
    @TableField(exist = false)
    private String remark;
    public BusStudentGrowthItem(){};
    public BusStudentGrowthItem(Long studentId,String growthKey,String path,
                                String remark,Long currentGrowthId){
        this.studentId = studentId;
        this.growthKey = growthKey;
        this.growthValue = path;
        this.remark = remark;
        this.growthId = currentGrowthId;
    }
    public BusStudentGrowthItem(Long currentGrowthId,Long studentId,
                                String growthKey,String remark){
        this.growthId = currentGrowthId;
        this.studentId = studentId;
        this.growthKey = growthKey;
        this.remark = remark;
    }

}
