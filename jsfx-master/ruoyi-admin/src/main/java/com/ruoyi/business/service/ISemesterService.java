package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.SemesterManageMent;

import java.util.List;

public interface ISemesterService extends IService<SemesterManageMent> {
    /**
     * 后台学期学年管理查询列表
     * @return
     */
    public List<SemesterManageMent> BusSemesterList();

    /**
     * 新增学年学期
     * @param semesterManageMent
     * @return
     */
    public boolean addBusSemester(SemesterManageMent semesterManageMent);


    //编辑条件回显
    List<SemesterManageMent> echoBusSemesterManageInfo(Long semesterId);


}
