package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.BusGradeStudentHistory;

import java.util.List;

/**
 * 班级学生Service接口
 * 
 * @author ruoyi
 * @date 2022-11-12
 */
public interface IBusGradeStudentHistoryService extends IService<BusGradeStudentHistory>
{
    /**
     * 查询班级学生
     * 
     * @param gradeId 班级学生主键
     * @return 班级学生
     */
    public BusGradeStudentHistory selectBusGradeStudentHistoryByGradeId(Long gradeId);

    /**
     * 查询班级学生列表
     * 
     * @param busGradeStudentHistory 班级学生
     * @return 班级学生集合
     */
    public List<BusGradeStudentHistory> selectBusGradeStudentHistoryList(BusGradeStudentHistory busGradeStudentHistory);

    /**
     * 新增班级学生
     * 
     * @param busGradeStudentHistory 班级学生
     * @return 结果
     */
    public int insertBusGradeStudentHistory(BusGradeStudentHistory busGradeStudentHistory);

    /**
     * 修改班级学生
     * 
     * @param busGradeStudentHistory 班级学生
     * @return 结果
     */
    public int updateBusGradeStudentHistory(BusGradeStudentHistory busGradeStudentHistory);

    /**
     * 批量删除班级学生
     * 
     * @param gradeIds 需要删除的班级学生主键集合
     * @return 结果
     */
    public int deleteBusGradeStudentHistoryByGradeIds(Long[] gradeIds);

    /**
     * 删除班级学生信息
     * 
     * @param gradeId 班级学生主键
     * @return 结果
     */
    public int deleteBusGradeStudentHistoryByGradeId(Long gradeId);
}
