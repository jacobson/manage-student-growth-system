package com.ruoyi.business.Util;

import com.ruoyi.business.service.IBusStudentGrowthItemService;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.common.RedisKeyPrefix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
public class RedisValue {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private IBusStudentGrowthItemService growthItemService;
    public Boolean getReportFinish(){
        Boolean finishFlag = (Boolean) redisTemplate.opsForValue()
                .get(RedisKeyPrefix.CURRENT_REPORT_FINISH_FLAG);
        if (finishFlag == null){
            Date endTime = growthItemService.getEndTime();
            if (DateUtils.getNowDate().after(endTime)){
                finishFlag = true;
                redisTemplate.opsForValue()
                        .set(RedisKeyPrefix.CURRENT_REPORT_FINISH_FLAG,true,3, TimeUnit.DAYS);
            }else{
                finishFlag = false;
            }
        }
        return finishFlag;
    }
}
