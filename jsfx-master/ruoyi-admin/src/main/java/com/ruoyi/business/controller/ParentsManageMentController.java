package com.ruoyi.business.controller;

import com.ruoyi.business.domain.ParentsManageMent;
import com.ruoyi.business.service.IParentsManageMentService;
import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/parents/management")
public class ParentsManageMentController {
    @Autowired
    private IParentsManageMentService parentsManageMentService;

    //查询家长的列表
    @GetMapping("/parentslists")
    public AjaxResult ParentManageMent() {
        List<ParentsManageMent> parentsManageMents = parentsManageMentService.allParentsManages();
        return AjaxResult.success(parentsManageMents);
    }

    //删除家长
    @DeleteMapping("/delectparent")
    public AjaxResult delectParentsManages(Long UserId){
        boolean b = parentsManageMentService.deleteParentsManageInfo(UserId);
        return AjaxResult.success(b);
    }

    //条件查询
    @GetMapping("/conditionparentsmanages")
    public AjaxResult conditionParentsManages(String name,String phonenumber){
        List<ParentsManageMent> parentsManageMents = parentsManageMentService.conditionParentsManages(name, phonenumber);
        return AjaxResult.success(parentsManageMents);
    }

    //新增家长
    @PostMapping("/addparents")
    public AjaxResult addParentsManage(@RequestBody ParentsManageMent parentsManageMent){
        boolean b = parentsManageMentService.addParentsManageInfo(parentsManageMent);
        return AjaxResult.success(b);
    }

    //编辑家长
    //回显家长数据
    @GetMapping("/echoParents")
    public AjaxResult echoParentsManageInfo(Long UserId){
        List<ParentsManageMent> parentsManageMents = parentsManageMentService.echoParentsManageInfo(UserId);
        //最后教师的结果集
        return AjaxResult.success(parentsManageMents);
    }

    //修改家长的信息
    @PostMapping("/updateParents")
    public AjaxResult updateParentsManage(@RequestBody ParentsManageMent parentsManageMent){
        //先删除
        boolean b = parentsManageMentService.deleteParentsManageInfo(parentsManageMent.getUserId());
        //再新增
        boolean b1 = parentsManageMentService.addParentsManageInfo(parentsManageMent);
        return AjaxResult.success(b && b1);
    }
}
