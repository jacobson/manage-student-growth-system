package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusGrade;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 班级Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-22
 */
@Mapper
public interface BusGradeMapper extends BaseMapper<BusGrade>
{
    /**
     * 查询班级
     * 
     * @param gradeId 班级主键
     * @return 班级
     */
    public BusGrade selectBusGradeByGradeId(Long gradeId);


    /**
     * 修改班级
     *
     * @param busGrade 班级
     * @return 结果
     */
    public int myUpdateBusGrade(BusGrade busGrade);
    /**
     * 查询班级列表
     * 
     * @param busGrade 班级
     * @return 班级集合
     */
    public List<BusGrade> selectBusGradeList(BusGrade busGrade);

    /**
     * 新增班级
     * 
     * @param busGrade 班级
     * @return 结果
     */
    public int insertBusGrade(BusGrade busGrade);

    /**
     * 修改班级
     * 
     * @param busGrade 班级
     * @return 结果
     */
    public int updateBusGrade(BusGrade busGrade);

    /**
     * 删除班级
     * 
     * @param gradeId 班级主键
     * @return 结果
     */
    public int deleteBusGradeByGradeId(Long gradeId);

    /**
     * 批量删除班级
     * 
     * @param gradeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusGradeByGradeIds(Long[] gradeIds);
    @Select("select student_id from bus_grade_student")
    List<Long> getAll();
    @Select("select grade_id from bus_grade_student where student_id =#{studentId}")
    Long getGradeIdByStuId(Long studentId);
    @Select("select user_id from bus_grade where grade_id=#{gradeId}")
    Long getUserIdByGradeId(Long gradeId);

    @Select("select * from bus_grade where semester_id = #{semesterId}")
    List<BusGrade> getClassBySemesterId(Long semesterId);


}
