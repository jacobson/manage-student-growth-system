package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.MusicManage;
import com.ruoyi.business.mapper.MusicManageMapper;
import com.ruoyi.business.service.IMusicManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MusicManageServiceImpl extends ServiceImpl<MusicManageMapper, MusicManage> implements IMusicManageService {

    @Autowired
    private IMusicManageService musicManageService;

    /**
     * 上传音乐
     * @return
     */
    @Override
    public List<MusicManage> selectMusicList() {
        List<MusicManage> list = musicManageService.list();
        return list;
    }
    
    /**
     * 根据musicId删除音乐
     * @param musicId
     * @return
     */
    @Override
    public boolean delete(Long musicId) {
        LambdaQueryWrapper<MusicManage> mmlqw = new LambdaQueryWrapper<>();
        mmlqw.eq(MusicManage::getMusicId,musicId);
        boolean remove = musicManageService.remove(mmlqw);
        return remove;
    }

    /**
     * 添加音乐
     * @param musicManage
     * @return
     */
    @Override
    public boolean addMusic(MusicManage musicManage) {
        boolean save = musicManageService.save(musicManage);
        return save;
    }

    /**
     * 条件查询
     * @param musicname
     * @return
     */
    @Override
    public List<MusicManage> conditionMusicManages(String musicname) {
        LambdaQueryWrapper<MusicManage> mmlqw = new LambdaQueryWrapper<>();
        mmlqw.like(!musicname.isEmpty(),MusicManage::getMusicName,musicname);
        List<MusicManage> list = musicManageService.list(mmlqw);
        return list;
    }
}
