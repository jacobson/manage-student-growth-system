package com.ruoyi.business.controller;

import com.ruoyi.business.Dto.ClassGradeInfoDto;
import com.ruoyi.business.service.IBusGradeService;
import com.ruoyi.business.service.IClassManageService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/classManage")
public class ClassManageController extends BaseController {
    //班级表
    @Autowired
    private IBusGradeService iBusGradeService;

    @Autowired
    private IClassManageService iClassManageService;


    //返回班级年级
    @GetMapping("/nowYearId")
    public TableDataInfo getNowYearId(){
        return getDataTable(iClassManageService.getAllYearId());
    }

    /**
     * 获取某个指定班级的所有信息
     * @param gradeId 指定班级ID
     * @return
     */
    @GetMapping
    public ClassGradeInfoDto getOneClassInfo(Long gradeId){
        return iClassManageService.getOneClassManageInfo(gradeId);
    }

    //返回该年级所有班级的基本信息
    @GetMapping("/allGradeInfo")
    public TableDataInfo allGradeInfo(String yearId) {
        System.out.println(yearId+"================================================");
        return getDataTable(iClassManageService.getListClassManageInfo(yearId));
    }

    //增加班级以及批量增加班级
    @GetMapping("/addClasses")
    public AjaxResult addClasses(String grade, int count){
        return iBusGradeService.addClasses(grade, count);
    }

    //删除指定班级
    @DeleteMapping("/remove")
    public AjaxResult removeClass(Long gradeId){
        return iBusGradeService.deleteBusGradeByGradeIdT(gradeId);
    }
    //批量删除指定班级
    @DeleteMapping("/removeList/{ids}")
    public AjaxResult removeClass(@PathVariable Long[] ids){

        return iBusGradeService.deleteBusGradeByGradeIds(ids);

    }


}
