package com.ruoyi.business.Dto;

import lombok.Data;

import java.util.List;

@Data
public class ClassGradeInfoDto {

    //班级ID
    private Long gradeId;

    //班主任名称
    private String headTeacherName;

    //班级人数
    private int countStudents;

    //该班级的学生信息及其父母
    private List<BusStudentParentsDtoT> StudentWithParentsInfo;
    public ClassGradeInfoDto(Long gradeId, String headTeacherName, int countStudents) {
        this.gradeId = gradeId;
        this.headTeacherName = headTeacherName;
        this.countStudents = countStudents;
    }
}
