package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.Dto.GradeTeacherHistory;
import com.ruoyi.business.Util.IdUtil;
import com.ruoyi.business.domain.BusGradeGallery;
import com.ruoyi.business.mapper.BusGalleryMapper;
import com.ruoyi.business.mapper.BusGradeGalleryMapper;
import com.ruoyi.business.service.IBusGradeGalleryService;
import com.ruoyi.business.service.IBusGradeTeacherService;
import com.ruoyi.business.service.IBusSemesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * 班级图库Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@Service
public class BusGradeGalleryServiceImpl extends ServiceImpl<BusGradeGalleryMapper,BusGradeGallery> implements IBusGradeGalleryService
{
    @Autowired
    private BusGradeGalleryMapper busGradeGalleryMapper;

    //学期学年表
    @Autowired
    private IBusSemesterService busSemesterService;

    @Autowired
    private IBusGradeTeacherService teacherService;

    @Autowired
    private BusGalleryMapper galleryMapper;
    /**
     * 根据班主任id获取所有带过班级的图库
     * @param userId 班主任id
     * @return 图库列表
     */
    @Override
    public List<BusGradeGallery> getGradeGalleryByUserId(Long userId) {
        //获取到该班主任各学期所带的班级
        Set<GradeTeacherHistory> historyGradeByUserId = teacherService.
                getHistoryGradeByUserId(userId);

        //根据各学期所带班级的获取图库信息存入list
        List<List<BusGradeGallery>> busGradeGalleries = new ArrayList<>();
        historyGradeByUserId.forEach((item)->{
            busGradeGalleries.add(busGradeGalleryMapper.
                    getGradeGallery(item));
        });
        List<BusGradeGallery> gradeGalleries = new ArrayList<>();
        busGradeGalleries.forEach(gradeGalleries::addAll);
        return gradeGalleries;
    }

    /**
     * 新增班级图库
     * 
     * @param busGradeGallery 班级图库
     * @return 结果
     */
    @Override
    @Transactional
    public void insertBusGradeGallery(BusGradeGallery busGradeGallery)
    {
        Long id = IdUtil.getId();
        busGradeGallery.setGalleryId(id);
        Long currentSemesterId = busSemesterService.getCurrentSemesterId();
        busGradeGallery.setSemesterId(currentSemesterId);
        busGradeGalleryMapper.insertBusGradeGallery(busGradeGallery);

    }

    /**
     * 修改班级图库
     * 
     * @param busGradeGallery 班级图库
     * @return 结果
     */
    @Override
    public int updateBusGradeGallery(BusGradeGallery busGradeGallery)
    {
        return busGradeGalleryMapper.updateBusGradeGallery(busGradeGallery);
    }

    /**
     * 批量删除班级图库
     * 
     * @param galleryIds 需要删除的班级图库主键
     * @return 结果
     */
    @Override
    public int deleteBusGradeGalleryByGalleryIds(Long[] galleryIds)
    {
        //删除图库中所有图片
        for(Long galleryId : galleryIds){
            galleryMapper.clearImages(galleryId);
        }
        return busGradeGalleryMapper.
                deleteBusGradeGalleryByGalleryIds(galleryIds);
    }

    /**
     * 删除班级图库信息
     * 
     * @param gradeId 班级图库主键
     * @return 结果
     */
    @Override
    public int deleteBusGradeGalleryByGradeId(Long gradeId)
    {
        return busGradeGalleryMapper.deleteBusGradeGalleryByGradeId(gradeId);
    }

    @Override
    public List<Long> getGradeGalleryByStuId(Long studentId) {
        Long currentSemesterId = busSemesterService.getCurrentSemesterId();
        return busGradeGalleryMapper.getGradeGalleryByStuId(studentId,currentSemesterId);
    }
}
