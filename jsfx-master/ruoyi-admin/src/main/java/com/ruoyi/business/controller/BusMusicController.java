package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusMusic;
import com.ruoyi.business.service.IBusMusicService;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 推荐音乐Controller
 * 
 * @author ruoyi
 * @date 2022-10-23
 */
@RestController
@RequestMapping("/business/music")
public class BusMusicController extends BaseController
{
    @Autowired
    private IBusMusicService busMusicService;


    /**
     * 查询推荐音乐列表
     */
    @GetMapping("/list")
    public TableDataInfo list() {
        startPage();
        List<BusMusic> list = busMusicService.selectBusMusicList();
        return getDataTable(list);
    }

    /**
     * 新增推荐音乐
     */
    @PostMapping
    public AjaxResult add(@RequestParam(value = "musicFile") MultipartFile imgMusicFile,String musicName) throws IOException, InvalidExtensionException {
        String music = FileUploadUtils.upload(RuoYiConfig.getSealPath(), imgMusicFile, MimeTypeUtils.MEDIA_EXTENSION);
        //获取到URL后，将URL保存到数据库中
            if (busMusicService.insertBusMusic(musicName, music) == 0) {
                return toAjax(Integer.parseInt("保存URL到数据库失败"));
            } else {
                return toAjax(Integer.parseInt("保存URL到数据库成功"));
            }
    }


    /**
     * 修改推荐音乐
     */
    @PutMapping
    public AjaxResult edit(@RequestBody BusMusic busMusic) {
        return toAjax(busMusicService.updateBusMusic(busMusic));
    }

    /**
     * 删除推荐音乐
     */

	@DeleteMapping("/{musicIds}")
    public AjaxResult remove(@PathVariable Long[] musicIds) {
        return toAjax(busMusicService.deleteBusMusicByMusicIds(musicIds));
    }
}
