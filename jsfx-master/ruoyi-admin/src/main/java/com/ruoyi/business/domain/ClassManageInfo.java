package com.ruoyi.business.domain;


import lombok.Data;

@Data
public class ClassManageInfo {
    //班级ID
    private Long gradeId;
    //班主任名称
    private String headTeacherName;
    //班级人数
    private int countStudents;

    public ClassManageInfo() {
    }

    public ClassManageInfo(Long gradeId, String headTeacherName, int countStudents) {
        this.gradeId = gradeId;
        this.headTeacherName = headTeacherName;
        this.countStudents = countStudents;
    }
}
