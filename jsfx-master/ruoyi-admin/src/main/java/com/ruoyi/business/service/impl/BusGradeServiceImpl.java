package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.BusGrade;
import com.ruoyi.business.mapper.BusGradeMapper;
import com.ruoyi.business.mapper.BusSemesterMapper;
import com.ruoyi.business.service.IBusGradeService;
import com.ruoyi.business.service.IBusSemesterService;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 班级Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-22
 */
@Service
public class BusGradeServiceImpl extends ServiceImpl<BusGradeMapper,BusGrade> implements IBusGradeService
{
    @Autowired
    private BusGradeMapper busGradeMapper;
    //班级表
    @Autowired
    private IBusGradeService busGradeService;
    //学期表
    @Autowired
    private IBusSemesterService busSemesterService;
    //用户表
    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BusSemesterMapper busSemesterMapper;
    //获取某个班级的班主任
    @Override
    public String getHeadTeacherName(Long gradeId) {
        LambdaQueryWrapper<BusGrade> gradeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        gradeLambdaQueryWrapper.eq(BusGrade::getGradeId,gradeId);
        //拿到这个指定的班级信息
        BusGrade busGrade = busGradeService.getOne(gradeLambdaQueryWrapper);
        if (busGrade.getUserId() != null){
            //拿到这个班主任ID
            Long userId = busGrade.getUserId();
            //拿到这个老师的用户对象信息
            SysUser sysUser = iSysUserService.selectUserById(userId);
            return sysUser.getNickName();
        }else {
            //没有就返回一个空
            return "未设置班主任";
        }
    }



    //增加班级以及批量增加班级
    @Override
    public AjaxResult addClasses(String grade, int count) {
        //标志每一次添加都要成功
        int countFlag =0;
        String gradeId;
        //当前学期
        Long currentSemesterId = busSemesterMapper.getCurrentSemesterIdByMapper();

        LambdaQueryWrapper<BusGrade> busGradeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        busGradeLambdaQueryWrapper.likeRight(BusGrade::getGradeId,grade);
        //看看这个添加是不是第一次了，有没有
        int flag = this.count(busGradeLambdaQueryWrapper);
        //如果已经有这个年级段的班级了，那么就先获取它最大的班级编号
        if (flag>0){
            QueryWrapper<BusGrade> busGradeQueryWrapper = new QueryWrapper<>();
            busGradeQueryWrapper.select("max(grade_id) as maxGradeId , year_level_id as yearId").likeRight("grade_id",grade);
            Map<String, Object> map = this.getMap(busGradeQueryWrapper);
            Object o = map.get("maxGradeId");
            Long maxGradeId = (Long) o;
            Long yearId = (Long)map.get("yearId");

            for (int i = 1; i<=count; i++) {
                maxGradeId++;
                BusGrade busGrade = new BusGrade();
                busGrade.setGradeId(maxGradeId);
                busGrade.setSemesterId(currentSemesterId);
                busGrade.setYearLevelId(yearId);
                busGrade.setGradeNumber(0);
                if (this.save(busGrade)) {
                    countFlag++;
                } else {
                    AjaxResult.error(maxGradeId + "添加失败");
                }
            }
        }else {
            //第一次创建这个年级段的班级
            //添加
            for (int i = 1; i<=count; i++){
                if (i<10){
                    gradeId = grade +"0"+i;
                }else {
                    gradeId = grade + i;
                }
                BusGrade busGrade = new BusGrade();
                busGrade.setGradeId(Long.parseLong(gradeId));
                busGrade.setSemesterId(currentSemesterId);
                busGrade.setYearLevelId(1L);
                if (this.save(busGrade)){
                    countFlag++;
                }else {
                    AjaxResult.error(gradeId+"添加失败");
                }
            }
        }
        return AjaxResult.success(countFlag == count);
    }

    /**
     * 这个班主任当前学期所带的班级
     * @param userId
     * @return
     */
    @Override
    public List<BusGrade> getBusGradedList(Long userId) {
        Long semesterId = busSemesterMapper.getCurrentSemesterIdByMapper();
        LambdaQueryWrapper<BusGrade> gradeLambdaQueryWrapper=new LambdaQueryWrapper<>();
        gradeLambdaQueryWrapper.eq(BusGrade::getUserId,userId);
        gradeLambdaQueryWrapper.eq(BusGrade::getSemesterId,semesterId);
        return busGradeService.list(gradeLambdaQueryWrapper);
    }
    /**
     * 查询班级
     * 
     * @param gradeId 班级主键
     * @return 班级
     */
    @Override
    public BusGrade selectBusGradeByGradeId(Long gradeId)
    {
        return busGradeMapper.selectBusGradeByGradeId(gradeId);
    }

    /**
     * 查询班级列表
     * 
     * @param busGrade 班级
     * @return 班级
     */
    @Override
    public List<BusGrade> selectBusGradeList(BusGrade busGrade)
    {
        return busGradeMapper.selectBusGradeList(busGrade);
    }

    /**
     * 新增班级
     * 
     * @param busGrade 班级
     * @return 结果
     */
    @Override
    public int insertBusGrade(BusGrade busGrade)
    {
        return busGradeMapper.insertBusGrade(busGrade);
    }

    @Override
    public boolean myUpdateBusGrade(BusGrade busGrade) {

        return busGradeMapper.myUpdateBusGrade(busGrade) > 0;
    }


    /**
     * 修改班级
     * 
     * @param busGrade 班级
     * @return 结果
     */
    @Override
    public int updateBusGrade(BusGrade busGrade)
    {
        return busGradeMapper.updateBusGrade(busGrade);
    }

    /**
     * 批量删除班级
     * 
     * @param gradeIds 需要删除的班级主键
     * @return 结果
     */
    @Override
    public AjaxResult deleteBusGradeByGradeIds(Long[] gradeIds)
    {
        AjaxResult ajaxResult = AjaxResult.error("未选中班级");
        for (Long gradeId : gradeIds) {
            LambdaQueryWrapper<BusGrade> busGradeLambdaQueryWrapper = new LambdaQueryWrapper<>();
            busGradeLambdaQueryWrapper.eq(BusGrade::getGradeId,gradeId);
            //先查一下这个班级是否存在
            BusGrade busGrade = this.getOne(busGradeLambdaQueryWrapper);
            if (busGrade != null && busGrade.getGradeNumber() == 0){
                this.remove(busGradeLambdaQueryWrapper);
                ajaxResult = AjaxResult.success("删除成功");
            }else{
                ajaxResult = AjaxResult.error(gradeId+"班级有学生，不得删除");
            }
        }
        return ajaxResult;
    }

    /**
     * 删除班级信息
     * 
     * @param gradeId 班级主键
     * @return 结果
     */
    @Override
    public int deleteBusGradeByGradeId(Long gradeId)
    {
        return busGradeMapper.deleteBusGradeByGradeId(gradeId);
    }
    //自己的删除方法
    @Override
    public AjaxResult deleteBusGradeByGradeIdT(Long gradeId) {
        LambdaQueryWrapper<BusGrade> busGradeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        busGradeLambdaQueryWrapper.eq(BusGrade::getGradeId,gradeId);
        //先查一下这个班级是否存在
        BusGrade busGrade = this.getOne(busGradeLambdaQueryWrapper);
        if (busGrade != null  && busGrade.getGradeNumber() == 0){
            this.remove(busGradeLambdaQueryWrapper);
            return AjaxResult.success("删除成功");
        }else{
            return AjaxResult.error(gradeId+"班级有学生，不得删除");
        }
    }
}
