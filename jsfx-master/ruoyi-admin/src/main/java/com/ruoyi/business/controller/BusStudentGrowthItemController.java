package com.ruoyi.business.controller;


import cn.hutool.json.JSONObject;
import com.ruoyi.business.Util.RedisValue;
import com.ruoyi.business.common.CommonValues;
import com.ruoyi.business.domain.BusStudentGrowth;
import com.ruoyi.business.domain.BusStudentGrowthItem;
import com.ruoyi.business.service.IBusSemesterService;
import com.ruoyi.business.service.IBusStudentGrowthItemService;
import com.ruoyi.business.service.IBusStudentGrowthService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 学生成长报告项Controller
 *
 * @date 2022-10-23
 */
@RestController
@RequestMapping("/business/growthItem")
public class BusStudentGrowthItemController extends BaseController
{
    @Autowired
    private IBusStudentGrowthItemService busStudentGrowthItemService;
    @Autowired
    private IBusSemesterService semesterService;
    @Autowired
    private RedisValue redisValue;
    @Autowired
    private IBusStudentGrowthService growthService;
    /**
     * 获取学生成长报告项详细信息
     */
    @GetMapping(value = "/getItems")
    public AjaxResult getInfo(BusStudentGrowth growth)
    {
        Long growthId = growth.getGrowthId();
        if (growthService.growthNotIsCurrentSemester(growthId)){
            return AjaxResult.
                    success(new JSONObject(growthService.getBeforeGrowth(growthId)));
        }
        return AjaxResult.success(busStudentGrowthItemService.getInfo(growth));
    }

    @GetMapping("/verifyModule")
    public TableDataInfo getGrowthItemVerifyModule(Long gradeId){
        Long semesterId = semesterService.getCurrentSemesterId();
        List<Map<String, List<String>>> growthStudentName = busStudentGrowthItemService.getGrowthStudentName(gradeId,semesterId);
        return getDataTable(growthStudentName);
    }
    @GetMapping("/notVerifyModule")
    public TableDataInfo getGrowthItemNotVerifyModule(Long gradeId){
        Long semesterId = semesterService.getCurrentSemesterId();
        List<Map<String, List<String>>> growthStudentName = busStudentGrowthItemService.getGrowthNotVerifyStudentName(gradeId,semesterId);
        return getDataTable(growthStudentName);
    }


    /**
     * 新增学生成长报告项
     * 此处应被开启新学期按钮调用
     */
    @PreAuthorize("@ss.hasPermi('business:growthItem:add')")
    @Log(title = "学生成长报告项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusStudentGrowthItem busStudentGrowthItem)
    {
        return toAjax(busStudentGrowthItemService.insertBusStudentGrowthItem(busStudentGrowthItem));
    }

    /**
     * 填写或修改学生成长报告
     */

    @PostMapping("upload/{studentId}/{growthKey}/{remark}")
    public AjaxResult edit(@PathVariable Long studentId,@PathVariable Integer growthKey,@PathVariable String remark,
                           MultipartFile file) throws IOException, InvalidExtensionException, InterruptedException {
        if (!semesterService.exitReport()){
            return AjaxResult.error("本学期尚未开启成长报告");
        }
        if (file == null){
            return AjaxResult.error("未接收到图片");
        }
        String key = CommonValues.ITEMS[growthKey];
        Long currentGrowthId = busStudentGrowthItemService.
                getCurrentGrowthId(studentId);
        if (redisValue.getReportFinish()){
            //更新成长报告状态
            busStudentGrowthItemService.updateStatus(currentGrowthId);
            return AjaxResult.error("已定稿,无法再上传,如需重新开启请联系管理员");
        }
        BusStudentGrowthItem busStudentGrowthItem = new BusStudentGrowthItem(
                currentGrowthId,studentId,key,remark);
        if (busStudentGrowthItemService.
                uploadImg(file,busStudentGrowthItem) != 0){
            return AjaxResult.success("上传成功");
        }
        return AjaxResult.success("上传失败请重试");

    }



    /**
     * 获取学生当前学期的growthId
     */
    @GetMapping("{studentId}")
    public AjaxResult getCurrentGrowthId(@PathVariable Long studentId){
        return AjaxResult.success(busStudentGrowthItemService.
                getCurrentGrowthId(studentId));
    }


    /**
     * 获取各学期指定环节的值
     */
    @GetMapping("/getHistoryItems")
    public AjaxResult getHistoryItems(Long studentId, String itemKey){
        return AjaxResult.success(busStudentGrowthItemService.getHistoryItems(studentId,itemKey));
    }
    /**
     * 从班级图库中选择照片填写成长报告
     */
    @PostMapping("/writeGrowthByGallery")
    public AjaxResult writeGrowthByGallery(Long studentId,Integer growthKey,String remark,String path) throws InterruptedException {
        if (!semesterService.exitReport()){
            return AjaxResult.error("本学期尚未开启成长报告");
        }
        Long currentGrowthId = busStudentGrowthItemService.getCurrentGrowthId(studentId);
        String key = CommonValues.ITEMS[growthKey];
        if (redisValue.getReportFinish()){
            //更新成长报告状态
            busStudentGrowthItemService.updateStatus(currentGrowthId);
            return AjaxResult.error("已定稿,无法再上传,如需重新开启请联系管理员");
        }
        BusStudentGrowthItem busStudentGrowthItem = new BusStudentGrowthItem(
                studentId,key,path,remark,currentGrowthId);
        if (busStudentGrowthItemService.
                uploadImg(busStudentGrowthItem)  != 0){
            return AjaxResult.success("上传成功");
        }
        return AjaxResult.success("上传失败请重试");
    }
}
