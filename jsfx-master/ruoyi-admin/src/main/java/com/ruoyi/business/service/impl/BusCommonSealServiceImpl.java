package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.BusCommonSeal;
import com.ruoyi.business.mapper.BusCommonSealMapper;
import com.ruoyi.business.service.IBusCommonSealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-01
 */
@Service
public class BusCommonSealServiceImpl implements IBusCommonSealService
{
    @Autowired
    private BusCommonSealMapper busCommonSealMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param sealName 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public BusCommonSeal selectBusCommonSealBySealName(String sealName)
    {
        return busCommonSealMapper.selectBusCommonSealBySealName(sealName);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param busCommonSeal 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<BusCommonSeal> selectBusCommonSealList(BusCommonSeal busCommonSeal)
    {
        return busCommonSealMapper.selectBusCommonSealList(busCommonSeal);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param  【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertBusCommonSeal(String sealName, String sealUrl,String sealIllustrate) {
        return busCommonSealMapper.insertBusCommonSeal(sealName, sealUrl,sealIllustrate);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param busCommonSeal 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateBusCommonSeal(BusCommonSeal busCommonSeal)
    {
        return busCommonSealMapper.updateBusCommonSeal(busCommonSeal);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param sealNames 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBusCommonSealBySealNames(String[] sealNames)
    {
        return busCommonSealMapper.deleteBusCommonSealBySealNames(sealNames);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param sealName 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBusCommonSealBySealName(String sealName)
    {
        return busCommonSealMapper.deleteBusCommonSealBySealName(sealName);
    }

}
