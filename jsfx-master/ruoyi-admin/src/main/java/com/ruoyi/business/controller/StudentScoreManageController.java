package com.ruoyi.business.controller;

import cn.hutool.json.JSONObject;
import com.ruoyi.business.service.IStudentScoreManageService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/scoreManage")
@Transactional
public class StudentScoreManageController extends BaseController {

    @Autowired
    private IStudentScoreManageService iStudentScoreManageService;


    //返回全部当前学期所有学生素养报告单
    @GetMapping("/page")
    public TableDataInfo allStudentScoreList(String gradeId, String semesterYearName,int pageNum,int pageSize){
        List<JSONObject> jsonObjects = iStudentScoreManageService.allStudentScoreInfo(gradeId,semesterYearName);
        //结果集
        int size = jsonObjects.size();
        //解析分页参数
        if (pageSize > size) {
            pageSize = size;
        }
        // 求出最大页数，防止currentPage越界
        int maxPage = size % pageSize == 0 ? size / pageSize : size / pageSize + 1;
        if (pageNum > maxPage) {
            pageNum = maxPage;
        }
        // 当前页第一条数据的下标
        int curIdx = pageNum > 1 ? (pageNum - 1) * pageSize : 0;
        // 将当前页的数据放进busGradeTeacherDtos
        List<JSONObject> busGradeTeacherDtos=new ArrayList<>();
        for (int i = 0; i < pageSize && curIdx + i < size; i++) {
            busGradeTeacherDtos.add(jsonObjects.get(curIdx + i));
        }
        return getDataTable(busGradeTeacherDtos);
    }

    //回显一个学生的成绩报告
    @GetMapping("/echoStudentScoreInfo")
    public JSONObject echoStudentScoreInfo(Long studentId){
        return iStudentScoreManageService.echoStudentScoreInfo(studentId);
    }

    /**
     *
     * 修改数据
     * @param object 素质报告数据
     */
    @PostMapping
    public AjaxResult updateStudentScoreInfo(@RequestBody JSONObject object){
        return toAjax(iStudentScoreManageService.updateStudentScoreInfo(object));
    }


    //选择一个学生，条件查询
    @GetMapping("/choose")
    public TableDataInfo choose(String gradeId,String name){
        return getDataTable(iStudentScoreManageService.chooseStudentScoreInfo(gradeId,name));
    }

}
