package com.ruoyi.business.Dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class BusStudentParentsDtoT extends BusStudents {

    @TableField(exist = false)
    private Long fatherUserId;

    @TableField(exist = false)
    private Long motherUserId;

    //爸爸的姓名
    @Excel(name = "爸爸的姓名")
    private String fatherName;

    //妈妈的姓名
    @Excel(name = "妈妈的姓名")
    private String motherName;

    //爸爸的电话号码
    @Excel(name = "爸爸的电话号码")
    private String fatherNumber;

    //妈妈的电话号码
    @Excel(name = "妈妈的电话号码")
    private String motherNumber;

}
