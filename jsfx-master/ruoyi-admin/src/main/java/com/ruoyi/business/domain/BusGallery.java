package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * 图库对象 bus_gallery
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@Data
public class BusGallery
{
    /** 图库id */
    private Long galleryId;

    /** 图库url */
    @Excel(name = "图库url")
    private String galleryUrl;

}
