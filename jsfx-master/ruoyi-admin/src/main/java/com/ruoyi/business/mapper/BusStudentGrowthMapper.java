package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusStudentGrowth;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 学生成长报告Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-22
 */
@Mapper
public interface BusStudentGrowthMapper extends BaseMapper<BusStudentGrowth>
{
    /**
     * 查询学生成长报告
     * 
     * @param growthId 学生成长报告主键
     * @return 学生成长报告
     */
    BusStudentGrowth selectBusStudentGrowthByGrowthId(Long growthId);

    /**
     * 查询学生成长报告列表
     * 
     * @param busStudentGrowth 学生成长报告
     * @return 学生成长报告集合
     */
    List<BusStudentGrowth> selectBusStudentGrowthList(BusStudentGrowth busStudentGrowth);

    /**
     * 新增学生成长报告
     * 
     * @param busStudentGrowth 学生成长报告
     * @return 结果
     */
    int insertBusStudentGrowth(BusStudentGrowth busStudentGrowth);

    /**
     * 修改学生成长报告
     * 
     * @param busStudentGrowth 学生成长报告
     * @return 结果
     */
    int updateBusStudentGrowth(BusStudentGrowth busStudentGrowth);

    /**
     * 删除学生成长报告
     * 
     * @param growthId 学生成长报告主键
     * @return 结果
     */
    int deleteBusStudentGrowthByGrowthId(Long growthId);

    /**
     * 批量删除学生成长报告
     * 
     * @param growthIds 需要删除的数据主键集合
     * @return 结果
     */
    int deleteBusStudentGrowthByGrowthIds(Long[] growthIds);

    /**
     * 获取成长报告表中所含的数据
     */
    List<BusStudentGrowth> getGrowthsByStuId(Long growthId);

    Map<String,Object> getSemesterName(Long semesterId);

    /**
     * 获取单个学生当前学期的growthId
     */
    Long getStudentCurrentGrowthId(@Param("studentId") Long studentId, @Param("semesterId")Long semesterId);
    //修改已存在学期
    @Update("update bus_student_growth set report_start_end = #{endTime} where growth_id = #{growthId}")
    int updateReportStartEndInt(@Param("growthId")Long growthId, @Param("endTime")Date endDate);
    String getStudentName(Long studentId);
    @Select("select growth_id from bus_student_growth where semester_id = #{currentSemesterId}")
    List<Long> getActiveId(Long currentSemesterId);
    //查看当前学期是否已开启成长报告
    Long exitGrowth(Long currentSemesterId);

    List<Character> getStatus(Long growthId);

    int setStatus(@Param("growthId") Long growthId,@Param("status") Character status);

    int resetEndTime(@Param("date") Date date,@Param("currentSemesterId") Long currentSemesterId);

    List<Long> getUnActiveGrowth();

    void setResult(@Param("growthId") Long growthId,@Param("value") String entries);

    Long getSemesterIdByGrowthId(Long growthId);

    String getBeforeGrowth(Long growthId);

    List<String> getGrowthResByStudentId(@Param("studentId") Long studentId,@Param("itemKey") String itemKey);
}
