package com.ruoyi.business.controller;

import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.PhoneUtil;
import com.ruoyi.business.Dto.BusStudentParentsDtoT;
import com.ruoyi.business.service.IStudentManageService;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.exception.MyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/studentManage")
@Transactional
public class StudentManageController extends BaseController {
    @Autowired
    private IStudentManageService iStudentManageService;


    /**
     * 指定一个班级的学生信息
     * @param gradeId 指定班级ID
     * @return
     */
    @GetMapping
    public TableDataInfo allStudentInfoByGradeId(Long gradeId,int pageSize,int pageNum) {

        List<BusStudentParentsDtoT> pointGradeStudentInfo = iStudentManageService.getPointGradeStudentInfo(gradeId);

        List<BusStudentParentsDtoT> PointGradeStudentInfo =new ArrayList<>();
        //结果集
        int size = pointGradeStudentInfo.size();
        if (!pointGradeStudentInfo.isEmpty()){
            //解析分页参数
            if (pageSize > size) {
                pageSize = size;
            }
            // 求出最大页数，防止currentPage越界
            int maxPage = size % pageSize == 0 ? size / pageSize : size / pageSize + 1;
            if (pageNum > maxPage) {
                pageNum = maxPage;
            }
            // 当前页第一条数据的下标
            int curIdx = pageNum > 1 ? (pageNum - 1) * pageSize : 0;
            // 将当前页的数据放进busGradeTeacherDtos

            for (int i = 0; i < pageSize && curIdx + i < size; i++) {
                PointGradeStudentInfo.add(pointGradeStudentInfo.get(curIdx + i));
            }
        }else {
            TableDataInfo rspData = new TableDataInfo();
            rspData.setCode(HttpStatus.SUCCESS);
            rspData.setMsg("查询成功,教师为空");
            rspData.setRows(PointGradeStudentInfo);
            rspData.setTotal(size);
            return rspData;
        }


        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(PointGradeStudentInfo);
        rspData.setTotal(size);
        return rspData;
    }


    //年级及其班级数量
    @GetMapping("/allYearLevelWithGradeID")
    public Map<Long, List<Long>> allYearLevelWithGradeID(){
        return iStudentManageService.getYearLevelGradeId();
    }

    //指定学生编辑回显
    @GetMapping("/echoPointStudentInfo")
    public BusStudentParentsDtoT echoPointStudentInfo(Long studentId){
        return iStudentManageService.getStudentWithParentsInfo(studentId);
    }

    //编辑修改
    @Transactional
    @PostMapping
    public AjaxResult updateStudentInfo(@RequestBody BusStudentParentsDtoT busStudentParentsDtoT){
        if (!IdcardUtil.isValidCard(busStudentParentsDtoT.getIdCard())){
            throw  new MyException("无效身份证号");
        }
        if (!PhoneUtil.isPhone(busStudentParentsDtoT.getMotherNumber())||
                !PhoneUtil.isPhone(busStudentParentsDtoT.getFatherNumber())){
            return AjaxResult.error("手机号无效");
        }
        return iStudentManageService.updateStudentWithParentsInfo(busStudentParentsDtoT);

    }

    //添加新增
    @PutMapping
    public AjaxResult addStudentInfo(@RequestBody BusStudentParentsDtoT busStudentParentsDtoT){
        if (!IdcardUtil.isValidCard(busStudentParentsDtoT.getIdCard())){
            throw  new MyException("无效身份证号");
        }
        if (!PhoneUtil.isPhone(busStudentParentsDtoT.getMotherNumber())||
                !PhoneUtil.isPhone(busStudentParentsDtoT.getFatherNumber())){
            return AjaxResult.error("手机号无效");
        }
        return toAjax(iStudentManageService.addStudentWithParentsInfo(busStudentParentsDtoT));
    }

    //excel导入
    @Transactional
    @PostMapping("/import")
    public AjaxResult importData(MultipartFile file) throws Exception {
        boolean flag =false;
        ExcelUtil<BusStudentParentsDtoT> excelUtil = new ExcelUtil<>(BusStudentParentsDtoT.class);
        List<BusStudentParentsDtoT> busStudentParentsDtoTS = excelUtil.importExcel(file.getInputStream());
        if (busStudentParentsDtoTS.isEmpty()){
            return AjaxResult.error("导入为空");
        }
        for (BusStudentParentsDtoT busStudentParentsDtoT : busStudentParentsDtoTS) {
            System.out.println(busStudentParentsDtoT);
            if (!IdcardUtil.isValidCard(busStudentParentsDtoT.getIdCard())){
                throw  new MyException("含有无效身份证号");
            }
            if (!PhoneUtil.isPhone(busStudentParentsDtoT.getMotherNumber())||
                    !PhoneUtil.isPhone(busStudentParentsDtoT.getFatherNumber())){
                throw  new MyException("含无效手机号");
            }
            flag=iStudentManageService.addStudentWithParentsInfo(busStudentParentsDtoT);
        }
        return toAjax(flag);
    }

    //excel导出
    @PostMapping("/export/{gradeId}")
    public void export(HttpServletResponse response, @PathVariable("gradeId") Long gradeId){
        List<BusStudentParentsDtoT> pointGradeStudentInfo = iStudentManageService.getPointGradeStudentInfo(gradeId);
        ExcelUtil<BusStudentParentsDtoT> excelUtil = new ExcelUtil<>(BusStudentParentsDtoT.class);
        excelUtil.exportExcel(response,pointGradeStudentInfo,gradeId+"");
    }

    //excel模板导出
    @PostMapping("/exportMoodle")
    public void exportMoodle(HttpServletResponse response){
        ExcelUtil<BusStudentParentsDtoT> excelUtil = new ExcelUtil<>(BusStudentParentsDtoT.class);
        excelUtil.importTemplateExcel(response,"模板");
    }


}