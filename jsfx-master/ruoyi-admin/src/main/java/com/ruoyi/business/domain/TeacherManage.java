package com.ruoyi.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@TableName("sys_user")
public class TeacherManage {
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    private Long userId;

    /** 部门ID */
    private Long deptId;

    /** 微信openid */
    private String openId;

    /** 用户账号 */
    @Excel(name = "手机号")
    private String userName;

    /** 用户昵称 */
    @Excel(name = "姓名")
    private String nickName;

    /** 用户类型（00系统用户） */
    private String userType;


    /** 手机号码 */
    @Excel(name = "手机号码")
    private String phonenumber;

    /** 用户性别（0男 1女 2未知） */
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    private String avatar;

    /** 密码 */
    private String password;

    /** 帐号状态（0正常 1停用） */
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 最后登录IP */
    private String loginIp;

    /** 最后登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date loginDate;

    private String rawPassword;
    @TableField(exist = false)
    private String headTeacher;

    @Excel(name = "所带班级")
    @TableField(exist = false)
    private String strGradeNameList;

    @TableField(exist = false)
    List<Long> gradeNameList = new ArrayList<>();



    @Excel(name = "授课及其班级")
    @TableField(exist = false)
    private String strMapSubject;

    //课程---班级
    @TableField(exist = false)
    private Map<String, List<Long>> mapSubject;




    public void setMapSubject(Map<String, List<Long>> mapSubject) {
        this.mapSubject = mapSubject;
    }


    public void addGradeName(Long gradeName) {
        //如果这个班级名不在列表中就添加
        if (!gradeNameList.contains(gradeName)){
            gradeNameList.add(gradeName);
        }
    }

}
