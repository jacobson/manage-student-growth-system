package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusGradeGallery;
import com.ruoyi.business.service.IBusGradeGalleryService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.framework.security.context.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 班级图库Controller
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@RestController
@RequestMapping("/business/gradegallery")
public class BusGradeGalleryController extends BaseController
{
    @Autowired
    private IBusGradeGalleryService busGradeGalleryService;


    /**
     * 班主任获取班级图库
     */
    @GetMapping
    public AjaxResult getGradeGalleryByUserId() {
        return AjaxResult.success( busGradeGalleryService.
                getGradeGalleryByUserId(UserContext.getUserId()));
    }





    /**
     * 新增班级图库
     */
    @PostMapping
    public AjaxResult add(@RequestBody BusGradeGallery busGradeGallery)
    {
        busGradeGalleryService.insertBusGradeGallery(busGradeGallery);
        return AjaxResult.success();
    }

    /**
     * 修改班级图库
     */
    @PutMapping
    public AjaxResult edit(@RequestBody BusGradeGallery busGradeGallery)
    {
        return toAjax(busGradeGalleryService.updateBusGradeGallery(busGradeGallery));
    }

    /**
     * 删除班级图库
     */
	@DeleteMapping("/{galleryId}")
    public AjaxResult remove(@PathVariable Long[] galleryId)
    {
        return toAjax(busGradeGalleryService.
                deleteBusGradeGalleryByGalleryIds(galleryId));
    }



}
