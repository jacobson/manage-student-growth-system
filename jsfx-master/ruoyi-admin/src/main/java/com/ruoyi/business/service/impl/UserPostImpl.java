package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.UserPost;
import com.ruoyi.business.mapper.UserPostMapper;
import com.ruoyi.business.service.IUserPostService;
import org.springframework.stereotype.Service;

@Service
public class UserPostImpl extends ServiceImpl<UserPostMapper, UserPost> implements IUserPostService {
}
