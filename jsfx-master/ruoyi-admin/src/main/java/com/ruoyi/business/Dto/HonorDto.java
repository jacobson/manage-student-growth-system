package com.ruoyi.business.Dto;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class HonorDto {
  private List<Long> candidate = new ArrayList<>();

  private String module;
}
