package com.ruoyi.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
@TableName("bus_music")
public class MusicManage {
    private static final long serialVersionUID = 1L;
    /** 音乐ID */
    private Long musicId;

    /** 音乐名称 */
    @Excel(name = "音乐名称")
    private String musicName;

    /** 音乐地址 */
    @Excel(name = "音乐地址")
    private String musicUrl;

    /** 歌手 */
    @TableField(exist = false)
    private String Singer;

    /** 时长 */
    @TableField(exist = false)
    private String Duration;

}
