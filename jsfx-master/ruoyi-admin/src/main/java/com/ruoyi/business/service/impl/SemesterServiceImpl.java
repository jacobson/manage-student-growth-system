package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.*;
import com.ruoyi.business.mapper.BusGradeTeacherHistoryMapper;
import com.ruoyi.business.mapper.BusStudentHonorMapper;
import com.ruoyi.business.mapper.BusUserCommentsMapper;
import com.ruoyi.business.mapper.SemesterMapper;
import com.ruoyi.business.service.*;
import com.ruoyi.framework.web.exception.MyException;
import com.ruoyi.system.common.RedisKeyPrefix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SemesterServiceImpl extends ServiceImpl<SemesterMapper, SemesterManageMent> implements ISemesterService {
    @Autowired
    private ISemesterService semesterService;

    @Autowired
    private IBusGradeTeacherHistoryService busGradeTeacherHistoryService;

    @Autowired
    private IBusGradeStudentHistoryService busGradeStudentHistoryService;


    @Autowired
    private IBusGradeService busGradeService;

    @Autowired
    private IBusGradeStudentService busGradeStudentService;

    @Value("${period}")
    private int period;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private BusStudentHonorMapper honorMapper;
    @Autowired
    private BusUserCommentsMapper commentsMapper;

    @Autowired
    private IBusStudentGrowthItemService growthItemService;

    @Autowired
    private BusGradeTeacherHistoryMapper gradeTeacherHistoryMapper;

    /**
     * 查询学年学期管理列表
     * @return
     */
    @Override
    public List<SemesterManageMent> BusSemesterList() {
        List<SemesterManageMent> list = semesterService.list(null);
        List<SemesterManageMent> collect = list.stream().peek(item -> {
        }).collect(Collectors.toList());
        return collect;
    }

    /**
     * 新增学年学期
     * @param semesterManageMent
     * @return
     */
    @Transactional
    @Override
    public boolean addBusSemester(SemesterManageMent semesterManageMent) {
        //清空评语表
        commentsMapper.clear();
        //清空荣誉表
        honorMapper.clear();
        growthItemService.moveGrowthItem();
        boolean save2=false;
        boolean save4=false;
        boolean remove1=false;
        boolean remove2=false;
        boolean remove3=false;
        int count = semesterService.count(null);
        if (count == 0) {
            SemesterManageMent semesterManageMent1 = new SemesterManageMent();
            Long semesterYear = semesterManageMent.getSemesterYear();

            String semesterName = semesterManageMent.getSemesterName();

            semesterManageMent1.setSemesterYear(semesterYear);
            semesterManageMent1.setSemesterName(semesterName);
            semesterManageMent1.setSemesterNow("1");
            semesterManageMent1.setCreateTime(new Date());
            return semesterService.save(semesterManageMent1);

        } else {
            LambdaQueryWrapper<SemesterManageMent> semlqw = new LambdaQueryWrapper<>();
            semlqw.eq(SemesterManageMent::getSemesterNow, "1");
            SemesterManageMent one1 = semesterService.getOne(semlqw);
            Date createTime = one1.getCreateTime();
            long l = new Date().getTime() - createTime.getTime();
            long l2 = l / (2592000000L);
            if (l2 < period) {
                throw new MyException("短时间内请勿重复新增学期");
            }
            semesterManageMent.setCreateTime(new Date());
            //学年学期的新增操作
            LambdaQueryWrapper<SemesterManageMent> smmlqw = new LambdaQueryWrapper<>();
            smmlqw.eq(SemesterManageMent::getSemesterNow, "1");
            SemesterManageMent one = semesterService.getOne(smmlqw);
            SemesterManageMent semesterManageMent1 = new SemesterManageMent();
            semesterManageMent1.setSemesterId(one.getSemesterId());
            semesterManageMent1.setSemesterYear(one.getSemesterYear());
            semesterManageMent1.setSemesterName(one.getSemesterName());
            semesterManageMent1.setCreateTime(one.getCreateTime());
            semesterManageMent1.setSemesterNow("0");
            //将其他的为当前学年学期的都改为0
            boolean remove = semesterService.remove(smmlqw);
            boolean save1 = semesterService.save(semesterManageMent1);
            //新增的当前学年学期为1
            semesterManageMent.setSemesterNow("1");
            Long semesterYear = semesterManageMent.getSemesterYear();
            semesterManageMent.setSemesterYear(semesterYear);
            boolean save = semesterService.save(semesterManageMent);


            //新增到历史的班级教师表
            List<BusGrade> list = busGradeService.list(null);
            for (BusGrade busGrade : list) {
                Long semesterId = busGrade.getSemesterId();
                Long gradeId = busGrade.getGradeId();
                Long userId = gradeTeacherHistoryMapper.getLeadTeacher(gradeId);
                //读取班级中的内容添加到班级教师历史表中
                BusGradeTeacherHistory busGradeTeacherHistory = new BusGradeTeacherHistory();
                busGradeTeacherHistory.setGradeId(gradeId);
                busGradeTeacherHistory.setSemesterId(semesterId);
                busGradeTeacherHistory.setUserId(userId);
                gradeTeacherHistoryMapper.recordTeacherTeach(busGradeTeacherHistory);

                //新增到班级学生历史表
                LambdaQueryWrapper<BusGradeStudent> bgslqw = new LambdaQueryWrapper<>();
                bgslqw.eq(BusGradeStudent::getGradeId, gradeId).select(BusGradeStudent::getStudentId);
                List<BusGradeStudent> list1 = busGradeStudentService.list(bgslqw);
                for (BusGradeStudent busGradeStudent : list1) {

                    Long studentId = busGradeStudent.getStudentId();

                    BusGradeStudentHistory busGradeStudentHistory = new BusGradeStudentHistory();
                    busGradeStudentHistory.setStudentId(studentId);
                    busGradeStudentHistory.setGradeId(gradeId);
                    busGradeStudentHistory.setSemesterId(semesterId);
                    save2 = busGradeStudentHistoryService.save(busGradeStudentHistory);
                }
            }
            //修改班级表中的
            for (BusGrade busGrade1 : list) {
                Long gradeId1 = busGrade1.getGradeId();
                Long semesterId = busGrade1.getSemesterId();
                Long yearLevelId = busGrade1.getYearLevelId();
                String campusName = busGrade1.getCampusName();
                String gradeName = busGrade1.getGradeName();
                Integer gradeNumber = busGrade1.getGradeNumber();
                String gradeStatus = busGrade1.getGradeStatus();
                String gradeRemark = busGrade1.getGradeRemark();
                Long userId1 = busGrade1.getUserId();

                BusGrade busGrade2 = new BusGrade();
                busGrade2.setGradeId(gradeId1);
                busGrade2.setSemesterId(semesterId + 1);
                busGrade2.setYearLevelId(yearLevelId + 1);
                busGrade2.setCampusName(campusName);
                busGrade2.setGradeName(gradeName);
                busGrade2.setGradeNumber(gradeNumber);
                busGrade2.setGradeStatus(gradeStatus);
                busGrade2.setGradeRemark(gradeRemark);
                busGrade2.setUserId(userId1);

                LambdaQueryWrapper<BusGrade> glqw = new LambdaQueryWrapper<>();
                glqw.eq(BusGrade::getGradeId, gradeId1);
                remove1 = busGradeService.remove(glqw);
                save4 = busGradeService.save(busGrade2);


            }

            //如果年级id大于12说明这个班级已经毕业，就要删除学生，和班级
            for (BusGrade busGrade2 : list) {
                Long yearLevelId = busGrade2.getYearLevelId();
                if (yearLevelId == 12) {
                    Long gradeId = busGrade2.getGradeId();
                    System.out.println("===========" + gradeId);

                    LambdaQueryWrapper<BusGradeStudent> lqw = new LambdaQueryWrapper<>();
                    lqw.eq(BusGradeStudent::getGradeId, gradeId);
                    remove2 = busGradeStudentService.remove(lqw);

                    LambdaQueryWrapper<BusGrade> lqw1 = new LambdaQueryWrapper<>();
                    lqw1.eq(BusGrade::getGradeId, gradeId);
                    remove3 = busGradeService.remove(lqw1);
                }
            }
            //为了第一学期是没有删除的东西的，所以这个两个删除会显示为flase
            if (remove2 == false && remove3 == false) {
                remove2 = true;
            }
            redisTemplate.delete(RedisKeyPrefix.CURRENT_SEMESTER_ID);
            return   save2 && save4 && remove1 && remove2 && remove3;
        }



    }
    /**
     * 条件回显
     * @param semesterId
     * @return
     */
    @Override
    public List<SemesterManageMent> echoBusSemesterManageInfo(Long semesterId) {
        LambdaQueryWrapper<SemesterManageMent> smmlqw = new LambdaQueryWrapper<>();
        smmlqw.eq(SemesterManageMent::getSemesterId,semesterId);
        List<SemesterManageMent> list = semesterService.list(smmlqw);
        List<SemesterManageMent> collect = list.stream().peek(item -> {
        }).collect(Collectors.toList());
        return collect;
    }
}
