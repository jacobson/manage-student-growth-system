package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * 用户评语（教师、班主任）对象 bus_user_comments
 * 
 * @author ruoyi
 * @date 2022-10-28
 */
@Data
public class BusUserComments
{
    private static final long serialVersionUID = 1L;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 学生id */
    @Excel(name = "学生id")
    private Long studentId;

    /** 评语状态 */
    @Excel(name = "评语状态")
    private String status;

    /** 评语内容 */
    @Excel(name = "评语内容")
    private String comments;

    @Excel(name = "是否受邀")
    private String remark;

}
