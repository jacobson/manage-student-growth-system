package com.ruoyi.business.Util;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;

public class IdUtil {
    public static Long getId(){
        return  Long.valueOf(IdWorker.getIdStr().substring(3, 19));
    }
}
