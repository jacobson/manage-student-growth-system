package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * 班级图库对象 bus_grade_gallery
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@Data
public class BusGradeGallery
{

    /** 班级id */
    @Excel(name = "班级id")
    private Long gradeId;

    /** 图库id */
    @Excel(name = "图库id")
    private Long galleryId;

    /** 图库名字 */
    @Excel(name = "图库name")
    private String galleryName;

    private Long semesterId;

}
