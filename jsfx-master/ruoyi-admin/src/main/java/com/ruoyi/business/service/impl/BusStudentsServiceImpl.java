package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.business.mapper.BusStudentsMapper;
import com.ruoyi.business.service.IBusStudentsService;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 学生管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-21
 */
@Service
public class BusStudentsServiceImpl extends ServiceImpl<BusStudentsMapper, BusStudents> implements IBusStudentsService
{
    @Autowired
    private BusStudentsMapper busStudentsMapper;

    /**
     * 查询学生管理
     * 
     * @param stuId 学生管理主键
     * @return 学生管理
     */
    @Override
    public Map<Object,Object> selectBusStudentsByStuId(Long stuId)
    {
        HashMap<Object, Object> info = new HashMap<>();
        List<Map<Object, Object>> parentInfos = busStudentsMapper.
                getParentInfoByStuId(stuId);
        parentInfos.forEach(info::putAll);
        BusStudents busStudents = busStudentsMapper.
                selectBusStudentsByStuId(stuId);
        Long gradeId = busStudentsMapper.getGradeIdByStuIdLong(stuId);
        info.put("班级",gradeId);
        info.put("身份证号",busStudents.getIdCard());
        return info;
    }

    /**
     * 查询学生管理列表
     * 
     * @param busStudents 学生管理
     * @return 学生管理
     */
    @Override
    public List<BusStudents> selectBusStudentsList(BusStudents busStudents)
    {
        return busStudentsMapper.selectBusStudentsList(busStudents);
    }

    /**
     * 新增学生管理
     * 
     * @param busStudents 学生管理
     * @return 结果
     */
    @Override
    public int insertBusStudents(BusStudents busStudents)
    {
        return busStudentsMapper.insertBusStudents(busStudents);
    }

    /**
     * 修改学生管理
     * 
     * @param busStudents 学生管理
     * @return 结果
     */
    @Override
    public int updateBusStudents(BusStudents busStudents)
    {
//        busStudents.setUpdateTime(DateUtils.getNowDate());
        return busStudentsMapper.updateBusStudents(busStudents);
    }

    /**
     * 批量删除学生管理
     * 
     * @param stuIds 需要删除的学生管理主键
     * @return 结果
     */
    @Override
    public int deleteBusStudentsByStuIds(Long[] stuIds)
    {
        return busStudentsMapper.deleteBusStudentsByStuIds(stuIds);
    }

    /**
     * 删除学生管理信息
     * 
     * @param stuId 学生管理主键
     * @return 结果
     */
    @Override
    public int deleteBusStudentsByStuId(Long stuId)
    {
        return busStudentsMapper.deleteBusStudentsByStuId(stuId);
    }

    @Override
    public List<BusStudents> selectBusStudentsByGradeId(Long gradeId) {
        List<BusStudents> busStudents = busStudentsMapper.selectBusStudentsByGradeId(gradeId);
        busStudents.forEach((item)->{
            item.setGradeId(gradeId);
        });
        return busStudents;
    }

    @Override
    public int editAvatar(Long studentId, MultipartFile file) throws IOException, InvalidExtensionException {
        String avatar = FileUploadUtils.upload(RuoYiConfig.getAvatarPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
        return busStudentsMapper.editAvatar(studentId,avatar);
    }

    @Override
    public List<Long> selectStudentIdsByGradeId(Long gradeId) {
        return busStudentsMapper.selectStudentIdsByGradeId(gradeId);
    }

    @Override
    public List<BusStudents> getAll() {
        return busStudentsMapper.getAll();
    }

}
