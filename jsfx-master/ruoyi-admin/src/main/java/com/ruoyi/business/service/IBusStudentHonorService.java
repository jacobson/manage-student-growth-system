package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.BusStudentHonor;

import java.util.List;
import java.util.Map;

/**
 * 学生荣誉Service接口
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
public interface IBusStudentHonorService  extends IService<BusStudentHonor>
{
    /**
     * 确认调整荣誉模块
     * @param studentList 需要修改的荣誉学生名单
     * @param flag
     * @return
     */
    boolean updateModuleInfo(List<Long> studentList,String module,boolean flag);

    //封装荣誉模块信息
    List<Map<String, List<String>>> getHonorModuleInfo(Long gradeId);

    /**
     * 查询学生荣誉
     * 
     * @param growthId 学生荣誉主键
     * @return 学生荣誉
     */
    public BusStudentHonor selectBusStudentHonorByGrowthId(Long growthId);

    /**
     * 查询学生荣誉列表
     * 
     * @param busStudentHonor 学生荣誉
     * @return 学生荣誉集合
     */
    public List<BusStudentHonor> selectBusStudentHonorList(BusStudentHonor busStudentHonor);

    /**
     * 新增学生荣誉
     * 
     * @param busStudentHonor 学生荣誉
     * @return 结果
     */
    public int insertBusStudentHonor(BusStudentHonor busStudentHonor);

    /**
     * 修改学生荣誉
     * 
     * @param busStudentHonor 学生荣誉
     * @return 结果
     */
    public int updateBusStudentHonor(BusStudentHonor busStudentHonor);

    /**
     * 批量删除学生荣誉
     * 
     * @param growthIds 需要删除的学生荣誉主键集合
     * @return 结果
     */
    public int deleteBusStudentHonorByGrowthIds(Long[] growthIds);

    /**
     * 删除学生荣誉信息
     * 
     * @param growthId 学生荣誉主键
     * @return 结果
     */
    public int deleteBusStudentHonorByGrowthId(Long growthId);

    int confirmList(Long gradeId);
}
