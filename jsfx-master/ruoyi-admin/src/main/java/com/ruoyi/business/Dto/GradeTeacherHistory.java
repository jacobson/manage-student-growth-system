package com.ruoyi.business.Dto;

import lombok.Data;

@Data
public class GradeTeacherHistory {
    private Long gradeId;
    private Long semesterId;
}
