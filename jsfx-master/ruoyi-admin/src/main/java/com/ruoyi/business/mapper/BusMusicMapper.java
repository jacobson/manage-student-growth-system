package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.BusMusic;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 推荐音乐Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-23
 */
public interface BusMusicMapper 
{

    /**
     * 查询推荐音乐列表
     * 
     * @return 推荐音乐集合
     */
    public List<BusMusic> selectBusMusicList();

    /**
     * 新增推荐音乐
     * 
     * @return 结果
     */
    public int insertBusMusic(@Param("musicName")String musicName, @Param("musicUrl")String musicUrl);

    /**
     * 修改推荐音乐
     * 
     * @param busMusic 推荐音乐
     * @return 结果
     */
    public int updateBusMusic(BusMusic busMusic);

    /**
     * 删除推荐音乐
     * 
     * @param musicId 推荐音乐主键
     * @return 结果
     */
    public int deleteBusMusicByMusicId(Long musicId);

    /**
     * 批量删除推荐音乐
     * 
     * @param musicIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusMusicByMusicIds(Long[] musicIds);
}
