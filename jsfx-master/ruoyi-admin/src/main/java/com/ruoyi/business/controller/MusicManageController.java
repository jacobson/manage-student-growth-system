package com.ruoyi.business.controller;

import com.ruoyi.business.domain.MusicManage;
import com.ruoyi.business.service.IMusicManageService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 音乐管理Controller
 */
@RestController
@RequestMapping("/musicManage")
public class MusicManageController extends BaseController {

    @Autowired
    private IMusicManageService musicManageService;

    /**
     *上传音乐
     * @return
     */
    @GetMapping("/add")
    public AjaxResult selectMusicList() {
        List<MusicManage> musicManages = musicManageService.selectMusicList();
        return AjaxResult.success(musicManages);
    }

    /**
     * 条件查询，模糊查询
     * @param musicname
     * @return
     */
    @GetMapping("/conditionMusicManages")
    public AjaxResult conditionMusicManages(String musicname) {
        List<MusicManage> musicManages = musicManageService.conditionMusicManages(musicname);
        return AjaxResult.success(musicManages);
    }

    /**
     *删除音乐
     * @return
     */
    @DeleteMapping("/delete")
    public AjaxResult deleteMusic(Long musicId) {
        boolean delete = musicManageService.delete(musicId);
        return AjaxResult.success(delete);
    }

    /**
     * 添加数据
     * @param musicManage
     * @return
     */
    @PostMapping("/addmusics")
    public AjaxResult addMusic(@RequestBody MusicManage musicManage) {
        boolean b = musicManageService.addMusic(musicManage);
        return AjaxResult.success(b);
    }




    /**
     * 导入Excel接口
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("/import")
    public AjaxResult importData(MultipartFile file) throws Exception {
        boolean flag = false;
        ExcelUtil<MusicManage> manageExcelUtil = new ExcelUtil<>(MusicManage.class);
        List<MusicManage> musicManages = manageExcelUtil.importExcel(file.getInputStream());
        for (MusicManage musicManage : musicManages) {
            flag = musicManageService.addMusic(musicManage);
        }
        return AjaxResult.success(flag);
    }


    /**
     * 导出Excel文件
     * @param response
     * @param musicManage
     */
    @GetMapping("/export")
    public void exportData(HttpServletResponse response, MusicManage musicManage){
        List<MusicManage> musicManages = musicManageService.selectMusicList();
        ExcelUtil<MusicManage> excelUtil = new ExcelUtil<MusicManage>(MusicManage.class);
        excelUtil.exportExcel(response,musicManages,"音乐数据");
    }


}
