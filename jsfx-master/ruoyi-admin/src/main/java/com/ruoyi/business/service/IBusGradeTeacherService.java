package com.ruoyi.business.service;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.Dto.BusGradeTeacherDto;
import com.ruoyi.business.Dto.GradeTeacherHistory;
import com.ruoyi.business.domain.BusGradeTeacher;
import com.ruoyi.business.domain.SubjectTeacher;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 班级教师Service接口
 * 
 * @author ruoyi
 * @date 2022-10-22
 */
public interface IBusGradeTeacherService extends IService<BusGradeTeacher>
{
    /**
     * 用作教师管理时将多表字段综合的数据封装
     * @return
     */
    public List<BusGradeTeacherDto> selectBusGradeTeacherDto();

    /**
     * 删除教师记录
     * @param userId
     * @return 布尔型
     */
    public boolean removeBusGradeTeacherDto(Long userId);
    /**
     * 查询班级教师
     * 
     * @param gradeId 班级教师主键
     * @return 班级教师
     */
    public BusGradeTeacher selectBusGradeTeacherByGradeId(Long gradeId);

    /**
     * 查询班级教师列表
     * 
     * @param busGradeTeacher 班级教师
     * @return 班级教师集合
     */
    public List<BusGradeTeacher> selectBusGradeTeacherList(BusGradeTeacher busGradeTeacher);

    /**
     * 新增班级教师
     * 
     * @param busGradeTeacher 班级教师
     * @return 结果
     */
    public int insertBusGradeTeacher(BusGradeTeacher busGradeTeacher);

    /**
     * 修改班级教师
     * 
     * @param busGradeTeacher 班级教师
     * @return 结果
     */
    public int updateBusGradeTeacher(BusGradeTeacher busGradeTeacher) throws DataAccessException;

    /**
     * 批量删除班级教师
     * 
     * @param gradeIds 需要删除的班级教师主键集合
     * @return 结果
     */
    public int deleteBusGradeTeacherByGradeIds(Long[] gradeIds);

    /**
     * 删除班级教师信息
     * 
     * @param gradeId 班级教师主键
     * @return 结果
     */
    public int deleteBusGradeTeacherByGradeId(Long gradeId);

    /**
     * 这个对应科目的所有老师信息
     * @return
     */
    Map<String, List<String>> getPostTeachers();

    JSONObject getGradeByUserId(Long userId);


    Set<GradeTeacherHistory> getHistoryGradeByUserId(Long userId);

    Set<GradeTeacherHistory> getCurrentGradeByUserId(Long userId);

    List<SubjectTeacher> getGradeTeacher(Long gradeId);

    List<Long> getLeadGrade(Long userId);
}
