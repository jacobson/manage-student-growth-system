package com.ruoyi.business.service;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.BusStudentGrowth;
import com.ruoyi.business.domain.BusStudentGrowthItem;
import com.ruoyi.business.domain.BusUserComments;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 学生成长报告项Service接口
 *
 */
public interface IBusStudentGrowthItemService extends IService<BusStudentGrowthItem>
{
    /**
     * 查询学生成长报告项
     * 
     * @param growth 学生成长报告
     * @return 学生成长报告项
     */
    Map<String, Object>selectBusStudentGrowthItemByGrowthId(BusStudentGrowth growth);
    /**
     * 成长报告名单的统计
     */
    List<Map<String,List<String>>> getGrowthStudentName(Long gradeId,Long semesterId);
    //成长报告未完成
    List<Map<String,List<String>>> getGrowthNotVerifyStudentName(Long gradeId,Long semesterId);


    /**
     * 新增学生成长报告项
     * 
     * @param busStudentGrowthItem 学生成长报告项
     * @return 结果
     */
    int insertBusStudentGrowthItem(BusStudentGrowthItem busStudentGrowthItem);

    /**
     * 修改学生成长报告项
     * 
     * @param busStudentGrowthItem 学生成长报告项
     * @return 结果
     */
    int updateBusStudentGrowthItem(BusStudentGrowthItem busStudentGrowthItem);


    /**
     * 删除学生成长报告项信息
     * 
     * @param growthItemId 学生成长报告项主键
     * @return 结果
     */
    int deleteBusStudentGrowthItemByGrowthItemId(Long growthItemId);

    void insertComments(BusUserComments busUserComments);

    Long getCurrentGrowthId(Long studentId);

    int uploadImg(MultipartFile file,BusStudentGrowthItem busStudentGrowthItem) throws IOException, InvalidExtensionException, InterruptedException;
    int uploadImg(BusStudentGrowthItem busStudentGrowthItem) throws InterruptedException;
    //将学生的成长报告存入redis中
    void storeGrowthItems(Long growthId,Map<String,Object> items);
    List<JSONObject> getHistoryItems(Long studentId, String itemKey);

    Date getEndTime();

    void updateStatus(Long growthId);

    void moveGrowthItem();

    Map<String, Object> getInfo(BusStudentGrowth growth);
}
