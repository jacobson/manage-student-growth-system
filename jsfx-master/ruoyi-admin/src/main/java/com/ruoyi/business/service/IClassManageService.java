package com.ruoyi.business.service;

import com.ruoyi.business.Dto.ClassGradeInfoDto;
import com.ruoyi.business.domain.ClassManageInfo;

import java.util.List;

public interface IClassManageService {

    //获取所有年级的班级基本信息
    List<ClassManageInfo> getListClassManageInfo(String YearLevelId);

    //获取单个班级的基本信息
    ClassGradeInfoDto getOneClassManageInfo(Long gradeId);

    //获取当前所有年级
    List<String> getAllYearId();

}
