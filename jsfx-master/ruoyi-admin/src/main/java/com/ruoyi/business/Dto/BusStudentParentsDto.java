package com.ruoyi.business.Dto;

import com.ruoyi.business.domain.BusStudents;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class BusStudentParentsDto extends BusStudents {

    //用来存家长的信息集合
    private Map<String, String> studentParentsMap;

    //家长的姓名
    private List<String> nickName;
}
