package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.BusSemester;

public interface IBusSemesterService extends IService<BusSemester> {
    /**
     * 获取当前学年学期的id
     * @return 学年学期id
     */
    Long getCurrentSemesterId();

    void updateStatus();

    Boolean exitReport();

}
