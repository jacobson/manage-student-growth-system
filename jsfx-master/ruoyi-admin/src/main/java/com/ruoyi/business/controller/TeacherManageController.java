package com.ruoyi.business.controller;


import cn.hutool.core.util.PhoneUtil;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.business.domain.*;
import com.ruoyi.business.service.IBusSubjectService;
import com.ruoyi.business.service.ITeacherManageService;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.exception.MyException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/business/teacherManage")
@Transactional
public class TeacherManageController extends BaseController {

    @Autowired
    private ITeacherManageService iTeacherManageService;

    @Autowired
    private IBusSubjectService subjectService;

//    全部教师集
    @GetMapping("/page")
    public TableDataInfo allTeacherManages(int pageSize,int pageNum){

        List<TeacherManage> teacherManages = iTeacherManageService.allTeacherManages();

        List<TeacherManage> TeacherManages =new ArrayList<>();
        //结果集
        int size = teacherManages.size();
        if (!teacherManages.isEmpty()){
            //解析分页参数
            if (pageSize > size) {
                pageSize = size;
            }
            // 求出最大页数，防止currentPage越界
            int maxPage = size % pageSize == 0 ? size / pageSize : size / pageSize + 1;
            if (pageNum > maxPage) {
                pageNum = maxPage;
            }
            // 当前页第一条数据的下标
            int curIdx = pageNum > 1 ? (pageNum - 1) * pageSize : 0;
            // 将当前页的数据放进busGradeTeacherDtos

            for (int i = 0; i < pageSize && curIdx + i < size; i++) {
                TeacherManages.add(teacherManages.get(curIdx + i));
            }
        }else {
            TableDataInfo rspData = new TableDataInfo();
            rspData.setCode(HttpStatus.SUCCESS);
            rspData.setMsg("查询成功,教师为空");
            rspData.setRows(TeacherManages);
            rspData.setTotal(size);
            return rspData;
        }


        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(TeacherManages);
        rspData.setTotal(size);
        return rspData;
    }

    /**
     * 条件筛选
     * @param name 名字
     */


    @GetMapping("/conditionTeacherManages")
    public TableDataInfo conditionTeacherManages(String name){
        List<TeacherManage> teacherManages = iTeacherManageService.conditionTeacherManages1(name);
        return getDataTable(teacherManages);
    }



    //新增教师
    @PostMapping("/addTeacherManageInfo")
    public AjaxResult addTeacherManageInfo(@RequestBody PointOnwTeacherManageInfo teacherManage) throws MyException {
        if (!PhoneUtil.isPhone(teacherManage.getPhonenumber())){
            return AjaxResult.error("所填手机号无效");
        }
        return iTeacherManageService.addTeacherManageInfoTest(teacherManage);
    }


    //编辑教师
    //回显教师数据
    @GetMapping("/echoTeacherManageInfo")
    public PointOnwTeacherManageInfo echoTeacherManageInfo(Long userId){
        return iTeacherManageService.echoTeacherManageInfo(userId);
    }



    //修改教师信息
    @PostMapping("/updateTeacherManageInfo")
    public AjaxResult updateTeacherManageInfo(@RequestBody PointOnwTeacherManageInfo teacherManage){
        if (!PhoneUtil.isPhone(teacherManage.getPhonenumber())){
            return AjaxResult.error("所填手机号无效");
        }
        return iTeacherManageService.updateTeacherManageInfo(teacherManage);
    }


    //删除教师信息
    @DeleteMapping("/removeTeacherManageInfo")
    public AjaxResult removeTeacherManageInfo(Long UserId){
        iTeacherManageService.deleteTeacherManageInfo(UserId);
        return AjaxResult.success("删除成功");
    }

    //new-全部教师及任课信息导出
    @PostMapping("/export")
    public void  export(HttpServletResponse response){
        List<TeacherManageExport> teacherManageExports = iTeacherManageService.exportGradeTeacherInfo();

        ExcelUtil<TeacherManageExport> teacherManageExportExcelUtil = new ExcelUtil<>(TeacherManageExport.class);
        teacherManageExportExcelUtil.exportExcel(response,teacherManageExports,"教师管理");
    }

    //excel导出-旧
    @PostMapping("/exportT")
    public void exportT(HttpServletResponse response){
        List<TeacherManage> teacherManages = iTeacherManageService.allTeacherManages();
        for (TeacherManage teacherManage : teacherManages) {
            teacherManage.setStrGradeNameList(StringUtils.join(teacherManage.getGradeNameList()));
            teacherManage.setStrMapSubject(JSONObject.toJSONString(teacherManage.getStrMapSubject()));
        }
        ExcelUtil<TeacherManage> excelUtil = new ExcelUtil<>(TeacherManage.class);
        excelUtil.exportExcel(response,teacherManages,"教师管理");
    }

    //new-excel全部教师及任课信息导入
    @Transactional
    @PostMapping("/import")
    public AjaxResult importData(MultipartFile file) throws Exception {
        boolean flag =false;
        ExcelUtil<TeacherManageExport> excelUtil = new ExcelUtil<>(TeacherManageExport.class);
        List<TeacherManageExport> teacherManageExports = excelUtil.importExcel(file.getInputStream());
        if (teacherManageExports.isEmpty()){
            return AjaxResult.error("导入为空");
        }
        for (TeacherManageExport teacherManageExport : teacherManageExports) {
            if (!PhoneUtil.isPhone(teacherManageExport.getPhonenumber())) {
                throw  new MyException("含无效手机号");
            }
            flag = iTeacherManageService.addTeacherManageExcelInfo(teacherManageExport);

        }

        return toAjax(flag);
    }

    //new-教师导入模板
    @PostMapping("exportTeacherManageModel")
    public void exportTeacherManageModel(HttpServletResponse response){
        ExcelUtil<TeacherManageExport> excelUtil = new ExcelUtil<>(TeacherManageExport.class);
        excelUtil.importTemplateExcel(response,"教师导入模板");
    }

    @Transactional
    @PostMapping("/importTeacher")
    public AjaxResult importTeacher(MultipartFile file) throws Exception {
        boolean flag =false;
        ExcelUtil<TeacherManage> excelUtil = new ExcelUtil<>(TeacherManage.class);
        //导入清洗
        List<TeacherManage> teacherManages = iTeacherManageService.washTeacherMangerExcel(excelUtil.importExcel(file.getInputStream()));
        if (teacherManages.isEmpty()){
            return AjaxResult.error("导入为空");
        }
        for (TeacherManage teacherManage : teacherManages) {
            if (PhoneUtil.isPhone(teacherManage.getPhonenumber())) {
                throw  new MyException("含无效手机号");
            }
            PointOnwTeacherManageInfo pointOnwTeacherManageInfo = new PointOnwTeacherManageInfo();
            BeanUtils.copyProperties(teacherManage,pointOnwTeacherManageInfo);
            flag=iTeacherManageService.addTeacherManageInfo(pointOnwTeacherManageInfo);
        }
        return toAjax(flag);
    }


    //excel导入  带课程班级
    @Transactional
    @PostMapping("/import2")
    public AjaxResult importData2(MultipartFile file) throws Exception {
        boolean flag =false;
        ExcelUtil<TeacherManageExport> excelUtil = new ExcelUtil<>(TeacherManageExport.class);

        List<TeacherManageExport> teacherManageExports = excelUtil.importExcel(file.getInputStream());
        if (teacherManageExports.isEmpty()){
            return AjaxResult.error("导入为空");
        }
        for (TeacherManageExport t : teacherManageExports) {
            if (PhoneUtil.isPhone(t.getPhonenumber())) {
                throw  new MyException("含无效手机号");
            }
            flag = iTeacherManageService.addTeacherManageExcelInfo(t);
        }
        return toAjax(flag);
    }

    //excel导入纯教师信息导入
    @Transactional
    @PostMapping("/importTeacherAccount")
    public AjaxResult importData3(MultipartFile file) throws Exception {
        boolean flag =false;
        ExcelUtil<TeacherInfoExport> excelUtil = new ExcelUtil<>(TeacherInfoExport.class);

        List<TeacherInfoExport> teacherInfoExport = excelUtil.importExcel(file.getInputStream());
        if (teacherInfoExport.isEmpty()) {
            return AjaxResult.error("导入为空");
        }
        for (TeacherInfoExport t : teacherInfoExport) {
            if (!PhoneUtil.isPhone(t.getPhonenumber())) {
                throw  new MyException("含无效手机号");
            }
            flag = iTeacherManageService.addTeacherInfo(t);
        }

        return toAjax(flag);
    }

    @PostMapping("exportTeacherModel")
    public void exportTeacherModel(HttpServletResponse response){
        ExcelUtil<TeacherInfoExport> excelUtil = new ExcelUtil<>(TeacherInfoExport.class);
        excelUtil.importTemplateExcel(response,"教师导入模板");
    }
    //班级教师配置
    @Transactional
    @PutMapping("/changeGradeTeacher")
    public AjaxResult changeGradeTeacher(@RequestBody GradeTeacherBody gradeTeacher){
        if (iTeacherManageService.changeGradeTeacher(gradeTeacher)== 0){
            throw new MyException("修改失败");
        }
        return AjaxResult.success("修改成功");
    }
    //查看某科的所有教师
    @GetMapping("getSubjectTeacher")
    public AjaxResult getSubjectTeacher(String subjectName){
        Boolean exit = subjectService.exitSubject(subjectName);
        if (!exit){
            return AjaxResult.error("不存在该学科");
        }
        return AjaxResult.success(iTeacherManageService.
               getSubjectTeacher(subjectName+"老师"));
    }

}
