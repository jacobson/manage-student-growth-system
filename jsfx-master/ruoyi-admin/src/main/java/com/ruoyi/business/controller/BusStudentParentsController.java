package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusStudentParents;
import com.ruoyi.business.service.IBusStudentParentsService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 学生家长Controller
 * 
 * @author ruoyi
 * @date 2022-10-30
 */
@RestController
@RequestMapping("/system/parents")
public class BusStudentParentsController extends BaseController
{
    @Autowired
    private IBusStudentParentsService busStudentParentsService;

    /**
     * 查询学生家长列表
     */
    @PreAuthorize("@ss.hasPermi('system:parents:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusStudentParents busStudentParents)
    {
        startPage();
        List<BusStudentParents> list = busStudentParentsService.selectBusStudentParentsList(busStudentParents);
        return getDataTable(list);
    }

    /**
     * 导出学生家长列表
     */
    @PreAuthorize("@ss.hasPermi('system:parents:export')")
    @Log(title = "学生家长", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusStudentParents busStudentParents)
    {
        List<BusStudentParents> list = busStudentParentsService.selectBusStudentParentsList(busStudentParents);
        ExcelUtil<BusStudentParents> util = new ExcelUtil<BusStudentParents>(BusStudentParents.class);
        util.exportExcel(response, list, "学生家长数据");
    }

    /**
     * 获取学生家长详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:parents:query')")
    @GetMapping(value = "/{studentId}")
    public AjaxResult getInfo(@PathVariable("studentId") Long studentId)
    {
        return AjaxResult.success(busStudentParentsService.selectBusStudentParentsByStudentId(studentId));
    }

    /**
     * 新增学生家长
     */
    @PreAuthorize("@ss.hasPermi('system:parents:add')")
    @Log(title = "学生家长", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusStudentParents busStudentParents)
    {
        return toAjax(busStudentParentsService.insertBusStudentParents(busStudentParents));
    }

    /**
     * 修改学生家长
     */
    @PreAuthorize("@ss.hasPermi('system:parents:edit')")
    @Log(title = "学生家长", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusStudentParents busStudentParents)
    {
        return toAjax(busStudentParentsService.updateBusStudentParents(busStudentParents));
    }

    /**
     * 删除学生家长
     */
    @PreAuthorize("@ss.hasPermi('system:parents:remove')")
    @Log(title = "学生家长", businessType = BusinessType.DELETE)
	@DeleteMapping("/{studentIds}")
    public AjaxResult remove(@PathVariable Long[] studentIds)
    {
        return toAjax(busStudentParentsService.deleteBusStudentParentsByStudentIds(studentIds));
    }
}
