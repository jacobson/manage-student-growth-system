package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.Dto.GradeTeacherHistory;
import com.ruoyi.business.domain.BusGradeGallery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 班级图库Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@Mapper
public interface BusGradeGalleryMapper extends BaseMapper<BusGradeGallery>
{


    /**
     * 新增班级图库
     * 
     * @param busGradeGallery 班级图库
     * @return 结果
     */
    public int insertBusGradeGallery(BusGradeGallery busGradeGallery);

    /**
     * 修改班级图库
     * 
     * @param busGradeGallery 班级图库
     * @return 结果
     */
    public int updateBusGradeGallery(BusGradeGallery busGradeGallery);

    /**
     * 删除班级图库
     * 
     * @param gradeId 班级图库主键
     * @return 结果
     */
    public int deleteBusGradeGalleryByGradeId(Long gradeId);

    /**
     * 批量删除班级图库
     * 
     * @param gradeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusGradeGalleryByGalleryIds(Long[] gradeIds);
    /**
     * 查询班主任的历史所有图库
     */
    List<BusGradeGallery> getGradeGallery(GradeTeacherHistory history);
    //获取该班级当前学期的图库
    List<Long> getGradeGalleryByStuId(@Param("studentId") Long studentId, @Param("currentSemesterId") Long currentSemesterId);
}
