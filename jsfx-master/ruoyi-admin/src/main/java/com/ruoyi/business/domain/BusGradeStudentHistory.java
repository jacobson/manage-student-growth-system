package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * 班级学生对象 bus_grade_student_history
 * 
 * @author ruoyi
 * @date 2022-11-12
 */
@Data
public class BusGradeStudentHistory
{
    private static final long serialVersionUID = 1L;

    /** 班级id */
    @Excel(name = "班级id")
    private Long gradeId;

    /** 学生id */
    @Excel(name = "学生id")
    private Long studentId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long semesterId;

}
