package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.UserPost;

public interface IUserPostService extends IService<UserPost> {
}
