package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.YearGrade;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface IYearGradeService extends IService<YearGrade> {
    //年级，班级，学年，学期接口
    Map<String, HashMap<Long,List<String>>> getyearGrades();
    //年级，班级接口
    Map<String, List<Long>> getYearGrades();

}
