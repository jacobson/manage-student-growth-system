package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusSubject;
import com.ruoyi.business.service.IBusSubjectService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 学科Controller
 * 
 * @author ruoyi
 * @date 2022-10-18
 */
@RestController
@RequestMapping("/business/subject")
public class BusSubjectController extends BaseController
{
    @Autowired
    private IBusSubjectService busSubjectService;


    @GetMapping("/all")
    public TableDataInfo allSubject(){
        return getDataTable( busSubjectService.list());
    }
    /**
     * 查询学科列表
     */
    @GetMapping("test/{id}")
    public String test(@PathVariable int id){
        return "id" + id;
    }
//    @PreAuthorize("@ss.hasPermi('business:subject:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusSubject busSubject)
    {
        startPage();
        List<BusSubject> list = busSubjectService.selectBusSubjectList(busSubject);
        return getDataTable(list);
    }

    /**
     * 导出学科列表
     */
    @PreAuthorize("@ss.hasPermi('business:subject:export')")
    @Log(title = "学科", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusSubject busSubject)
    {
        List<BusSubject> list = busSubjectService.selectBusSubjectList(busSubject);
        ExcelUtil<BusSubject> util = new ExcelUtil<BusSubject>(BusSubject.class);
        util.exportExcel(response, list, "学科数据");
    }

    /**
     * 获取学科详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:subject:query')")
    @GetMapping(value = "/{subjectId}")
    public AjaxResult getInfo(@PathVariable("subjectId") Long subjectId)
    {
        return AjaxResult.success(busSubjectService.selectBusSubjectBySubjectId(subjectId));
    }

    /**
     * 新增学科
     */
    @PreAuthorize("@ss.hasPermi('business:subject:add')")
    @Log(title = "学科", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusSubject busSubject)
    {
        return toAjax(busSubjectService.insertBusSubject(busSubject));
    }

    /**
     * 修改学科
     */
    @PreAuthorize("@ss.hasPermi('business:subject:edit')")
    @Log(title = "学科", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusSubject busSubject)
    {
        return toAjax(busSubjectService.updateBusSubject(busSubject));
    }

    /**
     * 删除学科
     */
    @PreAuthorize("@ss.hasPermi('business:subject:remove')")
    @Log(title = "学科", businessType = BusinessType.DELETE)
	@DeleteMapping("/{subjectIds}")
    public AjaxResult remove(@PathVariable Long[] subjectIds)
    {
        return toAjax(busSubjectService.deleteBusSubjectBySubjectIds(subjectIds));
    }
}
