package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusGradeTeacher;
import com.ruoyi.business.domain.BusUserComments;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户评语（教师、班主任）Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-28
 */
@Mapper
public interface BusUserCommentsMapper extends BaseMapper<BusUserComments>
{



    /**
     * 查询用户评语（教师、班主任）
     * 
     * @param userId 用户评语（教师、班主任）主键
     * @return 用户评语（教师、班主任）
     */
     BusUserComments selectBusUserCommentsByUserId(Long userId);

    /**
     * 查询用户评语（教师、班主任）列表
     * 
     * @param busUserComments 用户评语（教师、班主任）
     * @return 用户评语（教师、班主任）集合
     */
     List<BusUserComments> selectBusUserCommentsList(BusUserComments busUserComments);

    /**
     * 新增用户评语（教师、班主任）
     * 
     * @param busUserComments 用户评语（教师、班主任）
     * @return 结果
     */
     int insertBusUserComments(BusUserComments busUserComments);

    /**
     * 修改用户评语（教师、班主任）
     * 
     * @param busUserComments 用户评语（教师、班主任）
     * @return 结果
     */
     int updateBusUserComments(BusUserComments busUserComments);

    /**
     * 删除用户评语（教师、班主任）
     * 
     * @param userId 用户评语（教师、班主任）主键
     * @return 结果
     */
     int deleteBusUserCommentsByUserId(Long userId);

    /**
     * 批量删除用户评语（教师、班主任）
     * 
     * @param userIds 需要删除的数据主键集合
     * @return 结果
     */
     int deleteBusUserCommentsByUserIds(Long[] userIds);

    /**
     * 查询学生的教师评语和班主任评语是否都已完成
     */
    List<String> checkCommentFinish(BusUserComments busUserComments);
    /**
     * 查询该学生的教师及班主任评语
     */
    List<BusUserComments> getAllComments(BusUserComments busUserComments);


    Long getGradeId(Long studentId);

    List<BusGradeTeacher> getTeacher(Long gradeId);

    String getTeacherName(Long userId);
    @Delete("delete from bus_user_comments")
    void clear();
    @Select("select student_id from bus_user_comments where student_id=#{studentId} and remark=1")
    Long exitComment(Long studentId);
}
