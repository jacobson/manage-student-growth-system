package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.Dto.BusStudentParentsDto;
import com.ruoyi.business.domain.BusStudentParents;
import com.ruoyi.business.domain.BusUser;
import com.ruoyi.business.mapper.BusUserMapper;
import com.ruoyi.business.service.IBusStudentParentsService;
import com.ruoyi.business.service.IBusUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 用户 业务层处理
 * 
 * @author ruoyi
 */
@Service
public class BusUserServiceImpl extends ServiceImpl<BusUserMapper,BusUser> implements IBusUserService
{
    private static final Logger log = LoggerFactory.getLogger(BusUserServiceImpl.class);

    @Autowired
    private BusUserMapper userMapper;

    @Autowired
    private IBusStudentParentsService iBusStudentParentsService;

    @Autowired
    private IBusUserService iBusUserService;



    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    @Override
    public BusUser selectUserById(Long userId)
    {
        return userMapper.selectUserById(userId);
    }

    @Override
    public boolean insertUser(BusUser user) {
        return userMapper.insertUser(user);
    }

    @Override
    public boolean updateBusUserByCreatBy(BusStudentParentsDto busStudentParentsDto) {

        Map<String, String> studentParentsMap = busStudentParentsDto.getStudentParentsMap();
        boolean b1 =false,b2 =false;
        Long stuId = busStudentParentsDto.getStuId();
        List<BusStudentParents> busStudentParents = iBusStudentParentsService.selectBusStudentParentsByStudentIds(busStudentParentsDto.getStuId());
        for (BusStudentParents busStudentParent : busStudentParents) {
            List<String> nickName = busStudentParentsDto.getNickName();
            for (String s : nickName) {
                busStudentParent.setRelation(s);
                b1 = iBusStudentParentsService.save(busStudentParent);
                nickName.remove(0);
                break;
            }
        }
        for (String key : studentParentsMap.keySet()) {

            for (BusStudentParents busStudentParent : busStudentParents) {
                String name = key;
                String phoneNumber = studentParentsMap.get(key);

                BusUser busUser = new BusUser();
                busUser.setUserId(busStudentParent.getUserId());
                busUser.setNickName(name);
                busUser.setPhonenumber(phoneNumber);
                baseMapper.deleteUserById(busUser.getUserId());

                String str = UUID.randomUUID().toString();
                busUser.setCreateBy(str);
                busUser.setDeptId(201L);
                b2 = iBusUserService.insertUser(busUser);
                studentParentsMap.remove(0);
                break;
            }

        }

        return b1 && b2;
    }

    @Override
    public boolean updateBusUser(BusUser busUser) {
        System.out.println("=======================");
        LambdaQueryWrapper<BusUser> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(BusUser::getUserId,busUser.getUserId());
        return this.update(busUser,userLambdaQueryWrapper);
    }


}
