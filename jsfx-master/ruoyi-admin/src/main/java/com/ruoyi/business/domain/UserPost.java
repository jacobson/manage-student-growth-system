package com.ruoyi.business.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("sys_user_post")
public class UserPost {

    /** 用户ID */
    private Long userId;

    /** 岗位ID */
    private Long postId;
}
