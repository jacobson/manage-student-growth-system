package com.ruoyi.business.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.Dto.BusStudentParentsDtoT;
import com.ruoyi.business.domain.BusUser;
import com.ruoyi.common.core.domain.AjaxResult;

import java.util.List;
import java.util.Map;


public interface IStudentManageService extends IService<BusStudentParentsDtoT> {
    //拿这个指定班级的所有学生信息
    List<BusStudentParentsDtoT> getPointGradeStudentInfo(Long gradeId);

    //返回年级段及其班级信息
    Map<Long,List<Long>> getYearLevelGradeId();

    //编辑学生回显
    BusStudentParentsDtoT getStudentWithParentsInfo(Long StudentId);


    //修改保存
    AjaxResult updateStudentWithParentsInfo(BusStudentParentsDtoT busStudentParentsDtoT);


    //新增学生
    boolean addStudentWithParentsInfo(BusStudentParentsDtoT busStudentParentsDtoT);



    //找到与孩子绑定的爸爸信息
    BusUser findFatherInfo(BusStudentParentsDtoT busStudentParentsDtoT);

    //找到与孩子绑定的爸爸信息
    BusUser findMotherInfo(BusStudentParentsDtoT busStudentParentsDtoT);

    //修改孩子和家长所有的信息
    boolean updateAllInfo(BusStudentParentsDtoT busStudentParentsDtoT,BusUser fatherBusUser,BusUser motherBusUser);

    //修改孩子和家长所有的信息
    boolean updateAllInfoXg(Long stuId,BusStudentParentsDtoT busStudentParentsDtoT,BusUser fatherBusUser,BusUser motherBusUser);

    //修改当前班级人数
    boolean updateGradeStudentsNumber(Long gradeId);
    //设置当前学生学号
    Long setStudentId(Long gradeId);
    //新增一个学生时，班级人数变化，以及学生id设置，以及保存这个学生和学生班级表
    boolean addANewStudent(BusStudentParentsDtoT busStudentParentsDtoT,Long studentId);

    //新增爸爸的信息
    boolean addANewFather(BusStudentParentsDtoT busStudentParentsDtoT,String fatherId);
    //新增妈妈的信息
    boolean addANewMother(BusStudentParentsDtoT busStudentParentsDtoT, String motherId);
    //新增孩子和爸爸之间关系的信息
    boolean addAnewStudentWithFather(String fatherId,Long studentId);
    //家长角色表中增加爸爸信息
    boolean addFatherWithRole(Long roleId, String fatherId);
    //新增孩子和妈妈之间关系的信息
    boolean addAnewStudentWithMother(String motherId, Long studentId);
    //家长角色表中增加妈妈信息
    boolean addMotherWithRole(Long roleId, String motherId);

    //编辑操作中班级减少一个人
    boolean updateGradeStudentsNumberBj(Long gradeId);

    //班级改变后，修改学生和家长
    boolean updateAllInfoBj(BusStudentParentsDtoT busStudentParentsDtoT,BusUser fatherBusUser,BusUser motherBusUser);

}
