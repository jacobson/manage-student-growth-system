package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.BusGradeStudentHistory;
import com.ruoyi.business.mapper.BusGradeStudentHistoryMapper;
import com.ruoyi.business.service.IBusGradeStudentHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 班级学生Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-12
 */
@Service
public class BusGradeStudentHistoryServiceImpl extends ServiceImpl<BusGradeStudentHistoryMapper,BusGradeStudentHistory> implements IBusGradeStudentHistoryService
{
    @Autowired
    private BusGradeStudentHistoryMapper busGradeStudentHistoryMapper;

    /**
     * 查询班级学生
     * 
     * @param gradeId 班级学生主键
     * @return 班级学生
     */
    @Override
    public BusGradeStudentHistory selectBusGradeStudentHistoryByGradeId(Long gradeId)
    {
        return busGradeStudentHistoryMapper.selectBusGradeStudentHistoryByGradeId(gradeId);
    }

    /**
     * 查询班级学生列表
     * 
     * @param busGradeStudentHistory 班级学生
     * @return 班级学生
     */
    @Override
    public List<BusGradeStudentHistory> selectBusGradeStudentHistoryList(BusGradeStudentHistory busGradeStudentHistory)
    {
        return busGradeStudentHistoryMapper.selectBusGradeStudentHistoryList(busGradeStudentHistory);
    }

    /**
     * 新增班级学生
     * 
     * @param busGradeStudentHistory 班级学生
     * @return 结果
     */
    @Override
    public int insertBusGradeStudentHistory(BusGradeStudentHistory busGradeStudentHistory)
    {
        return busGradeStudentHistoryMapper.insertBusGradeStudentHistory(busGradeStudentHistory);
    }

    /**
     * 修改班级学生
     * 
     * @param busGradeStudentHistory 班级学生
     * @return 结果
     */
    @Override
    public int updateBusGradeStudentHistory(BusGradeStudentHistory busGradeStudentHistory)
    {
        return busGradeStudentHistoryMapper.updateBusGradeStudentHistory(busGradeStudentHistory);
    }

    /**
     * 批量删除班级学生
     * 
     * @param gradeIds 需要删除的班级学生主键
     * @return 结果
     */
    @Override
    public int deleteBusGradeStudentHistoryByGradeIds(Long[] gradeIds)
    {
        return busGradeStudentHistoryMapper.deleteBusGradeStudentHistoryByGradeIds(gradeIds);
    }

    /**
     * 删除班级学生信息
     * 
     * @param gradeId 班级学生主键
     * @return 结果
     */
    @Override
    public int deleteBusGradeStudentHistoryByGradeId(Long gradeId)
    {
        return busGradeStudentHistoryMapper.deleteBusGradeStudentHistoryByGradeId(gradeId);
    }
}
