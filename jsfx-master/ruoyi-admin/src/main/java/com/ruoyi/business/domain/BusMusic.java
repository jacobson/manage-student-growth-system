package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 推荐音乐对象 bus_music
 * 
 * @author ruoyi
 * @date 2022-10-23
 */
public class BusMusic
{
    private static final long serialVersionUID = 1L;

    /** 音乐id */
    private Long musicId;

    /** 音乐名称 */
    @Excel(name = "音乐名称")
    private String musicName;

    /** 音乐地址 */
    @Excel(name = "音乐地址")
    private String musicUrl;

    public void setMusicId(Long musicId) 
    {
        this.musicId = musicId;
    }

    public Long getMusicId() 
    {
        return musicId;
    }
    public void setMusicName(String musicName) 
    {
        this.musicName = musicName;
    }

    public String getMusicName() 
    {
        return musicName;
    }
    public void setMusicUrl(String musicUrl) 
    {
        this.musicUrl = musicUrl;
    }

    public String getMusicUrl() 
    {
        return musicUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("musicId", getMusicId())
            .append("musicName", getMusicName())
            .append("musicUrl", getMusicUrl())
            .toString();
    }
}
