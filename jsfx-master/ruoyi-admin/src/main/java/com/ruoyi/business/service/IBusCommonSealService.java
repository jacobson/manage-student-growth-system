package com.ruoyi.business.service;

import com.ruoyi.business.domain.BusCommonSeal;

import java.util.List;
/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2022-11-01
 */
public interface IBusCommonSealService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param sealName 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public BusCommonSeal selectBusCommonSealBySealName(String sealName);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param busCommonSeal 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<BusCommonSeal> selectBusCommonSealList(BusCommonSeal busCommonSeal);

    /**
     * 新增【请填写功能名称】
     *
     * @return 结果
     */
    public int insertBusCommonSeal(String sealName, String url, String sealIllustrate);

    /**
     * 修改【请填写功能名称】
     * 
     * @param busCommonSeal 【请填写功能名称】
     * @return 结果
     */
    public int updateBusCommonSeal(BusCommonSeal busCommonSeal);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param sealNames 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteBusCommonSealBySealNames(String[] sealNames);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param sealName 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteBusCommonSealBySealName(String sealName);

}
