package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusGrade;
import com.ruoyi.business.service.IBusGradeService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 班级Controller
 * 
 * @author ruoyi
 * @date 2022-10-22
 */
@RestController
@RequestMapping("/business/grade")
public class BusGradeController extends BaseController
{
    @Autowired
    private IBusGradeService busGradeService;

    /**
     * 查询班级列表
     */
    @GetMapping("/class")
    public TableDataInfo getClassInfo(Long userId){
        List<BusGrade> busGradedList = busGradeService.getBusGradedList(userId);
        return getDataTable(busGradedList);
    }

    /**
     * 查询班级列表
     */
    @PreAuthorize("@ss.hasPermi('business:grade:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusGrade busGrade)
    {
        startPage();
        List<BusGrade> list = busGradeService.selectBusGradeList(busGrade);
        return getDataTable(list);
    }

    /**
     * 导出班级列表
     */
    @PreAuthorize("@ss.hasPermi('business:grade:export')")
    @Log(title = "班级", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusGrade busGrade)
    {
        List<BusGrade> list = busGradeService.selectBusGradeList(busGrade);
        ExcelUtil<BusGrade> util = new ExcelUtil<BusGrade>(BusGrade.class);
        util.exportExcel(response, list, "班级数据");
    }

    /**
     * 获取班级详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:grade:query')")
    @GetMapping(value = "/{gradeId}")
    public AjaxResult getInfo(@PathVariable("gradeId") Long gradeId)
    {
        return AjaxResult.success(busGradeService.selectBusGradeByGradeId(gradeId));
    }

    /**
     * 新增班级
     */
    @PreAuthorize("@ss.hasPermi('business:grade:add')")
    @Log(title = "班级", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusGrade busGrade)
    {
        return toAjax(busGradeService.insertBusGrade(busGrade));
    }

    /**
     * 修改班级
     */
    @PreAuthorize("@ss.hasPermi('business:grade:edit')")
    @Log(title = "班级", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusGrade busGrade)
    {
        return toAjax(busGradeService.updateBusGrade(busGrade));
    }


}
