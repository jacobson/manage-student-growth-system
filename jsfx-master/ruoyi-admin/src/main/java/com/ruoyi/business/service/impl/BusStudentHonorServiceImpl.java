package com.ruoyi.business.service.impl;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.Util.HonorUtil;
import com.ruoyi.business.domain.BusGradeStudent;
import com.ruoyi.business.domain.BusStudentHonor;
import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.business.domain.HonorBody;
import com.ruoyi.business.mapper.BusStudentHonorMapper;
import com.ruoyi.business.service.IBusGradeStudentService;
import com.ruoyi.business.service.IBusSemesterService;
import com.ruoyi.business.service.IBusStudentHonorService;
import com.ruoyi.business.service.IBusStudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 学生荣誉Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@Service
public class BusStudentHonorServiceImpl extends ServiceImpl<BusStudentHonorMapper,BusStudentHonor> implements IBusStudentHonorService
{
    @Autowired
    private BusStudentHonorMapper busStudentHonorMapper;

    @Autowired
    private IBusGradeStudentService gradeStudentService;

    @Autowired
    private IBusStudentsService busStudentsService;

    @Autowired
    private IBusStudentHonorService busStudentHonorService;

    @Autowired
    private IBusSemesterService semesterService;

    @Override
    public boolean updateModuleInfo(List<Long> studentList, String module, boolean flag) {
        boolean result = false;
        for (Long key:studentList) {
            //先查询这一个学生的荣誉对象
            LambdaQueryWrapper<BusStudentHonor> busStudentWrapper = new LambdaQueryWrapper<>();
            busStudentWrapper.eq(BusStudentHonor::getStudentId,key);
            //通过自定义的模块分析工具进行分析该值
            BusStudentHonor busStudentHonor = HonorUtil.confirmBusStudentHonor(busStudentHonorService.getOne(busStudentWrapper), module,flag);
            LambdaQueryWrapper<BusStudentHonor> busStudentHonorLambdaQueryWrapper = new LambdaQueryWrapper<>();
            busStudentHonorLambdaQueryWrapper.eq(BusStudentHonor::getStudentId,busStudentHonor.getStudentId());
            //重新修改荣誉对象的对应值
            result = busStudentHonorService.update(busStudentHonor,busStudentHonorLambdaQueryWrapper);
        }
        return result;
    }

    @Override
    public List<Map<String, List<String>>> getHonorModuleInfo(Long gradeId) {

        //存储各个模块的人员信息
        Map<String, List<String>> mapHonor = HonorUtil.getMapHonor();
        //拿到这个班级ID
        LambdaQueryWrapper<BusGradeStudent> gradeStudentWrapper=new LambdaQueryWrapper<>();
        gradeStudentWrapper.eq(BusGradeStudent::getGradeId,gradeId);
        List<BusGradeStudent> list = gradeStudentService.list(gradeStudentWrapper);
//        历遍这个班级的学生
        for (BusGradeStudent item:list) {
            //先拿到当前这个学生的ID
            Long studentId = item.getStudentId();
            //主要是需要这个学生的姓名
            LambdaQueryWrapper<BusStudents> busStudentsLambdaQueryWrapper=new LambdaQueryWrapper<>();
            busStudentsLambdaQueryWrapper.eq(BusStudents::getStuId,studentId);
            BusStudents studentInfo = busStudentsService.getOne(busStudentsLambdaQueryWrapper);
            List<String> StudentAllName = mapHonor.get("candidate");
            StudentAllName.add(studentId+":"+studentInfo.getStuName());
            mapHonor.put("candidate",StudentAllName);
            //拿到这个学生的荣誉信息
            LambdaQueryWrapper<BusStudentHonor> honorLambdaQueryWrapper=new LambdaQueryWrapper<>();
            honorLambdaQueryWrapper.eq(BusStudentHonor::getStudentId,studentId);
            BusStudentHonor studentHonorInfo = busStudentHonorService.getOne(honorLambdaQueryWrapper);
            //统计该学生获得的荣誉项
            int Count=0;
            List<String> honorList = studentHonorInfo.getHonorList();
            //添加荣誉者名单
            for (int i=1;i<=5;i++) {
                //字段的拼接
                String honor="honor"+""+i;
                if (honorList.get(i-1).equals("1")) {
                    Count++;
                    List<String> honorName = mapHonor.get(honor);
                    honorName.add(studentId+":"+studentInfo.getStuName());
                    mapHonor.put(honor, honorName);
                }
            }
            //移除三项荣誉获得者
            if (Count==3){
                for (int i=0;i< StudentAllName.size();i++){
                    if (StudentAllName.get(i).equals(studentId+":"+studentInfo.getStuName())){
                        StudentAllName.remove(i);
                        break;
                    }
                }
            }
        }
        List<Map<String, List<String>>> list1=new ArrayList<>();
        list1.add(mapHonor);
        return list1;
    }

    /**
     * 查询学生荣誉
     * 
     * @param growthId 学生荣誉主键
     * @return 学生荣誉
     */
    @Override
    public BusStudentHonor selectBusStudentHonorByGrowthId(Long growthId)
    {
        return busStudentHonorMapper.selectBusStudentHonorByGrowthId(growthId);
    }

    /**
     * 查询学生荣誉列表
     * 
     * @param busStudentHonor 学生荣誉
     * @return 学生荣誉
     */
    @Override
    public List<BusStudentHonor> selectBusStudentHonorList(BusStudentHonor busStudentHonor)
    {
        return busStudentHonorMapper.selectBusStudentHonorList(busStudentHonor);
    }

    /**
     * 新增学生荣誉
     * 
     * @param busStudentHonor 学生荣誉
     * @return 结果
     */
    @Override
    public int insertBusStudentHonor(BusStudentHonor busStudentHonor)
    {
        return busStudentHonorMapper.insertBusStudentHonor(busStudentHonor);
    }

    /**
     * 修改学生荣誉
     * 
     * @param busStudentHonor 学生荣誉
     * @return 结果
     */
    @Override
    public int updateBusStudentHonor(BusStudentHonor busStudentHonor)
    {
        return busStudentHonorMapper.updateBusStudentHonor(busStudentHonor);
    }

    /**
     * 批量删除学生荣誉
     * 
     * @param growthIds 需要删除的学生荣誉主键
     * @return 结果
     */
    @Override
    public int deleteBusStudentHonorByGrowthIds(Long[] growthIds)
    {
        return busStudentHonorMapper.deleteBusStudentHonorByGrowthIds(growthIds);
    }

    /**
     * 删除学生荣誉信息
     * 
     * @param growthId 学生荣誉主键
     * @return 结果
     */
    @Override
    public int deleteBusStudentHonorByGrowthId(Long growthId)
    {
        return busStudentHonorMapper.deleteBusStudentHonorByGrowthId(growthId);
    }
    @Transactional
    @Override
    public int confirmList(Long gradeId) {
        //获取班级学生
        List<Long> studentIds = busStudentsService.
                selectStudentIdsByGradeId(gradeId);
        if (studentIds.isEmpty()){
            return 0;
        }
        //获取其荣誉
        List<HonorBody> studentHonor = busStudentHonorMapper.getStudentHonor(studentIds);
        studentHonor.forEach((honor)->{
            JSONObject entries = new JSONObject();
            entries.set("honor1",honor.getHonor1());
            entries.set("honor2",honor.getHonor2());
            entries.set("honor3",honor.getHonor3());
            entries.set("honor4",honor.getHonor4());
            entries.set("honor5",honor.getHonor5());
            busStudentHonorMapper.confirmList(entries.toString(),
                    honor.getStudentId(), semesterService.getCurrentSemesterId());
        });
        return 1;
    }


}
