package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.MyPost;

public interface IMyPostService extends IService<MyPost> {
}
