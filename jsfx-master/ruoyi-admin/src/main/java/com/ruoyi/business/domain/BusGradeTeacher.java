package com.ruoyi.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 班级教师对象 bus_grade_teacher
 * 
 * @author ruoyi
 * @date 2022-10-22
 */
public class BusGradeTeacher
{
    private static final long serialVersionUID = 1L;

    /** 班级id */
    @Excel(name = "班级id")
    private Long gradeId;

    /** 岗位id */
    @Excel(name = "岗位id")
    private Long postId;

    /** 教师id */
    @Excel(name = "教师id")
    private Long userId;

    /** 任课科目id */
    @Excel(name = "任课科目id")
    private Long subjectId;
    /**教师名*/
    @TableField(exist = false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGradeId(Long gradeId)
    {
        this.gradeId = gradeId;
    }

    public Long getGradeId() 
    {
        return gradeId;
    }
    public void setPostId(Long postId) 
    {
        this.postId = postId;
    }

    public Long getPostId() 
    {
        return postId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setSubjectId(Long subjectId) 
    {
        this.subjectId = subjectId;
    }

    public Long getSubjectId() 
    {
        return subjectId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("gradeId", getGradeId())
            .append("postId", getPostId())
            .append("userId", getUserId())
            .append("subjectId", getSubjectId())
            .toString();
    }
}
