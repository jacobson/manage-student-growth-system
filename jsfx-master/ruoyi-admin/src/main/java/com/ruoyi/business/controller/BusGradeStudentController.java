package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusGradeStudent;
import com.ruoyi.business.service.IBusGradeStudentService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 班级学生Controller
 * 
 * @author ruoyi
 * @date 2022-10-24
 */
@RestController
@RequestMapping("/business/student")
public class BusGradeStudentController extends BaseController
{
    @Autowired
    private IBusGradeStudentService busGradeStudentService;

    /**
     * 查询班级学生列表
     */
    @PreAuthorize("@ss.hasPermi('system:student:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusGradeStudent busGradeStudent)
    {
        startPage();
        List<BusGradeStudent> list = busGradeStudentService.selectBusGradeStudentList(busGradeStudent);
        return getDataTable(list);
    }

    /**
     * 导出班级学生列表
     */
    @PreAuthorize("@ss.hasPermi('system:student:export')")
    @Log(title = "班级学生", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusGradeStudent busGradeStudent)
    {
        List<BusGradeStudent> list = busGradeStudentService.selectBusGradeStudentList(busGradeStudent);
        ExcelUtil<BusGradeStudent> util = new ExcelUtil<BusGradeStudent>(BusGradeStudent.class);
        util.exportExcel(response, list, "班级学生数据");
    }

    /**
     * 获取班级学生详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:student:query')")
    @GetMapping(value = "/{gradeId}")
    public AjaxResult getInfo(@PathVariable("gradeId") Long gradeId)
    {
        return AjaxResult.success(busGradeStudentService.selectBusGradeStudentByGradeId(gradeId));
    }

    /**
     * 新增班级学生
     */
    @PreAuthorize("@ss.hasPermi('system:student:add')")
    @Log(title = "班级学生", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusGradeStudent busGradeStudent)
    {
        return toAjax(busGradeStudentService.insertBusGradeStudent(busGradeStudent));
    }

    /**
     * 修改班级学生
     */
    @PreAuthorize("@ss.hasPermi('system:student:edit')")
    @Log(title = "班级学生", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusGradeStudent busGradeStudent)
    {
        return toAjax(busGradeStudentService.updateBusGradeStudent(busGradeStudent));
    }

    /**
     * 删除班级学生
     */
    @PreAuthorize("@ss.hasPermi('system:student:remove')")
    @Log(title = "班级学生", businessType = BusinessType.DELETE)
	@DeleteMapping("/{gradeIds}")
    public AjaxResult remove(@PathVariable Long[] gradeIds)
    {
        return toAjax(busGradeStudentService.deleteBusGradeStudentByGradeIds(gradeIds));
    }
}
