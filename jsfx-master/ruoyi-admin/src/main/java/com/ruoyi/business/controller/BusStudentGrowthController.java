package com.ruoyi.business.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONObject;
import com.ruoyi.business.domain.BusStudentGrowth;
import com.ruoyi.business.service.IBusSemesterService;
import com.ruoyi.business.service.IBusStudentGrowthItemService;
import com.ruoyi.business.service.IBusStudentGrowthService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 学生成长报告Controller
 * 
 * @author ruoyi
 * @date 2022-10-22
 */
@RestController
@RequestMapping("/business/growth")
public class BusStudentGrowthController extends BaseController
{
    @Autowired
    private IBusStudentGrowthService busStudentGrowthService;
    @Autowired
    private IBusSemesterService semesterService;
    @Autowired
    private IBusStudentGrowthItemService growthItemService;
    /**
     * 根据学生id查询其拥有的成长报告
     */
    @GetMapping("/{stuId}")
    public AjaxResult getGrowthsByStu(@PathVariable Long stuId){
        List<BusStudentGrowth> growths = busStudentGrowthService.
                getGrowthsByStuId(stuId);
        return AjaxResult.success(growths);
    }

    /**
     * 访问此接口，返回统计的数据
     * @param gradeId
     * @return
     */
    @GetMapping("/pace")
    public TableDataInfo getGrowthInfo(Long gradeId){
        Long semesterId = semesterService.getCurrentSemesterId();
        List<Map<String, Integer>> studentGrowPace = busStudentGrowthService.getStudentGrowPace(gradeId, semesterId);
        return getDataTable(studentGrowPace);
    }



    /**
     * 开启新一轮成长报告
     */
    @PostMapping("/newGrowth")
    public AjaxResult newGrowth(@RequestBody JSONObject endTime)
    {
        if (semesterService.exitReport()){
            return AjaxResult.error("本学期已开启过成长报告,请勿重复开启");
        }
        Date date;
        try {
            date = DateUtil.parse(endTime.get("endTime").toString());
        }catch (Exception e){
            return AjaxResult.error("时间上传异常");
        }
        if (date.before(DateUtils.getNowDate())){
            return AjaxResult.error("请勿选择比当前时间更早的时间");
        }
        busStudentGrowthService.newGrowth(date);
        return AjaxResult.success();
    }

    /**
     * 修改学生成长报告
     */
    @PreAuthorize("@ss.hasPermi('business:growth:edit')")
    @Log(title = "学生成长报告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusStudentGrowth busStudentGrowth)
    {
        return toAjax(busStudentGrowthService.updateBusStudentGrowth(busStudentGrowth));
    }

    /**
     * 删除学生成长报告
     */
    @PreAuthorize("@ss.hasPermi('business:growth:remove')")
    @Log(title = "学生成长报告", businessType = BusinessType.DELETE)
	@DeleteMapping("/{growthIds}")
    public AjaxResult remove(@PathVariable Long[] growthIds)
    {
        return toAjax(busStudentGrowthService.deleteBusStudentGrowthByGrowthIds(growthIds));
    }

    @PutMapping("resetEndTime")
    public AjaxResult resetEndTime(@RequestBody JSONObject endTime){
        Date date;
        try {
            date = DateUtil.parse(endTime.get("endTime").toString());
        }catch (Exception e){
            return AjaxResult.error("时间上传异常");
        }
        if (date.before(DateUtils.getNowDate())){
            return AjaxResult.error("请勿选择比当前时间更早的时间");
        }
        if(busStudentGrowthService.resetEndTime(date) != 0){
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.error("修改失败");
    }
    @GetMapping("getEndTime")
    public AjaxResult getEndTime(){
        return AjaxResult.success(growthItemService.getEndTime());
    }
}
