package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.MusicManage;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MusicManageMapper extends BaseMapper<MusicManage> {
}
