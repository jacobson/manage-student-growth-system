package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * 学科对象 bus_subject
 * 
 * @author ruoyi
 * @date 2022-10-18
 */
@Data
public class BusSubject
{
    private static final long serialVersionUID = 1L;

    /** 学科id */
    private Long subjectId;

    /** 学科名称 */
    @Excel(name = "学科名称")
    private String subjectName;
    /** 学科描述 */
    @Excel(name = "学科描述")
    private String subjectDescription;

}
