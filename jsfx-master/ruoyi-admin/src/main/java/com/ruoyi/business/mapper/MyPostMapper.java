package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.MyPost;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MyPostMapper extends BaseMapper<MyPost> {
}
