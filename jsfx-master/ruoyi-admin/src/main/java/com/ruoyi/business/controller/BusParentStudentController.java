package com.ruoyi.business.controller;

import com.ruoyi.business.service.IBusParentStudentService;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/business/busParentStudent")
public class BusParentStudentController{
    @Autowired
    private IBusParentStudentService parentStudentService;
    /** 获取用户的所有孩子*/
    @GetMapping("/getMyChild")
    public AjaxResult getMyChile(){
        Long userId = SecurityUtils.getUserId();
        return AjaxResult.success(parentStudentService.getChild(userId));
    }

}
