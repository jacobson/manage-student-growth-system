package com.ruoyi.business.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusStudents;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;


/**
 * 学生管理Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-21
 */
public interface BusStudentsMapper extends BaseMapper<BusStudents>
{
    /**
     * 查询学生管理
     * 
     * @param stuId 学生管理主键
     * @return 学生管理
     */
    public BusStudents selectBusStudentsByStuId(Long stuId);

    /**
     * 查询学生管理列表
     * 
     * @param busStudents 学生管理
     * @return 学生管理集合
     */
    public List<BusStudents> selectBusStudentsList(BusStudents busStudents);

    /**
     * 新增学生管理
     * 
     * @param busStudents 学生管理
     * @return 结果
     */
    public int insertBusStudents(BusStudents busStudents);

    /**
     * 修改学生管理
     * 
     * @param busStudents 学生管理
     * @return 结果
     */
    public int updateBusStudents(BusStudents busStudents);

    /**
     * 删除学生管理
     * 
     * @param stuId 学生管理主键
     * @return 结果
     */
    public int deleteBusStudentsByStuId(Long stuId);

    /**
     * 批量删除学生管理
     * 
     * @param stuIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusStudentsByStuIds(Long[] stuIds);

    List<BusStudents> selectBusStudentsByGradeId(Long gradeId);

    List<Map<Object,Object>> getParentInfoByStuId(Long stuId);

    Long getGradeIdByStuIdLong(Long stuId);

    List<BusStudents> getChild(Long userId);
    //根据学生id获取该学生的gradeId
    Long getGradeId(Long studentId);

    int editAvatar(@Param("studentId") Long studentId, @Param("avatar") String avatar);

    List<Long> getParentUserIds(Long studentId);
    @Select("select student_id from bus_grade_student where grade_id = #{gradeId}")
    List<Long> selectStudentIdsByGradeId(Long gradeId);

    List<BusStudents> getAll();
}
