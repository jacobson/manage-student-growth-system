package com.ruoyi.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * 学生荣誉对象 bus_student_honor
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@Data
public class BusStudentHonor
{
    private static final long serialVersionUID = 1L;

    /** growth_id */
    private Long studentId;

    /** 是否获取卓越少年荣誉 */
    @Excel(name = "是否获取卓越少年荣誉")
    private String honor1;

    /** 是否获取励志少年 */
    @Excel(name = "是否获取励志少年")
    private String honor2;

    /** 是否获取模范学生 */
    @Excel(name = "是否获取模范学生")
    private String honor3;

    /** 是否获取梦想领袖 */
    @Excel(name = "是否获取梦想领袖")
    private String honor4;

    /** 是否获取文明学生 */
    @Excel(name = "是否获取文明学生")
    private String honor5;
    /**
     * 封装荣誉数值
     */
    @TableField(exist = false)
    private List<String> honorList = new ArrayList<>();
    public List<String> getHonorList(){
        honorList.add(honor1);
        honorList.add(honor2);
        honorList.add(honor3);
        honorList.add(honor4);
        honorList.add(honor5);
        return honorList;
    }
    public void setHonor1(String honor1) 
    {
        this.honor1 = honor1;
    }

    public String getHonor1() 
    {
        return honor1;
    }
    public void setHonor2(String honor2) 
    {
        this.honor2 = honor2;
    }

    public String getHonor2() 
    {
        return honor2;
    }
    public void setHonor3(String honor3) 
    {
        this.honor3 = honor3;
    }

    public String getHonor3() 
    {
        return honor3;
    }
    public void setHonor4(String honor4) 
    {
        this.honor4 = honor4;
    }

    public String getHonor4() 
    {
        return honor4;
    }
    public void setHonor5(String honor5) 
    {
        this.honor5 = honor5;
    }

    public String getHonor5() 
    {
        return honor5;
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("honor1", getHonor1())
            .append("honor2", getHonor2())
            .append("honor3", getHonor3())
            .append("honor4", getHonor4())
            .append("honor5", getHonor5())
            .toString();
    }
}
