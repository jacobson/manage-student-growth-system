package com.ruoyi.business.controller;

import com.ruoyi.business.service.IYearGradeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/yearGrades")
public class YearGradeController extends BaseController {
    @Autowired
    private IYearGradeService IYearGradeService;

    /**
     * 2020界---202101----2020第几学期
     * @return
     */
    @GetMapping("/list")
    public AjaxResult yearGrades(){
        Map<String, HashMap<Long, List<String>>> stringListMap = IYearGradeService.getyearGrades();
        return AjaxResult.success(stringListMap);
    }

    /**
     * 2020界---202101
     * @return
     */
    @GetMapping("/list1")
    public Map<String, List<Long>> yearGrades1(){
        Map<String, List<Long>> stringListMap = IYearGradeService.getYearGrades();
        return stringListMap;
    }

}
