package com.ruoyi.business.domain;

import lombok.Data;

@Data
public class GradeTeacher {
    private Long gradeId;
    private String postName;
    private String name;
}
