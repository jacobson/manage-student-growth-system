package com.ruoyi.business.controller;

import com.ruoyi.business.Dto.HonorDto;
import com.ruoyi.business.domain.BusStudentHonor;
import com.ruoyi.business.service.IBusGradeStudentService;
import com.ruoyi.business.service.IBusStudentHonorService;
import com.ruoyi.business.service.IBusStudentsService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 学生荣誉Controller
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@RestController
@RequestMapping("/business/honor")
public class BusStudentHonorController extends BaseController
{
    @Autowired
    private IBusStudentHonorService busStudentHonorService;

    @Autowired
    private IBusStudentsService studentService;

    @Autowired
    private IBusGradeStudentService gradeStudentService;
    /**
     *调整方法
     * @return 布尔型
     */
    @PostMapping("/confirm/adjust")
    public AjaxResult test(@RequestBody HonorDto honorDto){
        //选中人
        List<Long> candidate = honorDto.getCandidate();
        if (candidate.isEmpty()){
            return AjaxResult.error("未进行选择");
        }
        Long gradeId = gradeStudentService.getGradeIdByStudentId(candidate.get(0));
        List<Long> all = studentService.
                selectStudentIdsByGradeId(gradeId);
        all.removeAll(candidate);
        //荣誉模块
        String module = honorDto.getModule();

        //修改选中人
        boolean flag1 = busStudentHonorService.updateModuleInfo(candidate,module,true);
        //修改未选中人
        boolean flag2=  busStudentHonorService.updateModuleInfo(all,module,false);

        return toAjax(flag1 && flag2);
    }

    /**
     * 1、当已经有人获得该荣誉
     * 2、当这个荣誉模块没有人获得时，返回非三项荣誉获得者的人选
     * @param gradeId 班级ID
     * @param honorModule 模块名
     * @return 返回所有名单
     */
    @GetMapping("/pointModuleHonor")
    public TableDataInfo pointModuleHonorInfo(Long gradeId,String honorModule){
        //拿到全部人员的信息
        List<Map<String, List<String>>> honorModuleInfo = busStudentHonorService.getHonorModuleInfo(gradeId);
        Map<String, List<String>> mapHonor = honorModuleInfo.get(0);
        //返回指定的模块
        List<String> moduleNameList = mapHonor.get(honorModule);
        if(moduleNameList.isEmpty()){
            //如果这个模块还没有学生获得荣誉那就应该将所有没有得到三项荣誉的学生展示出来以供选择
            return getDataTable(mapHonor.get("candidate"));
        }else {
            //否则就将这个以及获得该荣誉的名单返回
            return getDataTable(moduleNameList);
        }
    }
    @GetMapping("/adjust")
    public TableDataInfo adjustPointModuleHonor(Long gradeId,String honorModule){
         //返回可选的人，应该时这个模块已经获得荣誉的人加上，非有三项荣誉的学生
        //拿到全部人员的信息
        List<Map<String, List<String>>> honorModuleInfo = busStudentHonorService.getHonorModuleInfo(gradeId);
        Map<String, List<String>> mapHonor = honorModuleInfo.get(0);
        //返回指定的模块
        List<String> moduleNameList = mapHonor.get(honorModule);
        //如果当前的荣誉为空，就直接返回可选的人
        if(moduleNameList.isEmpty()){
            //如果这个模块还没有学生获得荣誉那就应该将所有没有得到三项荣誉的学生展示出来以供选择
            return getDataTable(mapHonor.get("candidate"));
        }else {
            //否则就将这个模块获得该荣誉的名单加上备选名单返回
            List<String> candidateList = mapHonor.get("candidate");
            for (String name:candidateList) {
                if (!moduleNameList.contains(name)){
                    moduleNameList.add(name);
                }
            }
            return getDataTable(moduleNameList);
        }
    }

    @GetMapping("/honorNameList")
    public TableDataInfo honor(Long gradeId){
        List<Map<String, List<String>>> honorModuleInfo = busStudentHonorService.getHonorModuleInfo(gradeId);
        return getDataTable(honorModuleInfo);
    }


    /**
     * 查询学生荣誉列表
     */
    @PreAuthorize("@ss.hasPermi('business:honor:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusStudentHonor busStudentHonor)
    {
        startPage();
        List<BusStudentHonor> list = busStudentHonorService.selectBusStudentHonorList(busStudentHonor);
        return getDataTable(list);
    }

    /**
     * 导出学生荣誉列表
     */
    @PreAuthorize("@ss.hasPermi('business:honor:export')")
    @Log(title = "学生荣誉", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusStudentHonor busStudentHonor)
    {
        List<BusStudentHonor> list = busStudentHonorService.selectBusStudentHonorList(busStudentHonor);
        ExcelUtil<BusStudentHonor> util = new ExcelUtil<>(BusStudentHonor.class);
        util.exportExcel(response, list, "学生荣誉数据");
    }

    /**
     * 获取学生荣誉详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:honor:query')")
    @GetMapping(value = "/{growthId}")
    public AjaxResult getInfo(@PathVariable("growthId") Long growthId)
    {
        return AjaxResult.success(busStudentHonorService.selectBusStudentHonorByGrowthId(growthId));
    }

    /**
     * 新增学生荣誉
     */
    @PreAuthorize("@ss.hasPermi('business:honor:add')")
    @Log(title = "学生荣誉", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusStudentHonor busStudentHonor)
    {
        return toAjax(busStudentHonorService.insertBusStudentHonor(busStudentHonor));
    }

    /**
     * 修改学生荣誉
     */
    @PreAuthorize("@ss.hasPermi('business:honor:edit')")
    @Log(title = "学生荣誉", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusStudentHonor busStudentHonor)
    {
        return toAjax(busStudentHonorService.updateBusStudentHonor(busStudentHonor));
    }

    /**
     * 删除学生荣誉
     */
    @PreAuthorize("@ss.hasPermi('business:honor:remove')")
    @Log(title = "学生荣誉", businessType = BusinessType.DELETE)
	@DeleteMapping("/{growthIds}")
    public AjaxResult remove(@PathVariable Long[] growthIds)
    {
        return toAjax(busStudentHonorService.deleteBusStudentHonorByGrowthIds(growthIds));
    }

    @PostMapping("/confirmList/{gradeId}")
    public AjaxResult confirmList(@PathVariable Long gradeId){
        if (busStudentHonorService.confirmList(gradeId) == 1){
            return AjaxResult.success("提交成功");
        }
        return AjaxResult.error("提交失败");
    }
}
