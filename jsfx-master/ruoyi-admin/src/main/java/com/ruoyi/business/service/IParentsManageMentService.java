package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.ParentsManageMent;

import java.util.List;

public interface IParentsManageMentService extends IService<ParentsManageMent> {
   //全部家长
    List<ParentsManageMent> allParentsManages();

    //条件查询
    List<ParentsManageMent> conditionParentsManages(String name,String phonenumber);

    //删除家长
    boolean deleteParentsManageInfo(Long UserId);

    //新增家长
    boolean addParentsManageInfo(ParentsManageMent parentsManageMent);

    //编辑条件回显
    List<ParentsManageMent> echoParentsManageInfo(Long UserId);



}
