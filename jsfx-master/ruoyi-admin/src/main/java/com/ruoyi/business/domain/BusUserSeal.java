package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户印章对象 bus_user_seal
 * 
 * @author ruoyi
 * @date 2022-10-31
 */

public class BusUserSeal
{
    private static final long serialVersionUID = 1L;

    /** 印章id */
    private Long sealId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 印章地址 */
    @Excel(name = "印章地址")
    private String sealUrl;

    public void setSealId(Long sealId) 
    {
        this.sealId = sealId;
    }

    public Long getSealId() 
    {
        return sealId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setSealUrl(String sealUrl) 
    {
        this.sealUrl = sealUrl;
    }

    public String getSealUrl() 
    {
        return sealUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("sealId", getSealId())
            .append("userId", getUserId())
            .append("sealUrl", getSealUrl())
            .toString();
    }
}
