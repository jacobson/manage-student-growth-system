package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class BusStudentScore {
    @Excel(name = "班级")
    private Long gradeId;
    @Excel(name = "学生Id")
    private Long studentId;

    @Excel(name = "学生姓名")
    private String name;

    @Excel(name = "身高")
    private String height;

    @Excel(name = "体重")
    private String weight;

    @Excel(name = "左眼视力")
    private String eyeLeft;

    @Excel(name = "右眼视力")
    private String eyeRight;

    @Excel(name = "数学")
    private String math;

    @Excel(name = "语文")
    private String chinese;

    @Excel(name = "英语")
    private String english;

    @Excel(name = "科学")
    private String science;

    @Excel(name = "道德与法")
    private String mal;

    @Excel(name = "体育")
    private String sport;

    @Excel(name = "音乐")
    private String music;

    @Excel(name = "信息")
    private String it;

    @Excel(name = "美术")
    private String art;

    @Excel(name = "综合实践")
    private String combine;

    @Excel(name = "听课专注")
    private String earnest;

    @Excel(name = "学习主动")
    private String active;

    @Excel(name = "做事负责")
    private String responsible;

    @Excel(name = "同学合作")
    private String cooperation;

    @Excel(name = "综合评价")
    private String ca;
}
