package com.ruoyi.business.service;

import com.ruoyi.business.domain.BusStudentScore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface IBusStudentScoreService {
    int importScore(List<BusStudentScore> scoreList);

    void getTemplate(Long gradeId, HttpServletResponse response) throws IOException;

}
