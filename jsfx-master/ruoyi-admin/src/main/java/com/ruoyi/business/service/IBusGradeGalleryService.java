package com.ruoyi.business.service;

import com.ruoyi.business.domain.BusGradeGallery;

import java.util.List;

/**
 * 班级图库Service接口
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
public interface IBusGradeGalleryService 
{
    List<BusGradeGallery> getGradeGalleryByUserId(Long userId);

    /**
     * 新增班级图库
     * 
     * @param busGradeGallery 班级图库
     * @return 结果
     */
    public void insertBusGradeGallery(BusGradeGallery busGradeGallery);

    /**
     * 修改班级图库
     * 
     * @param busGradeGallery 班级图库
     * @return 结果
     */
    public int updateBusGradeGallery(BusGradeGallery busGradeGallery);

    /**
     * 批量删除班级图库
     * 
     * @param galleryIds 需要删除的班级图库主键集合
     * @return 结果
     */
    public int deleteBusGradeGalleryByGalleryIds(Long[] galleryIds);

    /**
     * 删除班级图库信息
     * 
     * @param gradeId 班级图库主键
     * @return 结果
     */
    public int deleteBusGradeGalleryByGradeId(Long gradeId);

    List<Long> getGradeGalleryByStuId(Long studentId);
}
