package com.ruoyi.business.domain;

import lombok.Data;

/**
 * 方便以map形式存放存放成长环节值
 * Map<String,BusGrowthItemDto>
 */
@Data
public class BusGrowthItemDto {
    private String key;
    private String value;
    private String status;
}
