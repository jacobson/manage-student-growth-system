package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusSubject;
import com.ruoyi.business.service.IBusSubjectService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/SubjectManage")
@Transactional
public class SubjectManageController extends BaseController {
    @Autowired
    private IBusSubjectService iBusSubjectService;
    //返回所有的课程记录
    @GetMapping("/all")
    public TableDataInfo allSubjectInfo(){
        return getDataTable(iBusSubjectService.getAllSubject());
    }

    //新增学科
    @PostMapping("/add")
    public AjaxResult allSubjectInfo(@RequestBody BusSubject busSubject){
        return toAjax(iBusSubjectService.addSubject(busSubject));
    }

    //编辑学科数据回显
    @GetMapping
    public BusSubject echoSubjectInfo(Long subjectId){
        return iBusSubjectService.selectBusSubjectBySubjectId(subjectId);
    }

    //编辑数据以后确定修改
    @PostMapping
    public AjaxResult updateStudentInfo(@RequestBody BusSubject busSubject){
      return  toAjax(iBusSubjectService.updateBusSubject(busSubject));
    }


    //删除学科
    @DeleteMapping("/remove")
    public AjaxResult removeStudentInfo(Long subjectId){
        return toAjax(iBusSubjectService.deleteBusSubjectBySubjectId(subjectId));
    }

}
