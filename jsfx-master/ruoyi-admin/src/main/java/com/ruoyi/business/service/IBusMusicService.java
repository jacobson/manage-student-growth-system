package com.ruoyi.business.service;

import com.ruoyi.business.domain.BusMusic;

import java.util.List;

/**
 * 推荐音乐Service接口
 *
 */
public interface IBusMusicService 
{


    /**
     * 查询推荐音乐列表
     * 
     * @return 推荐音乐集合
     */
    List<BusMusic> selectBusMusicList();

    /**
     * 新增推荐音乐
     * 
     * @return 结果
     */
    public int insertBusMusic(String musicName,String musicUrl);

    /**
     * 修改推荐音乐
     * 
     * @param busMusic 推荐音乐
     * @return 结果
     */
    public int updateBusMusic(BusMusic busMusic);

    /**
     * 批量删除推荐音乐
     * 
     * @param musicIds 需要删除的推荐音乐主键集合
     * @return 结果
     */
    public int deleteBusMusicByMusicIds(Long[] musicIds);

    /**
     * 删除推荐音乐信息
     * 
     * @param musicId 推荐音乐主键
     * @return 结果
     */
    public int deleteBusMusicByMusicId(Long musicId);
}
