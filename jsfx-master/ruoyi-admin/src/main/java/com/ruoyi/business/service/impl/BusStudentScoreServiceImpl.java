package com.ruoyi.business.service.impl;

import cn.hutool.core.io.IoUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.ruoyi.business.common.CommonValues;
import com.ruoyi.business.domain.BusStudentGrowth;
import com.ruoyi.business.domain.BusStudentGrowthItem;
import com.ruoyi.business.domain.BusStudentScore;
import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.business.mapper.BusStudentGrowthItemMapper;
import com.ruoyi.business.mapper.BusStudentGrowthMapper;
import com.ruoyi.business.service.IBusGradeTeacherService;
import com.ruoyi.business.service.IBusStudentGrowthItemService;
import com.ruoyi.business.service.IBusStudentScoreService;
import com.ruoyi.business.service.IBusStudentsService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Service
public class BusStudentScoreServiceImpl implements IBusStudentScoreService {
    @Autowired
    private BusStudentGrowthItemMapper itemMapper;
    @Autowired
    private BusStudentGrowthMapper growthMapper;
    @Autowired
    private BusSemesterServiceImpl semesterService;
    @Autowired
    private IBusStudentGrowthItemService growthItemService;
    @Autowired
    private IBusStudentsService studentService;
    @Autowired
    private IBusGradeTeacherService gradeTeacherService;
    @Autowired
    private ISysUserService userService;
    @Override
    @Transactional
    public int importScore(List<BusStudentScore> scoreList) {
        scoreList.forEach((item)->{
            Long gradeId = item.getGradeId();
            JSONObject entries = scoreToJson(item,gradeId);
            BusStudentGrowthItem busStudentGrowthItem = new BusStudentGrowthItem();
            Long growthId = growthMapper.getStudentCurrentGrowthId(item.getStudentId(),semesterService.getCurrentSemesterId());
            busStudentGrowthItem.setGrowthKey(CommonValues.ITEMS[13]);
            busStudentGrowthItem.setGrowthValue(entries.toString());
            busStudentGrowthItem.setGrowthId(growthId);
            busStudentGrowthItem.setStatus("1");
            itemMapper.updateBusStudentGrowthItem(busStudentGrowthItem);
            //将成长报告读出存入redis
            BusStudentGrowth busStudentGrowth = new BusStudentGrowth();
            busStudentGrowth.setGrowthId(growthId);
            Map<String, Object> stringObjectMap = growthItemService.selectBusStudentGrowthItemByGrowthId(busStudentGrowth);
            growthItemService.storeGrowthItems(growthId,stringObjectMap);
        });
        return 1;
    }

    @Override
    public void getTemplate(Long userId,HttpServletResponse response) throws IOException {
        List<BusStudents> busStudents = new ArrayList<>();
        //判断是否为管理员,是管理员返回所有学生名单
        if (userService.isAdmin(userId)){
            busStudents.addAll(studentService.getAll());
        }else{
            //不是则返回所担任班主任的班级
            List<Long> leadGrade = gradeTeacherService.getLeadGrade(userId);
            leadGrade.forEach((gradeId)->{
                busStudents.addAll(studentService.selectBusStudentsByGradeId(gradeId));
            });
        }
        //根据班级id获取学生列表
        exportCommonMethod(busStudents,response);
    }

    private void exportCommonMethod(List<BusStudents> busStudents, HttpServletResponse httpResponse) throws IOException {
        ArrayList<Map<String, Object>> rows = new ArrayList<>();
        busStudents.forEach((student)->{
            HashMap<String, Object> map = new LinkedHashMap<>();
            map.put("班级",student.getGradeId());
            map.put("学生Id",student.getStuId());
            map.put("学生姓名",student.getStuName());
            map.put("身高","");
            map.put("体重","");
            map.put("左眼","5.0");
            map.put("右眼","5.0");
            map.put("语文","");
            map.put("数学","");
            map.put("科学","");
            map.put("英语","");
            map.put("道德与法","良好");
            map.put("体育","良好");
            map.put("音乐","良好");
            map.put("信息","良好");
            map.put("美术","良好");
            map.put("综合实践","良好");
            map.put("听课专注","AAA");
            map.put("做事负责","AAA");
            map.put("学习主动","AAA");
            map.put("同学合作","AAA");
            map.put("综合评价","AAA");
            map.put("综合评价等第","AAAAAA");
            rows.add(map);
        });
        ExcelWriter writer = ExcelUtil.getWriter(true);
        writer.write(rows, true);
        httpResponse.setContentType("application/vnd.ms-excel;charset=utf-8");
        httpResponse.setHeader("Content-Disposition","attachment;filename=score.xlsx");
        ServletOutputStream out=httpResponse.getOutputStream();
        writer.flush(out, true);
        writer.close();
        IoUtil.close(out);
    }

    /** 根据得分评级*/
    private String scoreConvert(String score){
        float v = Float.parseFloat(score);
        String comment;
        if (v>=80){
            comment = "优秀";
        }else if(v>=70){
            comment = "良好";
        }else if(v>=60){
            comment = "及格";
        }else {
            comment = "不及格";
        }
        return comment;
    }
    /** 将得分对象变为json*/
    private JSONObject scoreToJson(BusStudentScore item,Long gradeId){
        JSONObject entries = new JSONObject();
        entries.set("班级",gradeId);
        entries.set("学生姓名",item.getName());
        entries.set("身高",item.getHeight());
        entries.set("体重",item.getWeight());
        entries.set("左眼",item.getEyeLeft());
        entries.set("右眼",item.getEyeRight());
        JSONObject score = new JSONObject();
        score.set("数学",item.getMath());
        score.set("语文",item.getChinese());
        score.set("英语",item.getEnglish());
        score.set("科学",item.getScience());
        entries.set("成绩",score);
        JSONObject comment = new JSONObject();
        comment.set("语文",scoreConvert(item.getChinese()));
        comment.set("数学",scoreConvert(item.getMath()));
        comment.set("科学",scoreConvert(item.getScience()));
        comment.set("英语",scoreConvert(item.getEnglish()));
        comment.set("道德与法",item.getMal());
        comment.set("体育",item.getSport());
        comment.set("音乐",item.getMusic());
        comment.set("信息",item.getIt());
        comment.set("美术",item.getArt());
        comment.set("综合实践",item.getCombine());
        comment.set("听课专注",item.getEarnest());
        comment.set("做事负责",item.getResponsible());
        comment.set("学习主动",item.getActive());
        comment.set("同学合作",item.getCooperation());
        comment.set("综合评价",item.getCa());
        entries.set("综合评价等第",comment);
        return entries;
    }
}
