package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;
//连同课程表一起导入
@Data
public class TeacherManageExport {
    @Excel(name = "姓名")
    private String nickName;

    @Excel(name = "手机号码")
    private String phonenumber;

    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    @Excel(name = "班主任所带班级")
    private String bringClass;

    @Excel(name = "科目")
    private String subjectName;

    @Excel(name = "班级")
    private String classes;


}
