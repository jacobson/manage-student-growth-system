package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * 班级学生对象 bus_grade_student
 * 
 * @author ruoyi
 * @date 2022-10-24
 */
@Data
public class BusGradeStudent
{
    /** 班级id */
    @Excel(name = "班级id")
    private Long gradeId;

    /** 学生id */
    @Excel(name = "学生id")
    private Long studentId;

}
