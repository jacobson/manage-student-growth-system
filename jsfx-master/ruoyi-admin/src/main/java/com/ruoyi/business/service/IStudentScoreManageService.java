package com.ruoyi.business.service;


import cn.hutool.json.JSONObject;

import java.util.List;

public interface IStudentScoreManageService {
    //返回全部当前学期所有学生素养报告单
    List<JSONObject> allStudentScoreInfo(String gradeId, String semesterYearName);

    //回显一个学生的成绩报告
     JSONObject echoStudentScoreInfo(Long studentId);

    /**
     *
     * @param studentId 需要修改的学生
     * @param object 素质报告数据
     */
    boolean updateStudentScoreInfo(JSONObject object);


    //选择一个学生，条件查询
    List<JSONObject> choose(String gradeId, String name);

    //条件查询
    List<JSONObject> chooseStudentScoreInfo(String gradeId, String name);
}
