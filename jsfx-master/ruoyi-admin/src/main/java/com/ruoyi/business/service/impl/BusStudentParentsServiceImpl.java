package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.Dto.BusStudentParentsDto;
import com.ruoyi.business.Dto.BusStudentParentsDtoT;
import com.ruoyi.business.domain.BusStudentParents;
import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.business.domain.BusUser;
import com.ruoyi.business.mapper.BusStudentParentsMapper;
import com.ruoyi.business.mapper.BusUserMapper;
import com.ruoyi.business.service.IBusStudentParentsService;
import com.ruoyi.business.service.IBusStudentsService;
import com.ruoyi.business.service.IBusUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


/**
 * 学生家长Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-31
 */
@Service
public  class BusStudentParentsServiceImpl extends ServiceImpl<BusStudentParentsMapper,BusStudentParents> implements IBusStudentParentsService
{
    @Autowired
    private BusStudentParentsMapper busStudentParentsMapper;

    @Autowired
    private IBusUserService iBusUserService;

    @Autowired
    private IBusStudentParentsService iBusStudentParentsService;

    @Autowired
    private IBusStudentsService iBusStudentsService;

    @Autowired
    private BusUserMapper userMapper;
    /**
     * 查询学生家长
     * 
     * @param studentId 学生家长主键
     * @return 学生家长
     */
    @Override
    public BusStudentParents selectBusStudentParentsByStudentId(Long studentId)
    {
        return busStudentParentsMapper.selectBusStudentParentsByStudentId(studentId);
    }

    /**
     * 这里是把家长的联系方式写入学生信息中
     * @param collect 学生信息
     * @return
     */
    @Override
    public List<BusStudentParentsDto> selectBusStudentParentsByList(List<BusStudents> collect) {

        List<BusStudentParentsDto> busStudentParentsDtos = collect.stream().map(item -> {
            //创建一个Dto对象 并且把这个学生的信息全部拷贝到这个对象中
            BusStudentParentsDto busStudentParentsDto = new BusStudentParentsDto();
            BeanUtils.copyProperties(item,busStudentParentsDto);
            //存放家长信息的map
            Map<String,String> stringListMap = new HashMap<>();
            List<String> nickNameList = new ArrayList<>();
            //获取学生家长的信息
            List<BusStudentParents> list1 = iBusStudentParentsService.selectBusStudentParentsByStudentIds(item.getStuId());

            for (BusStudentParents busStudentParents : list1) {
                String relation = busStudentParents.getRelation();
                BusUser busUser = iBusUserService.selectUserById(busStudentParents.getUserId());
                String nickName = busUser.getNickName();
                nickNameList.add(nickName);
                String phoneNumber = busUser.getPhonenumber();
                stringListMap.put(relation,phoneNumber);
            }
            busStudentParentsDto.setStudentParentsMap(stringListMap);
            busStudentParentsDto.setNickName(nickNameList);
            return busStudentParentsDto;
        }).collect(Collectors.toList());


        return busStudentParentsDtos;
    }

    @Override
    public BusStudentParentsDto selectBusStudentParentsByMySelf(BusStudents busStudents) {
        BusStudentParentsDto busStudentParentsDto =new BusStudentParentsDto();
        BeanUtils.copyProperties(busStudents,busStudentParentsDto);
        List<BusStudentParents> busStudentParents = iBusStudentParentsService.selectBusStudentParentsByStudentIds(busStudents.getStuId());
        List<String> list = new ArrayList<>();
        Map<String,String> map = new HashMap<>();
        for (BusStudentParents busStudentParent : busStudentParents) {
            Long userId = busStudentParent.getUserId();
            String relation = busStudentParent.getRelation();
            BusUser busUser = iBusUserService.selectUserById(userId);
            String nickName = busUser.getNickName();
            String phonenumber = busUser.getPhonenumber();
            map.put(nickName,phonenumber);
            list.add(relation);
        }
        busStudentParentsDto.setStudentParentsMap(map);
        busStudentParentsDto.setNickName(list);
        return busStudentParentsDto;
    }


    /**
     * 这里是把家长的联系方式写入学生信息中
     * @param collect 学生信息
     * @return
     */
    @Override
    public List<BusStudentParentsDtoT> selectBusStudentParentsByListT(List<BusStudents> collect) {

        return collect.stream().map(item -> {
            //创建一个Dto对象 并且把这个学生的信息全部拷贝到这个对象中
            BusStudentParentsDtoT busStudentParentsDtoT = new BusStudentParentsDtoT();
            BeanUtils.copyProperties(item,busStudentParentsDtoT);
            //获取学生家长的信息

            List<BusStudentParents> list1 = iBusStudentParentsService.selectBusStudentParentsByStudentIds(item.getStuId());

            for (BusStudentParents busStudentParents : list1) {
                String relation = busStudentParents.getRelation();
                Long userId = busStudentParents.getUserId();
                BusUser busUser = iBusUserService.selectUserById(userId);

                String nickName = busUser.getNickName();
                String phonenumber = busUser.getPhonenumber();
                if (relation.equals("爸爸")){
                    busStudentParentsDtoT.setFatherUserId(userId);
                    busStudentParentsDtoT.setFatherName(nickName);
                    busStudentParentsDtoT.setFatherNumber(phonenumber);
                }else if (relation.equals("妈妈")){
                    busStudentParentsDtoT.setMotherUserId(userId);
                    busStudentParentsDtoT.setMotherName(nickName);
                    busStudentParentsDtoT.setMotherNumber(phonenumber);
                }
            }
            return busStudentParentsDtoT;
        }).collect(Collectors.toList());

    }





    @Override
    public List<BusStudentParents> selectBusStudentParentsByStudentIds(Long studentId) {
        return busStudentParentsMapper.selectBusStudentParentsByStudentIds(studentId);
    }

    /**
     * 查询学生家长列表
     * 
     * @param busStudentParents 学生家长
     * @return 学生家长
     */
    @Override
    public List<BusStudentParents> selectBusStudentParentsList(BusStudentParents busStudentParents)
    {
        return busStudentParentsMapper.selectBusStudentParentsList(busStudentParents);
    }

    /**
     * 新增学生家长
     * 
     * @param busStudentParents 学生家长
     * @return 结果
     */
    @Override
    public int insertBusStudentParents(BusStudentParents busStudentParents)
    {
        return busStudentParentsMapper.insertBusStudentParents(busStudentParents);
    }

    /**
     * 写入学生家长信息，以及学生家长联系方式
     * @param busStudentParentsDto 学生和家长信息
     * @return
     */
    @Override
    public boolean addBusStudentParents(BusStudentParentsDto busStudentParentsDto) {
        //先把父母联系方式取出
        Map<String, String> studentParentsMap = busStudentParentsDto.getStudentParentsMap();
        //遍历这个map 取出每个元素并添加到数据库
        boolean b3 = false;
        for (String key : studentParentsMap.keySet()) {
            String relation = key;
            String phonenumber = studentParentsMap.get(key);
            List<String> nickName = busStudentParentsDto.getNickName();
            for (String s : nickName) {
                //添加家长信息
                BusUser busUser = new BusUser();
                busUser.setNickName(s);
                busUser.setPhonenumber(phonenumber);
                String str = UUID.randomUUID().toString();
                busUser.setCreateBy(str);
                busUser.setDeptId(201L);
                boolean b1 = iBusUserService.insertUser(busUser);
                //获取家长的ID
                Long userId = userMapper.getUserId(str);
                //查询家长id和学生id
                String idCard = busStudentParentsDto.getIdCard();
                LambdaQueryWrapper<BusStudents> queryWrapper1 = new LambdaQueryWrapper<>();
                queryWrapper1.eq(BusStudents::getIdCard,idCard);
                BusStudents busStudentsServiceOne = iBusStudentsService.getOne(queryWrapper1);
                Long stuId = busStudentsServiceOne.getStuId();
                //根据家长id和学生id把学生家长表添加进去
                BusStudentParents busStudentParents = new BusStudentParents();
                busStudentParents.setStudentId(stuId);
                busStudentParents.setUserId(userId);
                busStudentParents.setRelation(relation);
                boolean b2 = iBusStudentParentsService.save(busStudentParents);
                nickName.remove(0);
                if (b1 == b2 ==true){
                     b3 = true;
                }
                break;
            }

        }
        return b3;
    }

    /**
     * 修改学生家长
     * 
     * @param busStudentParents 学生家长
     * @return 结果
     */
    @Override
    public int updateBusStudentParents(BusStudentParents busStudentParents)
    {
        return busStudentParentsMapper.updateBusStudentParents(busStudentParents);
    }

    /**
     * 批量删除学生家长
     * 
     * @param studentIds 需要删除的学生家长主键
     * @return 结果
     */
    @Override
    public int deleteBusStudentParentsByStudentIds(Long[] studentIds)
    {
        return busStudentParentsMapper.deleteBusStudentParentsByStudentIds(studentIds);
    }

    /**
     * 删除学生家长信息
     * 
     * @param studentId 学生家长主键
     * @return 结果
     */
    @Override
    public int deleteBusStudentParentsByStudentId(Long studentId)
    {
        return busStudentParentsMapper.deleteBusStudentParentsByStudentId(studentId);
    }


}
