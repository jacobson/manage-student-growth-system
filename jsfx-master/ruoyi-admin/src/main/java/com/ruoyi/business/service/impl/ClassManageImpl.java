package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.business.Dto.BusStudentParentsDtoT;
import com.ruoyi.business.Dto.ClassGradeInfoDto;
import com.ruoyi.business.domain.BusGrade;
import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.business.domain.ClassManageInfo;
import com.ruoyi.business.service.IBusGradeService;
import com.ruoyi.business.service.IBusGradeStudentService;
import com.ruoyi.business.service.IBusStudentParentsService;
import com.ruoyi.business.service.IClassManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClassManageImpl implements IClassManageService {
    //班级表
    @Autowired
    private IBusGradeService iBusGradeService;
    //班级学生表
    @Autowired
    private IBusGradeStudentService iBusGradeStudentService;
    //家长表
    @Autowired
    private IBusStudentParentsService iBusStudentParentsService;
    @Override
    public List<ClassManageInfo> getListClassManageInfo(String YearLevel) {
        Long YearLevelId = Long.valueOf(YearLevel.substring(0,4));
        LambdaQueryWrapper<BusGrade> busGradeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        busGradeLambdaQueryWrapper.likeRight(BusGrade::getGradeId,YearLevelId);
        //拿到这个学年的所有班级
        List<BusGrade> busGrades = iBusGradeService.list(busGradeLambdaQueryWrapper);
        //历遍所有的该年级的班级，以流的方式封装并收集为集合
        List<ClassManageInfo> classManageInfos = busGrades.stream().map(item -> {
            Long gradeId = item.getGradeId();
            //对应该班级的班主任名称
            String teacherName = iBusGradeService.getHeadTeacherName(gradeId);
            //对应的班级人数
            int countStudents = iBusGradeStudentService.getCountStudents(gradeId);
            //将其相关数据封装
            return new ClassManageInfo(gradeId,teacherName,countStudents);

        }).collect(Collectors.toList());
        iBusGradeService.list();
        return classManageInfos;
    }

    //单返其班级信息+班级全部学生学生信息
    @Override
    public ClassGradeInfoDto getOneClassManageInfo(Long gradeId) {
        //班主任名称
        String teacherName = iBusGradeService.getHeadTeacherName(gradeId);
        //指定班级的学生人数
        int countStudents = iBusGradeStudentService.getCountStudents(gradeId);
        //拿到这个班级的学生信息
        List<BusStudents> studentInfo = iBusGradeStudentService.getStudentInfoByGradeId(gradeId);
        List<BusStudentParentsDtoT> busStudentParentsDtoTS = new ArrayList<>();
        if (studentInfo != null){
            //对学生再封装其家长信息
             busStudentParentsDtoTS = iBusStudentParentsService.selectBusStudentParentsByListT(studentInfo);
        }
        ClassGradeInfoDto ClassGradeInfoDto = new ClassGradeInfoDto(gradeId,teacherName,countStudents);
        //如果家长信息为空，就不再赋值
        if (busStudentParentsDtoTS.isEmpty()){
            ClassGradeInfoDto.setStudentWithParentsInfo(null);
        }else {
            //如果不为空，那么就将家长信息添加进去
            ClassGradeInfoDto.setStudentWithParentsInfo(busStudentParentsDtoTS);
        }
        return ClassGradeInfoDto;
    }


    @Override
    public List<String> getAllYearId() {
        List<String> yearId = new ArrayList<>();
        List<BusGrade> busGrades = iBusGradeService.list();
        for (BusGrade busGrade : busGrades) {
            String gradeId = busGrade.getGradeId()+"";
            String YearLevel = gradeId.substring(0,4)+"年级";

            if (!yearId.contains(YearLevel)){
                yearId.add(YearLevel);
            }

        }
        return yearId;
    }
}
