package com.ruoyi.business.domain;

import lombok.Data;

@Data
public class HonorBody {
    private Long studentId;
    private String honor1;
    private String honor2;
    private String honor3;
    private String honor4;
    private String honor5;
}
