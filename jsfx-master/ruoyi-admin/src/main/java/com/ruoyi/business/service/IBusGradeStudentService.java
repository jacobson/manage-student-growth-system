package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.Dto.BusStudentParentsDto;
import com.ruoyi.business.domain.BusGradeStudent;
import com.ruoyi.business.domain.BusStudents;

import java.util.List;

/**
 * 班级学生Service接口
 * 
 * @author ruoyi
 * @date 2022-10-24
 */
public interface IBusGradeStudentService extends IService<BusGradeStudent>
{
    //返回这个班级学生的总人数
    int getCountStudents(Long gradeId);

    Long getGradeIdByStudentId(Long StudentId);

    //返回指定班级学生的信息
    List<BusStudents> getStudentInfoByGradeId(Long gradeId);
    /**
     * 查询班级学生
     * 
     * @param gradeId 班级学生主键
     * @return 班级学生
     */
    public BusGradeStudent selectBusGradeStudentByGradeId(Long gradeId);

    /**
     * 查询班级学生列表
     * 
     * @param busGradeStudent 班级学生
     * @return 班级学生集合
     */
    public List<BusGradeStudent> selectBusGradeStudentList(BusGradeStudent busGradeStudent);

    /**
     * 新增班级学生
     * 
     * @param busGradeStudent 班级学生
     * @return 结果
     */
    public int insertBusGradeStudent(BusGradeStudent busGradeStudent);

    /**
     * 修改班级学生
     * 
     * @param busGradeStudent 班级学生
     * @return 结果
     */
    public int updateBusGradeStudent(BusGradeStudent busGradeStudent);



    /**
     *  获取班级学生的两个字段
     * @param busStudentParentsDto 传入的数据
     * @return
     */
    public boolean addBusGradeStudent(BusStudentParentsDto busStudentParentsDto);

    BusGradeStudent selectBusGradeStudentByStudentId(Long studentId);

    /**
     * 修改班级学生
     *
     * @param busStudents 班级学生
     * @return 结果
     */
    public int updateBusGradeStudentZ(BusStudents busStudents);

    /**
     * 批量删除班级学生
     * 
     * @param gradeIds 需要删除的班级学生主键集合
     * @return 结果
     */
    public int deleteBusGradeStudentByGradeIds(Long[] gradeIds);

    /**
     * 删除班级学生信息
     * 
     * @param gradeId 班级学生主键
     * @return 结果
     */
    public int deleteBusGradeStudentByGradeId(Long gradeId);


    /**
     * 删除班级学生信息
     *
     * @param studentId 班级学生主键
     * @return 结果
     */
    public int deleteBusGradeStudentByStudentId(Long studentId);

    boolean insertBusGradeStudentByMyself(BusStudentParentsDto busStudentParentsDto);

}
