package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.Dto.BusStudentParentsDto;
import com.ruoyi.business.Dto.BusStudentParentsDtoT;
import com.ruoyi.business.domain.BusStudentParents;
import com.ruoyi.business.domain.BusStudents;

import java.util.List;


/**
 * 学生家长Service接口
 * 
 * @author ruoyi
 * @date 2022-10-31
 */
public interface IBusStudentParentsService extends IService<BusStudentParents>
{
    /**
     * 查询学生家长
     * 
     * @param studentId 学生家长主键
     * @return 学生家长
     */
    public BusStudentParents selectBusStudentParentsByStudentId(Long studentId);

    /**
     * 查询学生家长联系方式
     *
     * @param collect 学生信息
     * @return 学生家长的联系方式以及学生家长姓名
     */
    public List<BusStudentParentsDto> selectBusStudentParentsByList(List<BusStudents> collect);



    /**
     * 查询学生家长联系方式
     *
     * @param busStudents 学生信息
     * @return 学生家长的联系方式以及学生家长姓名
     */
    public BusStudentParentsDto selectBusStudentParentsByMySelf(BusStudents busStudents);



    /**
     *
     * @param collect
     * @return
     */
    public List<BusStudentParentsDtoT> selectBusStudentParentsByListT(List<BusStudents> collect);






    /**
     * 查询学生家长联系方式
     *
     * @param studentId 学生编号
     * @return 学生与家长的关系
     */
    public List<BusStudentParents> selectBusStudentParentsByStudentIds(Long studentId);




    /**
     * 查询学生家长列表
     * 
     * @param busStudentParents 学生家长
     * @return 学生家长集合
     */
    public List<BusStudentParents> selectBusStudentParentsList(BusStudentParents busStudentParents);

    /**
     * 新增学生家长
     * 
     * @param busStudentParents 学生家长
     * @return 结果
     */
    public int insertBusStudentParents(BusStudentParents busStudentParents);

    /**
     * 新增学生家长
     *
     * @param busStudentParentsDto 学生家长
     * @return 结果
     */
    public boolean addBusStudentParents(BusStudentParentsDto busStudentParentsDto);

    /**
     * 修改学生家长
     * 
     * @param busStudentParents 学生家长
     * @return 结果
     */
    public int updateBusStudentParents(BusStudentParents busStudentParents);

    /**
     * 批量删除学生家长
     * 
     * @param studentIds 需要删除的学生家长主键集合
     * @return 结果
     */
    public int deleteBusStudentParentsByStudentIds(Long[] studentIds);

    /**
     * 删除学生家长信息
     * 
     * @param studentId 学生家长主键
     * @return 结果
     */
    public int deleteBusStudentParentsByStudentId(Long studentId);


}
