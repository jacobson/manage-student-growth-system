package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.Dto.BusStudentParentsDto;
import com.ruoyi.business.domain.BusUser;

/**
 * 用户 业务层
 * 
 * @author ruoyi
 */
public interface IBusUserService extends IService<BusUser>
{

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    BusUser selectUserById(Long userId);

    boolean insertUser(BusUser user);

    //改学生家长信息
    boolean updateBusUserByCreatBy(BusStudentParentsDto busStudentParentsDto);



    /**
     * 修改学生管理
     *
     * @return 结果
     */
    public boolean updateBusUser(BusUser busUser);
}
