package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.business.domain.BusUserComments;
import com.ruoyi.business.service.IBusUserCommentsService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.security.context.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 用户评语（教师、班主任）Controller
 * 
 * @author ruoyi
 * @date 2022-10-28
 */
@RestController
@RequestMapping("/business/comments")
public class BusUserCommentsController extends BaseController
{
    @Autowired
    private IBusUserCommentsService busUserCommentsService;
    //单击确认，传入json数据 存入数据库
    @PostMapping("/confirm/remark")
    public AjaxResult confirmRemark(@RequestBody BusUserComments busUserComments){
        busUserComments.setUserId(UserContext.getUserId());
        //判断好这个评语是否都是空格或者为空，改变这个status的状态码
        if (busUserComments.getComments().trim().length() != 0){
                busUserComments.setStatus("1");
            }
        return toAjax(busUserCommentsService.updateBusUserComments(busUserComments));
    }

    //选择学生，返回其评语表信息，如果没有评语就为null  ----所以这个接口应该是可以对于已填写和未填写的公用接口
    @GetMapping("chooseStudent")
    public BusUserComments chooseStudent(Long studentId){

       return busUserCommentsService.chooseStudent(studentId, UserContext.getUserId());
    }

    //返回已填写或者未填写的人
    @GetMapping("/FillOutStatus")
    public TableDataInfo FillOutStatus(String status){
        List<Map<Long, List<BusStudents>>> fillOutStatusList =
                busUserCommentsService.
                        getFillOutStatusList(UserContext.getUserId());
        if (status.equals("1")){
            fillOutStatusList.remove(0);
        }else {
            fillOutStatusList.remove(1);
        }
        return getDataTable(fillOutStatusList);
    }

    /**
     * 查询用户评语（教师、班主任）列表
     */
    @PreAuthorize("@ss.hasPermi('business:comments:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusUserComments busUserComments)
    {
        startPage();
        List<BusUserComments> list = busUserCommentsService.selectBusUserCommentsList(busUserComments);
        return getDataTable(list);
    }

    /**
     * 导出用户评语（教师、班主任）列表
     */
    @PreAuthorize("@ss.hasPermi('business:comments:export')")
    @Log(title = "用户评语（教师、班主任）", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusUserComments busUserComments)
    {
        List<BusUserComments> list = busUserCommentsService.selectBusUserCommentsList(busUserComments);
        ExcelUtil<BusUserComments> util = new ExcelUtil<>(BusUserComments.class);
        util.exportExcel(response, list, "用户评语（教师、班主任）数据");
    }

    /**
     * 获取用户评语（教师、班主任）详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:comments:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId)
    {
        return AjaxResult.success(busUserCommentsService.selectBusUserCommentsByUserId(userId));
    }

    /**
     * 新增用户评语（教师、班主任）
     */
    @PreAuthorize("@ss.hasPermi('business:comments:add')")
    @Log(title = "用户评语（教师、班主任）", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusUserComments busUserComments)
    {
        return toAjax(busUserCommentsService.insertBusUserComments(busUserComments));
    }

    /**
     * 修改用户评语（教师、班主任）
     */
    @PreAuthorize("@ss.hasPermi('business:comments:edit')")
    @Log(title = "用户评语（教师、班主任）", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusUserComments busUserComments)
    {
        return toAjax(busUserCommentsService.updateBusUserComments(busUserComments));
    }

    /**
     * 删除用户评语（教师、班主任）
     */
    @PreAuthorize("@ss.hasPermi('business:comments:remove')")
    @Log(title = "用户评语（教师、班主任）", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds)
    {
        return toAjax(busUserCommentsService.deleteBusUserCommentsByUserIds(userIds));
    }

    /**
     * 学生邀请教师进行评语
     */
    @PostMapping("/invite")
    public AjaxResult invite(@RequestBody BusUserComments busUserComments){
        busUserComments.setStatus("0");
        busUserComments.setRemark("1");
        if (busUserCommentsService.insertBusUserComments(busUserComments) == 1){
            return AjaxResult.success();
        }else{
            return AjaxResult.error("已邀请过教师");
        }
    }
    /**
     * 查询可选择的教师
     */
    @GetMapping("/getTeacherOptions/{studentId}")
    public AjaxResult getTeacherOptions(@PathVariable Long studentId){
        return AjaxResult.success(busUserCommentsService.
                getTeacherOptions(studentId));
    }
}
