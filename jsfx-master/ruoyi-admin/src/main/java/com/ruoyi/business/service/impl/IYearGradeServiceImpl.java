package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.BusGradeTeacherHistory;
import com.ruoyi.business.domain.SemesterManageMent;
import com.ruoyi.business.domain.YearGrade;
import com.ruoyi.business.mapper.YearGeadeMapper;
import com.ruoyi.business.service.IBusGradeTeacherHistoryService;
import com.ruoyi.business.service.ISemesterService;
import com.ruoyi.business.service.IYearGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class IYearGradeServiceImpl extends ServiceImpl<YearGeadeMapper, YearGrade> implements IYearGradeService {
    //班级表
    @Autowired
    private IYearGradeService IYearGradeService;
    //学年学期
    @Autowired
    private ISemesterService semesterService;

    //老师的历史数据表
    @Autowired
    private IBusGradeTeacherHistoryService busGradeTeacherHistoryService;




    @Override
    public Map<String, HashMap<Long, List<String>>> getyearGrades() {
        String Years;
        //创建一个list为了添加各个年级的第几届（2020届）添加到list里面
        List<String> list1 = new ArrayList<>();

        //分装数据map中嵌套map
        Map<String, HashMap<Long, List<String>>> map = new HashMap<>();


        //从班级表中取出所有的班级
        List<YearGrade> list = IYearGradeService.list();
        for (YearGrade yearGrade : list) {
            //查询年级的2020级，从id中取前4位
            Long gradeId = yearGrade.getGradeId();
            String s = String.valueOf(gradeId);
            String substring = s.substring(0, 4);

            //将（2020级....）添加list，去除重复
            if (!list1.contains(substring)) {
                list1.add(substring);
                //将（2020级....）初始化外层的map
                map.put(substring, new HashMap<>());
            }

            //遍历每个年级段
            Set<String> strings = map.keySet();
            for (String string : strings) {
                //如果（2020级....）这样类型的年级段，和切割的班级前4位比较
                if (string.equals(substring)) {
                    //将给出年级，输出第一层嵌套的map的值是一个嵌套其中的map
                    HashMap<Long, List<String>> longListHashMap = map.get(string);
                    //内层的map的健添加这个年级段的班级有几个，初始化内层的map
                    List<String> put = longListHashMap.put(gradeId, new ArrayList<>());

                    //到实时的班级表中根据semester_id到学年学期表中读取数据
                    LambdaQueryWrapper<YearGrade> lqw = new LambdaQueryWrapper<>();
                    lqw.eq(YearGrade::getGradeId, gradeId);
                    List<YearGrade> list2 = IYearGradeService.list(lqw);
                    for (YearGrade grade : list2) {
                        Long semesterId = grade.getSemesterId();

                        LambdaQueryWrapper<SemesterManageMent> smmlqw = new LambdaQueryWrapper<>();
                        smmlqw.eq(SemesterManageMent::getSemesterId, semesterId);
                        SemesterManageMent one = semesterService.getOne(smmlqw);

                        //输出学年学期的年份
                        Long semesterYear = one.getSemesterYear();

                        String s1 = String.valueOf(semesterYear);
                        String semesterName = one.getSemesterName();
                        Years = s1 + semesterName;

                        //添加到list里面
                        List<String> list3 = longListHashMap.get(gradeId);
                        list3.add(Years);
                    }

                    //到班级教师的历史表中去读取数据
                    LambdaQueryWrapper<BusGradeTeacherHistory> bgtlqw = new LambdaQueryWrapper<>();
                    bgtlqw.eq(BusGradeTeacherHistory::getGradeId, gradeId);
                    List<BusGradeTeacherHistory> list4 = busGradeTeacherHistoryService.list(bgtlqw);
                    for (BusGradeTeacherHistory busGradeTeacherHistory : list4) {
                        Long semesterId1 = busGradeTeacherHistory.getSemesterId();

                        LambdaQueryWrapper<SemesterManageMent> smmlqw1 = new LambdaQueryWrapper<>();
                        smmlqw1.eq(SemesterManageMent::getSemesterId, semesterId1);
                        SemesterManageMent one1 = semesterService.getOne(smmlqw1);
                        //输出学年学期的年份
                        Long semesterYear1 = one1.getSemesterYear();


                        String s1 = String.valueOf(semesterYear1);
                        String semesterName = one1.getSemesterName();
                        String Years1 = s1 + semesterName;
                        //添加到list里面
                        List<String> list5 = longListHashMap.get(gradeId);
                        list5.add(Years1);
                    }
                }
            }
        }
        return map;
    }

    /**
     * 年级也班级数
     * @return
     */
    @Override
    public Map<String, List<Long>> getYearGrades() {

        Map<String, List<Long>> yearGrades = new HashMap<>();
        //从班级表中取出所有的班级
        List<YearGrade> list = IYearGradeService.list();
        for (YearGrade yearGrade : list) {
            Long gradeId = yearGrade.getGradeId();
            //切割再封装
            String year = (gradeId+"").substring(0,4);
            //过滤一下，然后进行封装
            if (yearGrades.containsKey(year)){
                List<Long> classesList = yearGrades.get(year);
                classesList.add(gradeId);
            }else {
                List<Long> gradeIds = new ArrayList<>();
                gradeIds.add(gradeId);
                yearGrades.put(year,gradeIds);
            }

        }
        return yearGrades;
    }
}