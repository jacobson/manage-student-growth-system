package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.UserPost;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserPostMapper extends BaseMapper<UserPost> {
}
