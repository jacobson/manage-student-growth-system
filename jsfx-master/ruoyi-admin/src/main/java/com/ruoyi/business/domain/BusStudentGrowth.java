package com.ruoyi.business.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 学生成长报告对象 bus_student_growth
 * 
 * @author ruoyi
 * @date 2022-10-22
 */
public class BusStudentGrowth
{
    private static final long serialVersionUID = 1L;

    /** 成长报告id */
    @TableId(type = IdType.AUTO)
    private Long growthId;

    /** 学生id */
    @Excel(name = "学生id")
    private Long studentId;

    /** 学年学期id */
    @Excel(name = "学年学期id")
    private Long semesterId;
    @TableField(exist = false)
    private String semesterName;
    @TableField(exist = false)
    private Long gradeId;
    @TableField(exist = false)
    private String stuName;
    public String getSemesterName() {
        return semesterName;
    }

    public void setSemesterName(String semesterName) {
        this.semesterName = semesterName;
    }

    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }


    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }


    /** 报告开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "报告开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date reportStartTime;

    /** 报告结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "报告结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date reportStartEnd;

    /** 状态（制作中、已定稿、未开始） */
    @Excel(name = "状态", readConverterExp = "制作中、已定稿、未开始")
    private String status;

    public void setGrowthId(Long growthId) 
    {
        this.growthId = growthId;
    }

    public Long getGrowthId() 
    {
        return growthId;
    }
    public void setStudentId(Long studentId) 
    {
        this.studentId = studentId;
    }

    public Long getStudentId() 
    {
        return studentId;
    }
    public void setSemesterId(Long semesterId) 
    {
        this.semesterId = semesterId;
    }

    public Long getSemesterId() 
    {
        return semesterId;
    }

    public void setReportStartTime(Date reportStartTime) 
    {
        this.reportStartTime = reportStartTime;
    }

    public Date getReportStartTime() 
    {
        return reportStartTime;
    }
    public void setReportStartEnd(Date reportStartEnd) 
    {
        this.reportStartEnd = reportStartEnd;
    }

    public Date getReportStartEnd() 
    {
        return reportStartEnd;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("growthId", getGrowthId())
            .append("studentId", getStudentId())
            .append("semesterId", getSemesterId())
            .append("gradeId",getGradeId())
            .append("semesterName",getSemesterName())
            .append("reportStartTime", getReportStartTime())
            .append("reportStartEnd", getReportStartEnd())
            .append("status", getStatus())
            .toString();
    }
}
