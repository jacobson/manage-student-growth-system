package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusGradeStudent;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 班级学生Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-24
 */
@Mapper
public interface BusGradeStudentMapper extends BaseMapper<BusGradeStudent> {
    /**
     * 查询班级学生
     *
     * @param gradeId 班级学生主键
     * @return 班级学生
     */
    public BusGradeStudent selectBusGradeStudentByGradeId(Long gradeId);

    /**
     * 查询班级学生列表
     *
     * @param busGradeStudent 班级学生
     * @return 班级学生集合
     */
    public List<BusGradeStudent> selectBusGradeStudentList(BusGradeStudent busGradeStudent);

    /**
     * 新增班级学生
     *
     * @param busGradeStudent 班级学生
     * @return 结果
     */
    public int insertBusGradeStudent(BusGradeStudent busGradeStudent);

    /**
     * 修改班级学生
     *
     * @param busGradeStudent 班级学生
     * @return 结果
     */
    public int updateBusGradeStudent(BusGradeStudent busGradeStudent);

    /**
     * 删除班级学生
     *
     * @param gradeId 班级学生主键
     * @return 结果
     */
    public int deleteBusGradeStudentByGradeId(Long gradeId);

    /**
     * 批量删除班级学生
     *
     * @param gradeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusGradeStudentByGradeIds(Long[] gradeIds);



    /**
     * 删除班级学生
     *
     * @param studentId 班级学生主键
     * @return 结果
     */
    public int deleteBusGradeStudentByStudentId(Long studentId);
}