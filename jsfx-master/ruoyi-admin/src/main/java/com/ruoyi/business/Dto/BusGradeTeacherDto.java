package com.ruoyi.business.Dto;

import com.ruoyi.business.domain.BusGradeTeacher;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class BusGradeTeacherDto extends BusGradeTeacher {

    private static final long serialVersionUID = 1L;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 姓名
     */
    private String userName;
    /**
     * 性别
     */
    private String sex;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 年级
     */
    private String gradeName;
    /**
     * 教学班级
     * 教学课程
     */
    private Map<String,String> TeachWithSubject =new HashMap<>();
    /**
     * 是否为班主任
     */
    private Boolean headTeacher;
    /**
     * 所带班级
     */
    private List<Long> Classes =new ArrayList<>();


    /**
     * 用来将所带班级与课程连接
     */
    public void setClassWithSubject(String ClassName, String Subject) {
        if(TeachWithSubject.containsKey(ClassName)){
            TeachWithSubject.put(ClassName, TeachWithSubject.get(ClassName)+","+Subject);
        }else {
            TeachWithSubject.put(ClassName,Subject);
        }
    }
    /**
     * 用来添加所带班级
     */
    public void addBusGrade(Long gradeId){
        //如果班级存在就不重复添加
        if (!Classes.contains(gradeId)) {
            Classes.add(gradeId);
        }
    }
}
