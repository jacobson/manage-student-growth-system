package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusSubject;
import com.ruoyi.system.domain.SysPost;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 学科Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@Mapper
public interface BusSubjectMapper extends BaseMapper<BusSubject>
{
    /**
     * 查询学科
     *
     * @param subjectId 学科主键
     * @return 学科
     */
    public BusSubject selectBusSubjectBySubjectId(Long subjectId);

    /**
     * 查询学科列表
     *
     * @param busSubject 学科
     * @return 学科集合
     */
    public List<BusSubject> selectBusSubjectList(BusSubject busSubject);

    /**
     * 新增学科
     *
     * @param busSubject 学科
     * @return 结果
     */
    @Insert("insert into bus_subject (subject_name,subject_description) values (#{subjectName},#{subjectDescription})")
    public int insertBusSubject(BusSubject busSubject);

    /**
     * 修改学科
     *
     * @param busSubject 学科
     * @return 结果
     */
    @Update("update bus_subject set subject_name = #{subjectName},subject_description=#{subjectDescription} where subject_id = #{subjectId}")
    public int updateBusSubject(BusSubject busSubject);

    /**
     * 删除学科
     *
     * @param subjectId 学科主键
     * @return 结果
     */
    public int deleteBusSubjectBySubjectId(Long subjectId);

    /**
     * 批量删除学科
     *
     * @param subjectIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusSubjectBySubjectIds(Long[] subjectIds);
    //获取所有学科名
    @Select("select subject_name from bus_subject")
    List<String> getAll();

    //获取所有学科
    @Select("select  * from bus_subject")
    List<BusSubject> getAllSubject();
    @Select("select * from sys_post")
    List<SysPost> getAllPost();

    @Select("select subject_id from bus_subject where subject_name=#{subjectName}")
    Long exitSubject(String subjectName);
}
