package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * 学生管理Service接口
 * 
 * @author ruoyi
 * @date 2022-10-21
 */
public interface IBusStudentsService extends IService<BusStudents>
{
    /**
     * 查询学生管理
     * 
     * @param stuId 学生管理主键
     * @return 学生管理
     */
    public Map<Object,Object> selectBusStudentsByStuId(Long stuId);

    /**
     * 查询学生列表
     * 
     * @param busStudents 学生
     * @return 学生集合
     */
    public List<BusStudents> selectBusStudentsList(BusStudents busStudents);

    /**
     * 新增学生管理
     * 
     * @param busStudents 学生管理
     * @return 结果
     */
    public int insertBusStudents(BusStudents busStudents);

    /**
     * 修改学生管理
     * 
     * @param busStudents 学生管理
     * @return 结果
     */
    public int updateBusStudents(BusStudents busStudents);

    /**
     * 批量删除学生管理
     * 
     * @param stuIds 需要删除的学生管理主键集合
     * @return 结果
     */
    int deleteBusStudentsByStuIds(Long[] stuIds);

    /**
     * 删除学生管理信息
     * 
     * @param stuId 学生管理主键
     * @return 结果
     */
    int deleteBusStudentsByStuId(Long stuId);

    List<BusStudents> selectBusStudentsByGradeId(Long gradeId);

    int editAvatar(Long studentId, MultipartFile file) throws IOException, InvalidExtensionException;

    List<Long> selectStudentIdsByGradeId(Long gradeId);

    List<BusStudents> getAll();
}
