package com.ruoyi.business.service.impl;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.business.domain.*;
import com.ruoyi.business.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentScoreManageImpl implements IStudentScoreManageService {

    //学期表
    @Autowired
    private IBusSemesterService busSemesterService;

    @Autowired
    private IBusStudentGrowthService iBusStudentGrowthService;

    @Autowired
    private IBusStudentGrowthItemService iBusStudentGrowthItemService;

    @Autowired
    private IBusGradeStudentService iBusGradeStudentService;

    @Autowired
    private IBusSemesterService iBusSemesterService;

    @Autowired
    private IBusStudentsService iBusStudentsService;

    //返回全部当前学期所有学生素养报告单
    @Override
    public List<JSONObject> allStudentScoreInfo(String gradeId,String semesterYearName) {
        //获取当前班级的所有学生
        LambdaQueryWrapper<BusGradeStudent> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BusGradeStudent::getGradeId,gradeId);
        List<BusGradeStudent> list = iBusGradeStudentService.list(queryWrapper);
        //把传入的参数进行切割
        String semesterYear = semesterYearName.substring(0, 4);
        String semesterName = semesterYearName.substring(4);
        //用上面切割出来的属性去表中获取semesterId
        LambdaQueryWrapper<BusSemester> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.eq(BusSemester::getSemesterYear,semesterYear);
        queryWrapper1.eq(BusSemester::getSemesterName,semesterName);
        BusSemester one = iBusSemesterService.getOne(queryWrapper1);
        Long semesterId = one.getSemesterId();
        //获取到该班级所有学生所对应的学期的成长报告数据
        List<BusStudentGrowth> studentGrowths = new ArrayList<>();
        for (BusGradeStudent busGradeStudent : list) {
            Long studentId = busGradeStudent.getStudentId();
            LambdaQueryWrapper<BusStudentGrowth> queryWrapper2 = new LambdaQueryWrapper<>();
            queryWrapper2.eq(BusStudentGrowth::getStudentId,studentId);
            queryWrapper2.eq(BusStudentGrowth::getSemesterId,semesterId);
            BusStudentGrowth one1 = iBusStudentGrowthService.getOne(queryWrapper2);
            if(one1 != null){
                studentGrowths.add(one1);
            }
        }


        //以流的方式历遍
        List<JSONObject> jsonObjects = studentGrowths.stream().map(item -> {
            Long studentId = null;
            for (BusGradeStudent busGradeStudent : list) {
                studentId = busGradeStudent.getStudentId();
                list.remove(0);
                break;
            }
            //拿到当前学期学生的一个
            Long growthId = item.getGrowthId();
            //拿这个学生的成长项表数据
            LambdaQueryWrapper<BusStudentGrowthItem> busStudentGrowthItemWrapper = new LambdaQueryWrapper<>();
            busStudentGrowthItemWrapper.eq(BusStudentGrowthItem::getGrowthId, growthId);
            //只需要这个学生的学生素养报告单
            busStudentGrowthItemWrapper.eq(BusStudentGrowthItem::getGrowthKey, "学生素养报告单");
            BusStudentGrowthItem growthItemOne = iBusStudentGrowthItemService.getOne(busStudentGrowthItemWrapper);
            if (growthItemOne == null){
                return null;
            }
            String growthValue = growthItemOne.getGrowthValue();
            //拿到学生素养报告单这个对象后将其value的值转化未Json对象
            JSONObject entries = new JSONObject();
            if (growthValue == null){
                growthValue = "成绩尚未导入";
                entries.set("data",growthValue);
                return entries;
            }
            growthValue ="{" + "\"studentId\"" + ":" + " " + studentId + ","+ growthValue.substring(1);
            return new JSONObject(growthValue);
        }).collect(Collectors.toList());
        return jsonObjects;
    }




    //回显一个学生的成绩报告
    @Override
    public JSONObject echoStudentScoreInfo(Long studentId) {
        //获取当前学期ID
        Long currentSemesterId = busSemesterService.getCurrentSemesterId();
        //拿这个学生的信息
        LambdaQueryWrapper<BusStudentGrowth> busStudentGrowthWrapper = new LambdaQueryWrapper<>();
        busStudentGrowthWrapper.eq(BusStudentGrowth::getSemesterId,currentSemesterId);
        //捕捉这个学生的当前学期，用来锁定growth_id
        busStudentGrowthWrapper.eq(BusStudentGrowth::getStudentId,studentId);
        //先拿到这个学生的成长报告表
        BusStudentGrowth studentGrowth = iBusStudentGrowthService.getOne(busStudentGrowthWrapper);
        //拿到其对应的growth_id
        Long growthId = studentGrowth.getGrowthId();
        //拿这个学生的成长项表数据
        LambdaQueryWrapper<BusStudentGrowthItem> busStudentGrowthItemWrapper = new LambdaQueryWrapper<>();
        busStudentGrowthItemWrapper.eq(BusStudentGrowthItem::getGrowthId, growthId);
        //只需要这个学生的学生素养报告单
        busStudentGrowthItemWrapper.eq(BusStudentGrowthItem::getGrowthKey, "学生素养报告单");
        BusStudentGrowthItem growthItemOne = iBusStudentGrowthItemService.getOne(busStudentGrowthItemWrapper);
        String growthValue = growthItemOne.getGrowthValue();
        //拿到学生素养报告单这个对象后将其value的值转化未Json对象
        growthValue ="{" + "\"studentId\"" + ":" + " " + studentId + ","+ growthValue.substring(1);
        return new JSONObject(growthValue);
    }

    //修改一个学生的信息
    @Override
    public boolean updateStudentScoreInfo(JSONObject object) {
        //获取学生ID
        Object studentId = object.get("studentId");
        object.remove("studentId");
        //获取当前学期ID
        Long currentSemesterId = busSemesterService.getCurrentSemesterId();
        //拿这个学生的信息
        LambdaQueryWrapper<BusStudentGrowth> busStudentGrowthWrapper = new LambdaQueryWrapper<>();
        busStudentGrowthWrapper.eq(BusStudentGrowth::getSemesterId,currentSemesterId);
        //捕捉这个学生的当前学期，用来锁定growth_id
        busStudentGrowthWrapper.eq(BusStudentGrowth::getStudentId,studentId);
        //先拿到这个学生的成长报告表
        BusStudentGrowth studentGrowth = iBusStudentGrowthService.getOne(busStudentGrowthWrapper);
        //拿到其对应的growth_id
        Long growthId = studentGrowth.getGrowthId();
        //拿这个学生的成长项表数据
        LambdaQueryWrapper<BusStudentGrowthItem> busStudentGrowthItemWrapper = new LambdaQueryWrapper<>();
        busStudentGrowthItemWrapper.eq(BusStudentGrowthItem::getGrowthId, growthId);
        //只需要这个学生的学生素养报告单
        busStudentGrowthItemWrapper.eq(BusStudentGrowthItem::getGrowthKey, "学生素养报告单");
        BusStudentGrowthItem growthItemOne = iBusStudentGrowthItemService.getOne(busStudentGrowthItemWrapper);
        //将新地成长素质表
        growthItemOne.setGrowthValue(object.toString());
        //锁一下这个学生项表中的信息
        LambdaQueryWrapper<BusStudentGrowthItem> updateBusStudentGrowthItemWrapper = new LambdaQueryWrapper<>();
        updateBusStudentGrowthItemWrapper.eq(BusStudentGrowthItem::getGrowthId, growthId);
        updateBusStudentGrowthItemWrapper.eq(BusStudentGrowthItem::getGrowthKey, "学生素养报告单");
        return iBusStudentGrowthItemService.update(growthItemOne,updateBusStudentGrowthItemWrapper);

    }

    //条件查询
    @Override
    public List<JSONObject> choose(String gradeId, String name) {
        //获取当前学期ID
        Long currentSemesterId = busSemesterService.getCurrentSemesterId();
        LambdaQueryWrapper<BusStudentGrowth> busStudentGrowthWrapper = new LambdaQueryWrapper<>();
        busStudentGrowthWrapper.eq(BusStudentGrowth::getSemesterId,currentSemesterId);
        List<BusStudentGrowth> studentGrowths = iBusStudentGrowthService.list(busStudentGrowthWrapper);
        //以流的方式历遍
        List<JSONObject> jsonObjects = studentGrowths.stream().map(item -> {
            //拿到当前学期学生的一个
            Long growthId = item.getGrowthId();
            //拿这个学生的成长项表数据
            LambdaQueryWrapper<BusStudentGrowthItem> busStudentGrowthItemWrapper = new LambdaQueryWrapper<>();
            busStudentGrowthItemWrapper.eq(BusStudentGrowthItem::getGrowthId, growthId);
            //只需要这个学生的学生素养报告单
            busStudentGrowthItemWrapper.eq(BusStudentGrowthItem::getGrowthKey, "学生素养报告单");
            BusStudentGrowthItem growthItemOne = iBusStudentGrowthItemService.getOne(busStudentGrowthItemWrapper);
            String growthValue = growthItemOne.getGrowthValue();
            //拿到学生素养报告单这个对象后将其value的值转化未Json对象
            return new JSONObject(growthValue);
        }).collect(Collectors.toList());
        List<JSONObject> result = new ArrayList<>();
        //筛选
        for (JSONObject item: jsonObjects) {
           if (item.size()>0){

               //成绩中项值的中的名字
               String scoreName = item.get("学生姓名").toString();
               String scoreClass = item.get("班级").toString();
               //如果姓名的条件为空,有班级-->，这个班级的所有人都要显示
               if (name.equals("") && !gradeId.equals("") && scoreClass.equals(gradeId)){
                   System.out.println("姓名的条件为空,有班级");
                   result.add(item);
                   //如果班级的条件为空,有姓名-->，这个名字的人，哪怕重名都要显示
               }else if (!name.equals("") && gradeId.equals("") && scoreName.equals(name)){
                   System.out.println("班级的条件为空,有姓名");
                   result.add(item);
                   //如果班级、姓名都不为空-->找到这个对应的人，极端同名同班的同学一样符合条件
               }else if (!name.equals("") && !gradeId.equals("") && scoreName.equals(name) && scoreClass.equals(gradeId)){
                   System.out.println("班级、姓名都不为空");
                   result.add(item);

               }else if (name.equals("") && gradeId.equals("") ){
                   //如果两个条件都是空的话，将所有人都返回
                   System.out.println("两个条件都是空");
                   return jsonObjects;
               }
           }
        }
        return result;

    }

    //条件查询2
    public List<JSONObject> chooseStudentScoreInfo(String gradeId, String name) {
        LambdaQueryWrapper<BusGradeStudent> busGradeStudentLambdaQueryWrapper = new LambdaQueryWrapper<>();
        busGradeStudentLambdaQueryWrapper.eq(BusGradeStudent::getGradeId,gradeId);
        //拿到这个班级的所有学生学号
        List<BusGradeStudent> gradeStudents = iBusGradeStudentService.list(busGradeStudentLambdaQueryWrapper);

        //获取这个班级的所有学生ID
        List<Long> studentIds = new ArrayList<>();
        //封装
        for (BusGradeStudent gradeStudent : gradeStudents) {
            Long studentId = gradeStudent.getStudentId();
            studentIds.add(studentId);
        }
        //拿到这个班级符合条件的学生
        LambdaQueryWrapper<BusStudents> busStudentsLambdaQueryWrapper = new LambdaQueryWrapper<>();
        busStudentsLambdaQueryWrapper.like(name != null,BusStudents::getStuName,name);
        busStudentsLambdaQueryWrapper.in(BusStudents::getStuId,studentIds);
        System.out.println(studentIds);
        //可能会有同名的人
        List<BusStudents> busStudents = iBusStudentsService.list(busStudentsLambdaQueryWrapper);
        if (busStudents.isEmpty()){
            //返回个空集合就好
            return new ArrayList<>();
        }else {
            //清空一下，重新存储符合条件的学号
            studentIds.clear();
            //存储符合条件的学生
            for (BusStudents busStudent : busStudents) {
                studentIds.add(busStudent.getStuId());
            }
            //获取当前学期ID
            Long currentSemesterId = busSemesterService.getCurrentSemesterId();
            LambdaQueryWrapper<BusStudentGrowth> busStudentGrowthWrapper = new LambdaQueryWrapper<>();
            busStudentGrowthWrapper.eq(BusStudentGrowth::getSemesterId,currentSemesterId);
            busStudentGrowthWrapper.in(BusStudentGrowth::getStudentId,studentIds);

            //拿到符合这个学生的所有成长报告
            List<BusStudentGrowth> studentGrowths = iBusStudentGrowthService.list(busStudentGrowthWrapper);
            //以流的方式历遍
            return studentGrowths.stream().map(item -> {
                Long studentId = item.getStudentId();
                //拿到当前学期学生的一个
                Long growthId = item.getGrowthId();
                //拿这个学生的成长项表数据
                LambdaQueryWrapper<BusStudentGrowthItem> busStudentGrowthItemWrapper = new LambdaQueryWrapper<>();
                busStudentGrowthItemWrapper.eq(BusStudentGrowthItem::getGrowthId, growthId);
                //只需要这个学生的学生素养报告单
                busStudentGrowthItemWrapper.eq(BusStudentGrowthItem::getGrowthKey, "学生素养报告单");
                BusStudentGrowthItem growthItemOne = iBusStudentGrowthItemService.getOne(busStudentGrowthItemWrapper);
                String growthValue = growthItemOne.getGrowthValue();
                growthValue ="{" + "\"studentId\"" + ":" + " " + studentId + ","+ growthValue.substring(1);
                //拿到学生素养报告单这个对象后将其value的值转化未Json对象
                return new JSONObject(growthValue);
            }).collect(Collectors.toList());
        }


    }
}
