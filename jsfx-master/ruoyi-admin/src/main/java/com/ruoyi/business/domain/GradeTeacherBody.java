package com.ruoyi.business.domain;

import lombok.Data;

import java.util.Map;

@Data
public class GradeTeacherBody {
    private Long gradeId;
    private Map<String,Long> teacher;
}
