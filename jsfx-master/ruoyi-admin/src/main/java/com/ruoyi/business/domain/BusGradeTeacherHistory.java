package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * 班主任与班级历史对象 bus_grade_teacher_history
 * 
 * @author ruoyi
 * @date 2022-11-12
 */
@Data
public class BusGradeTeacherHistory
{
    private static final long serialVersionUID = 1L;

    /** 班级id */
    @Excel(name = "班级id")
    private Long gradeId;

    /** 教师id */
    @Excel(name = "教师id")
    private Long userId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long semesterId;


}
