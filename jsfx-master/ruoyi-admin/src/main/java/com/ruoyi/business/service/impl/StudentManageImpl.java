package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.Dto.BusStudentParentsDtoT;
import com.ruoyi.business.Dto.UserGalleryDto;
import com.ruoyi.business.Util.IdUtil;
import com.ruoyi.business.domain.*;
import com.ruoyi.business.mapper.StudentManageMapper;
import com.ruoyi.business.service.*;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.exception.MyException;
import com.ruoyi.system.domain.MySysRole;
import com.ruoyi.system.domain.SysUserRole;
import com.ruoyi.system.service.ISysRoleService;
import com.ruoyi.system.service.ISysUserRoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StudentManageImpl extends ServiceImpl<StudentManageMapper, BusStudentParentsDtoT> implements IStudentManageService {
    //班级表
    @Autowired
    private IBusGradeService iBusGradeService;
    //班级学生表
    @Autowired
    private IBusGradeStudentService iBusGradeStudentService;
    //家长表
    @Autowired
    private IBusStudentParentsService iBusStudentParentsService;
    //用户表
    @Autowired
    private IBusUserService iBusUserService;

    //学生信息表
    @Autowired
    private IBusStudentsService iBusStudentsService;
    //成长报告
    @Autowired
    private IBusStudentGrowthService iBusStudentGrowthService;

    //角色表
    @Autowired
    private ISysRoleService iSysRoleService;
    //角色用户信息关联表
    @Autowired
    private ISysUserRoleService iSysUserRoleService;
    //图库
    @Autowired
    private IBusGalleryService iBusGalleryService;

    @Autowired
    private IStudentManageService iStudentManageService;

    //评语表
    @Autowired
    private IBusUserCommentsService iBusUserCommentsService;
    //荣誉表
    @Autowired
    private IBusStudentHonorService iBusStudentHonorService;
    //指定班级所有学生及其父母信息
    @Override
    public List<BusStudentParentsDtoT> getPointGradeStudentInfo(Long gradeId) {
        //拿到这个班级的学生信息
        List<BusStudents> studentInfo = iBusGradeStudentService.getStudentInfoByGradeId(gradeId);
        if (studentInfo != null){
            return iBusStudentParentsService.selectBusStudentParentsByListT(studentInfo);
        }else {
            return null;
        }

    }

    @Override
    public Map<Long, List<Long>> getYearLevelGradeId() {
        //拿到所有的学年班级信息
        List<BusGrade> busGrades = iBusGradeService.list();
        //存储信息
        Map<Long, List<Long>> mapYearLevelGradeId = new HashMap<>();
        for (BusGrade grade : busGrades) {
            //拿到这个班级所属学年
            Long yearLevelId = grade.getYearLevelId();
            //班级Id
            Long gradeId = grade.getGradeId();

            //封存数据
            List<Long> gradeList;
            if (mapYearLevelGradeId.containsKey(yearLevelId)){
                gradeList = mapYearLevelGradeId.get(yearLevelId);
            }else {
                gradeList = new ArrayList<>();
            }
            gradeList.add(gradeId);
            mapYearLevelGradeId.put(yearLevelId,gradeList);
        }
        return mapYearLevelGradeId;
    }


    @Override
    public BusStudentParentsDtoT getStudentWithParentsInfo(Long StudentId) {

        LambdaQueryWrapper<BusStudents> busStudentsLambdaQueryWrapper = new LambdaQueryWrapper<>();
        busStudentsLambdaQueryWrapper.eq(BusStudents::getStuId,StudentId);
        //拿到这个学生的信息
        BusStudents item = iBusStudentsService.getOne(busStudentsLambdaQueryWrapper);

        //拿到这个学生的班级ID
        Long gradeIdByStudentId = iBusGradeStudentService.getGradeIdByStudentId(StudentId);

        //创建一个Dto对象 并且把这个学生的信息全部拷贝到这个对象中
        BusStudentParentsDtoT busStudentParentsDtoT = new BusStudentParentsDtoT();
        BeanUtils.copyProperties(item,busStudentParentsDtoT);

        //封装该学生的班级Id
        busStudentParentsDtoT.setGradeId(gradeIdByStudentId);

        //获取学生家长的信息
        List<BusStudentParents> list1 = iBusStudentParentsService.selectBusStudentParentsByStudentIds(item.getStuId());

        for (BusStudentParents busStudentParents : list1) {
            String relation = busStudentParents.getRelation();
            BusUser busUser = iBusUserService.selectUserById(busStudentParents.getUserId());
            String nickName = busUser.getNickName();
            String phonenumber = busUser.getPhonenumber();
            if (relation.equals("爸爸")){
                busStudentParentsDtoT.setFatherUserId(busStudentParents.getUserId());
                busStudentParentsDtoT.setFatherName(nickName);
                busStudentParentsDtoT.setFatherNumber(phonenumber);
            }else if (relation.equals("妈妈")){
                busStudentParentsDtoT.setMotherUserId(busStudentParents.getUserId());
                busStudentParentsDtoT.setMotherName(nickName);
                busStudentParentsDtoT.setMotherNumber(phonenumber);
            }
        }

        return busStudentParentsDtoT;

    }


    @Override
    public AjaxResult updateStudentWithParentsInfo(BusStudentParentsDtoT busStudentParentsDtoT) {
        //判断当前的班级是否存在
        LambdaQueryWrapper<BusGrade> gradeQueryWrapper = new LambdaQueryWrapper();
        gradeQueryWrapper.eq(BusGrade::getGradeId,busStudentParentsDtoT.getGradeId());
        BusGrade one = iBusGradeService.getOne(gradeQueryWrapper);
        if (one != null){
            String fatherNumber = busStudentParentsDtoT.getFatherNumber();
            String motherNumber = busStudentParentsDtoT.getMotherNumber();
            //查找爸爸的手机号是否存在
            LambdaQueryWrapper<BusUser> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(BusUser::getPhonenumber,fatherNumber);
            List<BusUser> list = iBusUserService.list(queryWrapper);
            //获取爸爸的id
            Long userId = null;
            Long userId1 = null;

            if (list.size() == 1){
                userId = list.get(0).getUserId();
            }
            //查找妈妈的手机号是否存在
            LambdaQueryWrapper<BusUser> queryWrapper1 = new LambdaQueryWrapper<>();
            queryWrapper1.eq(BusUser::getPhonenumber,motherNumber);
            List<BusUser> list1 = iBusUserService.list(queryWrapper1);
            //拿到其母亲ID
            if (list1.size() == 1){
                userId1 = list1.get(0).getUserId();
            }

            //拿到学生绑定的爸爸信息
            BusUser fatherBusUser = iStudentManageService.findFatherInfo(busStudentParentsDtoT);
            //拿到这个学生的爸爸的UserId
            Long FatherUserId = fatherBusUser.getUserId();

            //拿到学生绑定的妈妈信息
            BusUser motherBusUser = iStudentManageService.findMotherInfo(busStudentParentsDtoT);
            //拿到这个学生的妈妈的UserId
            Long MotherUserId = motherBusUser.getUserId();

            //判断这个学生是否更换班级
            Long stuId = busStudentParentsDtoT.getStuId();
            Long gradeId = busStudentParentsDtoT.getGradeId();
            boolean equals = stuId.toString().substring(0, 6).equals(gradeId.toString());
            boolean equals1 = true;
            boolean equals2 = true;
            if (userId1 != null && userId != null ){
                equals1 = FatherUserId.toString().equals(userId.toString());
                equals2 = MotherUserId.toString().equals(userId1.toString());
            }

            //当这个号码没有重复
            if (list.size() == 0 && list1.size() == 0){
                //班级被修改了
                if (!equals){
                    //修改学生和家长信息
                    boolean updateAllInfoBj = iStudentManageService.updateAllInfoBj(busStudentParentsDtoT, fatherBusUser, motherBusUser);
                    if (updateAllInfoBj){
                        return AjaxResult.success(updateAllInfoBj);
                    }else{
                        return AjaxResult.error("修改失败");
                    }
                }else{
                    //修改学生和家长的所有信息
                    boolean updateAllInfo = iStudentManageService.updateAllInfo(busStudentParentsDtoT,fatherBusUser,motherBusUser);
                    if (updateAllInfo){
                        return AjaxResult.success(updateAllInfo);
                    }else{
                        return AjaxResult.error("修改失败");
                    }
                }
            //当号码重复的时候
            }else if (list.size() == 1 && list1.size() == 1 ){

                //当爸爸妈妈手机号都重复，但是是因为前端没修改手机号
                if (equals1 && equals2){

                    //班级被修改了
                    if (!equals){
                        //修改学生和家长信息
                        boolean updateAllInfoBj = iStudentManageService.updateAllInfoBj(busStudentParentsDtoT, fatherBusUser, motherBusUser);
                        if (updateAllInfoBj){
                            return AjaxResult.success(updateAllInfoBj);
                        }else{
                            return AjaxResult.error("修改失败");
                        }
                    }else{
                        //修改学生和家长的所有信息
                        boolean updateAllInfo = iStudentManageService.updateAllInfo(busStudentParentsDtoT,fatherBusUser,motherBusUser);
                        if (updateAllInfo){
                            return AjaxResult.success(updateAllInfo);
                        }else{
                            return AjaxResult.error("修改失败");
                        }
                    }
                }else if (!equals1 && equals2){
                    return AjaxResult.error("爸爸的手机号被占用了");
                }else if (equals1 && !equals2){
                    return AjaxResult.error("妈妈的手机号被占用");
                }else {
                    return AjaxResult.error("爸爸和妈妈的手机号都被占用了");
                }
                //当父母中只有一个手机号重复时
            }else{
                if (list.size() == 1){
                    //爸爸的手机号重复，但是是和自己重复，也就是前端并未做修改
                    if (equals1){
                        //班级被修改了
                        if (!equals){
                            //修改学生和家长信息
                            boolean updateAllInfoBj = iStudentManageService.updateAllInfoBj(busStudentParentsDtoT, fatherBusUser, motherBusUser);
                            if (updateAllInfoBj){
                                return AjaxResult.success(updateAllInfoBj);
                            }else{
                                return AjaxResult.error("修改失败");
                            }
                        }else{
                            //修改学生和家长的所有信息
                            boolean updateAllInfo = iStudentManageService.updateAllInfo(busStudentParentsDtoT,fatherBusUser,motherBusUser);
                            if (updateAllInfo){
                                return AjaxResult.success(updateAllInfo);
                            }else{
                                return AjaxResult.error("修改失败");
                            }
                        }
                    }else{
                        return AjaxResult.error("爸爸的手机号被占用了");
                    }
                }else {
                    if (equals2){
                        //修改学生和家长的所有信息
                        //班级被修改了
                        if (!equals){
                            //修改学生和家长信息
                            boolean updateAllInfoBj = iStudentManageService.updateAllInfoBj(busStudentParentsDtoT, fatherBusUser, motherBusUser);
                            if (updateAllInfoBj){
                                return AjaxResult.success(updateAllInfoBj);
                            }else{
                                return AjaxResult.error("修改失败");
                            }
                        }else{
                            //修改学生和家长的所有信息
                            boolean updateAllInfo = iStudentManageService.updateAllInfo(busStudentParentsDtoT,fatherBusUser,motherBusUser);
                            if (updateAllInfo){
                                return AjaxResult.success(updateAllInfo);
                            }else{
                                return AjaxResult.error("修改失败");
                            }
                        }
                    }else{
                        return AjaxResult.error("妈妈的手机号被占用了");
                    }
                }
            }
        }else {
            return AjaxResult.error("班级不存在");
        }

    }

    @Override
    public boolean addStudentWithParentsInfo(BusStudentParentsDtoT busStudentParentsDtoT) {
        //爸爸的新增操作是否成功

        boolean fatherUserSave = false;
        boolean fatherSave = false;
        boolean fatherUserRoleSave = false;
        //妈妈的新增操作是否成功
        boolean motherUserSave = false;
        boolean motherSave = false;
        boolean motherUserRoleSave = false;
        //成长报告报
        boolean growthSave = false;
        //学生是否新增成功
        boolean studentSave = false;
        //设置爸爸妈妈名字初始值
        String fatherNickName = null;
        String motherNickName = null;

        //拿到家长所对应的roleId
        LambdaQueryWrapper<MySysRole> sysRoleLambdaQueryWrapper = new LambdaQueryWrapper<>();
        sysRoleLambdaQueryWrapper.eq(MySysRole::getRoleName,"家长");
        MySysRole roleServiceOne = iSysRoleService.getOne(sysRoleLambdaQueryWrapper);
        Long roleId = roleServiceOne.getRoleId();
        Long gradeId = busStudentParentsDtoT.getGradeId();
        //获取当前新增的学生他的学生id
        Long studentId = iStudentManageService.setStudentId(gradeId);

        //存储其父母信息
        //------爸爸--------
        //生成爸爸其ID
        //雪花算法截取前面或后面16位作为
        String fatherId = IdUtil.getId().toString();
        //用爸爸的手机号去用户表里面去查是否存在
        LambdaQueryWrapper<BusUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BusUser::getPhonenumber,busStudentParentsDtoT.getFatherNumber());
        BusUser one = iBusUserService.getOne(queryWrapper);

        if (one != null){
            fatherNickName = one.getNickName();
        }


        //------妈妈--------
        //生成妈妈其ID
        //雪花截取前面或后面16位作为
        String motherId = IdUtil.getId().toString();

        //用妈妈的手机号去用户表里面去查是否存在
        LambdaQueryWrapper<BusUser> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.eq(BusUser::getPhonenumber,busStudentParentsDtoT.getMotherNumber());
        BusUser one1 = iBusUserService.getOne(queryWrapper1);
        if (one1 != null){
            motherNickName = one1.getNickName();
        }
        //判断当前号码是否存在且名字是否相等，名字相等不新增获取用户id  名字不相等不允许新增
        if (one == null && one1 == null){
            //新增爸爸的信息
            fatherUserSave = iStudentManageService.addANewFather(busStudentParentsDtoT,fatherId);

            //新增妈妈的信息
            motherUserSave = iStudentManageService.addANewMother(busStudentParentsDtoT,motherId);

            //分别找到这两个人 就是我们新增的这个爸爸妈妈，分别拿到他们的userId
        }else if (one != null && fatherNickName.equals(busStudentParentsDtoT.getFatherName())
                && one1 != null && motherNickName.equals(busStudentParentsDtoT.getMotherName()) ){

            fatherId = String.valueOf(one.getUserId());
            fatherUserSave = true;
            motherId = String.valueOf(one1.getUserId());
            motherUserSave = true;
            //没有找到人，直接新增这个爸爸
            //当只有爸爸的手机号存在且就是这个人 就新增妈妈的用户
        }else if (one != null && fatherNickName.equals(busStudentParentsDtoT.getFatherName()) && one1 == null ){
            fatherId = String.valueOf(one.getUserId());
            fatherUserSave = true;
            //新增妈妈的信息
            motherUserSave = iStudentManageService.addANewMother(busStudentParentsDtoT,motherId);
            //当只有妈妈的手机号存在且就是这个人 就新增爸爸的用户
        }else if (one == null && motherNickName.equals(busStudentParentsDtoT.getMotherName())){
            //新增爸爸的信息
            fatherUserSave = iStudentManageService.addANewFather(busStudentParentsDtoT,fatherId);
            motherId = String.valueOf(one1.getUserId());
            motherUserSave = true;
        }
        //如果爸爸妈妈都可以新增则学生也可以新增
        if (fatherUserSave && motherUserSave){
            //新增学生信息
            studentSave = iStudentManageService.addANewStudent(busStudentParentsDtoT,studentId);
            //新增爸爸与学生之间的关系信息
            fatherSave = iStudentManageService.addAnewStudentWithFather(fatherId, studentId);

            //将父亲的信息存入角色表
            fatherUserRoleSave = iStudentManageService.addFatherWithRole(roleId,fatherId);

            //新增妈妈与学生之间的关系
            motherSave =  iStudentManageService.addAnewStudentWithMother(motherId,studentId);
            //将母亲的信息存入角色表
            motherUserRoleSave = iStudentManageService.addMotherWithRole(roleId,motherId);

            //-------------爸爸妈妈图库进行创建----------------------------
            //对于这个家长的ID进行图库的创建
            UserGalleryDto fatherUserGalleryDto = new UserGalleryDto();
            fatherUserGalleryDto.setUserId(Long.valueOf(fatherId));
            iBusGalleryService.insertBusUserGallery(fatherUserGalleryDto);
            //对于这个家长的ID进行图库的创建
            UserGalleryDto motherUserGalleryDto = new UserGalleryDto();
            motherUserGalleryDto.setUserId(Long.valueOf(motherId));
            iBusGalleryService.insertBusUserGallery(motherUserGalleryDto);
            //----------------------------------------------------------

            //成长报告的新增
            growthSave = iBusStudentGrowthService.addStudentGrowth(studentId);

            //荣誉新增
            BusStudentHonor busStudentHonor = new BusStudentHonor();
            busStudentHonor.setStudentId(studentId);
            iBusStudentHonorService.insertBusStudentHonor(busStudentHonor);

            //评语新增
            //获取这个班级班主任
            LambdaQueryWrapper<BusGrade> busGradeLambdaQueryWrapper = new LambdaQueryWrapper<>();
            busGradeLambdaQueryWrapper.eq(BusGrade::getGradeId,gradeId);
            BusGrade busGrade = iBusGradeService.getOne(busGradeLambdaQueryWrapper);

            //班主任Id
            Long userId = busGrade.getUserId();
            BusUserComments busUserComments = new BusUserComments();
            busUserComments.setUserId(userId);
            busUserComments.setStudentId(studentId);
            iBusUserCommentsService.save(busUserComments);
        }

        return studentSave && fatherSave && motherSave && growthSave && fatherUserRoleSave && motherUserRoleSave;

    }

    //找到孩子爸爸的绑定信息
    @Override
    public BusUser findFatherInfo(BusStudentParentsDtoT busStudentParentsDtoT) {
        //找出于孩子绑定的家长id
        LambdaQueryWrapper<BusStudentParents> busStudentFatherWrapper = new LambdaQueryWrapper<>();
        busStudentFatherWrapper.eq(BusStudentParents::getStudentId,busStudentParentsDtoT.getStuId());
        busStudentFatherWrapper.eq(BusStudentParents::getRelation,"爸爸");
        BusStudentParents parentsFather = iBusStudentParentsService.getOne(busStudentFatherWrapper);
        //获取爸爸的id
        Long FatherUserId = parentsFather.getUserId();
        //根据孩子绑定的爸爸id拿到爸爸用户信息
        LambdaQueryWrapper<BusUser> fatherUserWrapper = new LambdaQueryWrapper<>();
        fatherUserWrapper.eq(BusUser::getUserId,FatherUserId);
        return iBusUserService.getOne(fatherUserWrapper);
    }
    //绑定孩子妈妈的信息
    @Override
    public BusUser findMotherInfo(BusStudentParentsDtoT busStudentParentsDtoT) {
        //查找与孩子绑定的妈妈信息
        LambdaQueryWrapper<BusStudentParents> busStudentMotherWrapper = new LambdaQueryWrapper<>();
        busStudentMotherWrapper.eq(BusStudentParents::getStudentId,busStudentParentsDtoT.getStuId());
        busStudentMotherWrapper.eq(BusStudentParents::getRelation,"妈妈");
        BusStudentParents parentsMother = iBusStudentParentsService.getOne(busStudentMotherWrapper);
        //拿到其母亲ID
        Long MotherUserId = parentsMother.getUserId();
        //获取孩子绑定的妈妈的用户信息
        LambdaQueryWrapper<BusUser> MotherUserWrapper = new LambdaQueryWrapper<>();
        MotherUserWrapper.eq(BusUser::getUserId,MotherUserId);
        return iBusUserService.getOne(MotherUserWrapper);
    }

    //修改孩子以及家长的所有信息
    @Override
    public boolean updateAllInfo(BusStudentParentsDtoT busStudentParentsDtoT,BusUser fatherBusUser,BusUser motherBusUser) {

        //修改学生信息
        int updateBusStudents = iBusStudentsService.updateBusStudents(busStudentParentsDtoT);

        fatherBusUser.setNickName(busStudentParentsDtoT.getFatherName());
        fatherBusUser.setPhonenumber(busStudentParentsDtoT.getFatherNumber());
        //执行父亲的修改
        boolean i1 = iBusUserService.updateBusUser(fatherBusUser);

        //执行母亲的修改
        motherBusUser.setPhonenumber(busStudentParentsDtoT.getMotherNumber());
        motherBusUser.setNickName(busStudentParentsDtoT.getMotherName());
        boolean i2 = iBusUserService.updateBusUser(motherBusUser);
        return updateBusStudents > 0 && i1 && i2;
    }

    @Override
    public boolean updateAllInfoXg(Long stuId,BusStudentParentsDtoT busStudentParentsDtoT, BusUser fatherBusUser, BusUser motherBusUser) {
        //修改学生信息
        LambdaQueryWrapper<BusStudents> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.eq(BusStudents::getStuId,stuId);
        boolean update = iBusStudentsService.update(busStudentParentsDtoT, queryWrapper1);

        fatherBusUser.setNickName(busStudentParentsDtoT.getFatherName());
        fatherBusUser.setPhonenumber(busStudentParentsDtoT.getFatherNumber());
        //执行父亲的修改
        boolean i1 = iBusUserService.updateBusUser(fatherBusUser);

        //执行母亲的修改
        motherBusUser.setPhonenumber(busStudentParentsDtoT.getMotherNumber());
        motherBusUser.setNickName(busStudentParentsDtoT.getMotherName());
        boolean i2 = iBusUserService.updateBusUser(motherBusUser);
        return update  && i1 && i2;
    }

    //修改班级人数
    @Override
    public boolean updateGradeStudentsNumber(Long gradeId) {
        //该班级人数+1
        BusGrade busGrade = iBusGradeService.selectBusGradeByGradeId(gradeId);
        busGrade.setGradeNumber(busGrade.getGradeNumber()+1);
        //修改班级人数
        LambdaQueryWrapper<BusGrade> busGradeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        busGradeLambdaQueryWrapper.eq(BusGrade::getGradeId,gradeId);
        return iBusGradeService.update(busGrade,busGradeLambdaQueryWrapper);
    }
    //编辑操作中班级减少一个人
    @Override
    public boolean updateGradeStudentsNumberBj(Long gradeId) {
        //该班级人数-1
        BusGrade busGrade = iBusGradeService.selectBusGradeByGradeId(gradeId);
        busGrade.setGradeNumber(busGrade.getGradeNumber()-1);
        //修改班级人数
        LambdaQueryWrapper<BusGrade> busGradeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        busGradeLambdaQueryWrapper.eq(BusGrade::getGradeId,gradeId);
        return iBusGradeService.update(busGrade,busGradeLambdaQueryWrapper);
    }

    @Override
    public boolean updateAllInfoBj(BusStudentParentsDtoT busStudentParentsDtoT, BusUser fatherBusUser, BusUser motherBusUser) {
        Long stuId = busStudentParentsDtoT.getStuId();
        System.out.println(stuId+"================================================");
        String substring = stuId.toString().substring(0, 6);
        Long gradeId = busStudentParentsDtoT.getGradeId();
        //编辑前的班级人数减一
        boolean updateGradeStudentsNumberB= updateGradeStudentsNumberBj(Long.valueOf(substring));
        if (!updateGradeStudentsNumberB){
            throw new MyException("数据库操作失败!");
        }
        //该班级人数加一
        boolean updateGradeStudentsNumber = updateGradeStudentsNumber(gradeId);
        if (!updateGradeStudentsNumber){
            throw new MyException("数据库操作失败!");
        }
        //设置当前学生学号
        Long aLong = setStudentId(gradeId);
        System.out.println(aLong+"==============================");
        //设置当前学生班级和学号
        BusGradeStudent busGradeStudent = new BusGradeStudent();
        busGradeStudent.setGradeId(gradeId);
        busGradeStudent.setStudentId(aLong);
        //对班级学生关系进行修改
        LambdaQueryWrapper<BusGradeStudent> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BusGradeStudent::getStudentId,stuId);
        boolean updateBusGradeStudent = iBusGradeStudentService.update(busGradeStudent, queryWrapper);
        if (!updateBusGradeStudent){
            throw new MyException("数据库操作失败！");
        }
        //对学生家长所有信息进行修改
        busStudentParentsDtoT.setStuId(aLong);
        boolean UpdateAllInfo = updateAllInfoXg(stuId,busStudentParentsDtoT, fatherBusUser, motherBusUser);
        if (!UpdateAllInfo){
            throw new MyException("数据库操作失败!");
        }




        //对学生与家长之间关系进行修改 仅修改学生id
        boolean updateStudentWithParents = false;
        LambdaQueryWrapper<BusStudentParents> parentsLambdaQueryWrapper = new LambdaQueryWrapper<>();
        parentsLambdaQueryWrapper.eq(BusStudentParents::getStudentId,stuId);
        List<BusStudentParents> list = iBusStudentParentsService.list(parentsLambdaQueryWrapper);
        for (BusStudentParents busStudentParents : list) {
            busStudentParents.setStudentId(aLong);
            if (busStudentParents.getRelation().equals("爸爸")){
                LambdaQueryWrapper<BusStudentParents> queryWrapper1 = new LambdaQueryWrapper<>();
                queryWrapper1.eq(BusStudentParents::getStudentId,stuId);
                queryWrapper1.eq(BusStudentParents::getRelation,"爸爸");
                updateStudentWithParents = iBusStudentParentsService.update(busStudentParents, queryWrapper1);
            }else{
                LambdaQueryWrapper<BusStudentParents> queryWrapper1 = new LambdaQueryWrapper<>();
                queryWrapper1.eq(BusStudentParents::getStudentId,stuId);
                updateStudentWithParents = iBusStudentParentsService.update(busStudentParents, queryWrapper1);
            }
            if (!updateStudentWithParents){
                throw new MyException("数据库操作失败！");
            }
        }

        return updateGradeStudentsNumberB && updateBusGradeStudent && UpdateAllInfo && updateGradeStudentsNumber && updateStudentWithParents;
    }

    //设置当前学生id
    @Override
    public Long setStudentId(Long gradeId) {
        Long studentId;
        int countStudents = iBusGradeStudentService.getCountStudents(gradeId);
        if (countStudents != 0){
            //拿到这个班级最大的学号
            QueryWrapper<BusGradeStudent> busGradeStudentQueryWrapper = new QueryWrapper<>();
            busGradeStudentQueryWrapper.select("max(student_id) as maxStudentId").eq("grade_id",gradeId);
            Map<String, Object> map = iBusGradeStudentService.getMap(busGradeStudentQueryWrapper);
            Object o = map.get("maxStudentId");
            Long maxStudentId = (Long) o;
            //最大的学生ID+1
            studentId = maxStudentId +1;

        }else {
            //假如班级没有学生
            studentId= Long.valueOf(gradeId +"01");
        }
        return studentId;
    }
    //新增一个学生
    @Override
    public boolean addANewStudent(BusStudentParentsDtoT busStudentParentsDtoT,Long studentId) {
        Long gradeId = busStudentParentsDtoT.getGradeId();
        //修改当前班级人数
        boolean b = iStudentManageService.updateGradeStudentsNumber(gradeId);

        busStudentParentsDtoT.setStuId(studentId);
        //存储这个学生的信息
        boolean studentSave = iBusStudentsService.save(busStudentParentsDtoT);

        //将这个学生添加到班级学生关系表
        BusGradeStudent busGradeStudent = new BusGradeStudent();
        busGradeStudent.setStudentId(studentId);
        busGradeStudent.setGradeId(gradeId);
        boolean studentWithGrade = iBusGradeStudentService.save(busGradeStudent);
        return b && studentSave && studentWithGrade;
    }
    //新增爸爸信息
    @Override
    public boolean addANewFather(BusStudentParentsDtoT busStudentParentsDtoT, String fatherId) {
        boolean fatherUserSave;
        BusUser fatherUser = new BusUser();
        fatherUser.setUserId(Long.valueOf(fatherId));
        //名称
        fatherUser.setNickName(busStudentParentsDtoT.getFatherName());
        //手机号
        fatherUser.setPhonenumber(busStudentParentsDtoT.getFatherNumber());
        //账号即手机号
        fatherUser.setUserName(busStudentParentsDtoT.getFatherNumber());
        fatherUser.setSex("0");
        //初始密码
        fatherUser.setPassword(SecurityUtils.encryptPassword("123456"));
        //存入用户表
        fatherUserSave = iBusUserService.save(fatherUser);
        return fatherUserSave;
    }
    //新增妈妈信息
    @Override
    public boolean addANewMother(BusStudentParentsDtoT busStudentParentsDtoT, String motherId) {
        boolean motherUserSave;
        BusUser motherUser = new BusUser();
        motherUser.setUserId(Long.valueOf(motherId));
        //名称
        motherUser.setNickName(busStudentParentsDtoT.getMotherName());
        //手机号
        motherUser.setPhonenumber(busStudentParentsDtoT.getMotherNumber());
        //账号即手机号
        motherUser.setUserName(busStudentParentsDtoT.getMotherNumber());
        //性别
        motherUser.setSex("1");
        //初始密码
        motherUser.setPassword(SecurityUtils.encryptPassword("123456"));
        motherUserSave = iBusUserService.save(motherUser);
        return motherUserSave;
    }
    //新增孩子家长信息
    @Override
    public boolean addAnewStudentWithFather(String fatherId,Long studentId) {
        boolean fatherSave;
        BusStudentParents Father = new BusStudentParents();
        //绑定其userID
        Father.setUserId(Long.valueOf(fatherId));
        //绑定其孩子学生ID
        Father.setStudentId(studentId);
        Father.setRelation("爸爸");
        //写入学生父母关系表
        fatherSave = iBusStudentParentsService.save(Father);
        return fatherSave;
    }

    @Override
    public boolean addFatherWithRole(Long roleId, String fatherId) {
        boolean fatherUserRoleSave;
        SysUserRole fatherUserRole = new SysUserRole();
        fatherUserRole.setRoleId(roleId);
        fatherUserRole.setUserId(Long.valueOf(fatherId));
        LambdaQueryWrapper<SysUserRole> sysUserRolequeryWrapper = new LambdaQueryWrapper<>();
        sysUserRolequeryWrapper.eq(SysUserRole::getUserId,Long.valueOf(fatherId));
        sysUserRolequeryWrapper.eq(SysUserRole::getRoleId,roleId);
        SysUserRole iSysUserRoleServiceOne1 = iSysUserRoleService.getOne(sysUserRolequeryWrapper);
        if (iSysUserRoleServiceOne1 == null){
            fatherUserRoleSave = iSysUserRoleService.save(fatherUserRole);
        }else{
            fatherUserRoleSave = true;
        }
        return fatherUserRoleSave;
    }

    @Override
    public boolean addAnewStudentWithMother(String motherId, Long studentId) {
        boolean motherSave;
        //新增存储母亲信息
        BusStudentParents Mother = new BusStudentParents();
        //绑定其userID
        Mother.setUserId(Long.valueOf(motherId));
        //绑定其孩子学生ID
        Mother.setStudentId(studentId);
        Mother.setRelation("妈妈");
        //写入学生父母关系表
        motherSave = iBusStudentParentsService.save(Mother);
        return motherSave;
    }

    @Override
    public boolean addMotherWithRole(Long roleId, String motherId) {
        boolean motherUserRoleSave;
        SysUserRole motherUserRole = new SysUserRole();
        motherUserRole.setRoleId(roleId);
        motherUserRole.setUserId(Long.valueOf(motherId));
        LambdaQueryWrapper<SysUserRole> sysUserRolequeryWrapper1 = new LambdaQueryWrapper<>();
        sysUserRolequeryWrapper1.eq(SysUserRole::getUserId,Long.valueOf(motherId));
        sysUserRolequeryWrapper1.eq(SysUserRole::getRoleId,roleId);
        SysUserRole iSysUserRoleServiceOne2 = iSysUserRoleService.getOne(sysUserRolequeryWrapper1);
        if (iSysUserRoleServiceOne2 == null){
            motherUserRoleSave = iSysUserRoleService.save(motherUserRole);
        }else{
            motherUserRoleSave = true;
        }
        return motherUserRoleSave;
    }




}
