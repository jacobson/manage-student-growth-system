package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.MusicManage;

import java.util.List;


public interface IMusicManageService extends IService<MusicManage> {
    //上传音乐
    List<MusicManage> selectMusicList();

    //删除音乐
    boolean delete(Long musicId);

    /**
     * 新增音乐
     * @return
     */
    public boolean addMusic(MusicManage musicManage);

    //条件查询
    List<MusicManage> conditionMusicManages(String musicname);
}
