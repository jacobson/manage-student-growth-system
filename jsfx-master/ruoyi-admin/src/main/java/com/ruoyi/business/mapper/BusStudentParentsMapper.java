package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusStudentParents;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 学生家长Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-31
 */
@Mapper
public interface BusStudentParentsMapper extends BaseMapper<BusStudentParents>
{
    /**
     * 查询学生家长
     * 
     * @param studentId 学生家长主键
     * @return 学生家长
     */
    public BusStudentParents selectBusStudentParentsByStudentId(Long studentId);

    /**
     * 查询学生家长
     *
     * @param studentId 学生家长主键
     * @return 学生家长
     */
    public List<BusStudentParents> selectBusStudentParentsByStudentIds(Long studentId);

    /**
     * 查询学生家长列表
     * 
     * @param busStudentParents 学生家长
     * @return 学生家长集合
     */
    public List<BusStudentParents> selectBusStudentParentsList(BusStudentParents busStudentParents);

    /**
     * 新增学生家长
     * 
     * @param busStudentParents 学生家长
     * @return 结果
     */
    public int insertBusStudentParents(BusStudentParents busStudentParents);

    /**
     * 修改学生家长
     * 
     * @param busStudentParents 学生家长
     * @return 结果
     */
    public int updateBusStudentParents(BusStudentParents busStudentParents);

    /**
     * 删除学生家长
     * 
     * @param studentId 学生家长主键
     * @return 结果
     */
    public int deleteBusStudentParentsByStudentId(Long studentId);

    /**
     * 批量删除学生家长
     * 
     * @param studentIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusStudentParentsByStudentIds(Long[] studentIds);
}
