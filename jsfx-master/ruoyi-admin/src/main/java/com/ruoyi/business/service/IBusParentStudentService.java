package com.ruoyi.business.service;

import com.ruoyi.business.domain.BusStudents;

import java.util.List;

public interface IBusParentStudentService {
    List<BusStudents> getChild(Long userId);
    List<Long> getParentUserIds(Long studentId);
}
