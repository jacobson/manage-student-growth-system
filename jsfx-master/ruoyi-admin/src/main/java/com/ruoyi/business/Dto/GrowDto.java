package com.ruoyi.business.Dto;

import com.ruoyi.business.common.CommonValues;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class GrowDto {
    //模块人数名单
    private Map<String, List<String>> mapName;
    //统计人数名单
    private Map<String, List<String>> mapNameCount;

    public Map<String, List<String>> getMapName() {
        mapName=new HashMap<>();
        String[] item= CommonValues.ITEMS;
        for (String x:item) {
            mapName.put(x, new ArrayList<>());
            }
        return mapName;
    }

    public void setMapName(Map<String, List<String>> mapNametest) {
        System.out.println("======setMapName=========");
        System.out.println(mapName);
        mapName = mapNametest;
    }

    public void setMapNameCount(Map<String, List<String>> mapNameCounttest) {
        System.out.println("======setMapNameCount=========");
        mapNameCount = mapNameCounttest;
    }

    public Map<String, List<String>> getMapNameCount() {
        mapNameCount=new HashMap<>();
        String[] item=new String[]{"已完成","进行中","未开始"};
        for (String x:item) {
            List<String> NameList=new ArrayList<>();
            mapNameCount.put(x,NameList);
        }
        return mapNameCount;
    }
}
