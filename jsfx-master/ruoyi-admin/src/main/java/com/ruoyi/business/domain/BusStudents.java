package com.ruoyi.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 学生管理对象 bus_students
 * 
 * @author wmc
 * @date 2022-10-21
 */
@Data
public class BusStudents
{
    private static final long serialVersionUID = 1L;
    /** 学生id */
    private Long stuId;
    /**班级id*/
    @TableField(exist = false)
    @Excel(name = "班级号" )
    private Long gradeId;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String stuName;

    /** 头像地址 */
    private String avatar;

    /** 用户性别（0男 1女 2未知） */
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 身份证 */
    @Excel(name = "身份证")
    private String idCard;

    /** 帐号状态（0正常 1停用） */
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String status;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("stuId", getStuId())
            .append("stuName", getStuName())
            .append("avatar", getAvatar())
            .append("sex", getSex())
            .append("idCard", getIdCard())
            .append("status", getStatus())
//            .append("createBy", getCreateBy())
//            .append("createTime", getCreateTime())
//            .append("updateBy", getUpdateBy())
//            .append("updateTime", getUpdateTime())
//            .append("remark", getRemark())
            .toString();
    }
}
