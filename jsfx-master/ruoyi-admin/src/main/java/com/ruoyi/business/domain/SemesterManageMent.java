package com.ruoyi.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

@Data
@TableName("bus_semester")
public class SemesterManageMent {
    /** 学年学期id */
    private Long semesterId;

    /** 学年 */
    @Excel(name = "学年")
    private Long semesterYear;

    /** 学期名称 */
    @Excel(name = "学期名称")
    private String semesterName;

    /** 是否为当前学期 */
    @Excel(name = "是否为当前学期")
    private String semesterNow;

    /**学年的2022~2023*/
    @TableField(exist = false)
    private String semesterYears;

    /**创建时间**/
    @Excel(name = "创建时间")
    private Date createTime;
}
