package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.YearGrade;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface YearGeadeMapper extends BaseMapper<YearGrade> {

}
