package com.ruoyi.business.controller;

import com.ruoyi.common.utils.file.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/getFile")
public class FileController {
    @Value("${ruoyi.profile}")
    private String path;
    @GetMapping()
    public void getFile(String name, HttpServletResponse response) throws IOException {
        String absPath = name.replaceFirst("/profile", path + "/");
        FileUtils.writeBytes(absPath,response.getOutputStream());
    }
}
