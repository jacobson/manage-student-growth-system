package com.ruoyi.business.controller;

import com.ruoyi.business.Dto.UserGalleryDto;
import com.ruoyi.business.service.IBusGalleryService;
import com.ruoyi.business.service.IBusGradeGalleryService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.security.context.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 图库Controller
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@RestController
@RequestMapping("/business/gallery")
public class BusGalleryController extends BaseController
{
    @Autowired
    private IBusGalleryService busGalleryService;
    @Autowired
    private IBusGradeGalleryService busGradeGalleryService;
    /**
     * 根据图库Id查询其中图片
     */
    @GetMapping("/{galleryId}")
    public AjaxResult getImagesByGalleryId(@PathVariable Long galleryId){
        return AjaxResult.success(busGalleryService.
                getImagesByGalleryId(galleryId));
    }
    /**
     * 新增用户图库
     */
    @PostMapping
    public AjaxResult add(@RequestBody UserGalleryDto busGallery)
    {
        busGallery.setUserId(UserContext.getUserId());
        return toAjax(busGalleryService.insertBusUserGallery(busGallery));
    }

    /**
     * 删除图库
     */

	@DeleteMapping("/{galleryId}")
    public AjaxResult remove(@PathVariable Long[] galleryId)
    {
        return toAjax(busGradeGalleryService.
                deleteBusGradeGalleryByGalleryIds(galleryId));
    }
    /**
     * 向指定图库上传照片
     */
    @PostMapping("/upload/{galleryId}")
    public AjaxResult upload(@PathVariable Long galleryId, MultipartFile file) throws IOException, InvalidExtensionException {
        if (file == null){
            return AjaxResult.error("未接收到图片");
        }
        busGalleryService.upload(galleryId,file);
        return AjaxResult.success("上传成功");
    }
    /**
     * 用户向图库上传图片
     */
    @PostMapping("/upload")
    public AjaxResult upload(MultipartFile file) throws IOException, InvalidExtensionException {
        Long userId = UserContext.getUserId();
        Long userGallery = busGalleryService.getUserGallery(userId);
        busGalleryService.upload(userGallery,file);
        return AjaxResult.success("上传成功");
    }
    /**
     * 删除指定图片
     */
    @DeleteMapping("/deleteImage")
    public AjaxResult delete(String imgName){
        busGalleryService.delete(imgName);
        return AjaxResult.success("删除成功");
    }
    @GetMapping("/getUserGallery")
    public AjaxResult getGalleryId(){
        Long userId = SecurityUtils.getUserId();
        return AjaxResult.success(busGalleryService.getUserGallery(userId));
    }
    /**
     * 根据学生id获取当前班级图库中及用户图库中的图片
     */
    @GetMapping("/getPhotos")
    public AjaxResult getPhotosByStuId(Long studentId){
        return AjaxResult.success(busGalleryService.getPhotosByStuId(studentId));
    }
}
