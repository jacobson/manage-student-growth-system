package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.BusGradeStudent;
import com.ruoyi.business.domain.BusGradeTeacher;
import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.business.domain.BusUserComments;
import com.ruoyi.business.mapper.BusUserCommentsMapper;
import com.ruoyi.business.service.IBusGradeStudentService;
import com.ruoyi.business.service.IBusStudentGrowthItemService;
import com.ruoyi.business.service.IBusStudentsService;
import com.ruoyi.business.service.IBusUserCommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 用户评语（教师、班主任）Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-28
 */
@Service
public class BusUserCommentsServiceImpl extends ServiceImpl<BusUserCommentsMapper,BusUserComments> implements IBusUserCommentsService
{
    @Autowired
    private BusUserCommentsMapper busUserCommentsMapper;
    //学生班级表
    @Autowired
    private IBusGradeStudentService busGradeStudentService;
    //学生评语表
    @Autowired
    private IBusUserCommentsService busUserCommentsService;
    //学生信息表
    @Autowired
    private IBusStudentsService busStudentsService;

    @Autowired
    private IBusStudentGrowthItemService growthItemService;

    @Override
    public BusUserComments chooseStudent(Long StudentId,Long userId) {
        LambdaQueryWrapper<BusUserComments> StudentsWrapper = new LambdaQueryWrapper<>();
        StudentsWrapper.eq(BusUserComments::getStudentId,StudentId);
        StudentsWrapper.eq(BusUserComments::getUserId,userId);
        return busUserCommentsService.getOne(StudentsWrapper);
    }
    /**
     *
     * @param UserId 教师ID
     * @return 填写与为未填写
     */
    @Override
    public List<Map<Long, List<BusStudents>>> getFillOutStatusList(Long UserId) {
        //创建存储map
        //1-存储未填写学生的名单map
        Map<Long,List<BusStudents>> mapNotConfirmName = new HashMap<>();
//        List<BusStudents> listNotConfirmName = new ArrayList<>();
        //2-存储已经填写的学生名单map
        Map<Long,List<BusStudents>> mapConfirmName = new HashMap<>();
//        List<BusStudents> listConfirmName = new ArrayList<>();
        //拿到需要这个老师填写评语 的所有学生
        LambdaQueryWrapper<BusUserComments> commentsWrapper=new LambdaQueryWrapper<>();
        commentsWrapper.eq(BusUserComments::getUserId,UserId);
        List<BusUserComments> busUserComments = busUserCommentsService.list(commentsWrapper);
        for (BusUserComments item:busUserComments) {
            //拿到这个学生的对应班级ID号
            LambdaQueryWrapper<BusGradeStudent> busGradeStudentWrapper = new LambdaQueryWrapper<>();
            busGradeStudentWrapper.eq(BusGradeStudent::getStudentId,item.getStudentId());
            BusGradeStudent gradeStudent = busGradeStudentService.getOne(busGradeStudentWrapper);
            //拿到这个学生对应的班级号
            Long gradeId = gradeStudent.getGradeId();
            //拿到这个学生的信息表
            LambdaQueryWrapper<BusStudents> busStudentsWrapper = new LambdaQueryWrapper<>();
            busStudentsWrapper.eq(BusStudents::getStuId,item.getStudentId());
            //查询这个学生的信息
            BusStudents StudentInfo = busStudentsService.getOne(busStudentsWrapper);
            //判断这个学生是否为未填学生
            if (item.getStatus().equals("0")){
                if(mapNotConfirmName.containsKey(gradeId)){
                    List<BusStudents> listNotConfirmName = mapNotConfirmName.get(gradeId);
                    listNotConfirmName.add(StudentInfo);
                    mapNotConfirmName.put(gradeId,listNotConfirmName);
                }else {
                    List<BusStudents> busStudents = new ArrayList<>();
                    busStudents.add(StudentInfo);
                    mapNotConfirmName.put(gradeId,busStudents);
                }
            } else {
                if(mapConfirmName.containsKey(gradeId)){
                    List<BusStudents> busStudents = mapConfirmName.get(gradeId);
                    busStudents.add(StudentInfo);
                }else {
                    List<BusStudents> busStudents = new ArrayList<>();
                    busStudents.add(StudentInfo);
                    mapConfirmName.put(gradeId,busStudents);
                }
            }
        }
        //封装结果
        List<Map<Long, List<BusStudents>>> resultList = new ArrayList<>();
        resultList.add(mapNotConfirmName);
        resultList.add(mapConfirmName);return resultList;
    }

    /**
     * 查询用户评语（教师、班主任）
     * 
     * @param userId 用户评语（教师、班主任）主键
     * @return 用户评语（教师、班主任）
     */
    @Override
    public BusUserComments selectBusUserCommentsByUserId(Long userId)
    {
        return busUserCommentsMapper.selectBusUserCommentsByUserId(userId);
    }

    /**
     * 查询用户评语（教师、班主任）列表
     * 
     * @param busUserComments 用户评语（教师、班主任）
     * @return 用户评语（教师、班主任）
     */
    @Override
    public List<BusUserComments> selectBusUserCommentsList(BusUserComments busUserComments)
    {
        return busUserCommentsMapper.selectBusUserCommentsList(busUserComments);
    }

    /**
     * 新增用户评语（教师、班主任）
     * 
     * @param busUserComments 用户评语（教师、班主任）
     * @return 结果
     */
    @Override
    public int insertBusUserComments(BusUserComments busUserComments)
    {
        if (busUserCommentsMapper.exitComment(busUserComments.getStudentId())!=null){
            return 0;
        }
        return busUserCommentsMapper.insertBusUserComments(busUserComments);
    }

    /**
     * 修改用户评语（教师、班主任）
     * 
     * @param busUserComments 用户评语（教师、班主任）
     * @return 结果
     */
    @Override
    public int updateBusUserComments(BusUserComments busUserComments)
    {
        int i = busUserCommentsMapper.updateBusUserComments(busUserComments);
        //判断该学生的班主任和教师评语是否都已完成
        List<String> strings = busUserCommentsMapper.checkCommentFinish(busUserComments);
        //未邀请教师
        if (strings.size()<2){
            return i;
        }
        for (String status : strings) {
            if (status.equals("0")){
                return i;
            }
        }

        growthItemService.insertComments(busUserComments);
        return i;

    }

    /**
     * 批量删除用户评语（教师、班主任）
     * 
     * @param userIds 需要删除的用户评语（教师、班主任）主键
     * @return 结果
     */
    @Override
    public int deleteBusUserCommentsByUserIds(Long[] userIds)
    {
        return busUserCommentsMapper.deleteBusUserCommentsByUserIds(userIds);
    }

    /**
     * 删除用户评语（教师、班主任）信息
     * 
     * @param userId 用户评语（教师、班主任）主键
     * @return 结果
     */
    @Override
    public int deleteBusUserCommentsByUserId(Long userId)
    {
        return busUserCommentsMapper.deleteBusUserCommentsByUserId(userId);
    }

    @Override
    public List<BusGradeTeacher> getTeacherOptions(Long studentId) {
        //获取该学生所在班级
        Long gradeId = busUserCommentsMapper.getGradeId(studentId);
        //根据gradeId查询出除班主任外的userId
        List<BusGradeTeacher> teachers = busUserCommentsMapper.getTeacher(gradeId);
        //无法选择的userId
        List<Long> userIds = new ArrayList<>();
        for (BusGradeTeacher teacher:teachers){
            if (teacher.getPostId() == 1){
                userIds.add(teacher.getUserId());
                break;
            }
        }
        List<BusGradeTeacher> busGradeTeachers = new ArrayList<>();
        //去除班主任及同一教师任课多门情况
        for (BusGradeTeacher teacher:teachers){
            if (!userIds.contains(teacher.getUserId())){
                busGradeTeachers.add(teacher);
                userIds.add(teacher.getUserId());
            }
        }
        teachers.forEach((teacher) -> {
            String teacherName = busUserCommentsMapper.
                    getTeacherName(teacher.getUserId());
            teacher.setName(teacherName);
        });
        return busGradeTeachers;
    }

}
