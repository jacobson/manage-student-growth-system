package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusGradeTeacherHistory;

import java.util.List;

/**
 * 班主任与班级历史Mapper接口
 * 
 * @author ruoyi
 * @date 2022-11-12
 */
public interface BusGradeTeacherHistoryMapper extends BaseMapper<BusGradeTeacherHistory>
{
    /**
     * 查询班主任与班级历史
     * 
     * @param gradeId 班主任与班级历史主键
     * @return 班主任与班级历史
     */
    public BusGradeTeacherHistory selectBusGradeTeacherHistoryByGradeId(Long gradeId);

    /**
     * 查询班主任与班级历史列表
     * 
     * @param busGradeTeacherHistory 班主任与班级历史
     * @return 班主任与班级历史集合
     */
    public List<BusGradeTeacherHistory> selectBusGradeTeacherHistoryList(BusGradeTeacherHistory busGradeTeacherHistory);

    /**
     * 新增班主任与班级历史
     * 
     * @param busGradeTeacherHistory 班主任与班级历史
     * @return 结果
     */
    public int insertBusGradeTeacherHistory(BusGradeTeacherHistory busGradeTeacherHistory);

    /**
     * 修改班主任与班级历史
     * 
     * @param busGradeTeacherHistory 班主任与班级历史
     * @return 结果
     */
    public int updateBusGradeTeacherHistory(BusGradeTeacherHistory busGradeTeacherHistory);

    /**
     * 删除班主任与班级历史
     * 
     * @param gradeId 班主任与班级历史主键
     * @return 结果
     */
    public int deleteBusGradeTeacherHistoryByGradeId(Long gradeId);

    /**
     * 批量删除班主任与班级历史
     * 
     * @param gradeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusGradeTeacherHistoryByGradeIds(Long[] gradeIds);

    int recordTeacherTeach(BusGradeTeacherHistory busGradeTeacherHistory);

    Long getLeadTeacher(Long gradeId);
}
