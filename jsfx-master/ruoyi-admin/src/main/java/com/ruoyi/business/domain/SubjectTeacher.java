package com.ruoyi.business.domain;

import lombok.Data;

@Data
public class SubjectTeacher {
    private String nickName;
    private Long userId;
    private String subjectName;
}
