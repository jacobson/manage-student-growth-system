package com.ruoyi.business.service;

import com.ruoyi.business.Dto.UserGalleryDto;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 图库Service接口
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
public interface IBusGalleryService 
{
    /**
     * 根据图库Id获取图片
     */
    List<String> getImagesByGalleryId(Long galleryId);



    /**
     * 新增图库
     * 
     * @param userGalleryDto 图库
     * @return 结果
     */
    public int insertBusUserGallery(UserGalleryDto userGalleryDto);



    /**
     * 向指定图库上传照片
     * @param galleryId 图库id
     * @param file  图片
     */
    void upload(Long galleryId, MultipartFile file) throws IOException, InvalidExtensionException;

    void delete(String imgName);
    Long getUserGallery(Long userId);

    Map<String,List<String>> getPhotosByStuId(Long studentId);
}
