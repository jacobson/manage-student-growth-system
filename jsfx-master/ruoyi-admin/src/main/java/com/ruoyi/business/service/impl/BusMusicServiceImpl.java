package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.BusMusic;
import com.ruoyi.business.mapper.BusMusicMapper;
import com.ruoyi.business.service.IBusMusicService;
import com.ruoyi.common.config.RuoYiConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 推荐音乐Service业务层处理
 *
 */
@Service
public class BusMusicServiceImpl implements IBusMusicService 
{
    @Autowired
    private BusMusicMapper busMusicMapper;
    /**
     * 查询推荐音乐
     * 
     * @param musicId 推荐音乐主键
     * @return 推荐音乐
     */

    /**
     * 查询推荐音乐列表
     * 
     * @return 推荐音乐
     */
    @Override
    public List<BusMusic> selectBusMusicList()
    {

        List<BusMusic> busMusics = busMusicMapper.selectBusMusicList();
        busMusics.forEach((item)->{
            item.setMusicUrl(RuoYiConfig.getMusicPath()+item.getMusicUrl());
        });
        return busMusics;
    }

    /**
     * 新增推荐音乐
     ** @return 结果
     */
    @Override
    public int insertBusMusic(String musicName,String musicUrl)
    {
        return busMusicMapper.insertBusMusic(musicName,musicUrl);
    }

    /**
     * 修改推荐音乐
     * 
     * @param busMusic 推荐音乐
     * @return 结果
     */
    @Override
    public int updateBusMusic(BusMusic busMusic)
    {
        return busMusicMapper.updateBusMusic(busMusic);
    }

    /**
     * 批量删除推荐音乐
     * 
     * @param musicIds 需要删除的推荐音乐主键
     * @return 结果
     */
    @Override
    public int deleteBusMusicByMusicIds(Long[] musicIds)
    {
        return busMusicMapper.deleteBusMusicByMusicIds(musicIds);
    }

    /**
     * 删除推荐音乐信息
     * 
     * @param musicId 推荐音乐主键
     * @return 结果
     */
    @Override
    public int deleteBusMusicByMusicId(Long musicId)
    {
        return busMusicMapper.deleteBusMusicByMusicId(musicId);
    }
}
