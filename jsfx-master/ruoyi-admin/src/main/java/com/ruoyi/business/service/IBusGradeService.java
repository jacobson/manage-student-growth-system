package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.BusGrade;
import com.ruoyi.common.core.domain.AjaxResult;

import java.util.List;

/**
 * 班级Service接口
 * 
 * @author ruoyi
 * @date 2022-10-22
 */
public interface IBusGradeService extends IService<BusGrade>
{


    //指定班级，获取这个班主任的信息
    String getHeadTeacherName(Long gradeId);


    /**
     * 修改班级
     *
     * @param busGrade 班级
     * @return 结果
     */
    public boolean myUpdateBusGrade(BusGrade busGrade);

    /**
     * 批量创建班级
     * @param grade 年级
     * @param count 班级数
     * @return
     */
    AjaxResult addClasses(String grade, int count);
    /**
     * 拿到这个班主任当前学期所带的班级
     */
    public List<BusGrade> getBusGradedList(Long userId);
    /**
     * 查询班级
     * 
     * @param gradeId 班级主键
     * @return 班级
     */
    public BusGrade selectBusGradeByGradeId(Long gradeId);

    /**
     * 查询班级列表
     * 
     * @param busGrade 班级
     * @return 班级集合
     */
    public List<BusGrade> selectBusGradeList(BusGrade busGrade);

    /**
     * 新增班级
     * 
     * @param busGrade 班级
     * @return 结果
     */
    public int insertBusGrade(BusGrade busGrade);

    /**
     * 修改班级
     * 
     * @param busGrade 班级
     * @return 结果
     */
    public int updateBusGrade(BusGrade busGrade);

    /**
     * 批量删除班级
     * 
     * @param gradeIds 需要删除的班级主键集合
     * @return 结果
     */
    public AjaxResult deleteBusGradeByGradeIds(Long[] gradeIds);

    /**
     * 删除班级信息
     * 
     * @param gradeId 班级主键
     * @return 结果
     */
    public int deleteBusGradeByGradeId(Long gradeId);
    //自己的删除班级的接口
    public AjaxResult deleteBusGradeByGradeIdT(Long gradeId);
    /**
     * 批量删除班级
     *
     * @param gradeIds 需要删除的班级主键集合
     * @return 结果
     */

}
