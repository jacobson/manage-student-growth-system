package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusGrowthItemDto;
import com.ruoyi.business.domain.BusStudentGrowth;
import com.ruoyi.business.domain.BusStudentGrowthItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.Date;
import java.util.List;

/**
 * 学生成长报告项Mapper接口
 *
 */
@Mapper
public interface BusStudentGrowthItemMapper extends BaseMapper<BusStudentGrowthItem>
{
    /**
     * 查询学生成长报告项
     * 
     * @param growth 学生成长报告项主键
     * @return 学生成长报告项
     */
    List<BusGrowthItemDto> selectBusStudentGrowthItemByGrowthId(BusStudentGrowth growth);


    /**
     * 新增学生成长报告项
     * 
     * @param busStudentGrowthItem 学生成长报告项
     * @return 结果
     */
    int insertBusStudentGrowthItem(BusStudentGrowthItem busStudentGrowthItem);

    /**
     * 修改学生成长报告项
     * 
     * @param busStudentGrowthItem 学生成长报告项
     * @return 结果
     */
    int updateBusStudentGrowthItem(BusStudentGrowthItem busStudentGrowthItem);

    /**
     * 删除学生成长报告项
     * 
     * @param growthItemId 学生成长报告项主键
     * @return 结果
     */
    int deleteBusStudentGrowthItemByGrowthItemId(Long growthItemId);

    /**
     * 批量删除学生成长报告项
     * 
     * @param growthItemIds 需要删除的数据主键集合
     * @return 结果
     */
    int deleteBusStudentGrowthItemByGrowthItemIds(Long[] growthItemIds);

    String getItemValueByKeyAndGrowthId(BusStudentGrowthItem busStudentGrowthItem);

    List<String> getHistoryItems(@Param("growthIds") List<Long> growthIds, @Param("itemKey") String itemKey);

    Date getEndTime(Long currentSemesterId);

    @Update("update bus_student_growth set status = 1 where growth_id = #{growthId}")
    void updateStatus(Long growthId);

    void clearItem(Long id);
}
