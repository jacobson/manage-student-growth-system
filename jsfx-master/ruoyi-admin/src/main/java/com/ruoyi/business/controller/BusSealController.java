package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusCommonSeal;
import com.ruoyi.business.domain.BusUserSeal;
import com.ruoyi.business.mapper.BusUserSealMapper;
import com.ruoyi.business.service.IBusUserSealService;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import com.ruoyi.framework.security.context.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
/**
 * 用户印章Controller
 * 
 * @author ruoyi
 * @date 2022-10-31
 */
@RestController
@RequestMapping("/business/seal")
public class BusSealController extends BaseController
{
    @Autowired
    private IBusUserSealService busUserSealService;


    @Autowired
    private BusUserSealMapper busUserSealMapper;

    /**
     * 查询用户印章列表
     */

    @GetMapping("/seals")
    public AjaxResult list() {
        Long userId = UserContext.getUserId();
        //根据用户id获得用户印章
        BusUserSeal busUserSeal1 = busUserSealMapper.list(userId);
        //获取用户印章
        List<BusCommonSeal> busCommonSeal = busUserSealMapper.selectAll();
        //用HashMap封装个人印章和通用印章
        HashMap<String, Object> map = new HashMap<>();
        map.put("个人印章",busUserSeal1);
        map.put("通用印章",busCommonSeal);
        return AjaxResult.success(map);
    }

    /**
     * 新增用户印章
     */
    @PostMapping("upload")
    public AjaxResult uploadFile(MultipartFile imgFile) throws IOException, InvalidExtensionException {
        Long userId = UserContext.getUserId();
        String seal = FileUploadUtils.upload(RuoYiConfig.getSealPath(), imgFile, MimeTypeUtils.IMAGE_EXTENSION);
        BusUserSeal busUserSeal = busUserSealMapper.selectBusUserSealBySealId(userId);
        if (busUserSeal == null){
            //获取到URL后，将URL保存到数据库中
            if (busUserSealService.insertBusUserSeal(userId, seal) == 0) {
                return AjaxResult.error("上传失败");
            } else {
                return AjaxResult.success("上传成功");
            }
        }else{
            if (busUserSealMapper.updateBusUserSeal(userId, seal) == 0) {
                return AjaxResult.error("上传失败");
            } else {
                return AjaxResult.success("上传成功");
            }
        }
    }
    @PostMapping("/uploadCommonSeal")
    public AjaxResult uploadCommonSeal(MultipartFile file,BusCommonSeal commonSeal) throws IOException, InvalidExtensionException {
        if (file!=null){
            String seal = FileUploadUtils.upload(RuoYiConfig.getSealPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
            commonSeal.setSealUrl(seal);
        }
        if (commonSeal.getSealId() == null){
            //获取到URL后，将URL保存到数据库中
            if (busUserSealMapper.insertBusCommonSeal(commonSeal) == 0) {
                return AjaxResult.error("上传失败");
            } else {
                return AjaxResult.success("上传成功");
            }
        }else{
            if (busUserSealMapper.updateBusCommonSeal(commonSeal) == 0) {
                return AjaxResult.error("上传失败");
            } else {
                return AjaxResult.success("上传成功");
            }
        }
    }
}
