package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * 学生家长对象 bus_student_parents
 * 
 * @author ruoyi
 * @date 2022-10-31
 */
@Data
public class BusStudentParents
{
    private static final long serialVersionUID = 1L;


    /** 学生id */
    @Excel(name = "学生id")
    private Long studentId;

    /** 家长id */
    @Excel(name = "家长id")
    private Long userId;

    /** 家长身份 */
    @Excel(name = "家长身份")
    private String relation;

}
