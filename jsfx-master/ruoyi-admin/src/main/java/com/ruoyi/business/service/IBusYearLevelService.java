package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.BusYearLevel;

import java.util.List;

/**
 * 年级Service接口
 * 
 * @author ruoyi
 * @date 2022-11-12
 */
public interface IBusYearLevelService extends IService<BusYearLevel>
{
    /**
     * 查询年级
     * 
     * @param yearLevelId 年级主键
     * @return 年级
     */
    public BusYearLevel selectBusYearLevelByYearLevelId(Long yearLevelId);

    /**
     * 查询年级列表
     * 
     * @param busYearLevel 年级
     * @return 年级集合
     */
    public List<BusYearLevel> selectBusYearLevelList(BusYearLevel busYearLevel);

    /**
     * 新增年级
     * 
     * @param busYearLevel 年级
     * @return 结果
     */
    public int insertBusYearLevel(BusYearLevel busYearLevel);

    /**
     * 修改年级
     * 
     * @param busYearLevel 年级
     * @return 结果
     */
    public int updateBusYearLevel(BusYearLevel busYearLevel);

    /**
     * 批量删除年级
     * 
     * @param yearLevelIds 需要删除的年级主键集合
     * @return 结果
     */
    public int deleteBusYearLevelByYearLevelIds(Long[] yearLevelIds);

    /**
     * 删除年级信息
     * 
     * @param yearLevelId 年级主键
     * @return 结果
     */
    public int deleteBusYearLevelByYearLevelId(Long yearLevelId);
}
