package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.BusSubject;

import java.util.List;

/**
 * 学科Service接口
 *
 * @author ruoyi
 * @date 2022-11-02
 */
public interface IBusSubjectService extends IService<BusSubject>
{

    //返回所有学课信息
    List<BusSubject> getAllSubject();

    //返回所有学科的中文名
    List<String> getAllSubjectName();

    //新增科目
    boolean addSubject(BusSubject busSubject);
    /**
     * 查询学科
     *
     * @param subjectId 学科主键
     * @return 学科
     */
     BusSubject selectBusSubjectBySubjectId(Long subjectId);

    /**
     * 查询学科列表
     *
     * @param busSubject 学科
     * @return 学科集合
     */
    public List<BusSubject> selectBusSubjectList(BusSubject busSubject);

    /**
     * 新增学科
     *
     * @param busSubject 学科
     * @return 结果
     */
    public int insertBusSubject(BusSubject busSubject);

    /**
     * 修改学科
     *
     * @param busSubject 学科
     * @return 结果
     */
    public int updateBusSubject(BusSubject busSubject);

    /**
     * 批量删除学科
     *
     * @param subjectIds 需要删除的学科主键集合
     * @return 结果
     */
    public int deleteBusSubjectBySubjectIds(Long[] subjectIds);

    /**
     * 删除学科信息
     *
     * @param subjectId 学科主键
     * @return 结果
     */
    public int deleteBusSubjectBySubjectId(Long subjectId);

    Boolean exitSubject(String subjectName);
}
