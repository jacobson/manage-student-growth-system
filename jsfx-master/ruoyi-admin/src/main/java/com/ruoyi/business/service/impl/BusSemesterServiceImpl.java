package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.BusSemester;
import com.ruoyi.business.mapper.BusSemesterMapper;
import com.ruoyi.business.service.IBusSemesterService;
import com.ruoyi.system.common.RedisKeyPrefix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class BusSemesterServiceImpl extends ServiceImpl<BusSemesterMapper, BusSemester> implements IBusSemesterService {
    @Autowired
    private BusSemesterMapper semesterMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public Long getCurrentSemesterId() {
        Long currentSemesterId;
        currentSemesterId = (Long)redisTemplate.opsForValue().
                get(RedisKeyPrefix.CURRENT_SEMESTER_ID);
        if (currentSemesterId == null){
            currentSemesterId = semesterMapper.getCurrentSemesterIdByMapper();
            redisTemplate.opsForValue().
                    set(RedisKeyPrefix.CURRENT_SEMESTER_ID,currentSemesterId,3, TimeUnit.DAYS);
        }
        return currentSemesterId;
    }

    @Override
    public void updateStatus() {
        semesterMapper.update();
    }

    @Override
    public Boolean exitReport() {
        return semesterMapper.exitReport() != null;
    }

}
