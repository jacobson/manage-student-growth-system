package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.BusGradeTeacher;
import com.ruoyi.business.domain.BusStudents;
import com.ruoyi.business.domain.BusUserComments;

import java.util.List;
import java.util.Map;

/**
 * 用户评语（教师、班主任）Service接口
 * 
 * @author ruoyi
 * @date 2022-10-28
 */
public interface IBusUserCommentsService extends IService<BusUserComments>
{
    //返回该学生的评语信息
    public BusUserComments chooseStudent(Long StudentId,Long userId);
    //所有与UseID相关的学生
    List<Map<Long,List<BusStudents>>> getFillOutStatusList(Long UserId);
    /**
     * 查询用户评语（教师、班主任）
     * 
     * @param userId 用户评语（教师、班主任）主键
     * @return 用户评语（教师、班主任）
     */
    public BusUserComments selectBusUserCommentsByUserId(Long userId);

    /**
     * 查询用户评语（教师、班主任）列表
     * 
     * @param busUserComments 用户评语（教师、班主任）
     * @return 用户评语（教师、班主任）集合
     */
    public List<BusUserComments> selectBusUserCommentsList(BusUserComments busUserComments);

    /**
     * 新增用户评语（教师、班主任）
     * 
     * @param busUserComments 用户评语（教师、班主任）
     * @return 结果
     */
    public int insertBusUserComments(BusUserComments busUserComments);

    /**
     * 修改用户评语（教师、班主任）
     * 
     * @param busUserComments 用户评语（教师、班主任）
     * @return 结果
     */
    public int updateBusUserComments(BusUserComments busUserComments);

    /**
     * 批量删除用户评语（教师、班主任）
     * 
     * @param userIds 需要删除的用户评语（教师、班主任）主键集合
     * @return 结果
     */
    public int deleteBusUserCommentsByUserIds(Long[] userIds);

    /**
     * 删除用户评语（教师、班主任）信息
     * 
     * @param userId 用户评语（教师、班主任）主键
     * @return 结果
     */
    public int deleteBusUserCommentsByUserId(Long userId);

    List<BusGradeTeacher> getTeacherOptions(Long studentId);
}
