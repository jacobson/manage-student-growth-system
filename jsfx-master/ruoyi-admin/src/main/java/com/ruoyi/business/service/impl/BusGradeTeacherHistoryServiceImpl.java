package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.BusGradeTeacherHistory;
import com.ruoyi.business.mapper.BusGradeTeacherHistoryMapper;
import com.ruoyi.business.service.IBusGradeTeacherHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 班主任与班级历史Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-11-12
 */
@Service
public class BusGradeTeacherHistoryServiceImpl extends ServiceImpl<BusGradeTeacherHistoryMapper,BusGradeTeacherHistory> implements IBusGradeTeacherHistoryService
{
    @Autowired
    private BusGradeTeacherHistoryMapper busGradeTeacherHistoryMapper;

    /**
     * 查询班主任与班级历史
     * 
     * @param gradeId 班主任与班级历史主键
     * @return 班主任与班级历史
     */
    @Override
    public BusGradeTeacherHistory selectBusGradeTeacherHistoryByGradeId(Long gradeId)
    {
        return busGradeTeacherHistoryMapper.selectBusGradeTeacherHistoryByGradeId(gradeId);
    }

    /**
     * 查询班主任与班级历史列表
     * 
     * @param busGradeTeacherHistory 班主任与班级历史
     * @return 班主任与班级历史
     */
    @Override
    public List<BusGradeTeacherHistory> selectBusGradeTeacherHistoryList(BusGradeTeacherHistory busGradeTeacherHistory)
    {
        return busGradeTeacherHistoryMapper.selectBusGradeTeacherHistoryList(busGradeTeacherHistory);
    }

    /**
     * 新增班主任与班级历史
     * 
     * @param busGradeTeacherHistory 班主任与班级历史
     * @return 结果
     */
    @Override
    public int insertBusGradeTeacherHistory(BusGradeTeacherHistory busGradeTeacherHistory)
    {
        return busGradeTeacherHistoryMapper.insertBusGradeTeacherHistory(busGradeTeacherHistory);
    }

    /**
     * 修改班主任与班级历史
     * 
     * @param busGradeTeacherHistory 班主任与班级历史
     * @return 结果
     */
    @Override
    public int updateBusGradeTeacherHistory(BusGradeTeacherHistory busGradeTeacherHistory)
    {
        return busGradeTeacherHistoryMapper.updateBusGradeTeacherHistory(busGradeTeacherHistory);
    }

    /**
     * 批量删除班主任与班级历史
     * 
     * @param gradeIds 需要删除的班主任与班级历史主键
     * @return 结果
     */
    @Override
    public int deleteBusGradeTeacherHistoryByGradeIds(Long[] gradeIds)
    {
        return busGradeTeacherHistoryMapper.deleteBusGradeTeacherHistoryByGradeIds(gradeIds);
    }

    /**
     * 删除班主任与班级历史信息
     * 
     * @param gradeId 班主任与班级历史主键
     * @return 结果
     */
    @Override
    public int deleteBusGradeTeacherHistoryByGradeId(Long gradeId)
    {
        return busGradeTeacherHistoryMapper.deleteBusGradeTeacherHistoryByGradeId(gradeId);
    }

}
