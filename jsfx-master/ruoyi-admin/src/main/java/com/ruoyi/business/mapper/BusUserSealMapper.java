package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusCommonSeal;
import com.ruoyi.business.domain.BusUserSeal;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 用户印章Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-31
 */
@Mapper
public interface BusUserSealMapper extends BaseMapper<BusUserSeal>
{
    /**
     * 查询用户印章
     * 
     * @param userId 用户印章主键
     * @return 用户印章
     */
    @Select("select * from bus_user_seal where user_id =#{userId} ;")
    BusUserSeal selectBusUserSealBySealId(Long userId);
    @Select("select * from bus_common_seal where seal_name = #{name}")
    BusCommonSeal getCommonSealBySealName(String name);
    @Insert("insert into bus_common_seal(seal_name, seal_url, seal_illustrate) values (#{sealName},#{sealUrl},#{sealIllustrate})")
    int insertBusCommonSeal(BusCommonSeal commonSeal);

    int updateBusCommonSeal(BusCommonSeal commonSeal);
    /**
     *
     * @param sealUrl
     * @return
     */
    public int insertBusUserSeal(@Param("userId") Long userId, @Param("sealUrl") String sealUrl);



    /**
     * 查询单个信息
     * @param userId
     * @return
     */
    @Select("select * from bus_user_seal where user_id=#{userId}")
    public BusUserSeal list(Long userId);

    @Select("select * from bus_common_seal")
    public List<BusCommonSeal> selectAll();

    @Update("update bus_user_seal set seal_url=#{sealUrl} where user_id=#{userId}")
    public int updateBusUserSeal(@Param("userId") Long userId, @Param("sealUrl") String sealUrl);

    @Delete("delete from bus_user_seal where user_id=#{userId}")
    public BusUserSeal remove(Long userId);
    @Select("select seal_url from bus_user_seal where user_id = #{userId}")
    String getSealByUserId(Long userId);

    List<BusCommonSeal> getCommonSeals();
}