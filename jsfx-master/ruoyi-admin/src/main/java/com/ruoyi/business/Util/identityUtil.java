package com.ruoyi.business.Util;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.business.domain.UserPost;
import com.ruoyi.business.service.IUserPostService;
import com.ruoyi.system.service.ISysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

//用来判断该用户的家长‘教师信息是否冲突
public class identityUtil {
    @Autowired
    private ISysUserRoleService iSysUserRoleService;

    @Autowired
    private IUserPostService iUserPostService;

    public  boolean checkTeacherInfo(Long userId){
        LambdaQueryWrapper<UserPost> userPostLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userPostLambdaQueryWrapper.eq(UserPost::getUserId,userId);
        List<UserPost> list = iUserPostService.list(userPostLambdaQueryWrapper);
        if (list.isEmpty()){
            return false;
        }else return true;
    }
}
