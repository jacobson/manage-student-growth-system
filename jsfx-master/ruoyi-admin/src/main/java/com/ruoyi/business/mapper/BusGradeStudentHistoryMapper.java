package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusGradeStudentHistory;

import java.util.List;

/**
 * 班级学生Mapper接口
 * 
 * @author ruoyi
 * @date 2022-11-12
 */
public interface BusGradeStudentHistoryMapper extends BaseMapper<BusGradeStudentHistory>
{
    /**
     * 查询班级学生
     * 
     * @param gradeId 班级学生主键
     * @return 班级学生
     */
    public BusGradeStudentHistory selectBusGradeStudentHistoryByGradeId(Long gradeId);

    /**
     * 查询班级学生列表
     * 
     * @param busGradeStudentHistory 班级学生
     * @return 班级学生集合
     */
    public List<BusGradeStudentHistory> selectBusGradeStudentHistoryList(BusGradeStudentHistory busGradeStudentHistory);

    /**
     * 新增班级学生
     * 
     * @param busGradeStudentHistory 班级学生
     * @return 结果
     */
    public int insertBusGradeStudentHistory(BusGradeStudentHistory busGradeStudentHistory);

    /**
     * 修改班级学生
     * 
     * @param busGradeStudentHistory 班级学生
     * @return 结果
     */
    public int updateBusGradeStudentHistory(BusGradeStudentHistory busGradeStudentHistory);

    /**
     * 删除班级学生
     * 
     * @param gradeId 班级学生主键
     * @return 结果
     */
    public int deleteBusGradeStudentHistoryByGradeId(Long gradeId);

    /**
     * 批量删除班级学生
     * 
     * @param gradeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusGradeStudentHistoryByGradeIds(Long[] gradeIds);
}
