package com.ruoyi.business.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
@TableName("bus_grade")
public class YearGrade {

    /** 班级id */
    private Long gradeId;

    /** 学期id */
    @Excel(name = "学期id")
    private Long semesterId;

    /** 年级id */
    @Excel(name = "年级id")
    private Long yearLevelId;


    /** 校区名称 */
    @Excel(name = "校区名称")
    private String campusName;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String gradeName;

    /** 班级人数 */
    @Excel(name = "班级人数")
    private Long gradeNumber;

    /** 班级状态 */
    @Excel(name = "班级状态")
    private String gradeStatus;

    /** 班级备注 */
    @Excel(name = "班级备注")
    private String gradeRemark;
    /**班主任
     *
     */
    @Excel(name = "班主任id")
    private Long userId;

}
