package com.ruoyi.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Map;

@Data
@TableName("sys_user")
public class ParentsManageMent {
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    private Long deptId;

//    /** 微信openid */
//    @Excel(name = "微信openid")
//    private String openId;

    /** 用户账号 */
    @Excel(name = "用户账号")
    private String userName;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    private String nickName;

//    /** 用户类型（00系统用户） */
//    @Excel(name = "用户类型", readConverterExp = "0=0系统用户")
//    private String userType;

//    /** 用户邮箱 */
//    @Excel(name = "用户邮箱")
//    private String email;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String phonenumber;

//    /** 用户性别（0男 1女 2未知） */
//    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
//    private String sex;

    /** 头像地址 */
    @Excel(name = "头像地址")
    private String avatar;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

//    /** 帐号状态（0正常 1停用） */
//    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
//    private String status;

//    /** 删除标志（0代表存在 2代表删除） */
//    private String delFlag;
//
//    /** 最后登录IP */
//    @Excel(name = "最后登录IP")
//    private String loginIp;
//
//    /** 最后登录时间 */
//    @JsonFormat(pattern = "yyyy-MM-dd")
//    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd")
//    private Date loginDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String rawPassword;

//    家长身份
    @TableField(exist = false)
    private String identity;


    //封装孩子名字---身份证
    //"张大大:33031"，
    // "张小小":330324
    @TableField(exist = false)
    private Map<String, String> parentAndChild;

}


