package com.ruoyi.business.controller;

import com.ruoyi.business.domain.BusStudentScore;
import com.ruoyi.business.service.IBusStudentScoreService;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/studentScore")
public class BusStudentScoreController {
    @Autowired
    private IBusStudentScoreService scoreService;

    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        scoreService.getTemplate(SecurityUtils.getUserId(),response);
    }

    /**
     * excel形式导入学生成绩
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception
    {
        ExcelUtil<BusStudentScore> util = new ExcelUtil<>(BusStudentScore.class);
        List<BusStudentScore> list = util.importExcel(file.getInputStream());
        if (list.isEmpty()){
            return AjaxResult.error("导入为空");
        }
        scoreService.importScore(list);
        return AjaxResult.success("导入成功");
    }
}
