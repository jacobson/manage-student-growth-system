package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusGradeTeacher;
import com.ruoyi.business.domain.SubjectTeacher;
import com.ruoyi.business.domain.TeacherManage;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface TeacherManageMapper extends BaseMapper<TeacherManage> {


    @Select("select post_id from sys_post where post_name = #{name}")
    Long getPostIdByName(String name);

    @Select("select  subject_id from bus_subject where subject_name =#{name}")
    Long getSubjectIdByName(String name);

    @Insert("insert into bus_grade_teacher (grade_id, post_id, user_id, subject_id) VALUES (#{gradeId},#{postId},#{userId},#{subjectId})")
    int setGradeTeacher(BusGradeTeacher teacher);

    List<SubjectTeacher> getSubjectTeacher(String subjectName);

    int reSetGradeTeacher(BusGradeTeacher teacher);
}
