package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.Dto.BusStudentParentsDtoT;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudentManageMapper extends BaseMapper<BusStudentParentsDtoT> {
}
