package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.BusCommonSeal;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2022-11-01
 */
@Mapper
public interface BusCommonSealMapper {

    /**
     * 查询【请填写功能名称】
     * 
     * @param sealName 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public BusCommonSeal selectBusCommonSealBySealName(String sealName);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param busCommonSeal 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<BusCommonSeal> selectBusCommonSealList(BusCommonSeal busCommonSeal);

    /**
     * 新增
     * @param sealName
     * @param sealUrl
     * @param sealIllustrate
     * @return
     */
    public int insertBusCommonSeal(@Param("sealName") String sealName, @Param("sealUrl") String sealUrl, @Param("sealIllustrate") String sealIllustrate);

    /**
     * 修改【请填写功能名称】
     * 
     * @param busCommonSeal 【请填写功能名称】
     * @return 结果
     */
    public int updateBusCommonSeal(BusCommonSeal busCommonSeal);

    /**
     * 删除【请填写功能名称】
     * 
     * @param sealName 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteBusCommonSealBySealName(String sealName);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param sealNames 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusCommonSealBySealNames(String[] sealNames);

}
