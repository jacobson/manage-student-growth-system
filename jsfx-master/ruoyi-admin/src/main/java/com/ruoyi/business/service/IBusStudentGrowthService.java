package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.BusStudentGrowth;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 学生成长报告Service接口
 * 
 * @author ruoyi
 * @date 2022-10-22
 */
public interface IBusStudentGrowthService extends IService<BusStudentGrowth>
{
    /**
     * 查询学生成长报告
     * 
     * @param growthId 学生成长报告主键
     * @return 学生成长报告
     */
    public BusStudentGrowth selectBusStudentGrowthByGrowthId(Long growthId);

    /**
     * 查询学生成长报告列表
     * 
     * @param busStudentGrowth 学生成长报告
     * @return 学生成长报告集合
     */
    public List<BusStudentGrowth> selectBusStudentGrowthList(BusStudentGrowth busStudentGrowth);

    /**
     * 新增学生成长报告
     * 
     * @param busStudentGrowth 学生成长报告
     * @return 结果
     */
    public int insertBusStudentGrowth(BusStudentGrowth busStudentGrowth);

    /**
     * 修改学生成长报告
     * 
     * @param busStudentGrowth 学生成长报告
     * @return 结果
     */
    public int updateBusStudentGrowth(BusStudentGrowth busStudentGrowth);

    /**
     * 批量删除学生成长报告
     * 
     * @param growthIds 需要删除的学生成长报告主键集合
     * @return 结果
     */
    public int deleteBusStudentGrowthByGrowthIds(Long[] growthIds);

    /**
     * 删除学生成长报告信息
     * 
     * @param growthId 学生成长报告主键
     * @return 结果
     */
    public int deleteBusStudentGrowthByGrowthId(Long growthId);

    List<BusStudentGrowth> getGrowthsByStuId(Long stuId);
    /**
     * 学生成长报告，未完成的人数统计
     * 数字形式反回
     */

    List<Map<String,Integer>> getStudentGrowPace(Long gradeId, Long semesterId);

    void newGrowth(Date endTime);

    //传入单个学生的ID，为其增加当前学期的成长报告
    public boolean addStudentGrowth(Long StudentId);

    int resetEndTime(Date date);

    Boolean growthNotIsCurrentSemester(Long growthId);

    String getBeforeGrowth(Long growthId);

    List<String> getGrowthResByStudentId(Long studentId,String itemKey);
}
