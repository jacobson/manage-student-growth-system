package com.ruoyi.business.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.*;
import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface ITeacherManageService extends IService<TeacherManage> {
    //全部教师
    List<TeacherManage> allTeacherManages();


    //条件查询教师
    List<TeacherManage> conditionTeacherManages(String name,Integer sex,Integer ys);


    //条件查询教师一个参数的查询
    List<TeacherManage> conditionTeacherManages1(String name);


    //新增教师
    boolean addTeacherManageInfo(PointOnwTeacherManageInfo pointOnwTeacherManageInfo);

    //新增教师测试
    AjaxResult addTeacherManageInfoTest(PointOnwTeacherManageInfo pointOnwTeacherManageInfo);

    //修改教师
    AjaxResult updateTeacherManageInfo(PointOnwTeacherManageInfo pointOnwTeacherManageInfo);

    //返回角色为教师或者班主任的用户id
    List<Long> getTeacherManageInfo();


    //编辑教师，回显教师数据
    PointOnwTeacherManageInfo echoTeacherManageInfo(Long UserId);

    //删除教师信息
    boolean deleteTeacherManageInfo(Long UserId) throws DataAccessException;

    //导出
    List<TeacherManageExport> exportGradeTeacherInfo();

    //导入Excel时，对于集合与map的逻辑处理
    List<TeacherManage>  washTeacherMangerExcel(List<TeacherManage> teacherManages);

    //导入Excel时需要用到的添加方法，该方法只适用于教师导入的用户添加
    boolean addTeacherManageExcelInfo(TeacherManageExport teacherManageExport);



    //纯教师导入，存user表
    boolean addTeacherInfo(TeacherInfoExport teacherInfoExport);

    int changeGradeTeacher(GradeTeacherBody gradeTeacher);

    List<SubjectTeacher> getSubjectTeacher(String subjectName);
}
