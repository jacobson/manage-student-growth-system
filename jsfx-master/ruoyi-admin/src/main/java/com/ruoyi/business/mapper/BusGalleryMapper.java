package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.Dto.UserGalleryDto;
import com.ruoyi.business.domain.BusGallery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 图库Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-26
 */
@Mapper
public interface BusGalleryMapper extends BaseMapper<BusGallery>
{

    /**
     * 根据图库Id获取图片
     */
    List<String> getImagesByGalleryId(Long galleryId);



    /**
     * 新增 用户图库
     */
    public int insertUserGallery(UserGalleryDto userGalleryDto);






    /**
     * 清空图库中的照片记录
     * @param galleryIds 图库id
     * @return 是否成功
     */
    int clearImages(Long galleryIds);

    /**
     * 记录上传照片的名字
     */
    void recordImages(@Param("galleryId") Long galleryId, @Param("name") String name);

    void deleteByName(String name);
    @Select("select gallery_id from bus_user_gallery where user_id = #{userId}")
    Long getUserGalleryByUserId(Long userId);

}
