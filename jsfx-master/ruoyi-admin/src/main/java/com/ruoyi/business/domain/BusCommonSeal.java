package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * 【通用印章表】对象 bus_common_seal
 * 
 * @author ruoyi
 * @date 2022-11-01
 */
@Data
public class BusCommonSeal
{
    private static final long serialVersionUID = 1L;
    private Long sealId;
    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sealName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sealUrl;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sealIllustrate;

}
