package com.ruoyi.framework.security.context;

public class UserContext {
    private static ThreadLocal<Long> threadLocal= new ThreadLocal<>();

    public static Long getUserId() {
        return threadLocal.get();
    }

    public static void setUserId(Long id) {
        threadLocal.set(id);
    }
}
