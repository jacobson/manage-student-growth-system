import request from '@/utils/request'

// 上传音乐
export function uploadmusic(){
    return request({
        url:'/musicManage/add',
        method:'get',
    })
}
//删除音乐
export function deleteMusic(musicId){
    return request({
        url:'musicManage/delete',
        method:'delete',
        params:{ musicId }
    })
}
//条件查询
export function queryMusic(musicname){
    return request({
        url:'/musicManage/conditionMusicManages',
        method:'get',
        params: {musicname}
    })
    }