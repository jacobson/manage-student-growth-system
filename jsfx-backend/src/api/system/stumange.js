// 学生管理
import request from '@/utils/request'

// 查询学生管理列表
export function listStumange(query) {
  return request({
    url: '/studentManage',
    method: 'get',
    params: query
  })
}

// 查询学生信息详细
export function getStumange(stuId) {
  return request({
    url: '/studentManage/echoPointStudentInfo?studentId=' + stuId,
    method: 'get'
  })
}

// 新增学生信息
export function addStumange({ gradeId, stuName, sex, fatherName, motherName, fatherNumber, motherNumber,  idCard}) {
  return request({
    url: '/studentManage',
    method: 'PUT',
    data: {
      gradeId, stuName, sex, fatherName, motherName, fatherNumber, motherNumber, idCard
    }
  })
}

// 修改学生信息
export function updateStumange({ gradeId, stuName, sex, fatherName, motherName, fatherNumber, motherNumber, idCard, stuId, status }) {
  return request({
    url: '/studentManage',
    method: 'POST',
    data: {
      gradeId, stuName, sex, fatherName, motherName, fatherNumber, motherNumber, idCard, stuId,status
    }
  })
}

// 删除学生信息
// export function delStumange(stuId) {
//   return request({
//     url: '/system/stumange/' + stuId,
//     method: 'delete'
//   })
// }

//导出学生信息
export function getdownloadFile(gradeId) {
  return request({
    url: '/studentManage/export/' + gradeId,
    method: 'post',
    responseType: 'blob',
    params: {
      gradeId
    }
  })
}

//获取每个年级班级的信息
export function getClassGrade(){
  return request({
    url: 'yearGrades/list1',
    method: 'get',
  })
}
