import request from '@/utils/request'

// 查询学年学期列表
export function listSemester(query) {
  return request({
    url: '/system/semester/list',
    method: 'get',
    params: query
  })
}

// 查询学年学期详细
export function getSemester(semesterId) {
  return request({
    url: '/system/semester/' + semesterId,
    method: 'get'
  })
}

// 新增学年学期
export function addSemester(data) {
  return request({
    url: '/system/semester',
    method: 'post',
    data: data
  })
}

// 修改学年学期
export function updateSemester(data) {
  return request({
    url: '/system/semester',
    method: 'put',
    data: data
  })
}

// 删除学年学期
export function delSemester(semesterId) {
  return request({
    url: '/system/semester/' + semesterId,
    method: 'delete'
  })
}
