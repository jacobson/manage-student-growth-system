// 学年学期管理
import request from '@/utils/request'

// 查询学年学期列表
export function listSemester(query) {
  return request({
    url: '/business/semesters/semesterslist',
    method: 'get',
    params: query
  })
}

// 查询学年学期详细
export function getSemester(semesterId) {
  return request({
    url: '/business/semesters/echosemester?semesterId='+ semesterId,
  })
}

// 新增学年学期
export function addSemester(data) {
  return request({
    url: '/business/semesters/addsemesters',
    method: 'post',
    data: data
  })
}

// 修改学年学期 
export function updateSemester(data) {
  return request({
    url: '/business/semesters/updatesemester',
    method: 'post',
    data: data
  })
}

// 删除学年学期
export function delSemester(semesterId) {
  return request({
    url: '/business/semesters/' + semesterId,
    method: 'delete'
  })
}

