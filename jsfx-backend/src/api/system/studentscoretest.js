import request from '@/utils/request'

//学生成绩管理列表返回
export const listStudentscoretest = ({ pageNum, pageSize, gradeId, semesterYearName }) => {
  return request({
    url: '/scoreManage/page',
    method: 'get',
    params: {
      pageNum, pageSize, gradeId, semesterYearName
    }
  })
}

//获取每个年级班级的信息
export function getClassGradeAPI(){
  return request({
    url: '/yearGrades/list',
    method: 'get',
  })
}

//修改某学生的成绩项值
export const achevementUpdataAPI = ({ data }) => {
  return request({
    url: '/scoreManage',
    method: 'post',
    data: data
  })
}







// 查询【请填写功能名称】详细
export function getStudentscoretest(studentId) {
  return request({
    url: '/system/studentscoretest/' + studentId,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addStudentscoretest(data) {
  return request({
    url: '/system/studentscoretest',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateStudentscoretest(data) {
  return request({
    url: '/system/studentscoretest',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delStudentscoretest(studentId) {
  return request({
    url: '/system/studentscoretest/' + studentId,
    method: 'delete'
  })
}
