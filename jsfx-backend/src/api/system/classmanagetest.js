import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listClassmanagetest(query) {
  return request({
    url: '/classManage/allGradeInfo',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getClassmanagetest(gradeId) {
  return request({
    url: '/system/classmanagetest/' + gradeId,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addClassmanagetest(data) {
  return request({
    url: '/system/classmanagetest',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateClassmanagetest(data) {
  return request({
    url: '/system/classmanagetest',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delClassmanagetest(gradeId) {
  return request({
    url: '/system/classmanagetest/' + gradeId,
    method: 'delete'
  })
}

//新增班级的请求
export const classAddAPI = ({ grade, count }) => {
  return request({
    url: '/classManage/addClasses',
    method: 'get',
    params: {
      grade, count
    }
  })
}

//返回所有班级的基本信息的请求
export const getClassAPI = ({ yearId }) => {
  return request({
    url: '/classManage/allGradeInfo',
    method: 'get',
    params: {
      yearId
    }
  })
}
//删除班级信息的请求
export const classRemoveAPI = ({ gradeId }) => {
  return request({
    url: 'classManage/remove/?gradeId=' + gradeId,
    method: 'DELETE',
    data: {
    }
  })
}
//获取每个年级
export const getClassgradeAPI = ({ }) => {
  return request({
    url: '/classManage/nowYearId',
    method: 'get',
    params: {

    }
  })
}
//删除班级信息的请求
export const classRemoveListAPI = (ids) => {
  return request({
    url: 'classManage/removeList/' + ids,
    method: 'DELETE',
  })
}
