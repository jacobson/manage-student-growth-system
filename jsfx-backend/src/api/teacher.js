// 教师管理
import request from '@/utils/request'

// 获取老师列表信息
export function getteacherlist(data) {
    return request({
        url: '/business/teacherManage/page',
        method: 'get',
        params: data
    })
}

//编辑回显教师
export function getexitteacher(userId) {
    return request({
        url: '/business/teacherManage/echoTeacherManageInfo?userId=' + userId,
        method: 'get',
    })
}

//所有课程列表返回
export function getclasslist() {
    return request({
        url: '/business/subject/all',
        method: 'get',
    })
}

//新增教师
export function getaddteacher({ nickName, sex, password, phonenumber, teachingInfo }) {
    return request({
        url: '/business/teacherManage/addTeacherManageInfo',
        method: 'post',
        data: {
            nickName,
            sex,
            password,
            phonenumber,
            teachingInfo
        }
    })
}

//修改教师
export function getchangeteacher({ userId, nickName, sex, password, phonenumber, teachingInfo }) {
    return request({
        url: '/business/teacherManage/updateTeacherManageInfo',
        method: 'post',
        data: {
            userId,
            nickName,
            sex,
            password,
            phonenumber,
            teachingInfo
        }
    })
}

//删除教师
export function getdelteacher(UserIds) {
    return request({
        url: '/business/teacherManage/removeTeacherManageInfo?UserId=' + UserIds,
        method: 'delete',
    })
}

//条件查询
export function getTeacherManages(name) {
    return request({
        url: '/business/teacherManage/conditionTeacherManages?name=' + name,
        method: 'get',
    })
}

//年级班级列表
export function getGrade() {
    return request({
        url: '/yearGrades/list1',
        method: 'get',
    })
}
