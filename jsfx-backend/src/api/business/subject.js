/*低代码生成的学科管理API*/

// 引入axios(对axios进行二次封装)
import request from '@/utils/request'

// 对外暴露查询学科列表的函数
export function listSubject() {
  return request({
    url: '/SubjectManage/all',
    method: 'get',
  })
}

// 查询学科详细
export function getSubject(subjectId) {
  return request({
    url: '/business/subject/' + subjectId,
    method: 'get'
  })
}

// 新增学科
export function addSubject(data) {
  return request({
    url: '/business/subject',
    method: 'post',
    data: data
  })
}

// 修改学科
export function updateSubject(data) {
  return request({
    url: '/business/subject',
    method: 'put',
    data: data
  })
}

// 删除学科
export function delSubject(subjectId) {
  return request({
    url: '/business/subject/' + subjectId,
    method: 'delete'
  })
}
