/*低代码生成的音乐管理API*/
import request from '@/utils/request'

// 查询推荐音乐列表
export function listMusic(query) {
  return request({
    url: '/business/music/list',
    method: 'get',
    params: query
  })
}

// 查询推荐音乐详细
export function getMusic(musicId) {
  return request({
    url: '/business/music/' + musicId,
    method: 'get'
  })
}

// 新增推荐音乐
export function addMusic(data) {
  return request({
    url: '/business/music',
    method: 'post',
    data: data
  })
}

// 修改推荐音乐
export function updateMusic(data) {
  return request({
    url: '/business/music',
    method: 'put',
    data: data
  })
}

// 删除推荐音乐
export function delMusic(musicId) {
  return request({
    url: '/business/music/' + musicId,
    method: 'delete'
  })
}
