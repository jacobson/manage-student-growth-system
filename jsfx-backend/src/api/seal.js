import request from '@/utils/request'

// 查询用户印章列表
export function listSeal(query) {
  return request({
    url: '/business/seal/seals',
    method: 'get',
    params: query
  })
}

// 查询用户印章详细
// export function getSeal(sealId) {
//   return request({
//     url: '/system/seal/' + sealId,
//     method: 'get'
//   })
// }

// 新增用户印章
export function addSeal({ sealName, sealIllustrate }) {
  return request({
    url: '/business/seal/uploadCommonSeal?'+  'sealIllustrate=' + sealIllustrate + '&' + 'sealName=' + sealName,
    method: 'POST',
  })
}

// 修改用户印章
export function updateSeal({ sealName, sealIllustrate, sealId }) {
  return request({
    url: '/business/seal/uploadCommonSeal?' + 'sealId=' + sealId + '&' + 'sealIllustrate=' + sealIllustrate + '&' + 'sealName=' + sealName,
    method: 'POST',
  })
}

export function uploadCommonSeal({sealName,sealIllustrate,sealId}){
  return request({
    url:'/business/seal/uploadCommonSeal?sealId='+sealId+ '&' +'sealName='+sealName + '&' +'sealIllustrate='+sealIllustrate,
    method:'POST',
  })
}
// 删除用户印章
// export function delSeal(sealId) {
//   return request({
//     url: '/system/seal/' + sealId,
//     method: 'delete'
//   })
// }
