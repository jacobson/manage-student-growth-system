import request from '@/utils/request'

//学科管理 start
//获取学科列表的请求
export const subjectAPI = ({ }) => {
  return request({
    url: '/SubjectManage/all',
    method: 'get',
    data: {

    }
  })
}

//修新增学科后发送的post请求
export const subjectAddAPI = ({ subjectName, subjectDescription }) => {
  return request({
    url: '/SubjectManage/add',
    method: 'post',
    data: {
      subjectName,
      subjectDescription
    }
  })
}
//删除学科后发送的post请求
export const subjectRemoveAPI = ({ subjectId }) => {
  return request({
    url: `/SubjectManage/remove/?subjectId=`+subjectId,
    method: 'DELETE',
    data: {
      // subjectId
    }
  })
}
//修改学科后发送的post请求
export const subjectUpdataAPI = ({ subjectId,subjectName, subjectDescription }) => {
  return request({
    url: '/SubjectManage',
    method: 'POST',
    data: {
      subjectId,subjectName, subjectDescription
    }
  })
}

//学科管理 end


//班级管理 start

//获取每个年级
export const getClassgradeAPI = ({ }) => {
  return request({
    url: '/classManage/nowYearId',
    method: 'get',
    params: {

    }
  })
}


// 获取班级信息的回显，点击出现详细信息
export const getClassMessageAPI = ({ gradeId }) => {
  return request({
    url: '/classManage',
    method: 'get',
    params: {
      gradeId
    }
  })
}

//返回所有班级的基本信息的请求
export const getClassAPI = ({ yearId }) => {
  return request({
    url: '/classManage/allGradeInfo',
    method: 'get',
    params: {
      yearId
    }
  })
}

//删除班级信息的请求
export const classRemoveAPI = ({ gradeId }) => {
  return request({
    url: 'classManage/remove/?gradeId=' + gradeId,
    method: 'DELETE',
    data: {
    }
  })
}

//新增班级的请求
export const classAddAPI = ({ grade, count }) => {
  return request({
    url: '/classManage/addClasses',
    method: 'get',
    params: {
      grade, count
    }
  })
}

//成绩 start

//获取每个年级班级的信息
export const getClassGradeAPI = ({ }) => {
  return request({
    url: '/yearGrades/list',
    method: 'get',
    params: {

    }
  })
}

//学生成绩管理列表返回
export const achevementiAPI = ({ pageNum, pageSize, gradeId, semesterYearName }) => {
  return request({
    url: '/scoreManage/page',
    method: 'get',
    params: {
      pageNum, pageSize, gradeId, semesterYearName
    }
  })
}

//修改某学生的成绩项值
export const achevementUpdataAPI = ({ data }) => {
  return request({
    url: '/scoreManage',
    method: 'post',
    data: data
  })
}

//获取某个学生的成绩信息
export const getPersonScore = ({ gradeId, name }) => {
  return request({
    url: '/scoreManage/choose',
    method: 'get',
    params: {
      gradeId, name
    }
  })
}

//学生部分
// 获取用户详细信息
export function getstudentManage({ }) {
  return request({
    url: '/studentManage/allYearLevelWithGradeID',
    method: 'get'
  })
}

//下载导入模板
// export function getstudent({}) {
//   return request({
//     url: '/studentManage/import',
//     method: 'get'
//   })
// }


//开启成长报告
export function getnowgrowth({ endTime }) {
  return request({
    url: '/business/growth/newGrowth',
    method: 'POST',
    data: {
      endTime
    }

  })
}

//新增学生
export function getnewstudent({ gradeId, stuName, sex, fatherName, motherName, fatherNumber, motherNumber }) {
  return request({
    url: '/studentManage',
    method: 'PUT',
    data: {
      gradeId, stuName, sex, fatherName, motherName, fatherNumber, motherNumber
    }

  })
}

//获取学生列表
export function getstudentlist({ gradeId }) {
  return request({
    url: '/studentManage',
    method: 'get',
    params: {
      gradeId
    }

  })
}

//导出学生信息
export function getdownloadFile(gradeId) {
  return request({
    url: '/studentManage/export/' + gradeId,
    method: 'get',
    responseType: 'blob',
    params: {
      gradeId
    }
  })
}

//编辑数据回显
export function getedit({ studentId }) {
  return request({
    url: '/studentManage/echoPointStudentInfo?studentId=' + studentId,
    method: 'get',
    params: {
      // studentId
    }
  })
}
//编辑学生
export function getstudentManageedit({ gradeId, stuName, sex, fatherName, motherName, fatherNumber, motherNumber, idCard, stuId }) {
  return request({
    url: '/studentManage',
    method: 'POST',
    data: {
      gradeId, stuName, sex, fatherName, motherName, fatherNumber, motherNumber, idCard, stuId
    }

  })
}

///查询学年学期列表
export function getsemesterslist({ pageNum, pageSize }) {
  return request({
    url: '/business/semesters/semesterslist',
    method: 'get',
    params: {
      pageSize,
      pageNum

    }
  })
}


//新增学期学年
export function getaddsemesters({ semesterName, semesterYears }) {
  return request({
    url: '/business/semesters/addsemesters',
    method: 'post',
    data: {

      semesterName, semesterYears

    }
  })
}
//修改学期学年
export function getupdatesemester({ semesterId, semesterName, semesterYear, semesterNow }) {
  return request({
    url: '/business/semesters/updatesemester',
    method: 'post',
    data: {

      semesterName, semesterYear, semesterId, semesterNow

    }
  })
}


//获取印章列表
export function getseals({ }) {
  return request({
    url: '/business/seal/seals',
    method: 'get'

  })
}

//修改印章
// export function getsealsupload({}) {
//   return request({
//     url: '/business/seal/upload',
//     method: 'POST'

//   })
// }
//新增印章
export function getsealsupload({ sealName, sealIllustrate, sealId }) {
  return request({
    url: '/business/seal/uploadCommonSeal?' + 'sealId=' + sealId + '&' + 'sealIllustrate=' + sealIllustrate + '&' + 'sealName=' + sealName,
    method: 'POST',

  })
}
//修改用户状态

export function getchangeStatus({motherUserId,status}) {
  return request({
    url: '/system/user/changeStatus',
    method: 'POST',
    data: {
      motherUserId, status
    }

  })
}