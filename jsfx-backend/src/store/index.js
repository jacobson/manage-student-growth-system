import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import dict from './modules/dict'
import user from './modules/user'
import tagsView from './modules/tagsView'
import permission from './modules/permission'
import settings from './modules/settings'
import getters from './getters'
//引入teacher模块
import teacher from './modules/teacher'

//使用vuex插件
Vue.use(Vuex)

const store = new Vuex.Store({
  // state:{
  //   //用来新增删除教师岗位的数据
	// 	clickcount:0
	// },
  //暴露出去
  modules: {
    app,
    dict,
    user,
    tagsView,
    permission,
    settings,
    teacher
  },
  getters
})

export default store
